<main class="main-content" role="main">      
            <div id="shopify-section-template--16520034844894__main" class="shopify-section">


   
    <div class="product-template product-template-default product-template-2-blocks">
  <div class="container">
    
    <div class="next_prev-groups">
      


  <div class="breadcrumb"></div>




      
<div class="next-prev-product">
  <div class="next-prev-icons">
  	
    
    
    <a class="next-btn icon-pro" href="/collections/kurtas-tunics-anarkali/products/207-36" data-next-prev-icon="" data-target="#next-product-modal">
      
	<svg aria-hidden="true" data-prefix="fal" data-icon="angle-right" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 192 512" class="svg-inline--fa fa-angle-right fa-w-6 fa-3x"><path fill="currentColor" d="M166.9 264.5l-117.8 116c-4.7 4.7-12.3 4.7-17 0l-7.1-7.1c-4.7-4.7-4.7-12.3 0-17L127.3 256 25.1 155.6c-4.7-4.7-4.7-12.3 0-17l7.1-7.1c4.7-4.7 12.3-4.7 17 0l117.8 116c4.6 4.7 4.6 12.3-.1 17z" class=""></path></svg>

    </a>
    
  </div>
  
  <div class="next-prev-modal" data-next-prev-modal="">
  	
    
    
    <div id="next-product-modal" class="next-prev-content" data-next-prev-content="">
      <div class="content">
        <a class="product-image" href="/collections/kurtas-tunics-anarkali/products/207-36">
          <img src="//cdn.shopify.com/s/files/1/0613/2039/7022/products/276087-2019-04-30-15_51_28.332279-big-image_compact.jpg?v=1663261265" alt="The Mischeivious Yellow Kota Kurta">
        </a>
        <div class="column-right">
          <a class="product-name" href="/collections/kurtas-tunics-anarkali/products/207-36">
            
<span>The Mischeivious Yellow Kota Kurta</span>

          </a>
          
          <div class="price-box">
            
            <div class="price-sale">
              <span class="old-price">
                <span class="money" data-currency-inr="₹3,478.00" data-currency="INR">₹3,478.00</span>
              </span>
              <span class="special-price">
                
                <span class="money" data-currency-inr="₹2,435.00" data-currency="INR">₹2,435.00</span>
              </span>
            </div>

            
          </div>
        </div>
      </div>
    </div>
    
  </div>
</div>

<script>
  $(document).ready(function() {
    var nextPrevProduct = $('.next-prev-product'),
        iconNextPrev = nextPrevProduct.find('[data-next-prev-icon]'),
        contentNextPrev = nextPrevProduct.find('[data-next-prev-content]'),
        modal = nextPrevProduct.find('[data-next-prev-modal]');
    
    if(!('ontouchstart' in document)) {
      iconNextPrev.hover(function() {
        var curLink = $(this),
            curContent = $(curLink.data('target'));    
        
        if(!$(this).hasClass('active')) {
          iconNextPrev.removeClass('active');
          contentNextPrev.removeClass('active');
          
          curLink.addClass('active');
          curContent.addClass('active');
          modal.show();
        }
                
        nextPrevProduct.mouseleave(function() {
          if(modal.is(':visible')) {
            iconNextPrev.removeClass('active');
            contentNextPrev.removeClass('active');
            modal.hide();
          };
        });   
      });
    }
    
    else {
      iconNextPrev.off('click').on('click', function(e) {
      	e.preventDefault();
        e.stopPropagation();
        
        var curLink = $(this),
            curContent = $(curLink.data('target'));
        
        if(!$(this).hasClass('active')) {
          iconNextPrev.removeClass('active');
          contentNextPrev.removeClass('active');
          
          curLink.addClass('active');
          curContent.addClass('active');
          modal.show();
        }
        else {
          curLink.removeClass('active');
          curContent.removeClass('active');
          modal.hide();
        }
      });
    };
    
    $(document).on('click', function(e) {
      if(!$(e.target).closest('[data-next-prev-modal]').length && modal.is(':visible')) {
        iconNextPrev.removeClass('active');
        contentNextPrev.removeClass('active');
        modal.hide();
      };
    });
  });
</script>
  
    </div>
    
       
    <div class="viralb-product-content">
      <div class="row pro-page">
        

<style>  
  .product-template-supermarket .tabs__product-page .list-tabs {
    padding-left: 20px;
  }
  .tabs__product-page .list-tabs:after,
  .tabs__product-page .list-tabs:before {
    display: none;
  }
  
  .viralb-product-content .pro-page {
    padding-top: 10px;
  }
  
  
  @media (min-width: 1200px) { 
    
    .list-collections .col-main,
    .viralb-product-content .pro-page .col-main {
      width: 100%;
      -webkit-box-flex: 0;
      -ms-flex: 0 0 100%;
      flex: 0 0 100%;
      max-width: 100%;
    }
    
    .viralb-product-content .product .product-photos,
    .viralb-product-content .product .product-shop {
      -webkit-box-flex: 0;
      -ms-flex: 0 0 50%;
      flex: 0 0 50%;
      max-width: 50%;
    }
    
    
  }  
</style>


        <div id="shopify-section-product-template-default" class="shopify-section col-12 col-xl-9 col-main">
  <div data-section-id="template--16520034844894__main" data-section-type="product" id="ProductSection-template--16520034844894__main" data-enable-history-state="true" data-collections-related="/collections/kurtas-tunics-anarkali?view=related">
      <div class="product product-default">

          <div class="row product_top horizontal-tabs">
              <div class="col-md-6 product-photos" data-more-view-product="">
                  <div class="product-img-box left-vertical-moreview vertical-moreview">
                      <div style="position: relative;" class="wrapper-images">
                          <div class="product-photo-container slider-for slick-initialized slick-slider slick-dotted" role="toolbar">
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              
                          <div aria-live="polite" class="slick-list"><div class="slick-track" role="listbox" style="opacity: 1; width: 2070px;"><div class="thumb filter-mulmul-cotton-lilac-v-neck-kurta slick-slide slick-current slick-active" style="width: 345px; position: relative; left: 0px; top: 0px; z-index: 999; opacity: 1;" tabindex="-1" role="option" aria-describedby="slick-slide00" data-slick-index="0" aria-hidden="false">
                                  <a data-zoom="" class="fancybox" rel="gallery1" href="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-02_1.jpg?v=1663266908" data-fancybox="images" tabindex="0">
                                      <img id="product-featured-image-30634492461278" src="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-02_1.jpg?v=1663266908" alt="Mulmul Cotton Lilac V Neck Kurta" class="lazyautosizes ls-is-cached lazyloaded" data-src="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-02_1.jpg?v=1663266908" data-widths="[180, 360, 540, 720, 900, 1080, 1296, 1512, 1728, 2048]" data-aspectratio="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-02_1.jpg?v=1663266908" data-sizes="auto" sizes="345px">
                                  </a>
                              </div><div class="thumb filter-mulmul-cotton-lilac-v-neck-kurta slick-slide" style="width: 345px; position: relative; left: -345px; top: 0px; z-index: 998; opacity: 0;" tabindex="-1" role="option" aria-describedby="slick-slide01" data-slick-index="1" aria-hidden="true">
                                  <a data-zoom="" class="fancybox" rel="gallery1" href="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-02_2.jpg?v=1663266908" data-fancybox="images" tabindex="-1">
                                      <img id="product-featured-image-30634492494046" src="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-02_2.jpg?v=1663266908" alt="Mulmul Cotton Lilac V Neck Kurta" class="lazyautosizes ls-is-cached lazyloaded" data-src="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-02_2.jpg?v=1663266908" data-widths="[180, 360, 540, 720, 900, 1080, 1296, 1512, 1728, 2048]" data-aspectratio="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-02_2.jpg?v=1663266908" data-sizes="auto" sizes="345px">
                                  </a>
                              </div><div class="thumb filter-mulmul-cotton-lilac-v-neck-kurta slick-slide" style="width: 345px; position: relative; left: -690px; top: 0px; z-index: 998; opacity: 0;" tabindex="-1" role="option" aria-describedby="slick-slide02" data-slick-index="2" aria-hidden="true">
                                  <a data-zoom="" class="fancybox" rel="gallery1" href="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-02_3.jpg?v=1663266908" data-fancybox="images" tabindex="-1">
                                      <img id="product-featured-image-30634492526814" src="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-02_3.jpg?v=1663266908" alt="Mulmul Cotton Lilac V Neck Kurta" class="lazyautosizes ls-is-cached lazyloaded" data-src="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-02_3.jpg?v=1663266908" data-widths="[180, 360, 540, 720, 900, 1080, 1296, 1512, 1728, 2048]" data-aspectratio="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-02_3.jpg?v=1663266908" data-sizes="auto" sizes="345px">
                                  </a>
                              </div><div class="thumb filter-mulmul-cotton-lilac-v-neck-kurta slick-slide" style="width: 345px; position: relative; left: -1035px; top: 0px; z-index: 998; opacity: 0;" tabindex="-1" role="option" aria-describedby="slick-slide03" data-slick-index="3" aria-hidden="true">
                                  <a data-zoom="" class="fancybox" rel="gallery1" href="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-02_4.jpg?v=1663266908" data-fancybox="images" tabindex="-1">
                                      <img id="product-featured-image-30634492559582" src="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-02_4.jpg?v=1663266908" alt="Mulmul Cotton Lilac V Neck Kurta" class="lazyautosizes ls-is-cached lazyloaded" data-src="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-02_4.jpg?v=1663266908" data-widths="[180, 360, 540, 720, 900, 1080, 1296, 1512, 1728, 2048]" data-aspectratio="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-02_4.jpg?v=1663266908" data-sizes="auto" sizes="345px">
                                  </a>
                              </div><div class="thumb filter-mulmul-cotton-lilac-v-neck-kurta slick-slide" style="width: 345px; position: relative; left: -1380px; top: 0px; z-index: 998; opacity: 0;" tabindex="-1" role="option" aria-describedby="slick-slide04" data-slick-index="4" aria-hidden="true">
                                  <a data-zoom="" class="fancybox" rel="gallery1" href="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-02_5.jpg?v=1663266908" data-fancybox="images" tabindex="-1">
                                      <img id="product-featured-image-30634492592350" src="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-02_5.jpg?v=1663266908" alt="Mulmul Cotton Lilac V Neck Kurta" class="lazyautosizes ls-is-cached lazyloaded" data-src="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-02_5.jpg?v=1663266908" data-widths="[180, 360, 540, 720, 900, 1080, 1296, 1512, 1728, 2048]" data-aspectratio="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-02_5.jpg?v=1663266908" data-sizes="auto" sizes="345px">
                                  </a>
                              </div><div class="thumb filter-mulmul-cotton-lilac-v-neck-kurta slick-slide" style="width: 345px; position: relative; left: -1725px; top: 0px; z-index: 998; opacity: 0;" tabindex="-1" role="option" aria-describedby="slick-slide05" data-slick-index="5" aria-hidden="true">
                                  <a data-zoom="" class="fancybox" rel="gallery1" href="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-02_6.jpg?v=1663266908" data-fancybox="images" tabindex="-1">
                                      <img id="product-featured-image-30634492625118" src="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-02_6.jpg?v=1663266908" alt="Mulmul Cotton Lilac V Neck Kurta" class="lazyautosizes ls-is-cached lazyloaded" data-src="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-02_6.jpg?v=1663266908" data-widths="[180, 360, 540, 720, 900, 1080, 1296, 1512, 1728, 2048]" data-aspectratio="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-02_6.jpg?v=1663266908" data-sizes="auto" sizes="345px">
                                  </a>
                              </div></div></div><ul class="slick-dots" style="display: block;" role="tablist"><li class="slick-active" aria-hidden="false" role="presentation" aria-selected="true" aria-controls="navigation00" id="slick-slide00"><button type="button" data-role="none" role="button" tabindex="0">1</button></li><li aria-hidden="true" role="presentation" aria-selected="false" aria-controls="navigation01" id="slick-slide01"><button type="button" data-role="none" role="button" tabindex="0">2</button></li><li aria-hidden="true" role="presentation" aria-selected="false" aria-controls="navigation02" id="slick-slide02"><button type="button" data-role="none" role="button" tabindex="0">3</button></li><li aria-hidden="true" role="presentation" aria-selected="false" aria-controls="navigation03" id="slick-slide03"><button type="button" data-role="none" role="button" tabindex="0">4</button></li><li aria-hidden="true" role="presentation" aria-selected="false" aria-controls="navigation04" id="slick-slide04"><button type="button" data-role="none" role="button" tabindex="0">5</button></li><li aria-hidden="true" role="presentation" aria-selected="false" aria-controls="navigation05" id="slick-slide05"><button type="button" data-role="none" role="button" tabindex="0">6</button></li></ul></div>

                          

                          
  
  <div class="product-label" data-label-new-number="30">
    

    
      
        <strong class="label sale-label">
          Sale
        </strong>
        
      
    

    
    
    

    
    
    
  </div>
  





                      </div>

                      <div class="viewsimilarvish"><a class="smooth-scroll" href="#shopify-section-product-recommendations">View Similar</a></div>
                            
<a class="wishlist " data-icon-wishlist="" aria-label="Wish Lists" href="#" data-product-handle="mulmul-cotton-lilac-v-neck-kurta" data-id="7797834973406">
	    	<svg viewBox="0 0 512 512">
<g>
<g>
  <path d="M474.644,74.27C449.391,45.616,414.358,29.836,376,29.836c-53.948,0-88.103,32.22-107.255,59.25
    c-4.969,7.014-9.196,14.047-12.745,20.665c-3.549-6.618-7.775-13.651-12.745-20.665c-19.152-27.03-53.307-59.25-107.255-59.25
    c-38.358,0-73.391,15.781-98.645,44.435C13.267,101.605,0,138.213,0,177.351c0,42.603,16.633,82.228,52.345,124.7
    c31.917,37.96,77.834,77.088,131.005,122.397c19.813,16.884,40.302,34.344,62.115,53.429l0.655,0.574
    c2.828,2.476,6.354,3.713,9.88,3.713s7.052-1.238,9.88-3.713l0.655-0.574c21.813-19.085,42.302-36.544,62.118-53.431
    c53.168-45.306,99.085-84.434,131.002-122.395C495.367,259.578,512,219.954,512,177.351
    C512,138.213,498.733,101.605,474.644,74.27z M309.193,401.614c-17.08,14.554-34.658,29.533-53.193,45.646
    c-18.534-16.111-36.113-31.091-53.196-45.648C98.745,312.939,30,254.358,30,177.351c0-31.83,10.605-61.394,29.862-83.245
    C79.34,72.007,106.379,59.836,136,59.836c41.129,0,67.716,25.338,82.776,46.594c13.509,19.064,20.558,38.282,22.962,45.659
    c2.011,6.175,7.768,10.354,14.262,10.354c6.494,0,12.251-4.179,14.262-10.354c2.404-7.377,9.453-26.595,22.962-45.66
    c15.06-21.255,41.647-46.593,82.776-46.593c29.621,0,56.66,12.171,76.137,34.27C471.395,115.957,482,145.521,482,177.351
    C482,254.358,413.255,312.939,309.193,401.614z"></path>
</g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
</svg>
		
</a>
 



                            
                              <a class="icon-share" href="javascript:void(0)" aria-label="Share"><svg viewBox="0 0 227.216 227.216">
<path d="M175.897,141.476c-13.249,0-25.11,6.044-32.98,15.518l-51.194-29.066c1.592-4.48,2.467-9.297,2.467-14.317
	c0-5.019-0.875-9.836-2.467-14.316l51.19-29.073c7.869,9.477,19.732,15.523,32.982,15.523c23.634,0,42.862-19.235,42.862-42.879
	C218.759,19.229,199.531,0,175.897,0C152.26,0,133.03,19.229,133.03,42.865c0,5.02,0.874,9.838,2.467,14.319L84.304,86.258
	c-7.869-9.472-19.729-15.514-32.975-15.514c-23.64,0-42.873,19.229-42.873,42.866c0,23.636,19.233,42.865,42.873,42.865
	c13.246,0,25.105-6.042,32.974-15.513l51.194,29.067c-1.593,4.481-2.468,9.3-2.468,14.321c0,23.636,19.23,42.865,42.867,42.865
	c23.634,0,42.862-19.23,42.862-42.865C218.759,160.71,199.531,141.476,175.897,141.476z M175.897,15
	c15.363,0,27.862,12.5,27.862,27.865c0,15.373-12.499,27.879-27.862,27.879c-15.366,0-27.867-12.506-27.867-27.879
	C148.03,27.5,160.531,15,175.897,15z M51.33,141.476c-15.369,0-27.873-12.501-27.873-27.865c0-15.366,12.504-27.866,27.873-27.866
	c15.363,0,27.861,12.5,27.861,27.866C79.191,128.975,66.692,141.476,51.33,141.476z M175.897,212.216
	c-15.366,0-27.867-12.501-27.867-27.865c0-15.37,12.501-27.875,27.867-27.875c15.363,0,27.862,12.505,27.862,27.875
	C203.759,199.715,191.26,212.216,175.897,212.216z"></path>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
</svg></a>
                            

                      <div class="slider-nav slick-initialized slick-slider" data-rows="6" data-vertical="true" style=""><button type="button" aria-label="Previous" class="slick-prev slick-arrow" style="display: block;"><i class="fa fa-angle-left"></i></button>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                      <div aria-live="polite" class="slick-list draggable"><div class="slick-track" style="opacity: 1; width: 0px; transform: translate3d(0px, 0px, 0px);" role="listbox"><div class="item filter-mulmul-cotton-lilac-v-neck-kurta slick-slide slick-cloned" style="width: 0px;" tabindex="-1" role="option" aria-describedby="slick-slide12" data-slick-index="-4" aria-hidden="true">
                          <div class="product-single__media" data-media-id="30634492526814">
                            <a href="javascript:void(0)" data-image="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-02_3_1024x1024.jpg?v=1663266908" data-zoom-image="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-02_3_1024x1024.jpg?v=1663266908" tabindex="-1">
                              <img src="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-02_3_compact.jpg?v=1663266908" alt="Mulmul Cotton Lilac V Neck Kurta">
                            </a>
                          </div>
                        </div><div class="item filter-mulmul-cotton-lilac-v-neck-kurta slick-slide slick-cloned" style="width: 0px;" tabindex="-1" role="option" aria-describedby="slick-slide13" data-slick-index="-3" aria-hidden="true">
                          <div class="product-single__media" data-media-id="30634492559582">
                            <a href="javascript:void(0)" data-image="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-02_4_1024x1024.jpg?v=1663266908" data-zoom-image="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-02_4_1024x1024.jpg?v=1663266908" tabindex="-1">
                              <img src="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-02_4_compact.jpg?v=1663266908" alt="Mulmul Cotton Lilac V Neck Kurta">
                            </a>
                          </div>
                        </div><div class="item filter-mulmul-cotton-lilac-v-neck-kurta slick-slide slick-cloned" style="width: 0px;" tabindex="-1" role="option" aria-describedby="slick-slide14" data-slick-index="-2" aria-hidden="true">
                          <div class="product-single__media" data-media-id="30634492592350">
                            <a href="javascript:void(0)" data-image="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-02_5_1024x1024.jpg?v=1663266908" data-zoom-image="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-02_5_1024x1024.jpg?v=1663266908" tabindex="-1">
                              <img src="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-02_5_compact.jpg?v=1663266908" alt="Mulmul Cotton Lilac V Neck Kurta">
                            </a>
                          </div>
                        </div><div class="item filter-mulmul-cotton-lilac-v-neck-kurta slick-slide slick-cloned" style="width: 0px;" tabindex="-1" role="option" aria-describedby="slick-slide15" data-slick-index="-1" aria-hidden="true">
                          <div class="product-single__media" data-media-id="30634492625118">
                            <a href="javascript:void(0)" data-image="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-02_6_1024x1024.jpg?v=1663266908" data-zoom-image="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-02_6_1024x1024.jpg?v=1663266908" tabindex="-1">
                              <img src="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-02_6_compact.jpg?v=1663266908" alt="Mulmul Cotton Lilac V Neck Kurta">
                            </a>
                          </div>
                        </div><div class="item filter-mulmul-cotton-lilac-v-neck-kurta slick-slide slick-current slick-active" style="width: 0px;" tabindex="-1" role="option" aria-describedby="slick-slide10" data-slick-index="0" aria-hidden="false">
                          <div class="product-single__media" data-media-id="30634492461278">
                            <a href="javascript:void(0)" data-image="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-02_1_1024x1024.jpg?v=1663266908" data-zoom-image="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-02_1_1024x1024.jpg?v=1663266908" tabindex="0">
                              <img src="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-02_1_compact.jpg?v=1663266908" alt="Mulmul Cotton Lilac V Neck Kurta">
                            </a>
                          </div>
                        </div><div class="item filter-mulmul-cotton-lilac-v-neck-kurta slick-slide slick-active" style="width: 0px;" tabindex="-1" role="option" aria-describedby="slick-slide11" data-slick-index="1" aria-hidden="false">
                          <div class="product-single__media" data-media-id="30634492494046">
                            <a href="javascript:void(0)" data-image="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-02_2_1024x1024.jpg?v=1663266908" data-zoom-image="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-02_2_1024x1024.jpg?v=1663266908" tabindex="0">
                              <img src="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-02_2_compact.jpg?v=1663266908" alt="Mulmul Cotton Lilac V Neck Kurta">
                            </a>
                          </div>
                        </div><div class="item filter-mulmul-cotton-lilac-v-neck-kurta slick-slide slick-active" style="width: 0px;" tabindex="-1" role="option" aria-describedby="slick-slide12" data-slick-index="2" aria-hidden="false">
                          <div class="product-single__media" data-media-id="30634492526814">
                            <a href="javascript:void(0)" data-image="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-02_3_1024x1024.jpg?v=1663266908" data-zoom-image="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-02_3_1024x1024.jpg?v=1663266908" tabindex="0">
                              <img src="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-02_3_compact.jpg?v=1663266908" alt="Mulmul Cotton Lilac V Neck Kurta">
                            </a>
                          </div>
                        </div><div class="item filter-mulmul-cotton-lilac-v-neck-kurta slick-slide slick-active" style="width: 0px;" tabindex="-1" role="option" aria-describedby="slick-slide13" data-slick-index="3" aria-hidden="false">
                          <div class="product-single__media" data-media-id="30634492559582">
                            <a href="javascript:void(0)" data-image="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-02_4_1024x1024.jpg?v=1663266908" data-zoom-image="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-02_4_1024x1024.jpg?v=1663266908" tabindex="0">
                              <img src="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-02_4_compact.jpg?v=1663266908" alt="Mulmul Cotton Lilac V Neck Kurta">
                            </a>
                          </div>
                        </div><div class="item filter-mulmul-cotton-lilac-v-neck-kurta slick-slide" style="width: 0px;" tabindex="-1" role="option" aria-describedby="slick-slide14" data-slick-index="4" aria-hidden="true">
                          <div class="product-single__media" data-media-id="30634492592350">
                            <a href="javascript:void(0)" data-image="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-02_5_1024x1024.jpg?v=1663266908" data-zoom-image="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-02_5_1024x1024.jpg?v=1663266908" tabindex="-1">
                              <img src="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-02_5_compact.jpg?v=1663266908" alt="Mulmul Cotton Lilac V Neck Kurta">
                            </a>
                          </div>
                        </div><div class="item filter-mulmul-cotton-lilac-v-neck-kurta slick-slide" style="width: 0px;" tabindex="-1" role="option" aria-describedby="slick-slide15" data-slick-index="5" aria-hidden="true">
                          <div class="product-single__media" data-media-id="30634492625118">
                            <a href="javascript:void(0)" data-image="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-02_6_1024x1024.jpg?v=1663266908" data-zoom-image="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-02_6_1024x1024.jpg?v=1663266908" tabindex="-1">
                              <img src="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-02_6_compact.jpg?v=1663266908" alt="Mulmul Cotton Lilac V Neck Kurta">
                            </a>
                          </div>
                        </div><div class="item filter-mulmul-cotton-lilac-v-neck-kurta slick-slide slick-cloned" style="width: 0px;" tabindex="-1" role="option" aria-describedby="slick-slide10" data-slick-index="6" aria-hidden="true">
                          <div class="product-single__media" data-media-id="30634492461278">
                            <a href="javascript:void(0)" data-image="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-02_1_1024x1024.jpg?v=1663266908" data-zoom-image="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-02_1_1024x1024.jpg?v=1663266908" tabindex="-1">
                              <img src="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-02_1_compact.jpg?v=1663266908" alt="Mulmul Cotton Lilac V Neck Kurta">
                            </a>
                          </div>
                        </div><div class="item filter-mulmul-cotton-lilac-v-neck-kurta slick-slide slick-cloned" style="width: 0px;" tabindex="-1" role="option" aria-describedby="slick-slide11" data-slick-index="7" aria-hidden="true">
                          <div class="product-single__media" data-media-id="30634492494046">
                            <a href="javascript:void(0)" data-image="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-02_2_1024x1024.jpg?v=1663266908" data-zoom-image="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-02_2_1024x1024.jpg?v=1663266908" tabindex="-1">
                              <img src="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-02_2_compact.jpg?v=1663266908" alt="Mulmul Cotton Lilac V Neck Kurta">
                            </a>
                          </div>
                        </div><div class="item filter-mulmul-cotton-lilac-v-neck-kurta slick-slide slick-cloned" style="width: 0px;" tabindex="-1" role="option" aria-describedby="slick-slide12" data-slick-index="8" aria-hidden="true">
                          <div class="product-single__media" data-media-id="30634492526814">
                            <a href="javascript:void(0)" data-image="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-02_3_1024x1024.jpg?v=1663266908" data-zoom-image="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-02_3_1024x1024.jpg?v=1663266908" tabindex="-1">
                              <img src="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-02_3_compact.jpg?v=1663266908" alt="Mulmul Cotton Lilac V Neck Kurta">
                            </a>
                          </div>
                        </div><div class="item filter-mulmul-cotton-lilac-v-neck-kurta slick-slide slick-cloned" style="width: 0px;" tabindex="-1" role="option" aria-describedby="slick-slide13" data-slick-index="9" aria-hidden="true">
                          <div class="product-single__media" data-media-id="30634492559582">
                            <a href="javascript:void(0)" data-image="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-02_4_1024x1024.jpg?v=1663266908" data-zoom-image="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-02_4_1024x1024.jpg?v=1663266908" tabindex="-1">
                              <img src="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-02_4_compact.jpg?v=1663266908" alt="Mulmul Cotton Lilac V Neck Kurta">
                            </a>
                          </div>
                        </div></div></div><button type="button" aria-label="Next" class="slick-next slick-arrow" style="display: block;"><i class="fa fa-angle-right"></i></button></div>

                      
                  </div>
                  
              </div>

              <div class="col-md-6 product-shop">
                  <h1 class="product-title" data-main-product-handle="mulmul-cotton-lilac-v-neck-kurta">
                      
<span>Mulmul Cotton Lilac V Neck Kurta</span>

                  </h1>

                  
                  <div class="group_item">

                  <div class="prices">
                     
                      <span class="price on-sale" itemprop="price"><span class="money" data-currency-inr="₹1,738.00" data-currency="INR">₹1,738.00</span></span>
                      <span class="compare-price"><span class="money" data-currency-inr="₹2,483.00" data-currency="INR">₹2,483.00</span></span> 
                      <span class="percentage-price">
                      (30%)
                      </span>
                    
                      
                      <input type="hidden" id="product_regular_price" name="product_regular_price" value="173800">
                    
                  </div>
                    
                      
                      <span class="spr-badge" id="spr_badge_7797834973406" data-rating="0.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star-empty" aria-hidden="true"></i><i class="spr-icon spr-icon-star-empty" aria-hidden="true"></i><i class="spr-icon spr-icon-star-empty" aria-hidden="true"></i><i class="spr-icon spr-icon-star-empty" aria-hidden="true"></i><i class="spr-icon spr-icon-star-empty" aria-hidden="true"></i></span><span class="spr-badge-caption">No reviews</span>
</span>

                      

                      
                  </div>
                  

                  
                  

                  

                  


                  <form method="post" action="/cart/add" id="add-to-cart-form" accept-charset="UTF-8" class="shopify-product-form" enctype="multipart/form-data" novalidate="novalidate"><input type="hidden" name="form_type" value="product"><input type="hidden" name="utf8" value="✓">
                  
                    
                      <a data-toggle="modal" data-target="#size_chart" class="size-chart-open-popup" id="sizechart-sideslider">
                        
<span>Size Chart</span>

                      </a>
                    
                  
                 
                  <div id="product-variants">
                      
                      <div class="selector-wrapper"><select class="single-option-selector" data-option="option1" id="product-selectors-option-0"><option value="XS">XS</option><option value="S">S</option><option value="M">M</option><option value="L">L</option><option value="XL">XL</option><option value="XXL">XXL</option><option value="XXXL">XXXL</option></select></div><select id="product-selectors" name="id" style="display: none;">
                          
                          
                          <option data-imge="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-02_1_compact.jpg?v=1663266908" selected="selected" value="43399271874782">
                              XS
                          </option>
                          
                          
                          
                          <option data-imge="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-02_1_compact.jpg?v=1663266908" value="43399271907550">
                              S
                          </option>
                          
                          
                          
                          <option data-imge="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-02_1_compact.jpg?v=1663266908" value="43399271940318">
                              M
                          </option>
                          
                          
                          
                          <option data-imge="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-02_1_compact.jpg?v=1663266908" value="43399271973086">
                              L
                          </option>
                          
                          
                          
                          <option data-imge="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-02_1_compact.jpg?v=1663266908" value="43399272005854">
                              XL
                          </option>
                          
                          
                          
                          <option data-imge="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-02_1_compact.jpg?v=1663266908" value="43399272038622">
                              XXL
                          </option>
                          
                          
                          
                          <option data-imge="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-02_1_compact.jpg?v=1663266908" value="43399272071390">
                              XXXL
                          </option>
                          
                          
                      </select>

                      
                      
                      












    
    

    <style>
        #add-to-cart-form .selector-wrapper {
            display:none
        }
    </style>

    
    
    

    
    
    





<div class="swatch" data-option-index="0">
    <div class="header sizenameviralb has-size-chart a-">
        
<span>Size</span>
:
        <span data-option-select="">XS</span>
    </div>     
            <div class="product__label-bestseller"><span class="span-viralb-taging">Best Seller</span><a href="/collections/bestsellers" style="margin: 10px;" rel="tag"><span>View All &gt;</span></a></div>
            <style>.sizenameviralb {display:none;}</style>
     

    
    
        

        

        
        
        
        <div data-value="XS" class="swatch-element xs available">
            

            <input id="swatch-0-xs" type="radio" name="option-0" value="XS">

            
            <label for="swatch-0-xs">
                XS
            </label>
            
        <input class="text" data-value="swatch-0-xs" type="hidden" data-value-sticky="43399271874782"></div>
        

        
    
        

        

        
        
        
        <div data-value="S" class="swatch-element s available">
            

            <input id="swatch-0-s" type="radio" name="option-0" value="S">

            
            <label for="swatch-0-s">
                S
            </label>
            
        <input class="text" data-value="swatch-0-s" type="hidden" data-value-sticky="43399271907550"></div>
        

        
    
        

        

        
        
        
        <div data-value="M" class="swatch-element m available">
            

            <input id="swatch-0-m" type="radio" name="option-0" value="M">

            
            <label for="swatch-0-m">
                M
            </label>
            
        <input class="text" data-value="swatch-0-m" type="hidden" data-value-sticky="43399271940318"></div>
        

        
    
        

        

        
        
        
        <div data-value="L" class="swatch-element l available">
            

            <input id="swatch-0-l" type="radio" name="option-0" value="L">

            
            <label for="swatch-0-l">
                L
            </label>
            
        <input class="text" data-value="swatch-0-l" type="hidden" data-value-sticky="43399271973086"></div>
        

        
    
        

        

        
        
        
        <div data-value="XL" class="swatch-element xl available">
            

            <input id="swatch-0-xl" type="radio" name="option-0" value="XL">

            
            <label for="swatch-0-xl">
                XL
            </label>
            
        <input class="text" data-value="swatch-0-xl" type="hidden" data-value-sticky="43399272005854"></div>
        

        
    
        

        

        
        
        
        <div data-value="XXL" class="swatch-element xxl available">
            

            <input id="swatch-0-xxl" type="radio" name="option-0" value="XXL">

            
            <label for="swatch-0-xxl">
                XXL
            </label>
            
        <input class="text" data-value="swatch-0-xxl" type="hidden" data-value-sticky="43399272038622"></div>
        

        
    
        

        

        
        
        
        <div data-value="XXXL" class="swatch-element xxxl available">
            

            <input id="swatch-0-xxxl" type="radio" name="option-0" value="XXXL">

            
            <label for="swatch-0-xxxl">
                XXXL
            </label>
            
        <input class="text" data-value="swatch-0-xxxl" type="hidden" data-value-sticky="43399272071390"></div>
        

        
    
</div>



                      
                      

                      

                      
                  </div>
                
                  

                  

                  
                      <div class="groups-btn">
                          <div class="groups-btn-tree margin-button">
                            
                          
                            
                            
                          
                            
                            <input data-btn-addtocart="" type="submit" name="add" class="btn" id="product-add-to-cart" value="Add to Cart" data-form-id="#add-to-cart-form">
                            
                            
                          
                            

                            
<a class="wishlist " data-icon-wishlist="" aria-label="Wish Lists" href="#" data-product-handle="mulmul-cotton-lilac-v-neck-kurta" data-id="7797834973406">
	    	<svg viewBox="0 0 512 512">
<g>
<g>
  <path d="M474.644,74.27C449.391,45.616,414.358,29.836,376,29.836c-53.948,0-88.103,32.22-107.255,59.25
    c-4.969,7.014-9.196,14.047-12.745,20.665c-3.549-6.618-7.775-13.651-12.745-20.665c-19.152-27.03-53.307-59.25-107.255-59.25
    c-38.358,0-73.391,15.781-98.645,44.435C13.267,101.605,0,138.213,0,177.351c0,42.603,16.633,82.228,52.345,124.7
    c31.917,37.96,77.834,77.088,131.005,122.397c19.813,16.884,40.302,34.344,62.115,53.429l0.655,0.574
    c2.828,2.476,6.354,3.713,9.88,3.713s7.052-1.238,9.88-3.713l0.655-0.574c21.813-19.085,42.302-36.544,62.118-53.431
    c53.168-45.306,99.085-84.434,131.002-122.395C495.367,259.578,512,219.954,512,177.351
    C512,138.213,498.733,101.605,474.644,74.27z M309.193,401.614c-17.08,14.554-34.658,29.533-53.193,45.646
    c-18.534-16.111-36.113-31.091-53.196-45.648C98.745,312.939,30,254.358,30,177.351c0-31.83,10.605-61.394,29.862-83.245
    C79.34,72.007,106.379,59.836,136,59.836c41.129,0,67.716,25.338,82.776,46.594c13.509,19.064,20.558,38.282,22.962,45.659
    c2.011,6.175,7.768,10.354,14.262,10.354c6.494,0,12.251-4.179,14.262-10.354c2.404-7.377,9.453-26.595,22.962-45.66
    c15.06-21.255,41.647-46.593,82.776-46.593c29.621,0,56.66,12.171,76.137,34.27C471.395,115.957,482,145.521,482,177.351
    C482,254.358,413.255,312.939,309.193,401.614z"></path>
</g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
</svg>
		
</a>
 



                            
                              <a class="icon-share" href="javascript:void(0)"><svg viewBox="0 0 227.216 227.216">
<path d="M175.897,141.476c-13.249,0-25.11,6.044-32.98,15.518l-51.194-29.066c1.592-4.48,2.467-9.297,2.467-14.317
	c0-5.019-0.875-9.836-2.467-14.316l51.19-29.073c7.869,9.477,19.732,15.523,32.982,15.523c23.634,0,42.862-19.235,42.862-42.879
	C218.759,19.229,199.531,0,175.897,0C152.26,0,133.03,19.229,133.03,42.865c0,5.02,0.874,9.838,2.467,14.319L84.304,86.258
	c-7.869-9.472-19.729-15.514-32.975-15.514c-23.64,0-42.873,19.229-42.873,42.866c0,23.636,19.233,42.865,42.873,42.865
	c13.246,0,25.105-6.042,32.974-15.513l51.194,29.067c-1.593,4.481-2.468,9.3-2.468,14.321c0,23.636,19.23,42.865,42.867,42.865
	c23.634,0,42.862-19.23,42.862-42.865C218.759,160.71,199.531,141.476,175.897,141.476z M175.897,15
	c15.363,0,27.862,12.5,27.862,27.865c0,15.373-12.499,27.879-27.862,27.879c-15.366,0-27.867-12.506-27.867-27.879
	C148.03,27.5,160.531,15,175.897,15z M51.33,141.476c-15.369,0-27.873-12.501-27.873-27.865c0-15.366,12.504-27.866,27.873-27.866
	c15.363,0,27.861,12.5,27.861,27.866C79.191,128.975,66.692,141.476,51.33,141.476z M175.897,212.216
	c-15.366,0-27.867-12.501-27.867-27.865c0-15.37,12.501-27.875,27.867-27.875c15.363,0,27.862,12.505,27.862,27.875
	C203.759,199.715,191.26,212.216,175.897,212.216z"></path>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
</svg></a>
                            
                          </div>

                          
                          <div data-shopify="payment-button" data-has-selling-plan="false" data-has-fixed-selling-plan="false" class="shopify-payment-button"><div><div><div><div class="shopify-cleanslate"><div id="shopify-svg-symbols" class="R9tDu8JrE_i1ctOEo0v_" aria-hidden="true"><svg xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" focusable="false"><defs><symbol id="shopify-svg__warning" viewBox="0 0 16 14"><path d="M5.925 2.344c1.146-1.889 3.002-1.893 4.149 0l4.994 8.235c1.146 1.889.288 3.421-1.916 3.421h-10.305c-2.204 0-3.063-1.529-1.916-3.421l4.994-8.235zm1.075 1.656v5h2v-5h-2zm0 6v2h2v-2h-2z"></path></symbol><symbol id="shopify-svg__loading" viewBox="0 0 32 32"><path d="M32 16c0 8.837-7.163 16-16 16S0 24.837 0 16 7.163 0 16 0v2C8.268 2 2 8.268 2 16s6.268 14 14 14 14-6.268 14-14h2z"></path></symbol><symbol id="shopify-svg__error" viewBox="0 0 18 18"><path d="M9 18c5 0 9-4 9-9s-4-9-9-9-9 4-9 9 4 9 9 9z" style="fill: rgb(255, 62, 62);"></path><path d="M8 4h2v6H8z" style="fill: rgb(255, 255, 255);"></path><rect x="7.8" y="12" width="2.5" height="2.5" rx="1.3" style="fill: rgb(255, 255, 255);"></rect></symbol><symbol id="shopify-svg__close-circle" viewBox="0 0 16 16"><circle cx="8" cy="8" r="8"></circle><path d="M10.5 5.5l-5 5M5.5 5.5l5 5" stroke="#FFF" stroke-width="1.5" stroke-linecap="square"></path></symbol><symbol id="shopify-svg__close" viewBox="0 0 20 20"><path d="M17.1 4.3l-1.4-1.4-5.7 5.7-5.7-5.7-1.4 1.4 5.7 5.7-5.7 5.7 1.4 1.4 5.7-5.7 5.7 5.7 1.4-1.4-5.7-5.7z"></path></symbol><symbol id="shopify-svg__arrow-right" viewBox="0 0 16 16"><path d="M16 8.1l-8.1 8.1-1.1-1.1L13 8.9H0V7.3h13L6.8 1.1 7.9 0 16 8.1z"></path></symbol><symbol id="shopify-svg__payments-google-pay-light" viewBox="0 0 41 17"><path d="M19.526 2.635v4.083h2.518c.6 0 1.096-.202 1.488-.605.403-.402.605-.882.605-1.437 0-.544-.202-1.018-.605-1.422-.392-.413-.888-.62-1.488-.62h-2.518zm0 5.52v4.736h-1.504V1.198h3.99c1.013 0 1.873.337 2.582 1.012.72.675 1.08 1.497 1.08 2.466 0 .991-.36 1.819-1.08 2.482-.697.665-1.559.996-2.583.996h-2.485v.001zM27.194 10.442c0 .392.166.718.499.98.332.26.722.391 1.168.391.633 0 1.196-.234 1.692-.701.497-.469.744-1.019.744-1.65-.469-.37-1.123-.555-1.962-.555-.61 0-1.12.148-1.528.442-.409.294-.613.657-.613 1.093m1.946-5.815c1.112 0 1.989.297 2.633.89.642.594.964 1.408.964 2.442v4.932h-1.439v-1.11h-.065c-.622.914-1.45 1.372-2.486 1.372-.882 0-1.621-.262-2.215-.784-.594-.523-.891-1.176-.891-1.96 0-.828.313-1.486.94-1.976s1.463-.735 2.51-.735c.892 0 1.629.163 2.206.49v-.344c0-.522-.207-.966-.621-1.33a2.132 2.132 0 0 0-1.455-.547c-.84 0-1.504.353-1.995 1.062l-1.324-.834c.73-1.045 1.81-1.568 3.238-1.568M40.993 4.889l-5.02 11.53H34.42l1.864-4.034-3.302-7.496h1.635l2.387 5.749h.032l2.322-5.75z" style="fill: rgb(255, 255, 255);"></path><path d="M13.448 7.134c0-.473-.04-.93-.116-1.366H6.988v2.588h3.634a3.11 3.11 0 0 1-1.344 2.042v1.68h2.169c1.27-1.17 2.001-2.9 2.001-4.944" style="fill: rgb(66, 133, 244);"></path><path d="M6.988 13.7c1.816 0 3.344-.595 4.459-1.621l-2.169-1.681c-.603.406-1.38.643-2.29.643-1.754 0-3.244-1.182-3.776-2.774H.978v1.731a6.728 6.728 0 0 0 6.01 3.703" style="fill: rgb(52, 168, 83);"></path><path d="M3.212 8.267a4.034 4.034 0 0 1 0-2.572V3.964H.978A6.678 6.678 0 0 0 .261 6.98c0 1.085.26 2.11.717 3.017l2.234-1.731z" style="fill: rgb(251, 188, 5);"></path><path d="M6.988 2.921c.992 0 1.88.34 2.58 1.008v.001l1.92-1.918C10.324.928 8.804.262 6.989.262a6.728 6.728 0 0 0-6.01 3.702l2.234 1.731c.532-1.592 2.022-2.774 3.776-2.774" style="fill: rgb(234, 67, 53);"></path></symbol><symbol id="shopify-svg__payments-google-pay-dark" viewBox="0 0 41 17"><path d="M19.526 2.635v4.083h2.518c.6 0 1.096-.202 1.488-.605.403-.402.605-.882.605-1.437 0-.544-.202-1.018-.605-1.422-.392-.413-.888-.62-1.488-.62h-2.518zm0 5.52v4.736h-1.504V1.198h3.99c1.013 0 1.873.337 2.582 1.012.72.675 1.08 1.497 1.08 2.466 0 .991-.36 1.819-1.08 2.482-.697.665-1.559.996-2.583.996h-2.485v.001zM27.194 10.442c0 .392.166.718.499.98.332.26.722.391 1.168.391.633 0 1.196-.234 1.692-.701.497-.469.744-1.019.744-1.65-.469-.37-1.123-.555-1.962-.555-.61 0-1.12.148-1.528.442-.409.294-.613.657-.613 1.093m1.946-5.815c1.112 0 1.989.297 2.633.89.642.594.964 1.408.964 2.442v4.932h-1.439v-1.11h-.065c-.622.914-1.45 1.372-2.486 1.372-.882 0-1.621-.262-2.215-.784-.594-.523-.891-1.176-.891-1.96 0-.828.313-1.486.94-1.976s1.463-.735 2.51-.735c.892 0 1.629.163 2.206.49v-.344c0-.522-.207-.966-.621-1.33a2.132 2.132 0 0 0-1.455-.547c-.84 0-1.504.353-1.995 1.062l-1.324-.834c.73-1.045 1.81-1.568 3.238-1.568M40.993 4.889l-5.02 11.53H34.42l1.864-4.034-3.302-7.496h1.635l2.387 5.749h.032l2.322-5.75z" style="fill: rgba(0, 0, 0, 0.55);"></path><path d="M13.448 7.134c0-.473-.04-.93-.116-1.366H6.988v2.588h3.634a3.11 3.11 0 0 1-1.344 2.042v1.68h2.169c1.27-1.17 2.001-2.9 2.001-4.944" style="fill: rgb(66, 133, 244);"></path><path d="M6.988 13.7c1.816 0 3.344-.595 4.459-1.621l-2.169-1.681c-.603.406-1.38.643-2.29.643-1.754 0-3.244-1.182-3.776-2.774H.978v1.731a6.728 6.728 0 0 0 6.01 3.703" style="fill: rgb(52, 168, 83);"></path><path d="M3.212 8.267a4.034 4.034 0 0 1 0-2.572V3.964H.978A6.678 6.678 0 0 0 .261 6.98c0 1.085.26 2.11.717 3.017l2.234-1.731z" style="fill: rgb(251, 188, 5);"></path><path d="M6.988 2.921c.992 0 1.88.34 2.58 1.008v.001l1.92-1.918C10.324.928 8.804.262 6.989.262a6.728 6.728 0 0 0-6.01 3.702l2.234 1.731c.532-1.592 2.022-2.774 3.776-2.774" style="fill: rgb(234, 67, 53);"></path></symbol><symbol id="shopify-svg__payments-facebook-pay-dark" viewBox="0 0 300 50"><path d="M277.374 25.35a330.858 330.858 0 0 1-5.496-14.28h6.355c1.122 3.701 2.393 7.365 3.739 11.066 1.383 3.7 2.803 7.364 4.336 10.953a271.222 271.222 0 0 0 7.29-22.019h6.131a183.26 183.26 0 0 1-2.692 7.963 500.958 500.958 0 0 1-3.402 9.159c-1.271 3.29-2.766 6.99-4.411 11.028-1.57 3.813-3.327 6.542-5.196 8.224-1.907 1.645-4.225 2.505-7.028 2.505a11.37 11.37 0 0 1-2.243-.225v-5.046c.374.037.673.074.897.112h.673c1.645 0 3.028-.449 4.149-1.309 1.122-.86 2.131-2.28 3.066-4.299-2.168-4.523-4.187-9.121-6.131-13.832h-.037Zm-9.795 13.421h-5.682v-3.888c-1.009 1.458-2.28 2.58-3.85 3.365-1.571.785-3.328 1.196-5.309 1.196-2.467 0-4.635-.636-6.505-1.87-1.906-1.233-3.364-2.99-4.448-5.158-1.084-2.206-1.608-4.71-1.608-7.514 0-2.804.561-5.346 1.645-7.552 1.084-2.168 2.617-3.887 4.561-5.121 1.944-1.234 4.187-1.87 6.692-1.87 1.906 0 3.588.375 5.121 1.122 1.495.748 2.766 1.795 3.738 3.14v-3.55h5.683v27.775l-.038-.075Zm-5.794-18.056c-.636-1.57-1.608-2.841-2.953-3.738-1.346-.898-2.879-1.384-4.636-1.384-2.467 0-4.449.823-5.944 2.505-1.458 1.682-2.205 3.925-2.205 6.767 0 2.84.71 5.121 2.13 6.803 1.421 1.682 3.327 2.505 5.795 2.505 1.794 0 3.402-.449 4.785-1.383 1.383-.935 2.43-2.168 3.028-3.739v-8.336ZM209 1.5h14.131c4.747 0 8.411 1.084 10.99 3.252 2.58 2.169 3.888 5.234 3.888 9.271 0 4.038-1.271 7.103-3.85 9.271-2.58 2.169-6.243 3.253-11.028 3.253h-8.038v12.261H209V1.5Zm13.645 19.551c3.14 0 5.42-.56 6.916-1.72 1.495-1.158 2.243-2.915 2.243-5.27 0-2.355-.748-4.225-2.243-5.346-1.496-1.122-3.813-1.72-6.916-1.72h-7.552v14.056h7.552ZM71.937 1.249h7.429l12.63 22.85 12.632-22.85h7.268v37.546h-6.06V10.019L94.758 29.946h-5.686L77.997 10.019v28.776h-6.06V1.249Zm58.947 13.999c-4.346 0-6.964 3.27-7.59 7.32h14.75c-.304-4.171-2.711-7.32-7.16-7.32Zm-13.598 9.628c0-8.522 5.508-14.725 13.703-14.725 8.061 0 12.875 6.124 12.875 15.18v1.665h-20.57c.73 4.405 3.653 7.374 8.367 7.374 3.761 0 6.112-1.147 8.34-3.246l3.22 3.943c-3.033 2.79-6.891 4.398-11.775 4.398-8.872 0-14.16-6.47-14.16-14.589Zm33.926-9.09h-5.579v-4.963h5.579V2.618h5.846v8.205h8.475v4.962h-8.475v12.577c0 4.294 1.373 5.82 4.747 5.82 1.541 0 2.424-.132 3.728-.35v4.909c-1.625.459-3.176.67-4.854.67-6.312 0-9.467-3.449-9.467-10.352V15.785v.001Zm38.941 4.825c-1.174-2.965-3.794-5.148-7.644-5.148-5.003 0-8.205 3.55-8.205 9.333 0 5.638 2.948 9.36 7.966 9.36 3.944 0 6.76-2.296 7.883-5.15V20.61v.001ZM196 38.795h-5.739v-3.916c-1.605 2.305-4.524 4.586-9.253 4.586-7.604 0-12.686-6.366-12.686-14.67 0-8.381 5.204-14.644 13.009-14.644 3.858 0 6.885 1.543 8.93 4.266v-3.594H196v27.972Z" fill="#000000"></path><path d="M6.422 26.042c0 2.27.498 4.013 1.15 5.068.853 1.38 2.127 1.966 3.425 1.966 1.675 0 3.207-.415 6.16-4.499 2.364-3.273 5.151-7.867 7.027-10.747l3.175-4.88c2.206-3.388 4.76-7.155 7.687-9.708C37.436 1.158 40.015 0 42.61 0c4.357 0 8.506 2.524 11.682 7.259 3.475 5.185 5.162 11.717 5.162 18.457 0 4.007-.79 6.95-2.133 9.277-1.299 2.25-3.83 4.497-8.086 4.497v-6.414c3.645 0 4.554-3.35 4.554-7.182 0-5.463-1.273-11.525-4.079-15.856-1.99-3.073-4.571-4.95-7.41-4.95-3.07 0-5.54 2.316-8.317 6.445-1.477 2.193-2.992 4.867-4.694 7.883l-1.873 3.318c-3.763 6.672-4.716 8.192-6.597 10.7-3.298 4.391-6.114 6.056-9.82 6.056-4.398 0-7.18-1.905-8.901-4.774C.69 32.377 0 29.309 0 25.813l6.422.23v-.001Z" fill="#0081FB"></path><path d="M5.063 7.712C8.007 3.174 12.256 0 17.13 0c2.823 0 5.628.835 8.558 3.227 3.204 2.616 6.62 6.922 10.881 14.02l1.528 2.547c3.688 6.145 5.787 9.306 7.015 10.797 1.58 1.914 2.686 2.485 4.123 2.485 3.645 0 4.554-3.35 4.554-7.182l5.665-.178c0 4.007-.79 6.95-2.133 9.277-1.299 2.25-3.83 4.496-8.086 4.496-2.647 0-4.991-.574-7.584-3.02-1.993-1.877-4.323-5.212-6.116-8.21l-5.332-8.907c-2.675-4.47-5.13-7.803-6.55-9.312-1.528-1.623-3.492-3.583-6.626-3.583-2.537 0-4.691 1.78-6.494 4.503L5.064 7.712h-.001Z" fill="url(#meta-pay-button__a)"></path><path d="M17.026 6.457c-2.537 0-4.691 1.78-6.494 4.503-2.55 3.848-4.11 9.579-4.11 15.082 0 2.27.498 4.013 1.15 5.068l-5.476 3.606C.691 32.377 0 29.309 0 25.813c0-6.358 1.745-12.984 5.063-18.101C8.007 3.174 12.256 0 17.13 0l-.103 6.457h-.001Z" fill="url(#meta-pay-button__b)"></path><defs><linearGradient id="meta-pay-button__a" x1="12.612" y1="24.19" x2="53.549" y2="26.257" gradientUnits="userSpaceOnUse"><stop stop-color="#0064E1"></stop><stop offset=".4" stop-color="#0064E1"></stop><stop offset=".83" stop-color="#0073EE"></stop><stop offset="1" stop-color="#0082FB"></stop></linearGradient><linearGradient id="meta-pay-button__b" x1="9.304" y1="28.738" x2="9.304" y2="13.646" gradientUnits="userSpaceOnUse"><stop stop-color="#0082FB"></stop><stop offset="1" stop-color="#0064E0"></stop></linearGradient></defs></symbol><symbol id="shopify-svg__payments-facebook-pay-light" viewBox="0 0 300 50"><path d="M277.374 25.35a330.858 330.858 0 0 1-5.496-14.28h6.355c1.122 3.701 2.393 7.365 3.739 11.066 1.383 3.7 2.803 7.364 4.336 10.953a271.222 271.222 0 0 0 7.29-22.019h6.131a183.26 183.26 0 0 1-2.692 7.963 500.958 500.958 0 0 1-3.402 9.159c-1.271 3.29-2.766 6.99-4.411 11.028-1.57 3.813-3.327 6.542-5.196 8.224-1.907 1.645-4.225 2.505-7.028 2.505a11.37 11.37 0 0 1-2.243-.225v-5.046c.374.037.673.074.897.112h.673c1.645 0 3.028-.449 4.149-1.309 1.122-.86 2.131-2.28 3.066-4.299-2.168-4.523-4.187-9.121-6.131-13.832h-.037Zm-9.795 13.421h-5.682v-3.888c-1.009 1.458-2.28 2.58-3.85 3.365-1.571.785-3.328 1.196-5.309 1.196-2.467 0-4.635-.636-6.505-1.87-1.906-1.233-3.364-2.99-4.448-5.158-1.084-2.206-1.608-4.71-1.608-7.514 0-2.804.561-5.346 1.645-7.552 1.084-2.168 2.617-3.887 4.561-5.121 1.944-1.234 4.187-1.87 6.692-1.87 1.906 0 3.588.375 5.121 1.122 1.495.748 2.766 1.795 3.738 3.14v-3.55h5.683v27.775l-.038-.075Zm-5.794-18.056c-.636-1.57-1.608-2.841-2.953-3.738-1.346-.898-2.879-1.384-4.636-1.384-2.467 0-4.449.823-5.944 2.505-1.458 1.682-2.205 3.925-2.205 6.767 0 2.84.71 5.121 2.13 6.803 1.421 1.682 3.327 2.505 5.795 2.505 1.794 0 3.402-.449 4.785-1.383 1.383-.935 2.43-2.168 3.028-3.739v-8.336ZM209 1.5h14.131c4.747 0 8.411 1.084 10.99 3.252 2.58 2.169 3.888 5.234 3.888 9.271 0 4.038-1.271 7.103-3.85 9.271-2.58 2.169-6.243 3.253-11.028 3.253h-8.038v12.261H209V1.5Zm13.645 19.551c3.14 0 5.42-.56 6.916-1.72 1.495-1.158 2.243-2.915 2.243-5.27 0-2.355-.748-4.225-2.243-5.346-1.496-1.122-3.813-1.72-6.916-1.72h-7.552v14.056h7.552ZM71.937 1.249h7.429l12.63 22.85 12.632-22.85h7.268v37.546h-6.06V10.019L94.758 29.946h-5.686L77.997 10.019v28.776h-6.06V1.249Zm58.947 13.999c-4.346 0-6.964 3.27-7.59 7.32h14.75c-.304-4.171-2.711-7.32-7.16-7.32Zm-13.598 9.628c0-8.522 5.508-14.725 13.703-14.725 8.061 0 12.875 6.124 12.875 15.18v1.665h-20.57c.73 4.405 3.653 7.374 8.367 7.374 3.761 0 6.112-1.147 8.34-3.246l3.22 3.943c-3.033 2.79-6.891 4.398-11.775 4.398-8.872 0-14.16-6.47-14.16-14.589Zm33.926-9.09h-5.579v-4.963h5.579V2.618h5.846v8.205h8.475v4.962h-8.475v12.577c0 4.294 1.373 5.82 4.747 5.82 1.541 0 2.424-.132 3.728-.35v4.909c-1.625.459-3.176.67-4.854.67-6.312 0-9.467-3.449-9.467-10.352V15.785v.001Zm38.941 4.825c-1.174-2.965-3.794-5.148-7.644-5.148-5.003 0-8.205 3.55-8.205 9.333 0 5.638 2.948 9.36 7.966 9.36 3.944 0 6.76-2.296 7.883-5.15V20.61v.001ZM196 38.795h-5.739v-3.916c-1.605 2.305-4.524 4.586-9.253 4.586-7.604 0-12.686-6.366-12.686-14.67 0-8.381 5.204-14.644 13.009-14.644 3.858 0 6.885 1.543 8.93 4.266v-3.594H196v27.972Z" fill="#fff"></path><path d="M6.422 26.042c0 2.27.498 4.013 1.15 5.068.853 1.38 2.127 1.966 3.425 1.966 1.675 0 3.207-.415 6.16-4.499 2.364-3.273 5.151-7.867 7.027-10.747l3.175-4.88c2.206-3.388 4.76-7.155 7.687-9.708C37.436 1.158 40.015 0 42.61 0c4.357 0 8.506 2.524 11.682 7.259 3.475 5.185 5.162 11.717 5.162 18.457 0 4.007-.79 6.95-2.133 9.277-1.299 2.25-3.83 4.497-8.086 4.497v-6.414c3.645 0 4.554-3.35 4.554-7.182 0-5.463-1.273-11.525-4.079-15.856-1.99-3.073-4.571-4.95-7.41-4.95-3.07 0-5.54 2.316-8.317 6.445-1.477 2.193-2.992 4.867-4.694 7.883l-1.873 3.318c-3.763 6.672-4.716 8.192-6.597 10.7-3.298 4.391-6.114 6.056-9.82 6.056-4.398 0-7.18-1.905-8.901-4.774C.69 32.377 0 29.309 0 25.813l6.422.23v-.001Z" fill="#0081FB"></path><path d="M5.063 7.712C8.007 3.174 12.256 0 17.13 0c2.823 0 5.628.835 8.558 3.227 3.204 2.616 6.62 6.922 10.881 14.02l1.528 2.547c3.688 6.145 5.787 9.306 7.015 10.797 1.58 1.914 2.686 2.485 4.123 2.485 3.645 0 4.554-3.35 4.554-7.182l5.665-.178c0 4.007-.79 6.95-2.133 9.277-1.299 2.25-3.83 4.496-8.086 4.496-2.647 0-4.991-.574-7.584-3.02-1.993-1.877-4.323-5.212-6.116-8.21l-5.332-8.907c-2.675-4.47-5.13-7.803-6.55-9.312-1.528-1.623-3.492-3.583-6.626-3.583-2.537 0-4.691 1.78-6.494 4.503L5.064 7.712h-.001Z" fill="url(#meta-pay-button__a)"></path><path d="M17.026 6.457c-2.537 0-4.691 1.78-6.494 4.503-2.55 3.848-4.11 9.579-4.11 15.082 0 2.27.498 4.013 1.15 5.068l-5.476 3.606C.691 32.377 0 29.309 0 25.813c0-6.358 1.745-12.984 5.063-18.101C8.007 3.174 12.256 0 17.13 0l-.103 6.457h-.001Z" fill="url(#meta-pay-button__b)"></path><defs><linearGradient id="meta-pay-button__a" x1="12.612" y1="24.19" x2="53.549" y2="26.257" gradientUnits="userSpaceOnUse"><stop stop-color="#0064E1"></stop><stop offset=".4" stop-color="#0064E1"></stop><stop offset=".83" stop-color="#0073EE"></stop><stop offset="1" stop-color="#0082FB"></stop></linearGradient><linearGradient id="meta-pay-button__b" x1="9.304" y1="28.738" x2="9.304" y2="13.646" gradientUnits="userSpaceOnUse"><stop stop-color="#0082FB"></stop><stop offset="1" stop-color="#0064E0"></stop></linearGradient></defs></symbol><symbol id="shopify-svg__payments-amazon-pay" viewBox="0 0 102 20"><path d="M75.19 1.786c-.994 0-1.933.326-2.815.98v5.94c.896.683 1.82 1.023 2.774 1.023 1.932 0 2.899-1.32 2.899-3.96 0-2.655-.953-3.983-2.858-3.983zm-2.962-.277A5.885 5.885 0 0 1 73.93.444a4.926 4.926 0 0 1 1.85-.362c.672 0 1.282.127 1.827.383a3.763 3.763 0 0 1 1.387 1.108c.378.482.669 1.068.872 1.757.203.689.305 1.466.305 2.332 0 .88-.109 1.675-.326 2.385-.217.71-.522 1.314-.914 1.81a4.137 4.137 0 0 1-1.429 1.16 4.165 4.165 0 0 1-1.87.416c-1.26 0-2.346-.419-3.256-1.256v4.983c0 .284-.14.426-.42.426h-1.24c-.28 0-.42-.142-.42-.426V.827c0-.284.14-.426.42-.426h.925c.28 0 .441.142.483.426l.105.682zm13.194 8.37a4.21 4.21 0 0 0 1.45-.277 5.463 5.463 0 0 0 1.45-.81V6.62c-.35-.085-.719-.152-1.104-.202a8.8 8.8 0 0 0-1.124-.075c-1.583 0-2.374.617-2.374 1.853 0 .54.147.955.441 1.246.294.29.715.437 1.261.437zm-2.458-7.625l-.158.053a.561.561 0 0 1-.179.033c-.182 0-.273-.128-.273-.384V1.38c0-.199.028-.337.084-.415.056-.078.169-.153.337-.224.448-.199 1-.359 1.66-.48.657-.12 1.316-.18 1.974-.18 1.33 0 2.311.277 2.942.83.63.554.945 1.413.945 2.577v7.284c0 .284-.14.426-.42.426h-.903c-.267 0-.42-.135-.463-.405l-.105-.702a5.74 5.74 0 0 1-1.67 1.022 4.908 4.908 0 0 1-1.817.362c-1.009 0-1.807-.288-2.395-.863-.589-.575-.883-1.345-.883-2.31 0-1.037.364-1.864 1.092-2.481.73-.618 1.71-.927 2.942-.927.784 0 1.667.12 2.647.362V3.852c0-.767-.168-1.307-.504-1.619-.336-.313-.925-.469-1.764-.469-.982 0-2.01.163-3.09.49zm14.16 10.84c-.379.98-.816 1.683-1.314 2.109-.496.426-1.144.639-1.943.639-.448 0-.847-.05-1.197-.15a.606.606 0 0 1-.336-.202c-.07-.093-.105-.237-.105-.437V14.5c0-.27.105-.405.315-.405.07 0 .175.014.315.043.14.028.33.043.567.043.532 0 .946-.128 1.24-.384.294-.255.56-.724.798-1.406l.4-1.086-4.056-10.137c-.098-.241-.146-.411-.146-.511 0-.17.097-.256.294-.256h1.26c.224 0 .378.036.463.106.083.072.167.228.251.47l2.942 8.263L99.708.976c.084-.24.168-.397.252-.469.084-.07.238-.106.462-.106h1.177c.196 0 .294.086.294.256 0 .1-.05.27-.147.51l-4.622 11.927M40.15 15.47c-3.761 2.814-9.216 4.31-13.912 4.31-6.583 0-12.51-2.466-16.996-6.572-.352-.322-.038-.763.385-.513 4.84 2.855 10.825 4.574 17.006 4.574 4.17 0 8.753-.877 12.971-2.691.636-.273 1.17.425.547.891" style="fill: rgb(51, 62, 72);"></path><path d="M41.717 13.657c-.482-.624-3.181-.296-4.394-.148-.368.044-.425-.281-.093-.517 2.153-1.533 5.682-1.09 6.092-.577.413.518-.108 4.104-2.127 5.816-.31.263-.605.122-.468-.225.455-1.15 1.471-3.724.99-4.349M37.429 2.06V.57A.365.365 0 0 1 37.8.193l6.59-.001c.21 0 .38.155.38.376v1.278c-.003.214-.18.494-.496.938L40.86 7.722c1.267-.03 2.607.163 3.757.818.26.148.33.367.35.582v1.59c0 .218-.237.472-.485.34-2.028-1.077-4.718-1.194-6.96.013-.23.124-.47-.126-.47-.345V9.209c0-.242.005-.656.246-1.024l3.953-5.75H37.81a.369.369 0 0 1-.38-.375M13.4 11.365h-2.005a.38.38 0 0 1-.358-.343L11.038.595a.38.38 0 0 1 .387-.375h1.866a.38.38 0 0 1 .365.35v1.36h.037C14.18.615 15.096 0 16.331 0c1.253 0 2.039.614 2.6 1.93C19.418.615 20.521 0 21.7 0c.842 0 1.758.351 2.32 1.141.635.878.505 2.15.505 3.27l-.002 6.58a.38.38 0 0 1-.387.374h-2.001a.378.378 0 0 1-.36-.374V5.463c0-.438.037-1.535-.056-1.952-.15-.703-.6-.9-1.179-.9-.486 0-.991.33-1.197.855-.206.527-.188 1.405-.188 1.997v5.527a.38.38 0 0 1-.386.375h-2.002a.379.379 0 0 1-.36-.374l-.001-5.528c0-1.163.186-2.874-1.235-2.874-1.44 0-1.384 1.668-1.384 2.874l-.001 5.527a.38.38 0 0 1-.387.375m37.059-9.236c-1.478 0-1.571 2.04-1.571 3.312 0 1.273-.02 3.993 1.552 3.993 1.554 0 1.628-2.194 1.628-3.532 0-.877-.038-1.93-.3-2.764-.224-.724-.673-1.01-1.31-1.01zM50.439 0c2.975 0 4.584 2.59 4.584 5.88 0 3.181-1.777 5.705-4.584 5.705-2.918 0-4.508-2.59-4.508-5.814C45.93 2.523 47.539 0 50.439 0zm8.441 11.365h-1.997a.379.379 0 0 1-.36-.374L56.52.561a.381.381 0 0 1 .386-.34L58.764.22c.175.009.32.13.356.291v1.595h.038C59.72.68 60.505 0 61.89 0c.898 0 1.778.329 2.339 1.229.524.834.524 2.237.524 3.247v6.561a.382.382 0 0 1-.385.328H62.36a.38.38 0 0 1-.357-.328V5.376c0-1.141.13-2.809-1.253-2.809-.487 0-.936.33-1.16.834-.281.636-.319 1.272-.319 1.975v5.614a.386.386 0 0 1-.39.375m-24.684.075a.41.41 0 0 1-.473.047c-.665-.56-.785-.82-1.149-1.354-1.1 1.136-1.879 1.477-3.304 1.477-1.687 0-3-1.055-3-3.166 0-1.65.882-2.77 2.138-3.32 1.087-.484 2.606-.572 3.769-.704v-.264c0-.484.037-1.055-.245-1.473-.243-.374-.712-.528-1.124-.528-.765 0-1.444.397-1.611 1.22-.035.183-.167.364-.348.374l-1.943-.214c-.164-.037-.346-.17-.299-.425C27.055.721 29.183 0 31.09 0c.975 0 2.25.263 3.018 1.011.975.924.881 2.155.881 3.497v3.165c0 .952.39 1.37.757 1.882.128.185.156.405-.007.54-.409.348-1.136.988-1.537 1.35l-.005-.005zm-2.02-4.953v-.44c-1.45 0-2.98.314-2.98 2.045 0 .88.45 1.473 1.218 1.473.562 0 1.069-.352 1.387-.923.394-.704.376-1.363.376-2.155zM7.926 11.44a.41.41 0 0 1-.473.047c-.667-.56-.786-.82-1.15-1.354C5.204 11.27 4.425 11.61 3 11.61c-1.688 0-3-1.055-3-3.166 0-1.65.88-2.77 2.137-3.32 1.087-.484 2.606-.572 3.768-.704v-.264c0-.484.038-1.055-.243-1.473-.244-.374-.713-.528-1.125-.528-.764 0-1.444.397-1.61 1.22-.036.183-.168.364-.35.374l-1.94-.214c-.165-.037-.347-.17-.3-.425C.783.721 2.911 0 4.818 0c.975 0 2.25.263 3.018 1.011.975.924.882 2.155.882 3.497v3.165c0 .952.39 1.37.756 1.882.128.185.157.405-.006.54a78.47 78.47 0 0 0-1.537 1.35l-.005-.005zm-2.02-4.953v-.44c-1.45 0-2.982.314-2.982 2.045 0 .88.45 1.473 1.219 1.473.562 0 1.069-.352 1.387-.923.394-.704.375-1.363.375-2.155z" style="fill: rgb(51, 62, 72);"></path></symbol><symbol id="shopify-svg__payments-apple-pay-dark" viewBox="0 0 43 19"><path d="M6.948 1.409C7.934.147 9.305.147 9.305.147s.193 1.18-.771 2.316c-1.05 1.2-2.228.993-2.228.993s-.236-.93.642-2.047zM3.82 3.663c-1.735 0-3.6 1.51-3.6 4.363 0 2.916 2.186 6.555 3.943 6.555.6 0 1.543-.6 2.485-.6.922 0 1.607.559 2.464.559 1.907 0 3.322-3.826 3.322-3.826s-2.015-.744-2.015-2.936c0-1.944 1.629-2.73 1.629-2.73s-.836-1.447-2.936-1.447c-1.22 0-2.164.661-2.656.661-.622.021-1.5-.6-2.636-.6zM19.64 1.426c2.453 0 4.188 1.788 4.188 4.396 0 2.608-1.755 4.417-4.248 4.417h-2.932v4.564h-1.974V1.426h4.966zm-2.992 7.067h2.473c1.695 0 2.693-.967 2.693-2.65 0-1.683-.978-2.671-2.693-2.671h-2.473v5.321zm7.559 3.429c0-1.767 1.296-2.777 3.65-2.945l2.572-.147v-.78c0-1.156-.738-1.787-1.994-1.787-1.037 0-1.795.568-1.955 1.43h-1.775c.06-1.788 1.656-3.092 3.79-3.092 2.333 0 3.829 1.304 3.829 3.281v6.9h-1.815v-1.684h-.04c-.519 1.094-1.715 1.788-3.012 1.788-1.934.021-3.25-1.178-3.25-2.965zm6.222-.905v-.778l-2.313.168c-1.297.084-1.975.59-1.975 1.494 0 .862.718 1.409 1.815 1.409 1.396-.021 2.473-.968 2.473-2.293zm3.969 7.383v-1.64c.14.041.438.041.598.041.897 0 1.416-.4 1.735-1.472l.14-.526L33.4 4.707h2.054l2.453 8.224h.04L40.4 4.707h1.994l-3.57 10.538c-.818 2.419-1.715 3.197-3.67 3.197-.14.02-.598-.021-.757-.042z" style="fill: rgb(0, 0, 0);"></path></symbol><symbol id="shopify-svg__payments-apple-pay-light" viewBox="0 0 43 19"><path d="M6.948 1.409C7.934.147 9.305.147 9.305.147s.193 1.18-.771 2.316c-1.05 1.2-2.228.993-2.228.993s-.236-.93.642-2.047zM3.82 3.663c-1.735 0-3.6 1.51-3.6 4.363 0 2.916 2.186 6.555 3.943 6.555.6 0 1.543-.6 2.485-.6.922 0 1.607.559 2.464.559 1.907 0 3.322-3.826 3.322-3.826s-2.015-.744-2.015-2.936c0-1.944 1.629-2.73 1.629-2.73s-.836-1.447-2.936-1.447c-1.22 0-2.164.661-2.656.661-.622.021-1.5-.6-2.636-.6zM19.64 1.426c2.453 0 4.188 1.788 4.188 4.396 0 2.608-1.755 4.417-4.248 4.417h-2.932v4.564h-1.974V1.426h4.966zm-2.992 7.067h2.473c1.695 0 2.693-.967 2.693-2.65 0-1.683-.978-2.671-2.693-2.671h-2.473v5.321zm7.559 3.429c0-1.767 1.296-2.777 3.65-2.945l2.572-.147v-.78c0-1.156-.738-1.787-1.994-1.787-1.037 0-1.795.568-1.955 1.43h-1.775c.06-1.788 1.656-3.092 3.79-3.092 2.333 0 3.829 1.304 3.829 3.281v6.9h-1.815v-1.684h-.04c-.519 1.094-1.715 1.788-3.012 1.788-1.934.021-3.25-1.178-3.25-2.965zm6.222-.905v-.778l-2.313.168c-1.297.084-1.975.59-1.975 1.494 0 .862.718 1.409 1.815 1.409 1.396-.021 2.473-.968 2.473-2.293zm3.969 7.383v-1.64c.14.041.438.041.598.041.897 0 1.416-.4 1.735-1.472l.14-.526L33.4 4.707h2.054l2.453 8.224h.04L40.4 4.707h1.994l-3.57 10.538c-.818 2.419-1.715 3.197-3.67 3.197-.14.02-.598-.021-.757-.042z" style="fill: rgb(255, 255, 255);"></path></symbol><symbol id="shopify-svg__payments-paypal" viewBox="0 0 67 19"><path d="M8.44.57H3.29a.718.718 0 0 0-.707.61L.502 14.517c-.041.263.16.5.425.5h2.458a.718.718 0 0 0 .707-.61l.561-3.597a.717.717 0 0 1 .706-.611h1.63c3.391 0 5.349-1.658 5.86-4.944.23-1.437.01-2.566-.657-3.357C11.461 1.029 10.162.57 8.44.57zm.594 4.87C8.752 7.308 7.34 7.308 5.976 7.308h-.777l.545-3.485a.43.43 0 0 1 .424-.366h.356c.93 0 1.807 0 2.26.535.27.32.353.794.25 1.45zm14.796-.06h-2.466a.43.43 0 0 0-.424.367l-.109.696-.172-.252c-.534-.783-1.724-1.044-2.912-1.044-2.725 0-5.052 2.084-5.505 5.008-.235 1.46.1 2.854.919 3.827.75.894 1.826 1.267 3.105 1.267 2.195 0 3.412-1.426 3.412-1.426l-.11.692a.432.432 0 0 0 .424.502h2.22a.718.718 0 0 0 .707-.61l1.333-8.526a.43.43 0 0 0-.423-.5zm-3.437 4.849c-.238 1.422-1.356 2.378-2.782 2.378-.716 0-1.288-.232-1.655-.672-.365-.436-.503-1.058-.387-1.75.222-1.41 1.359-2.397 2.763-2.397.7 0 1.269.235 1.644.678.375.448.524 1.073.417 1.763zM36.96 5.38h-2.478a.716.716 0 0 0-.592.318l-3.417 5.085-1.448-4.887a.719.719 0 0 0-.687-.515h-2.435a.433.433 0 0 0-.407.573l2.73 8.09-2.567 3.66a.434.434 0 0 0 .35.684h2.475a.712.712 0 0 0 .588-.31l8.24-12.016a.434.434 0 0 0-.352-.681z" style="fill: rgb(37, 59, 128);"></path><path d="M45.163.57h-5.15a.717.717 0 0 0-.706.61l-2.082 13.337a.43.43 0 0 0 .423.5h2.642a.502.502 0 0 0 .494-.427l.591-3.78a.717.717 0 0 1 .706-.611h1.63c3.392 0 5.348-1.658 5.86-4.944.231-1.437.009-2.566-.657-3.357C48.183 1.029 46.886.57 45.163.57zm.593 4.87c-.28 1.867-1.692 1.867-3.057 1.867h-.777l.546-3.485a.429.429 0 0 1 .423-.366h.356c.93 0 1.807 0 2.26.535.27.32.353.794.25 1.45zm14.795-.06h-2.464a.428.428 0 0 0-.423.367l-.109.696-.173-.252c-.534-.783-1.723-1.044-2.911-1.044-2.724 0-5.05 2.084-5.504 5.008-.235 1.46.099 2.854.918 3.827.753.894 1.826 1.267 3.105 1.267 2.195 0 3.413-1.426 3.413-1.426l-.11.692a.432.432 0 0 0 .424.502h2.22a.717.717 0 0 0 .707-.61l1.333-8.526a.433.433 0 0 0-.426-.5zm-3.436 4.849c-.237 1.422-1.356 2.378-2.782 2.378-.714 0-1.288-.232-1.655-.672-.365-.436-.502-1.058-.387-1.75.223-1.41 1.359-2.397 2.763-2.397.7 0 1.269.235 1.644.678.377.448.526 1.073.417 1.763zM63.458.935l-2.113 13.582a.43.43 0 0 0 .423.5h2.124a.716.716 0 0 0 .707-.61L66.683 1.07a.432.432 0 0 0-.423-.5h-2.379c-.21 0-.39.156-.423.366z" style="fill: rgb(23, 155, 215);"></path></symbol><symbol id="shopify-svg__payments-shop-pay" viewBox="134 256 410 1"><path d="M241.22,242.74c-3.07-6.44-8.89-10.6-17.66-10.6a17.58,17.58,0,0,0-13.81,7.1l-.32.39V214.39a.55.55,0,0,0-.55-.55h-12.4a.55.55,0,0,0-.54.55v72.4a.54.54,0,0,0,.54.54h13.28a.55.55,0,0,0,.55-.54V255.92c0-6,4-10.25,10.4-10.25,7,0,8.77,5.76,8.77,11.63v29.49a.54.54,0,0,0,.54.54h13.25a.55.55,0,0,0,.55-.54V255.54c0-1.07,0-2.12-.14-3.14A27.63,27.63,0,0,0,241.22,242.74Z" style="fill: white;"></path><path d="M174.91,253.47s-6.76-1.59-9.25-2.23-6.84-2-6.84-5.29,3.51-4.34,7.07-4.34,7.52.86,7.83,4.81a.57.57,0,0,0,.57.52l13.09-.05a.56.56,0,0,0,.56-.6c-.81-12.64-11.9-17.16-22.13-17.16-12.13,0-21,8-21,16.82,0,6.44,1.82,12.48,16.13,16.68,2.51.73,5.92,1.68,8.9,2.51,3.58,1,5.51,2.51,5.51,4.89,0,2.76-4,4.68-7.93,4.68-5.69,0-9.73-2.11-10.06-5.9a.57.57,0,0,0-.57-.5l-13.06.06a.57.57,0,0,0-.57.59c.6,11.93,12.12,18.36,22.86,18.36,16,0,23.23-9,23.23-17.43C189.27,265.93,188.36,256.91,174.91,253.47Z" style="fill: white;"></path><path d="M343.31,232.12c-6.65,0-12.22,3.68-15.81,8.12v-7.6a.54.54,0,0,0-.53-.54H314.55a.54.54,0,0,0-.54.54v71a.54.54,0,0,0,.54.53h13.29a.53.53,0,0,0,.53-.53V280.3h.2c2.11,3.22,7.88,7.08,15.42,7.08,14.18,0,26-11.76,26-27.65C370,244.48,358.24,232.12,343.31,232.12Zm-1.23,41.73a14.09,14.09,0,1,1,13.74-14.12A13.9,13.9,0,0,1,342.08,273.85Z" style="fill: white;"></path><path d="M274.68,229c-12.39,0-18.57,4.21-23.53,7.58l-.15.1a1.23,1.23,0,0,0-.37,1.63l4.9,8.44a1.24,1.24,0,0,0,.87.6,1.21,1.21,0,0,0,1-.27l.39-.32c2.55-2.14,6.64-5,16.54-5.78,5.51-.44,10.27,1,13.78,4.28,3.86,3.56,6.17,9.31,6.17,15.38,0,11.17-6.58,18.19-17.15,18.33-8.71-.05-14.56-4.59-14.56-11.3,0-3.56,1.61-5.88,4.75-8.2a1.22,1.22,0,0,0,.37-1.56l-4.4-8.32a1.29,1.29,0,0,0-.77-.62,1.24,1.24,0,0,0-1,.13c-4.94,2.93-11,8.29-10.67,18.59.4,13.11,11.3,23.12,25.47,23.53l.71,0H278c16.84-.55,29-13.05,29-30C307,245.66,295.66,229,274.68,229Z" style="fill: white;"></path><path d="M342.08,245.68a14.09,14.09,0,1,0,13.74,14.05A13.84,13.84,0,0,0,342.08,245.68Z" style="fill: rgb(90, 49, 244);"></path><rect x="383.23" y="214.02" width="141.73" height="90.42" rx="14.17" style="fill: white;"></rect><path d="M439.07,246.62c0,9.67-6.77,16.57-16.23,16.57h-8.92a.75.75,0,0,0-.75.75v12.7a.75.75,0,0,1-.75.75h-6.28a.76.76,0,0,1-.75-.75V230.81a.75.75,0,0,1,.75-.75h16.7C432.3,230.06,439.07,237,439.07,246.62Zm-7.78,0c0-5.54-3.79-9.6-8.93-9.6h-8.44a.76.76,0,0,0-.75.75v17.71a.75.75,0,0,0,.75.74h8.44C427.5,256.22,431.29,252.17,431.29,246.62Z" style="fill: rgb(90, 49, 244);"></path><path d="M440.92,268.6a8.91,8.91,0,0,1,3.72-7.64c2.44-1.83,6.22-2.78,11.83-3l5.95-.2V256c0-3.51-2.36-5-6.15-5s-6.18,1.34-6.74,3.53a.72.72,0,0,1-.72.52h-5.87a.74.74,0,0,1-.75-.85c.88-5.2,5.18-9.15,14.35-9.15,9.74,0,13.25,4.53,13.25,13.18v18.38a.75.75,0,0,1-.75.76h-5.93a.75.75,0,0,1-.75-.76v-1.37a.56.56,0,0,0-1-.39c-1.77,1.93-4.65,3.33-9.24,3.33C445.39,278.2,440.92,274.68,440.92,268.6Zm21.5-4v-1.42l-7.7.4c-4.06.21-6.43,1.9-6.43,4.74,0,2.57,2.17,4,5.95,4C459.38,272.32,462.42,269.54,462.42,264.61Z" style="fill: rgb(90, 49, 244);"></path><path d="M475.75,291.27v-5.35a.76.76,0,0,1,.9-.75,14.84,14.84,0,0,0,2.75.26,7.11,7.11,0,0,0,7.17-5.07l.39-1.23a.74.74,0,0,0,0-.51l-12.34-31.7a.76.76,0,0,1,.71-1h6a.77.77,0,0,1,.71.49l8.38,22.36a.77.77,0,0,0,1.44,0l7.27-22.3a.75.75,0,0,1,.72-.52H506a.76.76,0,0,1,.71,1l-13.2,35.21c-3,8.18-8.25,10.28-14,10.28a11.17,11.17,0,0,1-3.21-.39A.77.77,0,0,1,475.75,291.27Z" style="fill: rgb(90, 49, 244);"></path></symbol></defs></svg></div></div><button type="button" class="shopify-payment-button__button shopify-payment-button__button--unbranded BUz42FHpSPncCPJ4Pr_f jjzYeefyWpPZLH9pIgyw RWJ0IfBjxIhflh4AIrUw" data-testid="Checkout-button">Buy it now</button><button aria-disabled="true" aria-hidden="true" class="shopify-payment-button__more-options BUz42FHpSPncCPJ4Pr_f shopify-payment-button__button--hidden" type="button" data-testid="sheet-open-button">More payment options</button><div><div></div></div><div></div></div></div></div></div>
                          

                          

                          <div class="wrapper-social-popup">
                            <div class="title-close">
                              <p class="social-title">Copy link</p>
                              <svg aria-hidden="true" data-prefix="fal" data-icon="times" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" class="svg-inline--fa fa-times fa-w-10 fa-2x"><path fill="currentColor" d="M193.94 256L296.5 153.44l21.15-21.15c3.12-3.12 3.12-8.19 0-11.31l-22.63-22.63c-3.12-3.12-8.19-3.12-11.31 0L160 222.06 36.29 98.34c-3.12-3.12-8.19-3.12-11.31 0L2.34 120.97c-3.12 3.12-3.12 8.19 0 11.31L126.06 256 2.34 379.71c-3.12 3.12-3.12 8.19 0 11.31l22.63 22.63c3.12 3.12 8.19 3.12 11.31 0L160 289.94 262.56 392.5l21.15 21.15c3.12 3.12 8.19 3.12 11.31 0l22.63-22.63c3.12-3.12 3.12-8.19 0-11.31L193.94 256z" class=""></path></svg>
                            </div>
                            <div class="social-garment-link" role="button" tabindex="-1"><input type="text" value="https://www.tjori.com/products/mulmul-cotton-lilac-v-neck-kurta" class="txt_copy" readonly="readonly"></div>
                            
                              <div class="share_toolbox">  
  
  	
<span>Share</span>

  
  <!----- www.addthis.com/dashboard ----->
  <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-632a8109588d2596"></script>
<div class="addthis_inline_share_toolbox"></div>
</div>
                            
                          </div>
                      </div>
                  
                  
                  </form>

                  
<div id="nofify_sold-out" data-form-notify="" style="display: none;" data-success="Thanks! We will send you a notification when the variant becomes available!" data-error="Please provide a valid email address.">
    
    <h4 class="title">Subcribe to back in stock notification</h4>
    <form method="post" action="/contact#sold-out_form" id="sold-out_form" accept-charset="UTF-8" class="contact-form"><input type="hidden" name="form_type" value="contact"><input type="hidden" name="utf8" value="✓">
        
        <div id="notify-me-wrapper" class="clearfix form-content">
            <!-- <input type="hidden" class="form-input" name="contact[NOTIFY ME OUT OF STOCK]" /> -->
            <input type="hidden" class="form-input" name="contact[Product Title]" value="Mulmul Cotton Lilac V Neck Kurta">
            <input type="hidden" class="form-input" name="contact[Product Link]" value="https://www.tjori.com/products/mulmul-cotton-lilac-v-neck-kurta?variant=43399271874782" data-value-email="">
            <input type="email" name="contact[email]" required="" id="contactFormEmail" placeholder="Insert your email" class="form-input input-field" value="">
            <input type="submit" class="button btn btn--secondary" id="soldOut-button" value="Subscribe">
        </div>
    </form>
</div>


                  
<link href="//cdn.shopify.com/s/files/1/0613/2039/7022/t/8/assets/component-custom-information.css?v=105342810525286010251664368304" rel="stylesheet" type="text/css" media="all">

<div class="custom-information"><div class="item">
					<div class="icon">
						
					</div>
					<div class="wrapper-content">
						<h3 class="title">
							<span>Offers:</span>
							<span class="icon-help">
								<svg xmlns="http://www.w3.org/2000/svg" aria-hidden="true" focusable="false" role="presentation" class="icon" viewBox="0 0 1024 1024"><path fill="#c2c8ce" d="M512 0c281.6 0 512 230.4 512 512s-230.4 512-512 512S0 793.6 0 512 230.4 0 512 0zm0 716.8c-42.667 0-76.8 34.133-76.8 68.267s34.133 68.266 76.8 68.266 76.8-34.133 76.8-68.266S554.667 716.8 512 716.8zm17.067-529.067c-136.534 0-221.867 76.8-221.867 187.734h128c0-51.2 34.133-85.334 93.867-85.334 51.2 0 85.333 34.134 85.333 76.8-8.533 42.667-17.067 59.734-68.267 93.867l-17.066 8.533c-59.734 34.134-85.334 76.8-85.334 153.6v25.6H563.2v-25.6c0-42.666 17.067-68.266 76.8-102.4 68.267-34.133 93.867-85.333 93.867-153.6 17.066-110.933-68.267-179.2-204.8-179.2z"></path></svg>
							</span>
						</h3>
						<div class="desc">
							<p class="offerpara">1.) Shop for ₹3000 &amp; get an Additional 10% Off, Use code: EXTRA10</p>
<p class="offerpara">2.) Shop for ₹4000 &amp; more &amp; get an Additional 15% Off, Use code: EXTRA15</p>
<p class="offerpara">** Not Valid on Clearance Sale, Essentials</p>
<p class="pdp-terms">Terms &amp; Conditions Apply</p>
						</div>
						<div class="desc_popup hide">
							<div class="viralb-text-format">
								
							</div>
						</div>
					</div>
				</div></div>


                  

                  

                  <div id="store_availability" class="product-single__store-availability-container" data-store-availability-container="" data-product-title="Mulmul Cotton Lilac V Neck Kurta" data-has-only-default-variant="false" data-base-url="https://www.tjori.com/">
                  </div>

                  
              </div>
          </div>

          
              

          

          
          

          
          <div class="product_bottom">

              
	            <div class="crafted-rais">
                <div class="contain-rais"><div class="left-crafted"><img alt="" src="https://cdn.shopify.com/s/files/1/0613/2039/7022/files/Genuine_v1.svg"><span>Hand <br>Crafted</span></div><div class="d-content"><p>The art and craft of India is the biggest inspiration for our collections and the we believe in the artisans hard work by showcasing the authentic handicraft products.</p></div></div>
                <div class="contain-rais"><div class="left-crafted"><img alt="" src="https://cdn.shopify.com/s/files/1/0613/2039/7022/files/Handmade_v1.svg"><span>100% Genuine</span></div><div class="d-content"><p>All our products are made with utmost care and pass a quality check through the global standards and hence we guarantee 100% authenticity.</p></div></div>
                <div class="contain-rais"><div class="left-crafted"><img alt="" src="https://cdn.shopify.com/s/files/1/0613/2039/7022/files/HighestQuality_v1.svg"><span>Highest <br>Quality</span></div><div class="d-content"><p>All products are made by the best and heritage old craftsmen who ensure the highest quality of each product that is being produced.</p></div></div>
              </div>
              

              
              
              <div class="tabs__product-page vertical-tabs">
  <div class="wrapper-tab-content">
    

    
      
      
      
      <div class="tab-title">
        <a href="#" data-taptop="" data-target="#collapse-tab1" class="tab-links active">
          
<span>Description</span>

          <span class="icon-dropdown">
            <i class="fa fa-plus-viralb"></i>
          </span>
        </a>
      </div>
      
      <div class="tab-content" data-tabcontent="" id="collapse-tab1">
        
        <div>
          <p>This premium mul mul lilac kurta will be best pick this season for your yoga sessions. This kurta featuring three quarter balloon sleeves felicitates ease of movement and utmost comfort. Team it up with matching pants and you are good to go.<br>

<strong>Wash Care:</strong> Wash separately in cold water. Do Not Bleach and dry in shade.</p>
        </div>
        
        
        
        
        
      </div>
      <div class="showmorebutton-vish">Show More</div>
      <script>
$(".showmorebutton-vish").click(function(){
$("#collapse-tab1").toggleClass("expanded");
$(".showmorebutton-vish").toggleClass("rmv-top-padding");
$(this).text($(this).text() == 'Show More' ? 'Show Less' : 'Show More');
});
      </script>
    
    
      <div class="tab-title">
        <a href="#" data-taptop="" data-target="#collapse-tab3" class="tab-links  active">
          
<span>Shipping</span>

          <span class="icon-dropdown">
            <i class="fa fa-plus-viralb"></i>
          </span>
        </a>
      </div>
      <div class="tab-content" data-tabcontent="" id="collapse-tab3" style="display: block;">
          
              
              <div>
<div class="vish-ship"><strong><span>Ships by : </span></strong><strong><span id="shipdateDate">1st June, Thursday</span></strong></div>
<div class="vish-estimated"><strong><span>Estimated Delivery : </span></strong><strong><span id="fromDate">Thursday June 8th</span></strong> to <strong><span id="toDate">Monday June 12th</span></strong></div>


            
<script src="//cdnjs.cloudflare.com/ajax/libs/datejs/1.0/date.min.js" type="text/javascript"></script>
<script>
    let shippingfromdays = 
    7
  ;
    let shippingtodays = 
    10
  ;
    let deliveryfromdays = shippingfromdays + 7;
    let deliverytodays = shippingtodays + 7;
  
var shipdateDate = Date.today().addDays(shippingfromdays);
if (shipdateDate.is().saturday() || shipdateDate.is().sunday()) {
shipdateDate = shipdateDate.next().monday();
}
var fromDate = Date.today().addDays(deliveryfromdays);
if (fromDate.is().saturday() || fromDate.is().sunday()) {
fromDate = fromDate.next().monday();
}
var toDate = Date.today().addDays(deliverytodays);
if (toDate.is().saturday() || toDate.is().sunday()) {
toDate = toDate.next().monday();
}
document.getElementById('shipdateDate').innerHTML = shipdateDate.toString('dS MMMM, dddd');
document.getElementById('fromDate').innerHTML = fromDate.toString('dddd MMMM dS');
document.getElementById('toDate').innerHTML = toDate.toString('dddd MMMM dS');
</script>
                
<br>                
<p>Please note, we aggrerated all orders, your delivery timeline may vary from above based on items in your order. <br>All Tjori orders are manufactured by craftsmen/manufacturers upon receiving the order. This allows us to offer a wider assortment at lower prices to you.</p>
              </div>
          
      </div>
    


    
      <div class="tab-title">
        <a href="#" data-taptop="" data-target="#collapse-tab4" class="tab-links ">
          
<span>Returns</span>

          <span class="icon-dropdown">
            <i class="fa fa-plus-viralb"></i>
          </span>
        </a>
      </div>
      <div class="tab-content" data-tabcontent="" id="collapse-tab4">
          
              
              <div>
                  <p>All products are made on order and can be returned or exchanged with any other products on the website within 14 days of delivery. However, since these products are made on order, they are not eligible for a refund.</p>
<p>Our return policy may vary based on products. Please read our policy by <a href="/returns">clicking here</a> for more details.</p>
<p>We are always there to help and you can write to us at support@tjori.com</p>
              </div>
          
      </div>
    

    
      <div class="tab-title">
        <a href="#" data-taptop="" data-target="#collapse-tab2" class="tab-links ">
        	
<span>Reviews</span>

          <span class="icon-dropdown">
            <i class="fa fa-plus-viralb"></i>
          </span>
        </a>
      </div>
      <div class="tab-content" data-tabcontent="" id="collapse-tab2">
        <div id="shopify-product-reviews" data-id="7797834973406"><style scoped="">.spr-container {
    padding: 24px;
    border-color: #ECECEC;}
  .spr-review, .spr-form {
    border-color: #ECECEC;
  }
</style>

<div class="spr-container">
  <div class="spr-header">
    <h2 class="spr-header-title">Customer Reviews</h2><div class="spr-summary rte">
        <span class="spr-summary-caption">No reviews yet</span><span class="spr-summary-actions">
        <a href="#" class="spr-summary-actions-newreview" onclick="SPR.toggleForm(7797834973406);return false">Write a review</a>
      </span>
    </div>
  </div>

  <div class="spr-content">
    <div class="spr-form" id="form_7797834973406" style="display: none"><form method="post" action="//productreviews.shopifycdn.com/api/reviews/create" id="new-review-form_7797834973406" class="new-review-form" onsubmit="SPR.submitForm(this);return false;"><input type="hidden" name="review[rating]"><input type="hidden" name="product_id" value="7797834973406"><h3 class="spr-form-title">Write a review</h3><fieldset class="spr-form-contact"><div class="spr-form-contact-name">
          <label class="spr-form-label" for="review_author_7797834973406">Name</label>
          <input class="spr-form-input spr-form-input-text " id="review_author_7797834973406" type="text" name="review[author]" value="" placeholder="Enter your name">
        </div><div class="spr-form-contact-email">
          <label class="spr-form-label" for="review_email_7797834973406">Email</label>
          <input class="spr-form-input spr-form-input-email " id="review_email_7797834973406" type="email" name="review[email]" value="" placeholder="john.smith@example.com">
        </div></fieldset>


    <fieldset class="spr-form-review">

      <div class="spr-form-review-rating">
        <label class="spr-form-label" for="review[rating]">Rating</label>
        <div class="spr-form-input spr-starrating ">
          <a href="#" onclick="SPR.setRating(this);return false;" class="spr-icon spr-icon-star spr-icon-star-empty" data-value="1" aria-label="1 of 5 stars">&nbsp;</a>
          <a href="#" onclick="SPR.setRating(this);return false;" class="spr-icon spr-icon-star spr-icon-star-empty" data-value="2" aria-label="2 of 5 stars">&nbsp;</a>
          <a href="#" onclick="SPR.setRating(this);return false;" class="spr-icon spr-icon-star spr-icon-star-empty" data-value="3" aria-label="3 of 5 stars">&nbsp;</a>
          <a href="#" onclick="SPR.setRating(this);return false;" class="spr-icon spr-icon-star spr-icon-star-empty" data-value="4" aria-label="4 of 5 stars">&nbsp;</a>
          <a href="#" onclick="SPR.setRating(this);return false;" class="spr-icon spr-icon-star spr-icon-star-empty" data-value="5" aria-label="5 of 5 stars">&nbsp;</a>
        </div>
      </div>

      <div class="spr-form-review-title">
        <label class="spr-form-label" for="review_title_7797834973406">Review Title</label>
        <input class="spr-form-input spr-form-input-text " id="review_title_7797834973406" type="text" name="review[title]" value="" placeholder="Give your review a title">
      </div>

      <div class="spr-form-review-body">
        <label class="spr-form-label" for="review_body_7797834973406">
          Body of Review
          <span role="status" aria-live="polite" aria-atomic="true">
            <span class="spr-form-review-body-charactersremaining">(1500)</span>
            <span class="visuallyhidden">characters remaining</span>
          </span>
        </label>
        <div class="spr-form-input">
          <textarea class="spr-form-input spr-form-input-textarea " id="review_body_7797834973406" data-product-id="7797834973406" name="review[body]" rows="10" placeholder="Write your comments here"></textarea>
          <script>
            function sprUpdateCount(e){
              var $el = SPR.$(e.currentTarget);
              SPR.$(".spr-form-review-body-charactersremaining").html('(' + (1500 - $el.val().length) + ')');
            }
            SPR.$("textarea[data-product-id=7797834973406]").keyup(sprUpdateCount).trigger("keyup");
          </script>
        </div>
      </div>
    </fieldset>

    <fieldset class="spr-form-actions">
      <input type="submit" class="spr-button spr-button-primary button button-primary btn btn-primary" value="Submit Review">
    </fieldset></form></div>
    <div class="spr-reviews" id="reviews_7797834973406" style="display: none"></div>
  </div>

</div></div>
      </div>
    

    

    

    

  </div>
</div>
              

              
              



<div id="shopify-section-product-recommendations">
  <div class="related-products">
    
    <div class="widget-title not-before">   
      <h3 class="box-title">
        <span class="title"> 
          Similar Products
        </span>
      </h3>    
    </div>
    
    
  <div id="product-recommendations"><div class="page-width product-recommendations js-product-recomendation" data-template="" data-product-id="7797834973406" data-limit="8" data-section-id="product-recommendations" data-section-type="product-recommendations" data-related-products-slider="">
    
    <div class="widget-product">
      <div class="products-grid grid grid--uniform grid--view-items row four-items slick-initialized slick-slider slick-dotted" role="toolbar">
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
      <div aria-live="polite" class="slick-list draggable"><div class="slick-track" role="listbox" style="opacity: 1; width: 1384px; transform: translate3d(0px, 0px, 0px);"><div class="grid-item small--one-half medium-up--one-quarter col-12 slick-slide slick-current slick-active" style="width: 173px;" tabindex="-1" role="option" aria-describedby="slick-slide20" data-slick-index="0" aria-hidden="false">
          
            
<div class="inner product-item on-sale" data-product-id="product-7798112616670" data-json-product="{&quot;id&quot;: 7798112616670,&quot;handle&quot;: &quot;mulmul-cotton-solid-lilac-harem-pants&quot;,&quot;media&quot;: [{&quot;alt&quot;:null,&quot;id&quot;:30634491314398,&quot;position&quot;:1,&quot;preview_image&quot;:{&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:2360,&quot;width&quot;:1600,&quot;src&quot;:&quot;https:\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/TJ-MK-277-03_1.jpg?v=1663266901&quot;},&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:2360,&quot;media_type&quot;:&quot;image&quot;,&quot;src&quot;:&quot;https:\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/TJ-MK-277-03_1.jpg?v=1663266901&quot;,&quot;width&quot;:1600},{&quot;alt&quot;:null,&quot;id&quot;:30634491347166,&quot;position&quot;:2,&quot;preview_image&quot;:{&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:2360,&quot;width&quot;:1600,&quot;src&quot;:&quot;https:\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/TJ-MK-277-03_2.jpg?v=1663266901&quot;},&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:2360,&quot;media_type&quot;:&quot;image&quot;,&quot;src&quot;:&quot;https:\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/TJ-MK-277-03_2.jpg?v=1663266901&quot;,&quot;width&quot;:1600},{&quot;alt&quot;:null,&quot;id&quot;:30634491379934,&quot;position&quot;:3,&quot;preview_image&quot;:{&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:2360,&quot;width&quot;:1600,&quot;src&quot;:&quot;https:\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/TJ-MK-277-03_3.jpg?v=1663266901&quot;},&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:2360,&quot;media_type&quot;:&quot;image&quot;,&quot;src&quot;:&quot;https:\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/TJ-MK-277-03_3.jpg?v=1663266901&quot;,&quot;width&quot;:1600},{&quot;alt&quot;:null,&quot;id&quot;:30634491412702,&quot;position&quot;:4,&quot;preview_image&quot;:{&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:2360,&quot;width&quot;:1600,&quot;src&quot;:&quot;https:\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/TJ-MK-277-03_4.jpg?v=1663266901&quot;},&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:2360,&quot;media_type&quot;:&quot;image&quot;,&quot;src&quot;:&quot;https:\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/TJ-MK-277-03_4.jpg?v=1663266901&quot;,&quot;width&quot;:1600}],&quot;variants&quot;: [{&quot;id&quot;:43400460370142,&quot;title&quot;:&quot;XS&quot;,&quot;option1&quot;:&quot;XS&quot;,&quot;option2&quot;:null,&quot;option3&quot;:null,&quot;sku&quot;:&quot;TJ-MK-277-03-XS&quot;,&quot;requires_shipping&quot;:true,&quot;taxable&quot;:true,&quot;featured_image&quot;:null,&quot;available&quot;:true,&quot;name&quot;:&quot;Mulmul Cotton Solid Lilac Harem Pants - XS&quot;,&quot;public_title&quot;:&quot;XS&quot;,&quot;options&quot;:[&quot;XS&quot;],&quot;price&quot;:139000,&quot;weight&quot;:0,&quot;compare_at_price&quot;:198600,&quot;inventory_management&quot;:&quot;shopify&quot;,&quot;barcode&quot;:null,&quot;quantity_rule&quot;:{&quot;min&quot;:1,&quot;max&quot;:null,&quot;increment&quot;:1}},{&quot;id&quot;:43400460402910,&quot;title&quot;:&quot;S&quot;,&quot;option1&quot;:&quot;S&quot;,&quot;option2&quot;:null,&quot;option3&quot;:null,&quot;sku&quot;:&quot;TJ-MK-277-03-S&quot;,&quot;requires_shipping&quot;:true,&quot;taxable&quot;:true,&quot;featured_image&quot;:null,&quot;available&quot;:true,&quot;name&quot;:&quot;Mulmul Cotton Solid Lilac Harem Pants - S&quot;,&quot;public_title&quot;:&quot;S&quot;,&quot;options&quot;:[&quot;S&quot;],&quot;price&quot;:139000,&quot;weight&quot;:0,&quot;compare_at_price&quot;:198600,&quot;inventory_management&quot;:&quot;shopify&quot;,&quot;barcode&quot;:null,&quot;quantity_rule&quot;:{&quot;min&quot;:1,&quot;max&quot;:null,&quot;increment&quot;:1}},{&quot;id&quot;:43400460435678,&quot;title&quot;:&quot;M&quot;,&quot;option1&quot;:&quot;M&quot;,&quot;option2&quot;:null,&quot;option3&quot;:null,&quot;sku&quot;:&quot;TJ-MK-277-03-M&quot;,&quot;requires_shipping&quot;:true,&quot;taxable&quot;:true,&quot;featured_image&quot;:null,&quot;available&quot;:true,&quot;name&quot;:&quot;Mulmul Cotton Solid Lilac Harem Pants - M&quot;,&quot;public_title&quot;:&quot;M&quot;,&quot;options&quot;:[&quot;M&quot;],&quot;price&quot;:139000,&quot;weight&quot;:0,&quot;compare_at_price&quot;:198600,&quot;inventory_management&quot;:&quot;shopify&quot;,&quot;barcode&quot;:null,&quot;quantity_rule&quot;:{&quot;min&quot;:1,&quot;max&quot;:null,&quot;increment&quot;:1}},{&quot;id&quot;:43400460468446,&quot;title&quot;:&quot;L&quot;,&quot;option1&quot;:&quot;L&quot;,&quot;option2&quot;:null,&quot;option3&quot;:null,&quot;sku&quot;:&quot;TJ-MK-277-03-L&quot;,&quot;requires_shipping&quot;:true,&quot;taxable&quot;:true,&quot;featured_image&quot;:null,&quot;available&quot;:true,&quot;name&quot;:&quot;Mulmul Cotton Solid Lilac Harem Pants - L&quot;,&quot;public_title&quot;:&quot;L&quot;,&quot;options&quot;:[&quot;L&quot;],&quot;price&quot;:139000,&quot;weight&quot;:0,&quot;compare_at_price&quot;:198600,&quot;inventory_management&quot;:&quot;shopify&quot;,&quot;barcode&quot;:null,&quot;quantity_rule&quot;:{&quot;min&quot;:1,&quot;max&quot;:null,&quot;increment&quot;:1}},{&quot;id&quot;:43400460501214,&quot;title&quot;:&quot;XL&quot;,&quot;option1&quot;:&quot;XL&quot;,&quot;option2&quot;:null,&quot;option3&quot;:null,&quot;sku&quot;:&quot;TJ-MK-277-03-XL&quot;,&quot;requires_shipping&quot;:true,&quot;taxable&quot;:true,&quot;featured_image&quot;:null,&quot;available&quot;:true,&quot;name&quot;:&quot;Mulmul Cotton Solid Lilac Harem Pants - XL&quot;,&quot;public_title&quot;:&quot;XL&quot;,&quot;options&quot;:[&quot;XL&quot;],&quot;price&quot;:139000,&quot;weight&quot;:0,&quot;compare_at_price&quot;:198600,&quot;inventory_management&quot;:&quot;shopify&quot;,&quot;barcode&quot;:null,&quot;quantity_rule&quot;:{&quot;min&quot;:1,&quot;max&quot;:null,&quot;increment&quot;:1}},{&quot;id&quot;:43400460533982,&quot;title&quot;:&quot;XXL&quot;,&quot;option1&quot;:&quot;XXL&quot;,&quot;option2&quot;:null,&quot;option3&quot;:null,&quot;sku&quot;:&quot;TJ-MK-277-03-XXL&quot;,&quot;requires_shipping&quot;:true,&quot;taxable&quot;:true,&quot;featured_image&quot;:null,&quot;available&quot;:true,&quot;name&quot;:&quot;Mulmul Cotton Solid Lilac Harem Pants - XXL&quot;,&quot;public_title&quot;:&quot;XXL&quot;,&quot;options&quot;:[&quot;XXL&quot;],&quot;price&quot;:139000,&quot;weight&quot;:0,&quot;compare_at_price&quot;:198600,&quot;inventory_management&quot;:&quot;shopify&quot;,&quot;barcode&quot;:null,&quot;quantity_rule&quot;:{&quot;min&quot;:1,&quot;max&quot;:null,&quot;increment&quot;:1}},{&quot;id&quot;:43400460566750,&quot;title&quot;:&quot;XXXL&quot;,&quot;option1&quot;:&quot;XXXL&quot;,&quot;option2&quot;:null,&quot;option3&quot;:null,&quot;sku&quot;:&quot;TJ-MK-277-03-XXXL&quot;,&quot;requires_shipping&quot;:true,&quot;taxable&quot;:true,&quot;featured_image&quot;:null,&quot;available&quot;:true,&quot;name&quot;:&quot;Mulmul Cotton Solid Lilac Harem Pants - XXXL&quot;,&quot;public_title&quot;:&quot;XXXL&quot;,&quot;options&quot;:[&quot;XXXL&quot;],&quot;price&quot;:139000,&quot;weight&quot;:0,&quot;compare_at_price&quot;:198600,&quot;inventory_management&quot;:&quot;shopify&quot;,&quot;barcode&quot;:null,&quot;quantity_rule&quot;:{&quot;min&quot;:1,&quot;max&quot;:null,&quot;increment&quot;:1}}]}">
  <div class="inner-top">
    <div class="product-top">
      <div class="product-image image-swap">
        <a href="/products/mulmul-cotton-solid-lilac-harem-pants?pr_prod_strat=copurchase&amp;pr_rec_id=e3a9801a1&amp;pr_rec_pid=7798112616670&amp;pr_ref_pid=7797834973406&amp;pr_seq=uniform" class="product-grid-image adaptive_height" data-collections-related="/collections/?view=related" style="padding-top: 147.5%" tabindex="0">
          



  <picture data-index="0">
    <source class="image-source-1" data-srcset="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-03_1_300x.jpg?v=1663266901" media="(max-width: 767px)" sizes="153px" srcset="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-03_1_300x.jpg?v=1663266901">
    <source class="image-source-2" data-srcset=" //cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-03_1_360x.jpg?v=1663266901 360w,
                          //cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-03_1_540x.jpg?v=1663266901 540w,
                          //cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-03_1_720x.jpg?v=1663266901 720w,
                          //cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-03_1_900x.jpg?v=1663266901 900w" media="(min-width: 768px)" sizes="153px" srcset=" //cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-03_1_360x.jpg?v=1663266901 360w,
                          //cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-03_1_540x.jpg?v=1663266901 540w,
                          //cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-03_1_720x.jpg?v=1663266901 720w,
                          //cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-03_1_900x.jpg?v=1663266901 900w">

    <img alt="Mulmul Cotton Solid Lilac Harem Pants" class="images-one lazyautosizes lazyloaded" data-widths="[180, 360, 540, 720, 900, 1080, 1296, 1512, 1728, 2048]" data-aspectratio="0.6779661016949152" data-sizes="auto" data-srcset="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-03_1_360x.jpg?v=1663266901 360w,
                    //cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-03_1_540x.jpg?v=1663266901 540w,
                    //cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-03_1_720x.jpg?v=1663266901 720w,
                    //cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-03_1_900x.jpg?v=1663266901 900w" data-image="" sizes="153px" srcset="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-03_1_360x.jpg?v=1663266901 360w,
                    //cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-03_1_540x.jpg?v=1663266901 540w,
                    //cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-03_1_720x.jpg?v=1663266901 720w,
                    //cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-03_1_900x.jpg?v=1663266901 900w">
  </picture>
  <span class="images-two">
    
    <picture data-index="1">
      <source data-srcset="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-03_2_300x.jpg?v=1663266901" media="(max-width: 767px)" sizes="153px" srcset="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-03_2_300x.jpg?v=1663266901">
      <source data-srcset="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-03_2_360x.jpg?v=1663266901 360w,
                            //cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-03_2_540x.jpg?v=1663266901 540w,
                            //cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-03_2_720x.jpg?v=1663266901 720w,
                            //cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-03_2_900x.jpg?v=1663266901 900w" media="(min-width: 768px)" sizes="153px" srcset="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-03_2_360x.jpg?v=1663266901 360w,
                            //cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-03_2_540x.jpg?v=1663266901 540w,
                            //cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-03_2_720x.jpg?v=1663266901 720w,
                            //cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-03_2_900x.jpg?v=1663266901 900w">

      <img alt="Mulmul Cotton Solid Lilac Harem Pants" class="lazyautosizes lazyloaded" data-widths="[180, 360, 540, 720, 900, 1080, 1296, 1512, 1728, 2048]" data-aspectratio="0.6779661016949152" data-sizes="auto" data-srcset="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-03_2_360x.jpg?v=1663266901 360w,
                        //cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-03_2_540x.jpg?v=1663266901 540w,
                        //cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-03_2_720x.jpg?v=1663266901 720w,
                        //cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-03_2_900x.jpg?v=1663266901 900w" data-image="" sizes="153px" srcset="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-03_2_360x.jpg?v=1663266901 360w,
                        //cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-03_2_540x.jpg?v=1663266901 540w,
                        //cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-03_2_720x.jpg?v=1663266901 720w,
                        //cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-277-03_2_900x.jpg?v=1663266901 900w">
    </picture>
    
  </span>

        </a>
      </div>

      
  
  <div class="product-label" data-label-new-number="30">
    

    
      
        <strong class="label sale-label">
          Sale
        </strong>
        
      
    

    
    
    

    
    
    
  </div>
  






      
<a class="wishlist " data-icon-wishlist="" aria-label="Wish Lists" href="#" data-product-handle="mulmul-cotton-solid-lilac-harem-pants" data-id="7798112616670" tabindex="0">
	    	<svg viewBox="0 0 512 512">
<g>
<g>
  <path d="M474.644,74.27C449.391,45.616,414.358,29.836,376,29.836c-53.948,0-88.103,32.22-107.255,59.25
    c-4.969,7.014-9.196,14.047-12.745,20.665c-3.549-6.618-7.775-13.651-12.745-20.665c-19.152-27.03-53.307-59.25-107.255-59.25
    c-38.358,0-73.391,15.781-98.645,44.435C13.267,101.605,0,138.213,0,177.351c0,42.603,16.633,82.228,52.345,124.7
    c31.917,37.96,77.834,77.088,131.005,122.397c19.813,16.884,40.302,34.344,62.115,53.429l0.655,0.574
    c2.828,2.476,6.354,3.713,9.88,3.713s7.052-1.238,9.88-3.713l0.655-0.574c21.813-19.085,42.302-36.544,62.118-53.431
    c53.168-45.306,99.085-84.434,131.002-122.395C495.367,259.578,512,219.954,512,177.351
    C512,138.213,498.733,101.605,474.644,74.27z M309.193,401.614c-17.08,14.554-34.658,29.533-53.193,45.646
    c-18.534-16.111-36.113-31.091-53.196-45.648C98.745,312.939,30,254.358,30,177.351c0-31.83,10.605-61.394,29.862-83.245
    C79.34,72.007,106.379,59.836,136,59.836c41.129,0,67.716,25.338,82.776,46.594c13.509,19.064,20.558,38.282,22.962,45.659
    c2.011,6.175,7.768,10.354,14.262,10.354c6.494,0,12.251-4.179,14.262-10.354c2.404-7.377,9.453-26.595,22.962-45.66
    c15.06-21.255,41.647-46.593,82.776-46.593c29.621,0,56.66,12.171,76.137,34.27C471.395,115.957,482,145.521,482,177.351
    C482,254.358,413.255,312.939,309.193,401.614z"></path>
</g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
</svg>
		
</a>
 


      
        <a class="quickview-button" href="javascript:void(0)" id="mulmul-cotton-solid-lilac-harem-pants" title="Quick View" tabindex="0">
          <span>Quick view</span>
          <svg viewBox="0 0 511.999 511.999" width="100%" height="100%">
    <path d="M508.745,246.041c-4.574-6.257-113.557-153.206-252.748-153.206S7.818,239.784,3.249,246.035         c-4.332,5.936-4.332,13.987,0,19.923c4.569,6.257,113.557,153.206,252.748,153.206s248.174-146.95,252.748-153.201         C513.083,260.028,513.083,251.971,508.745,246.041z M255.997,385.406c-102.529,0-191.33-97.533-217.617-129.418         c26.253-31.913,114.868-129.395,217.617-129.395c102.524,0,191.319,97.516,217.617,129.418         C447.361,287.923,358.746,385.406,255.997,385.406z"></path>
      <path d="M255.997,154.725c-55.842,0-101.275,45.433-101.275,101.275s45.433,101.275,101.275,101.275         s101.275-45.433,101.275-101.275S311.839,154.725,255.997,154.725z M255.997,323.516c-37.23,0-67.516-30.287-67.516-67.516         s30.287-67.516,67.516-67.516s67.516,30.287,67.516,67.516S293.227,323.516,255.997,323.516z"></path>
</svg>
        </a>
      

      

    
    <div class="product-bottom">
      
      
        <span class="spr-badge" id="spr_badge_7798112616670" data-rating="0.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star-empty" aria-hidden="true"></i><i class="spr-icon spr-icon-star-empty" aria-hidden="true"></i><i class="spr-icon spr-icon-star-empty" aria-hidden="true"></i><i class="spr-icon spr-icon-star-empty" aria-hidden="true"></i><i class="spr-icon spr-icon-star-empty" aria-hidden="true"></i></span><span class="spr-badge-caption">No reviews</span>
</span>

      
      <a class="product-title " href="/products/mulmul-cotton-solid-lilac-harem-pants?pr_prod_strat=copurchase&amp;pr_rec_id=e3a9801a1&amp;pr_rec_pid=7798112616670&amp;pr_ref_pid=7797834973406&amp;pr_seq=uniform" tabindex="0">
        
<span>Mulmul Cotton Solid Lilac Harem Pants</span>

        
          
          
      </a>
      <div class="half-grid-rais">
      <div class="price-box">
        
        <div class="price-sale">
          
            <span class="special-price" data-price-grid=""><span class="money">₹1390.00</span></span>
            <span class="old-price" data-compare-price-grid="">1986.00</span>
            <span class="percentage-price">(30%)</span>
         
        </div>
        
      </div>
    </div>

      <div class="add-to-cart-grid-rais">
        
        <div class="action">
          


<form action="/cart/add" method="post" class="variants" id="grid-product-form--7798112616670-product-recommendations" data-id="product-actions-7798112616670" enctype="multipart/form-data">
  
  

    
      
        <a class="rais-svgcart" data-href="/products/mulmul-cotton-solid-lilac-harem-pants?pr_prod_strat=copurchase&amp;pr_rec_id=e3a9801a1&amp;pr_rec_pid=7798112616670&amp;pr_ref_pid=7797834973406&amp;pr_seq=uniform" title="Mulmul Cotton Solid Lilac Harem Pants" data-init-quickshop="" tabindex="0">
        <svg width="27px" height="27px" viewBox="0 0 27 27" version="1.1" xmlns="http://www.w3.org/2000/svg"><g id="Tjori-" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g id="PLP" transform="translate(-361.000000, -962.000000)"><g id="Product-Widget-Copy" transform="translate(18.000000, 646.000000)"><g id="Product-Widget"><g id="Shopping-Bag-Icon" transform="translate(343.000000, 316.000000)"><path d="M21.6999607,6.00003576 L26.1999499,6.00003576 L26.1999499,20.2500018 C26.1999499,21.2812493 25.8327635,22.1640595 25.0983901,22.898433 C24.3640166,23.6328065 23.4812064,23.9999928 22.4499589,23.9999928 L8.94999106,23.9999928 C7.91874352,23.9999928 7.03593336,23.6328065 6.30155987,22.898433 C5.56718639,22.1640595 5.2,21.2812493 5.2,20.2500018 L5.2,6.00003576 L9.69998927,6.00003576 C9.69998927,4.90628861 9.96561388,3.89847851 10.4968624,2.97660547 C11.0281109,2.05473243 11.7546716,1.32817166 12.6765447,0.796923168 C13.5984177,0.265674673 14.6062278,5.00679016e-05 15.699975,5.00679016e-05 C16.7937221,5.00679016e-05 17.8015322,0.265674673 18.7234053,0.796923168 C19.6452783,1.32817166 20.3718391,2.05473243 20.9030876,2.97660547 C21.4343361,3.89847851 21.6999607,4.90628861 21.6999607,6.00003576 Z M15.699975,1.50004649 C14.4499777,1.50004649 13.3874807,1.93754521 12.5124826,2.81254336 C11.6374844,3.68754151 11.1999857,4.7500385 11.1999857,6.00003576 L20.1999642,6.00003576 C20.1999642,4.7500385 19.7624655,3.68754151 18.8874674,2.81254336 C18.0124692,1.93754521 16.9499722,1.50004649 15.699975,1.50004649 Z M24.6999535,20.2500018 L24.6999535,7.50003219 L21.6999607,7.50003219 L21.6999607,9.75002682 C21.6999607,9.96877654 21.6296483,10.1484634 21.4890237,10.289088 C21.348399,10.4297127 21.1687122,10.500025 20.9499624,10.500025 C20.7312127,10.500025 20.5515259,10.4297127 20.4109012,10.289088 C20.2702766,10.1484634 20.1999642,9.96877654 20.1999642,9.75002682 L20.1999642,7.50003219 L11.1999857,7.50003219 L11.1999857,9.75002682 C11.1999857,9.96877654 11.1296734,10.1484634 10.9890487,10.289088 C10.848424,10.4297127 10.6687372,10.500025 10.4499875,10.500025 C10.2312378,10.500025 10.0515509,10.4297127 9.91092627,10.289088 C9.7703016,10.1484634 9.69998927,9.96877654 9.69998927,9.75002682 L9.69998927,7.50003219 L6.69999642,7.50003219 L6.69999642,20.2500018 C6.69999642,20.8750001 6.91874614,21.4062493 7.35624486,21.843748 C7.79374358,22.2812467 8.32499279,22.4999964 8.94999106,22.4999964 L22.4499589,22.4999964 C23.0749571,22.4999964 23.6062064,22.2812467 24.0437051,21.843748 C24.4812038,21.4062493 24.6999535,20.8750001 24.6999535,20.2500018 Z" id="shopping-bag" fill="#34302B"></path><circle id="Oval" fill="#FFFFFF" cx="6.5" cy="20.5" r="5.5"></circle><path d="M6.34998569,14.6875246 C7.39685832,14.6875246 8.36560589,14.9492428 9.25622877,15.472679 C10.1468516,15.9961151 10.8538813,16.7031448 11.3773175,17.5937677 C11.9007536,18.4843905 12.1624718,19.4531381 12.1624718,20.5000107 C12.1624718,21.5468834 11.9007536,22.5156309 11.3773175,23.4062538 C10.8538813,24.2968767 10.1468516,25.0039064 9.25622877,25.5273425 C8.36560589,26.0507786 7.39685832,26.3124969 6.34998569,26.3124969 C5.30311307,26.3124969 4.3343655,26.0507786 3.44374262,25.5273425 C2.55311975,25.0039064 1.84609006,24.2968767 1.32265393,23.4062538 C0.799217798,22.5156309 0.537499553,21.5468834 0.537499553,20.5000107 C0.537499553,19.4531381 0.799217798,18.4843905 1.32265393,17.5937677 C1.84609006,16.7031448 2.55311975,15.9961151 3.44374262,15.472679 C4.3343655,14.9492428 5.30311307,14.6875246 6.34998569,14.6875246 Z M9.72497765,21.1562592 L9.72497765,19.8437623 C9.72497765,19.7656376 9.69763384,19.6992313 9.64294659,19.644544 C9.58825934,19.5898568 9.52185301,19.562513 9.44372832,19.562513 L7.28748346,19.562513 L7.28748346,17.4062681 C7.28748346,17.3281434 7.26013966,17.2617371 7.20545241,17.2070498 C7.15076515,17.1523626 7.08435882,17.1250188 7.00623413,17.1250188 L5.69373726,17.1250188 C5.61561256,17.1250188 5.54920623,17.1523626 5.49451898,17.2070498 C5.43983173,17.2617371 5.41248793,17.3281434 5.41248793,17.4062681 L5.41248793,19.562513 L3.25624307,19.562513 C3.17811838,19.562513 3.11171205,19.5898568 3.0570248,19.644544 C3.00233755,19.6992313 2.97499374,19.7656376 2.97499374,19.8437623 L2.97499374,21.1562592 C2.97499374,21.2343839 3.00233755,21.3007902 3.0570248,21.3554774 C3.11171205,21.4101647 3.17811838,21.4375085 3.25624307,21.4375085 L5.41248793,21.4375085 L5.41248793,23.5937534 C5.41248793,23.671878 5.43983173,23.7382844 5.49451898,23.7929716 C5.54920623,23.8476589 5.61561256,23.8750027 5.69373726,23.8750027 L7.00623413,23.8750027 C7.08435882,23.8750027 7.15076515,23.8476589 7.20545241,23.7929716 C7.26013966,23.7382844 7.28748346,23.671878 7.28748346,23.5937534 L7.28748346,21.4375085 L9.44372832,21.4375085 C9.52185301,21.4375085 9.58825934,21.4101647 9.64294659,21.3554774 C9.69763384,21.3007902 9.72497765,21.2343839 9.72497765,21.1562592 Z" id="plus-circle" fill="#C0B394"></path></g></g></g></g></g></svg>
        </a>
      
    
  
  
</form>

  
      
          <div class="product-card__variant--popup">
              <div class="product-card__variant--popup--content">
    
    <form action="/cart/add" method="post" class="variants" id="swatch-grid-product-form--7798112616670-product-recommendations-" data-id="product-actions-7798112616670" enctype="multipart/form-data">
        
            
            
            
                
                    <div class="selector-wrapper selector-wrapper-1 swatch js product-form__item" data-option-index="0">
                        <label class="form-label ">
                            Size: <span class="label-value-1">XS</span>
                        </label>
                        
                        
                        

                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size xs available" data-value="XS">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7798112616670-product-recommendations" data-index="option1" value="XS" id="SingleOptionSelector0-XS-7798112616670-product-recommendations" tabindex="0">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-XS-7798112616670-product-recommendations" data-original-title="XS">XS</label>
                                </div>
                            
                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size s available" data-value="S">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7798112616670-product-recommendations" data-index="option1" value="S" id="SingleOptionSelector0-S-7798112616670-product-recommendations" tabindex="0">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-S-7798112616670-product-recommendations" data-original-title="S">S</label>
                                </div>
                            
                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size m available" data-value="M">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7798112616670-product-recommendations" data-index="option1" value="M" id="SingleOptionSelector0-M-7798112616670-product-recommendations" tabindex="0">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-M-7798112616670-product-recommendations" data-original-title="M">M</label>
                                </div>
                            
                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size l available" data-value="L">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7798112616670-product-recommendations" data-index="option1" value="L" id="SingleOptionSelector0-L-7798112616670-product-recommendations" tabindex="0">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-L-7798112616670-product-recommendations" data-original-title="L">L</label>
                                </div>
                            
                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size xl available" data-value="XL">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7798112616670-product-recommendations" data-index="option1" value="XL" id="SingleOptionSelector0-XL-7798112616670-product-recommendations" tabindex="0">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-XL-7798112616670-product-recommendations" data-original-title="XL">XL</label>
                                </div>
                            
                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size xxl available" data-value="XXL">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7798112616670-product-recommendations" data-index="option1" value="XXL" id="SingleOptionSelector0-XXL-7798112616670-product-recommendations" tabindex="0">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-XXL-7798112616670-product-recommendations" data-original-title="XXL">XXL</label>
                                </div>
                            
                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size xxxl available" data-value="XXXL">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7798112616670-product-recommendations" data-index="option1" value="XXXL" id="SingleOptionSelector0-XXXL-7798112616670-product-recommendations" tabindex="0">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-XXXL-7798112616670-product-recommendations" data-original-title="XXXL">XXXL</label>
                                </div>
                            
                        
                    </div>
                
            
        

        <select name="id" id="ProductSelect-7798112616670-product-recommendations" class="product-form__variants no-js" tabindex="0">
            
                
                    <option value="43400460370142">
                        XS
                    </option>
                
            
                
                    <option value="43400460402910">
                        S
                    </option>
                
            
                
                    <option value="43400460435678">
                        M
                    </option>
                
            
                
                    <option value="43400460468446">
                        L
                    </option>
                
            
                
                    <option value="43400460501214">
                        XL
                    </option>
                
            
                
                    <option value="43400460533982">
                        XXL
                    </option>
                
            
                
                    <option value="43400460566750">
                        XXXL
                    </option>
                
            
        </select>
        <div class="product-card__button_cancel_mobile">
            <a class="btn btn-cancel" data-cancel-swatch-qs="" tabindex="0"><svg aria-hidden="true" data-prefix="fal" data-icon="times" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" class="svg-inline--fa fa-times fa-w-10 fa-2x"><path fill="currentColor" d="M193.94 256L296.5 153.44l21.15-21.15c3.12-3.12 3.12-8.19 0-11.31l-22.63-22.63c-3.12-3.12-8.19-3.12-11.31 0L160 222.06 36.29 98.34c-3.12-3.12-8.19-3.12-11.31 0L2.34 120.97c-3.12 3.12-3.12 8.19 0 11.31L126.06 256 2.34 379.71c-3.12 3.12-3.12 8.19 0 11.31l22.63 22.63c3.12 3.12 8.19 3.12 11.31 0L160 289.94 262.56 392.5l21.15 21.15c3.12 3.12 8.19 3.12 11.31 0l22.63-22.63c3.12-3.12 3.12-8.19 0-11.31L193.94 256z" class=""></path></svg></a>
        </div>
        <div class="product-card__button2">
            <input type="hidden" name="quantity" value="1" tabindex="0">
            <button data-btn-addtocart="" class="btn add-to-cart-btn" type="submit" data-form-id="#swatch-grid-product-form--7798112616670-product-recommendations-" tabindex="0">
                Submit
            </button>
            <a class="btn btn-cancel" data-cancel-swatch-qs="" tabindex="0">Cancel</a>
        </div>
        
    </form>
</div>
          </div>
      
  

        </div>
        
      </div>
    </div>

      <div class="wrapper-size">
        
      </div>
      
    </div>

    
     
    <div class="product-details">
      

      <a class="product-title " href="/products/mulmul-cotton-solid-lilac-harem-pants?pr_prod_strat=copurchase&amp;pr_rec_id=e3a9801a1&amp;pr_rec_pid=7798112616670&amp;pr_ref_pid=7797834973406&amp;pr_seq=uniform" tabindex="0">
        
<span>Mulmul Cotton Solid Lilac Harem Pants</span>

        
          
        
      </a>

      
      <span class="spr-badge" id="spr_badge_7798112616670" data-rating="0.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star-empty" aria-hidden="true"></i><i class="spr-icon spr-icon-star-empty" aria-hidden="true"></i><i class="spr-icon spr-icon-star-empty" aria-hidden="true"></i><i class="spr-icon spr-icon-star-empty" aria-hidden="true"></i><i class="spr-icon spr-icon-star-empty" aria-hidden="true"></i></span><span class="spr-badge-caption">No reviews</span>
</span>

      

      <div class="short-description">
        
        <div>
          A must have yoga harem pants for all the fitness lovers out there. This lilac colored solid mul mul pant with elasticated waist provides comfort and ease of movement. Wash Care: Wash separately in cold water. Do Not Bleach and...
        </div>
      </div>

      <div class="price-box">
        
        <div class="price-sale">
          
            <span class="old-price" data-compare-price-grid=""><span class="money">₹1,986.00</span></span>
            <span class="special-price" data-price-grid=""><span class="money">₹1,390.00</span></span>
          
        </div>
        
      </div>

      

      <div class="wrapper-button-card">
        
        <div class="action">
          


<form action="/cart/add" method="post" class="variants" id="list-product-form-7798112616670-product-recommendations" data-id="product-actions-7798112616670" enctype="multipart/form-data">
  
  

    
      
        <a class="rais-svgcart" data-href="/products/mulmul-cotton-solid-lilac-harem-pants?pr_prod_strat=copurchase&amp;pr_rec_id=e3a9801a1&amp;pr_rec_pid=7798112616670&amp;pr_ref_pid=7797834973406&amp;pr_seq=uniform" title="Mulmul Cotton Solid Lilac Harem Pants" data-init-quickshop="" tabindex="0">
        <svg width="27px" height="27px" viewBox="0 0 27 27" version="1.1" xmlns="http://www.w3.org/2000/svg"><g id="Tjori-" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g id="PLP" transform="translate(-361.000000, -962.000000)"><g id="Product-Widget-Copy" transform="translate(18.000000, 646.000000)"><g id="Product-Widget"><g id="Shopping-Bag-Icon" transform="translate(343.000000, 316.000000)"><path d="M21.6999607,6.00003576 L26.1999499,6.00003576 L26.1999499,20.2500018 C26.1999499,21.2812493 25.8327635,22.1640595 25.0983901,22.898433 C24.3640166,23.6328065 23.4812064,23.9999928 22.4499589,23.9999928 L8.94999106,23.9999928 C7.91874352,23.9999928 7.03593336,23.6328065 6.30155987,22.898433 C5.56718639,22.1640595 5.2,21.2812493 5.2,20.2500018 L5.2,6.00003576 L9.69998927,6.00003576 C9.69998927,4.90628861 9.96561388,3.89847851 10.4968624,2.97660547 C11.0281109,2.05473243 11.7546716,1.32817166 12.6765447,0.796923168 C13.5984177,0.265674673 14.6062278,5.00679016e-05 15.699975,5.00679016e-05 C16.7937221,5.00679016e-05 17.8015322,0.265674673 18.7234053,0.796923168 C19.6452783,1.32817166 20.3718391,2.05473243 20.9030876,2.97660547 C21.4343361,3.89847851 21.6999607,4.90628861 21.6999607,6.00003576 Z M15.699975,1.50004649 C14.4499777,1.50004649 13.3874807,1.93754521 12.5124826,2.81254336 C11.6374844,3.68754151 11.1999857,4.7500385 11.1999857,6.00003576 L20.1999642,6.00003576 C20.1999642,4.7500385 19.7624655,3.68754151 18.8874674,2.81254336 C18.0124692,1.93754521 16.9499722,1.50004649 15.699975,1.50004649 Z M24.6999535,20.2500018 L24.6999535,7.50003219 L21.6999607,7.50003219 L21.6999607,9.75002682 C21.6999607,9.96877654 21.6296483,10.1484634 21.4890237,10.289088 C21.348399,10.4297127 21.1687122,10.500025 20.9499624,10.500025 C20.7312127,10.500025 20.5515259,10.4297127 20.4109012,10.289088 C20.2702766,10.1484634 20.1999642,9.96877654 20.1999642,9.75002682 L20.1999642,7.50003219 L11.1999857,7.50003219 L11.1999857,9.75002682 C11.1999857,9.96877654 11.1296734,10.1484634 10.9890487,10.289088 C10.848424,10.4297127 10.6687372,10.500025 10.4499875,10.500025 C10.2312378,10.500025 10.0515509,10.4297127 9.91092627,10.289088 C9.7703016,10.1484634 9.69998927,9.96877654 9.69998927,9.75002682 L9.69998927,7.50003219 L6.69999642,7.50003219 L6.69999642,20.2500018 C6.69999642,20.8750001 6.91874614,21.4062493 7.35624486,21.843748 C7.79374358,22.2812467 8.32499279,22.4999964 8.94999106,22.4999964 L22.4499589,22.4999964 C23.0749571,22.4999964 23.6062064,22.2812467 24.0437051,21.843748 C24.4812038,21.4062493 24.6999535,20.8750001 24.6999535,20.2500018 Z" id="shopping-bag" fill="#34302B"></path><circle id="Oval" fill="#FFFFFF" cx="6.5" cy="20.5" r="5.5"></circle><path d="M6.34998569,14.6875246 C7.39685832,14.6875246 8.36560589,14.9492428 9.25622877,15.472679 C10.1468516,15.9961151 10.8538813,16.7031448 11.3773175,17.5937677 C11.9007536,18.4843905 12.1624718,19.4531381 12.1624718,20.5000107 C12.1624718,21.5468834 11.9007536,22.5156309 11.3773175,23.4062538 C10.8538813,24.2968767 10.1468516,25.0039064 9.25622877,25.5273425 C8.36560589,26.0507786 7.39685832,26.3124969 6.34998569,26.3124969 C5.30311307,26.3124969 4.3343655,26.0507786 3.44374262,25.5273425 C2.55311975,25.0039064 1.84609006,24.2968767 1.32265393,23.4062538 C0.799217798,22.5156309 0.537499553,21.5468834 0.537499553,20.5000107 C0.537499553,19.4531381 0.799217798,18.4843905 1.32265393,17.5937677 C1.84609006,16.7031448 2.55311975,15.9961151 3.44374262,15.472679 C4.3343655,14.9492428 5.30311307,14.6875246 6.34998569,14.6875246 Z M9.72497765,21.1562592 L9.72497765,19.8437623 C9.72497765,19.7656376 9.69763384,19.6992313 9.64294659,19.644544 C9.58825934,19.5898568 9.52185301,19.562513 9.44372832,19.562513 L7.28748346,19.562513 L7.28748346,17.4062681 C7.28748346,17.3281434 7.26013966,17.2617371 7.20545241,17.2070498 C7.15076515,17.1523626 7.08435882,17.1250188 7.00623413,17.1250188 L5.69373726,17.1250188 C5.61561256,17.1250188 5.54920623,17.1523626 5.49451898,17.2070498 C5.43983173,17.2617371 5.41248793,17.3281434 5.41248793,17.4062681 L5.41248793,19.562513 L3.25624307,19.562513 C3.17811838,19.562513 3.11171205,19.5898568 3.0570248,19.644544 C3.00233755,19.6992313 2.97499374,19.7656376 2.97499374,19.8437623 L2.97499374,21.1562592 C2.97499374,21.2343839 3.00233755,21.3007902 3.0570248,21.3554774 C3.11171205,21.4101647 3.17811838,21.4375085 3.25624307,21.4375085 L5.41248793,21.4375085 L5.41248793,23.5937534 C5.41248793,23.671878 5.43983173,23.7382844 5.49451898,23.7929716 C5.54920623,23.8476589 5.61561256,23.8750027 5.69373726,23.8750027 L7.00623413,23.8750027 C7.08435882,23.8750027 7.15076515,23.8476589 7.20545241,23.7929716 C7.26013966,23.7382844 7.28748346,23.671878 7.28748346,23.5937534 L7.28748346,21.4375085 L9.44372832,21.4375085 C9.52185301,21.4375085 9.58825934,21.4101647 9.64294659,21.3554774 C9.69763384,21.3007902 9.72497765,21.2343839 9.72497765,21.1562592 Z" id="plus-circle" fill="#C0B394"></path></g></g></g></g></g></svg>
        </a>
      
    
  
  
</form>

  
      
          <div class="product-card__variant--popup">
              <div class="product-card__variant--popup--content">
    
    <form action="/cart/add" method="post" class="variants" id="swatch-list-product-form-7798112616670-product-recommendations-list" data-id="product-actions-7798112616670" enctype="multipart/form-data">
        
            
            
            
                
                    <div class="selector-wrapper selector-wrapper-1 swatch js product-form__item" data-option-index="0">
                        <label class="form-label ">
                            Size: <span class="label-value-1">XS</span>
                        </label>
                        
                        
                        

                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size xs available" data-value="XS">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7798112616670-product-recommendationslist" data-index="option1" value="XS" id="SingleOptionSelector0-XS-7798112616670-product-recommendationslist" tabindex="0">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-XS-7798112616670-product-recommendationslist" data-original-title="XS">XS</label>
                                </div>
                            
                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size s available" data-value="S">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7798112616670-product-recommendationslist" data-index="option1" value="S" id="SingleOptionSelector0-S-7798112616670-product-recommendationslist" tabindex="0">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-S-7798112616670-product-recommendationslist" data-original-title="S">S</label>
                                </div>
                            
                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size m available" data-value="M">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7798112616670-product-recommendationslist" data-index="option1" value="M" id="SingleOptionSelector0-M-7798112616670-product-recommendationslist" tabindex="0">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-M-7798112616670-product-recommendationslist" data-original-title="M">M</label>
                                </div>
                            
                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size l available" data-value="L">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7798112616670-product-recommendationslist" data-index="option1" value="L" id="SingleOptionSelector0-L-7798112616670-product-recommendationslist" tabindex="0">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-L-7798112616670-product-recommendationslist" data-original-title="L">L</label>
                                </div>
                            
                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size xl available" data-value="XL">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7798112616670-product-recommendationslist" data-index="option1" value="XL" id="SingleOptionSelector0-XL-7798112616670-product-recommendationslist" tabindex="0">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-XL-7798112616670-product-recommendationslist" data-original-title="XL">XL</label>
                                </div>
                            
                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size xxl available" data-value="XXL">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7798112616670-product-recommendationslist" data-index="option1" value="XXL" id="SingleOptionSelector0-XXL-7798112616670-product-recommendationslist" tabindex="0">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-XXL-7798112616670-product-recommendationslist" data-original-title="XXL">XXL</label>
                                </div>
                            
                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size xxxl available" data-value="XXXL">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7798112616670-product-recommendationslist" data-index="option1" value="XXXL" id="SingleOptionSelector0-XXXL-7798112616670-product-recommendationslist" tabindex="0">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-XXXL-7798112616670-product-recommendationslist" data-original-title="XXXL">XXXL</label>
                                </div>
                            
                        
                    </div>
                
            
        

        <select name="id" id="ProductSelect-7798112616670-product-recommendationslist" class="product-form__variants no-js" tabindex="0">
            
                
                    <option value="43400460370142">
                        XS
                    </option>
                
            
                
                    <option value="43400460402910">
                        S
                    </option>
                
            
                
                    <option value="43400460435678">
                        M
                    </option>
                
            
                
                    <option value="43400460468446">
                        L
                    </option>
                
            
                
                    <option value="43400460501214">
                        XL
                    </option>
                
            
                
                    <option value="43400460533982">
                        XXL
                    </option>
                
            
                
                    <option value="43400460566750">
                        XXXL
                    </option>
                
            
        </select>
        <div class="product-card__button_cancel_mobile">
            <a class="btn btn-cancel" data-cancel-swatch-qs="" tabindex="0"><svg aria-hidden="true" data-prefix="fal" data-icon="times" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" class="svg-inline--fa fa-times fa-w-10 fa-2x"><path fill="currentColor" d="M193.94 256L296.5 153.44l21.15-21.15c3.12-3.12 3.12-8.19 0-11.31l-22.63-22.63c-3.12-3.12-8.19-3.12-11.31 0L160 222.06 36.29 98.34c-3.12-3.12-8.19-3.12-11.31 0L2.34 120.97c-3.12 3.12-3.12 8.19 0 11.31L126.06 256 2.34 379.71c-3.12 3.12-3.12 8.19 0 11.31l22.63 22.63c3.12 3.12 8.19 3.12 11.31 0L160 289.94 262.56 392.5l21.15 21.15c3.12 3.12 8.19 3.12 11.31 0l22.63-22.63c3.12-3.12 3.12-8.19 0-11.31L193.94 256z" class=""></path></svg></a>
        </div>
        <div class="product-card__button2">
            <input type="hidden" name="quantity" value="1" tabindex="0">
            <button data-btn-addtocart="" class="btn add-to-cart-btn" type="submit" data-form-id="#swatch-list-product-form-7798112616670-product-recommendations-list" tabindex="0">
                Submit
            </button>
            <a class="btn btn-cancel" data-cancel-swatch-qs="" tabindex="0">Cancel</a>
        </div>
        
    </form>
</div>
          </div>
      
  

        </div>
        
      </div>
      
    </div>
    
  </div>
</div>

          
        </div><div class="grid-item small--one-half medium-up--one-quarter col-12 slick-slide slick-active" style="width: 173px;" tabindex="-1" role="option" aria-describedby="slick-slide21" data-slick-index="1" aria-hidden="false">
          
            
<div class="inner product-item on-sale" data-product-id="product-7798196830430" data-json-product="{&quot;id&quot;: 7798196830430,&quot;handle&quot;: &quot;207-31&quot;,&quot;media&quot;: [{&quot;alt&quot;:null,&quot;id&quot;:30632456552670,&quot;position&quot;:1,&quot;preview_image&quot;:{&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:2360,&quot;width&quot;:1600,&quot;src&quot;:&quot;https:\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/276068-2019-04-30-15_24_42.893454-big-image.jpg?v=1663261251&quot;},&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:2360,&quot;media_type&quot;:&quot;image&quot;,&quot;src&quot;:&quot;https:\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/276068-2019-04-30-15_24_42.893454-big-image.jpg?v=1663261251&quot;,&quot;width&quot;:1600},{&quot;alt&quot;:null,&quot;id&quot;:30632456585438,&quot;position&quot;:2,&quot;preview_image&quot;:{&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:2360,&quot;width&quot;:1600,&quot;src&quot;:&quot;https:\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/276069-2019-04-30-15_25_44.222591-big-image.jpg?v=1663261251&quot;},&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:2360,&quot;media_type&quot;:&quot;image&quot;,&quot;src&quot;:&quot;https:\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/276069-2019-04-30-15_25_44.222591-big-image.jpg?v=1663261251&quot;,&quot;width&quot;:1600},{&quot;alt&quot;:null,&quot;id&quot;:30632456618206,&quot;position&quot;:3,&quot;preview_image&quot;:{&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:2360,&quot;width&quot;:1600,&quot;src&quot;:&quot;https:\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/276070-2019-04-30-15_25_46.192013-big-image.jpg?v=1663261251&quot;},&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:2360,&quot;media_type&quot;:&quot;image&quot;,&quot;src&quot;:&quot;https:\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/276070-2019-04-30-15_25_46.192013-big-image.jpg?v=1663261251&quot;,&quot;width&quot;:1600},{&quot;alt&quot;:null,&quot;id&quot;:30632456650974,&quot;position&quot;:4,&quot;preview_image&quot;:{&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:2360,&quot;width&quot;:1600,&quot;src&quot;:&quot;https:\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/276071-2019-04-30-15_26_10.651914-big-image.jpg?v=1663261251&quot;},&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:2360,&quot;media_type&quot;:&quot;image&quot;,&quot;src&quot;:&quot;https:\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/276071-2019-04-30-15_26_10.651914-big-image.jpg?v=1663261251&quot;,&quot;width&quot;:1600},{&quot;alt&quot;:null,&quot;id&quot;:30632456519902,&quot;position&quot;:5,&quot;preview_image&quot;:{&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:1180,&quot;width&quot;:800,&quot;src&quot;:&quot;https:\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/279899-2019-08-09-15_31_23.792813-big-image_a8acaa5e-3440-4365-a526-b209e41c2dff.jpg?v=1663261251&quot;},&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:1180,&quot;media_type&quot;:&quot;image&quot;,&quot;src&quot;:&quot;https:\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/279899-2019-08-09-15_31_23.792813-big-image_a8acaa5e-3440-4365-a526-b209e41c2dff.jpg?v=1663261251&quot;,&quot;width&quot;:800}],&quot;variants&quot;: [{&quot;id&quot;:43400766980318,&quot;title&quot;:&quot;XS&quot;,&quot;option1&quot;:&quot;XS&quot;,&quot;option2&quot;:null,&quot;option3&quot;:null,&quot;sku&quot;:&quot;TJ-MK-207-31-XS&quot;,&quot;requires_shipping&quot;:true,&quot;taxable&quot;:true,&quot;featured_image&quot;:null,&quot;available&quot;:true,&quot;name&quot;:&quot;The Innocent Baby Blue Cotton Kurta - XS&quot;,&quot;public_title&quot;:&quot;XS&quot;,&quot;options&quot;:[&quot;XS&quot;],&quot;price&quot;:208700,&quot;weight&quot;:0,&quot;compare_at_price&quot;:298100,&quot;inventory_management&quot;:&quot;shopify&quot;,&quot;barcode&quot;:null,&quot;quantity_rule&quot;:{&quot;min&quot;:1,&quot;max&quot;:null,&quot;increment&quot;:1}},{&quot;id&quot;:43400767013086,&quot;title&quot;:&quot;S&quot;,&quot;option1&quot;:&quot;S&quot;,&quot;option2&quot;:null,&quot;option3&quot;:null,&quot;sku&quot;:&quot;TJ-MK-207-31-S&quot;,&quot;requires_shipping&quot;:true,&quot;taxable&quot;:true,&quot;featured_image&quot;:null,&quot;available&quot;:true,&quot;name&quot;:&quot;The Innocent Baby Blue Cotton Kurta - S&quot;,&quot;public_title&quot;:&quot;S&quot;,&quot;options&quot;:[&quot;S&quot;],&quot;price&quot;:208700,&quot;weight&quot;:0,&quot;compare_at_price&quot;:298100,&quot;inventory_management&quot;:&quot;shopify&quot;,&quot;barcode&quot;:null,&quot;quantity_rule&quot;:{&quot;min&quot;:1,&quot;max&quot;:null,&quot;increment&quot;:1}},{&quot;id&quot;:43400767045854,&quot;title&quot;:&quot;M&quot;,&quot;option1&quot;:&quot;M&quot;,&quot;option2&quot;:null,&quot;option3&quot;:null,&quot;sku&quot;:&quot;TJ-MK-207-31-M&quot;,&quot;requires_shipping&quot;:true,&quot;taxable&quot;:true,&quot;featured_image&quot;:null,&quot;available&quot;:true,&quot;name&quot;:&quot;The Innocent Baby Blue Cotton Kurta - M&quot;,&quot;public_title&quot;:&quot;M&quot;,&quot;options&quot;:[&quot;M&quot;],&quot;price&quot;:208700,&quot;weight&quot;:0,&quot;compare_at_price&quot;:298100,&quot;inventory_management&quot;:&quot;shopify&quot;,&quot;barcode&quot;:null,&quot;quantity_rule&quot;:{&quot;min&quot;:1,&quot;max&quot;:null,&quot;increment&quot;:1}},{&quot;id&quot;:43400767078622,&quot;title&quot;:&quot;L&quot;,&quot;option1&quot;:&quot;L&quot;,&quot;option2&quot;:null,&quot;option3&quot;:null,&quot;sku&quot;:&quot;TJ-MK-207-31-L&quot;,&quot;requires_shipping&quot;:true,&quot;taxable&quot;:true,&quot;featured_image&quot;:null,&quot;available&quot;:true,&quot;name&quot;:&quot;The Innocent Baby Blue Cotton Kurta - L&quot;,&quot;public_title&quot;:&quot;L&quot;,&quot;options&quot;:[&quot;L&quot;],&quot;price&quot;:208700,&quot;weight&quot;:0,&quot;compare_at_price&quot;:298100,&quot;inventory_management&quot;:&quot;shopify&quot;,&quot;barcode&quot;:null,&quot;quantity_rule&quot;:{&quot;min&quot;:1,&quot;max&quot;:null,&quot;increment&quot;:1}},{&quot;id&quot;:43400767111390,&quot;title&quot;:&quot;XL&quot;,&quot;option1&quot;:&quot;XL&quot;,&quot;option2&quot;:null,&quot;option3&quot;:null,&quot;sku&quot;:&quot;TJ-MK-207-31-XL&quot;,&quot;requires_shipping&quot;:true,&quot;taxable&quot;:true,&quot;featured_image&quot;:null,&quot;available&quot;:true,&quot;name&quot;:&quot;The Innocent Baby Blue Cotton Kurta - XL&quot;,&quot;public_title&quot;:&quot;XL&quot;,&quot;options&quot;:[&quot;XL&quot;],&quot;price&quot;:208700,&quot;weight&quot;:0,&quot;compare_at_price&quot;:298100,&quot;inventory_management&quot;:&quot;shopify&quot;,&quot;barcode&quot;:null,&quot;quantity_rule&quot;:{&quot;min&quot;:1,&quot;max&quot;:null,&quot;increment&quot;:1}},{&quot;id&quot;:43400767144158,&quot;title&quot;:&quot;XXL&quot;,&quot;option1&quot;:&quot;XXL&quot;,&quot;option2&quot;:null,&quot;option3&quot;:null,&quot;sku&quot;:&quot;TJ-MK-207-31-XXL&quot;,&quot;requires_shipping&quot;:true,&quot;taxable&quot;:true,&quot;featured_image&quot;:null,&quot;available&quot;:true,&quot;name&quot;:&quot;The Innocent Baby Blue Cotton Kurta - XXL&quot;,&quot;public_title&quot;:&quot;XXL&quot;,&quot;options&quot;:[&quot;XXL&quot;],&quot;price&quot;:208700,&quot;weight&quot;:0,&quot;compare_at_price&quot;:298100,&quot;inventory_management&quot;:&quot;shopify&quot;,&quot;barcode&quot;:null,&quot;quantity_rule&quot;:{&quot;min&quot;:1,&quot;max&quot;:null,&quot;increment&quot;:1}},{&quot;id&quot;:43400767176926,&quot;title&quot;:&quot;XXXL&quot;,&quot;option1&quot;:&quot;XXXL&quot;,&quot;option2&quot;:null,&quot;option3&quot;:null,&quot;sku&quot;:&quot;TJ-MK-207-31-XXXL&quot;,&quot;requires_shipping&quot;:true,&quot;taxable&quot;:true,&quot;featured_image&quot;:null,&quot;available&quot;:true,&quot;name&quot;:&quot;The Innocent Baby Blue Cotton Kurta - XXXL&quot;,&quot;public_title&quot;:&quot;XXXL&quot;,&quot;options&quot;:[&quot;XXXL&quot;],&quot;price&quot;:208700,&quot;weight&quot;:0,&quot;compare_at_price&quot;:298100,&quot;inventory_management&quot;:&quot;shopify&quot;,&quot;barcode&quot;:null,&quot;quantity_rule&quot;:{&quot;min&quot;:1,&quot;max&quot;:null,&quot;increment&quot;:1}}]}">
  <div class="inner-top">
    <div class="product-top">
      <div class="product-image image-swap">
        <a href="/products/207-31?pr_prod_strat=copurchase&amp;pr_rec_id=e3a9801a1&amp;pr_rec_pid=7798196830430&amp;pr_ref_pid=7797834973406&amp;pr_seq=uniform" class="product-grid-image adaptive_height" data-collections-related="/collections/?view=related" style="padding-top: 147.5%" tabindex="0">
          



  <picture data-index="0">
    <source class="image-source-1" data-srcset="//cdn.shopify.com/s/files/1/0613/2039/7022/products/276068-2019-04-30-15_24_42.893454-big-image_300x.jpg?v=1663261251" media="(max-width: 767px)" sizes="153px" srcset="//cdn.shopify.com/s/files/1/0613/2039/7022/products/276068-2019-04-30-15_24_42.893454-big-image_300x.jpg?v=1663261251">
    <source class="image-source-2" data-srcset=" //cdn.shopify.com/s/files/1/0613/2039/7022/products/276068-2019-04-30-15_24_42.893454-big-image_360x.jpg?v=1663261251 360w,
                          //cdn.shopify.com/s/files/1/0613/2039/7022/products/276068-2019-04-30-15_24_42.893454-big-image_540x.jpg?v=1663261251 540w,
                          //cdn.shopify.com/s/files/1/0613/2039/7022/products/276068-2019-04-30-15_24_42.893454-big-image_720x.jpg?v=1663261251 720w,
                          //cdn.shopify.com/s/files/1/0613/2039/7022/products/276068-2019-04-30-15_24_42.893454-big-image_900x.jpg?v=1663261251 900w" media="(min-width: 768px)" sizes="153px" srcset=" //cdn.shopify.com/s/files/1/0613/2039/7022/products/276068-2019-04-30-15_24_42.893454-big-image_360x.jpg?v=1663261251 360w,
                          //cdn.shopify.com/s/files/1/0613/2039/7022/products/276068-2019-04-30-15_24_42.893454-big-image_540x.jpg?v=1663261251 540w,
                          //cdn.shopify.com/s/files/1/0613/2039/7022/products/276068-2019-04-30-15_24_42.893454-big-image_720x.jpg?v=1663261251 720w,
                          //cdn.shopify.com/s/files/1/0613/2039/7022/products/276068-2019-04-30-15_24_42.893454-big-image_900x.jpg?v=1663261251 900w">

    <img alt="The Innocent Baby Blue Cotton Kurta" class="images-one lazyautosizes lazyloaded" data-widths="[180, 360, 540, 720, 900, 1080, 1296, 1512, 1728, 2048]" data-aspectratio="0.6779661016949152" data-sizes="auto" data-srcset="//cdn.shopify.com/s/files/1/0613/2039/7022/products/276068-2019-04-30-15_24_42.893454-big-image_360x.jpg?v=1663261251 360w,
                    //cdn.shopify.com/s/files/1/0613/2039/7022/products/276068-2019-04-30-15_24_42.893454-big-image_540x.jpg?v=1663261251 540w,
                    //cdn.shopify.com/s/files/1/0613/2039/7022/products/276068-2019-04-30-15_24_42.893454-big-image_720x.jpg?v=1663261251 720w,
                    //cdn.shopify.com/s/files/1/0613/2039/7022/products/276068-2019-04-30-15_24_42.893454-big-image_900x.jpg?v=1663261251 900w" data-image="" sizes="153px" srcset="//cdn.shopify.com/s/files/1/0613/2039/7022/products/276068-2019-04-30-15_24_42.893454-big-image_360x.jpg?v=1663261251 360w,
                    //cdn.shopify.com/s/files/1/0613/2039/7022/products/276068-2019-04-30-15_24_42.893454-big-image_540x.jpg?v=1663261251 540w,
                    //cdn.shopify.com/s/files/1/0613/2039/7022/products/276068-2019-04-30-15_24_42.893454-big-image_720x.jpg?v=1663261251 720w,
                    //cdn.shopify.com/s/files/1/0613/2039/7022/products/276068-2019-04-30-15_24_42.893454-big-image_900x.jpg?v=1663261251 900w">
  </picture>
  <span class="images-two">
    
    <picture data-index="1">
      <source data-srcset="//cdn.shopify.com/s/files/1/0613/2039/7022/products/276069-2019-04-30-15_25_44.222591-big-image_300x.jpg?v=1663261251" media="(max-width: 767px)" sizes="153px" srcset="//cdn.shopify.com/s/files/1/0613/2039/7022/products/276069-2019-04-30-15_25_44.222591-big-image_300x.jpg?v=1663261251">
      <source data-srcset="//cdn.shopify.com/s/files/1/0613/2039/7022/products/276069-2019-04-30-15_25_44.222591-big-image_360x.jpg?v=1663261251 360w,
                            //cdn.shopify.com/s/files/1/0613/2039/7022/products/276069-2019-04-30-15_25_44.222591-big-image_540x.jpg?v=1663261251 540w,
                            //cdn.shopify.com/s/files/1/0613/2039/7022/products/276069-2019-04-30-15_25_44.222591-big-image_720x.jpg?v=1663261251 720w,
                            //cdn.shopify.com/s/files/1/0613/2039/7022/products/276069-2019-04-30-15_25_44.222591-big-image_900x.jpg?v=1663261251 900w" media="(min-width: 768px)" sizes="153px" srcset="//cdn.shopify.com/s/files/1/0613/2039/7022/products/276069-2019-04-30-15_25_44.222591-big-image_360x.jpg?v=1663261251 360w,
                            //cdn.shopify.com/s/files/1/0613/2039/7022/products/276069-2019-04-30-15_25_44.222591-big-image_540x.jpg?v=1663261251 540w,
                            //cdn.shopify.com/s/files/1/0613/2039/7022/products/276069-2019-04-30-15_25_44.222591-big-image_720x.jpg?v=1663261251 720w,
                            //cdn.shopify.com/s/files/1/0613/2039/7022/products/276069-2019-04-30-15_25_44.222591-big-image_900x.jpg?v=1663261251 900w">

      <img alt="The Innocent Baby Blue Cotton Kurta" class="lazyautosizes lazyloaded" data-widths="[180, 360, 540, 720, 900, 1080, 1296, 1512, 1728, 2048]" data-aspectratio="0.6779661016949152" data-sizes="auto" data-srcset="//cdn.shopify.com/s/files/1/0613/2039/7022/products/276069-2019-04-30-15_25_44.222591-big-image_360x.jpg?v=1663261251 360w,
                        //cdn.shopify.com/s/files/1/0613/2039/7022/products/276069-2019-04-30-15_25_44.222591-big-image_540x.jpg?v=1663261251 540w,
                        //cdn.shopify.com/s/files/1/0613/2039/7022/products/276069-2019-04-30-15_25_44.222591-big-image_720x.jpg?v=1663261251 720w,
                        //cdn.shopify.com/s/files/1/0613/2039/7022/products/276069-2019-04-30-15_25_44.222591-big-image_900x.jpg?v=1663261251 900w" data-image="" sizes="153px" srcset="//cdn.shopify.com/s/files/1/0613/2039/7022/products/276069-2019-04-30-15_25_44.222591-big-image_360x.jpg?v=1663261251 360w,
                        //cdn.shopify.com/s/files/1/0613/2039/7022/products/276069-2019-04-30-15_25_44.222591-big-image_540x.jpg?v=1663261251 540w,
                        //cdn.shopify.com/s/files/1/0613/2039/7022/products/276069-2019-04-30-15_25_44.222591-big-image_720x.jpg?v=1663261251 720w,
                        //cdn.shopify.com/s/files/1/0613/2039/7022/products/276069-2019-04-30-15_25_44.222591-big-image_900x.jpg?v=1663261251 900w">
    </picture>
    
  </span>

        </a>
      </div>

      
  
  <div class="product-label" data-label-new-number="30">
    

    
      
        <strong class="label sale-label">
          Sale
        </strong>
        
      
    

    
    
    

    
    
    
  </div>
  






      
<a class="wishlist " data-icon-wishlist="" aria-label="Wish Lists" href="#" data-product-handle="207-31" data-id="7798196830430" tabindex="0">
	    	<svg viewBox="0 0 512 512">
<g>
<g>
  <path d="M474.644,74.27C449.391,45.616,414.358,29.836,376,29.836c-53.948,0-88.103,32.22-107.255,59.25
    c-4.969,7.014-9.196,14.047-12.745,20.665c-3.549-6.618-7.775-13.651-12.745-20.665c-19.152-27.03-53.307-59.25-107.255-59.25
    c-38.358,0-73.391,15.781-98.645,44.435C13.267,101.605,0,138.213,0,177.351c0,42.603,16.633,82.228,52.345,124.7
    c31.917,37.96,77.834,77.088,131.005,122.397c19.813,16.884,40.302,34.344,62.115,53.429l0.655,0.574
    c2.828,2.476,6.354,3.713,9.88,3.713s7.052-1.238,9.88-3.713l0.655-0.574c21.813-19.085,42.302-36.544,62.118-53.431
    c53.168-45.306,99.085-84.434,131.002-122.395C495.367,259.578,512,219.954,512,177.351
    C512,138.213,498.733,101.605,474.644,74.27z M309.193,401.614c-17.08,14.554-34.658,29.533-53.193,45.646
    c-18.534-16.111-36.113-31.091-53.196-45.648C98.745,312.939,30,254.358,30,177.351c0-31.83,10.605-61.394,29.862-83.245
    C79.34,72.007,106.379,59.836,136,59.836c41.129,0,67.716,25.338,82.776,46.594c13.509,19.064,20.558,38.282,22.962,45.659
    c2.011,6.175,7.768,10.354,14.262,10.354c6.494,0,12.251-4.179,14.262-10.354c2.404-7.377,9.453-26.595,22.962-45.66
    c15.06-21.255,41.647-46.593,82.776-46.593c29.621,0,56.66,12.171,76.137,34.27C471.395,115.957,482,145.521,482,177.351
    C482,254.358,413.255,312.939,309.193,401.614z"></path>
</g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
</svg>
		
</a>
 


      
        <a class="quickview-button" href="javascript:void(0)" id="207-31" title="Quick View" tabindex="0">
          <span>Quick view</span>
          <svg viewBox="0 0 511.999 511.999" width="100%" height="100%">
    <path d="M508.745,246.041c-4.574-6.257-113.557-153.206-252.748-153.206S7.818,239.784,3.249,246.035         c-4.332,5.936-4.332,13.987,0,19.923c4.569,6.257,113.557,153.206,252.748,153.206s248.174-146.95,252.748-153.201         C513.083,260.028,513.083,251.971,508.745,246.041z M255.997,385.406c-102.529,0-191.33-97.533-217.617-129.418         c26.253-31.913,114.868-129.395,217.617-129.395c102.524,0,191.319,97.516,217.617,129.418         C447.361,287.923,358.746,385.406,255.997,385.406z"></path>
      <path d="M255.997,154.725c-55.842,0-101.275,45.433-101.275,101.275s45.433,101.275,101.275,101.275         s101.275-45.433,101.275-101.275S311.839,154.725,255.997,154.725z M255.997,323.516c-37.23,0-67.516-30.287-67.516-67.516         s30.287-67.516,67.516-67.516s67.516,30.287,67.516,67.516S293.227,323.516,255.997,323.516z"></path>
</svg>
        </a>
      

      

    
    <div class="product-bottom">
      
      
        <span class="spr-badge" id="spr_badge_7798196830430" data-rating="4.666666666666667"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star" aria-hidden="true"></i><i class="spr-icon spr-icon-star" aria-hidden="true"></i><i class="spr-icon spr-icon-star" aria-hidden="true"></i><i class="spr-icon spr-icon-star" aria-hidden="true"></i><i class="spr-icon spr-icon-star" aria-hidden="true"></i></span><span class="spr-badge-caption">3 reviews</span>
</span>

      
      <a class="product-title " href="/products/207-31?pr_prod_strat=copurchase&amp;pr_rec_id=e3a9801a1&amp;pr_rec_pid=7798196830430&amp;pr_ref_pid=7797834973406&amp;pr_seq=uniform" tabindex="0">
        
<span>The Innocent Baby Blue Cotton Kurta</span>

        
          
          
      </a>
      <div class="half-grid-rais">
      <div class="price-box">
        
        <div class="price-sale">
          
            <span class="special-price" data-price-grid=""><span class="money">₹2087.00</span></span>
            <span class="old-price" data-compare-price-grid="">2981.00</span>
            <span class="percentage-price">(30%)</span>
         
        </div>
        
      </div>
    </div>

      <div class="add-to-cart-grid-rais">
        
        <div class="action">
          


<form action="/cart/add" method="post" class="variants" id="grid-product-form--7798196830430-product-recommendations" data-id="product-actions-7798196830430" enctype="multipart/form-data">
  
  

    
      
        <a class="rais-svgcart" data-href="/products/207-31?pr_prod_strat=copurchase&amp;pr_rec_id=e3a9801a1&amp;pr_rec_pid=7798196830430&amp;pr_ref_pid=7797834973406&amp;pr_seq=uniform" title="The Innocent Baby Blue Cotton Kurta" data-init-quickshop="" tabindex="0">
        <svg width="27px" height="27px" viewBox="0 0 27 27" version="1.1" xmlns="http://www.w3.org/2000/svg"><g id="Tjori-" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g id="PLP" transform="translate(-361.000000, -962.000000)"><g id="Product-Widget-Copy" transform="translate(18.000000, 646.000000)"><g id="Product-Widget"><g id="Shopping-Bag-Icon" transform="translate(343.000000, 316.000000)"><path d="M21.6999607,6.00003576 L26.1999499,6.00003576 L26.1999499,20.2500018 C26.1999499,21.2812493 25.8327635,22.1640595 25.0983901,22.898433 C24.3640166,23.6328065 23.4812064,23.9999928 22.4499589,23.9999928 L8.94999106,23.9999928 C7.91874352,23.9999928 7.03593336,23.6328065 6.30155987,22.898433 C5.56718639,22.1640595 5.2,21.2812493 5.2,20.2500018 L5.2,6.00003576 L9.69998927,6.00003576 C9.69998927,4.90628861 9.96561388,3.89847851 10.4968624,2.97660547 C11.0281109,2.05473243 11.7546716,1.32817166 12.6765447,0.796923168 C13.5984177,0.265674673 14.6062278,5.00679016e-05 15.699975,5.00679016e-05 C16.7937221,5.00679016e-05 17.8015322,0.265674673 18.7234053,0.796923168 C19.6452783,1.32817166 20.3718391,2.05473243 20.9030876,2.97660547 C21.4343361,3.89847851 21.6999607,4.90628861 21.6999607,6.00003576 Z M15.699975,1.50004649 C14.4499777,1.50004649 13.3874807,1.93754521 12.5124826,2.81254336 C11.6374844,3.68754151 11.1999857,4.7500385 11.1999857,6.00003576 L20.1999642,6.00003576 C20.1999642,4.7500385 19.7624655,3.68754151 18.8874674,2.81254336 C18.0124692,1.93754521 16.9499722,1.50004649 15.699975,1.50004649 Z M24.6999535,20.2500018 L24.6999535,7.50003219 L21.6999607,7.50003219 L21.6999607,9.75002682 C21.6999607,9.96877654 21.6296483,10.1484634 21.4890237,10.289088 C21.348399,10.4297127 21.1687122,10.500025 20.9499624,10.500025 C20.7312127,10.500025 20.5515259,10.4297127 20.4109012,10.289088 C20.2702766,10.1484634 20.1999642,9.96877654 20.1999642,9.75002682 L20.1999642,7.50003219 L11.1999857,7.50003219 L11.1999857,9.75002682 C11.1999857,9.96877654 11.1296734,10.1484634 10.9890487,10.289088 C10.848424,10.4297127 10.6687372,10.500025 10.4499875,10.500025 C10.2312378,10.500025 10.0515509,10.4297127 9.91092627,10.289088 C9.7703016,10.1484634 9.69998927,9.96877654 9.69998927,9.75002682 L9.69998927,7.50003219 L6.69999642,7.50003219 L6.69999642,20.2500018 C6.69999642,20.8750001 6.91874614,21.4062493 7.35624486,21.843748 C7.79374358,22.2812467 8.32499279,22.4999964 8.94999106,22.4999964 L22.4499589,22.4999964 C23.0749571,22.4999964 23.6062064,22.2812467 24.0437051,21.843748 C24.4812038,21.4062493 24.6999535,20.8750001 24.6999535,20.2500018 Z" id="shopping-bag" fill="#34302B"></path><circle id="Oval" fill="#FFFFFF" cx="6.5" cy="20.5" r="5.5"></circle><path d="M6.34998569,14.6875246 C7.39685832,14.6875246 8.36560589,14.9492428 9.25622877,15.472679 C10.1468516,15.9961151 10.8538813,16.7031448 11.3773175,17.5937677 C11.9007536,18.4843905 12.1624718,19.4531381 12.1624718,20.5000107 C12.1624718,21.5468834 11.9007536,22.5156309 11.3773175,23.4062538 C10.8538813,24.2968767 10.1468516,25.0039064 9.25622877,25.5273425 C8.36560589,26.0507786 7.39685832,26.3124969 6.34998569,26.3124969 C5.30311307,26.3124969 4.3343655,26.0507786 3.44374262,25.5273425 C2.55311975,25.0039064 1.84609006,24.2968767 1.32265393,23.4062538 C0.799217798,22.5156309 0.537499553,21.5468834 0.537499553,20.5000107 C0.537499553,19.4531381 0.799217798,18.4843905 1.32265393,17.5937677 C1.84609006,16.7031448 2.55311975,15.9961151 3.44374262,15.472679 C4.3343655,14.9492428 5.30311307,14.6875246 6.34998569,14.6875246 Z M9.72497765,21.1562592 L9.72497765,19.8437623 C9.72497765,19.7656376 9.69763384,19.6992313 9.64294659,19.644544 C9.58825934,19.5898568 9.52185301,19.562513 9.44372832,19.562513 L7.28748346,19.562513 L7.28748346,17.4062681 C7.28748346,17.3281434 7.26013966,17.2617371 7.20545241,17.2070498 C7.15076515,17.1523626 7.08435882,17.1250188 7.00623413,17.1250188 L5.69373726,17.1250188 C5.61561256,17.1250188 5.54920623,17.1523626 5.49451898,17.2070498 C5.43983173,17.2617371 5.41248793,17.3281434 5.41248793,17.4062681 L5.41248793,19.562513 L3.25624307,19.562513 C3.17811838,19.562513 3.11171205,19.5898568 3.0570248,19.644544 C3.00233755,19.6992313 2.97499374,19.7656376 2.97499374,19.8437623 L2.97499374,21.1562592 C2.97499374,21.2343839 3.00233755,21.3007902 3.0570248,21.3554774 C3.11171205,21.4101647 3.17811838,21.4375085 3.25624307,21.4375085 L5.41248793,21.4375085 L5.41248793,23.5937534 C5.41248793,23.671878 5.43983173,23.7382844 5.49451898,23.7929716 C5.54920623,23.8476589 5.61561256,23.8750027 5.69373726,23.8750027 L7.00623413,23.8750027 C7.08435882,23.8750027 7.15076515,23.8476589 7.20545241,23.7929716 C7.26013966,23.7382844 7.28748346,23.671878 7.28748346,23.5937534 L7.28748346,21.4375085 L9.44372832,21.4375085 C9.52185301,21.4375085 9.58825934,21.4101647 9.64294659,21.3554774 C9.69763384,21.3007902 9.72497765,21.2343839 9.72497765,21.1562592 Z" id="plus-circle" fill="#C0B394"></path></g></g></g></g></g></svg>
        </a>
      
    
  
  
</form>

  
      
          <div class="product-card__variant--popup">
              <div class="product-card__variant--popup--content">
    
    <form action="/cart/add" method="post" class="variants" id="swatch-grid-product-form--7798196830430-product-recommendations-" data-id="product-actions-7798196830430" enctype="multipart/form-data">
        
            
            
            
                
                    <div class="selector-wrapper selector-wrapper-1 swatch js product-form__item" data-option-index="0">
                        <label class="form-label ">
                            Size: <span class="label-value-1">XS</span>
                        </label>
                        
                        
                        

                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size xs available" data-value="XS">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7798196830430-product-recommendations" data-index="option1" value="XS" id="SingleOptionSelector0-XS-7798196830430-product-recommendations" tabindex="0">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-XS-7798196830430-product-recommendations" data-original-title="XS">XS</label>
                                </div>
                            
                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size s available" data-value="S">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7798196830430-product-recommendations" data-index="option1" value="S" id="SingleOptionSelector0-S-7798196830430-product-recommendations" tabindex="0">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-S-7798196830430-product-recommendations" data-original-title="S">S</label>
                                </div>
                            
                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size m available" data-value="M">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7798196830430-product-recommendations" data-index="option1" value="M" id="SingleOptionSelector0-M-7798196830430-product-recommendations" tabindex="0">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-M-7798196830430-product-recommendations" data-original-title="M">M</label>
                                </div>
                            
                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size l available" data-value="L">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7798196830430-product-recommendations" data-index="option1" value="L" id="SingleOptionSelector0-L-7798196830430-product-recommendations" tabindex="0">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-L-7798196830430-product-recommendations" data-original-title="L">L</label>
                                </div>
                            
                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size xl available" data-value="XL">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7798196830430-product-recommendations" data-index="option1" value="XL" id="SingleOptionSelector0-XL-7798196830430-product-recommendations" tabindex="0">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-XL-7798196830430-product-recommendations" data-original-title="XL">XL</label>
                                </div>
                            
                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size xxl available" data-value="XXL">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7798196830430-product-recommendations" data-index="option1" value="XXL" id="SingleOptionSelector0-XXL-7798196830430-product-recommendations" tabindex="0">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-XXL-7798196830430-product-recommendations" data-original-title="XXL">XXL</label>
                                </div>
                            
                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size xxxl available" data-value="XXXL">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7798196830430-product-recommendations" data-index="option1" value="XXXL" id="SingleOptionSelector0-XXXL-7798196830430-product-recommendations" tabindex="0">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-XXXL-7798196830430-product-recommendations" data-original-title="XXXL">XXXL</label>
                                </div>
                            
                        
                    </div>
                
            
        

        <select name="id" id="ProductSelect-7798196830430-product-recommendations" class="product-form__variants no-js" tabindex="0">
            
                
                    <option value="43400766980318">
                        XS
                    </option>
                
            
                
                    <option value="43400767013086">
                        S
                    </option>
                
            
                
                    <option value="43400767045854">
                        M
                    </option>
                
            
                
                    <option value="43400767078622">
                        L
                    </option>
                
            
                
                    <option value="43400767111390">
                        XL
                    </option>
                
            
                
                    <option value="43400767144158">
                        XXL
                    </option>
                
            
                
                    <option value="43400767176926">
                        XXXL
                    </option>
                
            
        </select>
        <div class="product-card__button_cancel_mobile">
            <a class="btn btn-cancel" data-cancel-swatch-qs="" tabindex="0"><svg aria-hidden="true" data-prefix="fal" data-icon="times" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" class="svg-inline--fa fa-times fa-w-10 fa-2x"><path fill="currentColor" d="M193.94 256L296.5 153.44l21.15-21.15c3.12-3.12 3.12-8.19 0-11.31l-22.63-22.63c-3.12-3.12-8.19-3.12-11.31 0L160 222.06 36.29 98.34c-3.12-3.12-8.19-3.12-11.31 0L2.34 120.97c-3.12 3.12-3.12 8.19 0 11.31L126.06 256 2.34 379.71c-3.12 3.12-3.12 8.19 0 11.31l22.63 22.63c3.12 3.12 8.19 3.12 11.31 0L160 289.94 262.56 392.5l21.15 21.15c3.12 3.12 8.19 3.12 11.31 0l22.63-22.63c3.12-3.12 3.12-8.19 0-11.31L193.94 256z" class=""></path></svg></a>
        </div>
        <div class="product-card__button2">
            <input type="hidden" name="quantity" value="1" tabindex="0">
            <button data-btn-addtocart="" class="btn add-to-cart-btn" type="submit" data-form-id="#swatch-grid-product-form--7798196830430-product-recommendations-" tabindex="0">
                Submit
            </button>
            <a class="btn btn-cancel" data-cancel-swatch-qs="" tabindex="0">Cancel</a>
        </div>
        
    </form>
</div>
          </div>
      
  

        </div>
        
      </div>
    </div>

      <div class="wrapper-size">
        
      </div>
      
    </div>

    
     
    <div class="product-details">
      

      <a class="product-title " href="/products/207-31?pr_prod_strat=copurchase&amp;pr_rec_id=e3a9801a1&amp;pr_rec_pid=7798196830430&amp;pr_ref_pid=7797834973406&amp;pr_seq=uniform" tabindex="0">
        
<span>The Innocent Baby Blue Cotton Kurta</span>

        
          
        
      </a>

      
      <span class="spr-badge" id="spr_badge_7798196830430" data-rating="4.666666666666667"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star" aria-hidden="true"></i><i class="spr-icon spr-icon-star" aria-hidden="true"></i><i class="spr-icon spr-icon-star" aria-hidden="true"></i><i class="spr-icon spr-icon-star" aria-hidden="true"></i><i class="spr-icon spr-icon-star" aria-hidden="true"></i></span><span class="spr-badge-caption">3 reviews</span>
</span>

      

      <div class="short-description">
        
        <div>
          This baby blue A-line cotton kurta with gathers in the front. Full sleeved and v necked, with piping on the sleeves and neck, it is an elegant summer kurta with a yoke on the front. This product includes the kurta...
        </div>
      </div>

      <div class="price-box">
        
        <div class="price-sale">
          
            <span class="old-price" data-compare-price-grid=""><span class="money">₹2,981.00</span></span>
            <span class="special-price" data-price-grid=""><span class="money">₹2,087.00</span></span>
          
        </div>
        
      </div>

      

      <div class="wrapper-button-card">
        
        <div class="action">
          


<form action="/cart/add" method="post" class="variants" id="list-product-form-7798196830430-product-recommendations" data-id="product-actions-7798196830430" enctype="multipart/form-data">
  
  

    
      
        <a class="rais-svgcart" data-href="/products/207-31?pr_prod_strat=copurchase&amp;pr_rec_id=e3a9801a1&amp;pr_rec_pid=7798196830430&amp;pr_ref_pid=7797834973406&amp;pr_seq=uniform" title="The Innocent Baby Blue Cotton Kurta" data-init-quickshop="" tabindex="0">
        <svg width="27px" height="27px" viewBox="0 0 27 27" version="1.1" xmlns="http://www.w3.org/2000/svg"><g id="Tjori-" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g id="PLP" transform="translate(-361.000000, -962.000000)"><g id="Product-Widget-Copy" transform="translate(18.000000, 646.000000)"><g id="Product-Widget"><g id="Shopping-Bag-Icon" transform="translate(343.000000, 316.000000)"><path d="M21.6999607,6.00003576 L26.1999499,6.00003576 L26.1999499,20.2500018 C26.1999499,21.2812493 25.8327635,22.1640595 25.0983901,22.898433 C24.3640166,23.6328065 23.4812064,23.9999928 22.4499589,23.9999928 L8.94999106,23.9999928 C7.91874352,23.9999928 7.03593336,23.6328065 6.30155987,22.898433 C5.56718639,22.1640595 5.2,21.2812493 5.2,20.2500018 L5.2,6.00003576 L9.69998927,6.00003576 C9.69998927,4.90628861 9.96561388,3.89847851 10.4968624,2.97660547 C11.0281109,2.05473243 11.7546716,1.32817166 12.6765447,0.796923168 C13.5984177,0.265674673 14.6062278,5.00679016e-05 15.699975,5.00679016e-05 C16.7937221,5.00679016e-05 17.8015322,0.265674673 18.7234053,0.796923168 C19.6452783,1.32817166 20.3718391,2.05473243 20.9030876,2.97660547 C21.4343361,3.89847851 21.6999607,4.90628861 21.6999607,6.00003576 Z M15.699975,1.50004649 C14.4499777,1.50004649 13.3874807,1.93754521 12.5124826,2.81254336 C11.6374844,3.68754151 11.1999857,4.7500385 11.1999857,6.00003576 L20.1999642,6.00003576 C20.1999642,4.7500385 19.7624655,3.68754151 18.8874674,2.81254336 C18.0124692,1.93754521 16.9499722,1.50004649 15.699975,1.50004649 Z M24.6999535,20.2500018 L24.6999535,7.50003219 L21.6999607,7.50003219 L21.6999607,9.75002682 C21.6999607,9.96877654 21.6296483,10.1484634 21.4890237,10.289088 C21.348399,10.4297127 21.1687122,10.500025 20.9499624,10.500025 C20.7312127,10.500025 20.5515259,10.4297127 20.4109012,10.289088 C20.2702766,10.1484634 20.1999642,9.96877654 20.1999642,9.75002682 L20.1999642,7.50003219 L11.1999857,7.50003219 L11.1999857,9.75002682 C11.1999857,9.96877654 11.1296734,10.1484634 10.9890487,10.289088 C10.848424,10.4297127 10.6687372,10.500025 10.4499875,10.500025 C10.2312378,10.500025 10.0515509,10.4297127 9.91092627,10.289088 C9.7703016,10.1484634 9.69998927,9.96877654 9.69998927,9.75002682 L9.69998927,7.50003219 L6.69999642,7.50003219 L6.69999642,20.2500018 C6.69999642,20.8750001 6.91874614,21.4062493 7.35624486,21.843748 C7.79374358,22.2812467 8.32499279,22.4999964 8.94999106,22.4999964 L22.4499589,22.4999964 C23.0749571,22.4999964 23.6062064,22.2812467 24.0437051,21.843748 C24.4812038,21.4062493 24.6999535,20.8750001 24.6999535,20.2500018 Z" id="shopping-bag" fill="#34302B"></path><circle id="Oval" fill="#FFFFFF" cx="6.5" cy="20.5" r="5.5"></circle><path d="M6.34998569,14.6875246 C7.39685832,14.6875246 8.36560589,14.9492428 9.25622877,15.472679 C10.1468516,15.9961151 10.8538813,16.7031448 11.3773175,17.5937677 C11.9007536,18.4843905 12.1624718,19.4531381 12.1624718,20.5000107 C12.1624718,21.5468834 11.9007536,22.5156309 11.3773175,23.4062538 C10.8538813,24.2968767 10.1468516,25.0039064 9.25622877,25.5273425 C8.36560589,26.0507786 7.39685832,26.3124969 6.34998569,26.3124969 C5.30311307,26.3124969 4.3343655,26.0507786 3.44374262,25.5273425 C2.55311975,25.0039064 1.84609006,24.2968767 1.32265393,23.4062538 C0.799217798,22.5156309 0.537499553,21.5468834 0.537499553,20.5000107 C0.537499553,19.4531381 0.799217798,18.4843905 1.32265393,17.5937677 C1.84609006,16.7031448 2.55311975,15.9961151 3.44374262,15.472679 C4.3343655,14.9492428 5.30311307,14.6875246 6.34998569,14.6875246 Z M9.72497765,21.1562592 L9.72497765,19.8437623 C9.72497765,19.7656376 9.69763384,19.6992313 9.64294659,19.644544 C9.58825934,19.5898568 9.52185301,19.562513 9.44372832,19.562513 L7.28748346,19.562513 L7.28748346,17.4062681 C7.28748346,17.3281434 7.26013966,17.2617371 7.20545241,17.2070498 C7.15076515,17.1523626 7.08435882,17.1250188 7.00623413,17.1250188 L5.69373726,17.1250188 C5.61561256,17.1250188 5.54920623,17.1523626 5.49451898,17.2070498 C5.43983173,17.2617371 5.41248793,17.3281434 5.41248793,17.4062681 L5.41248793,19.562513 L3.25624307,19.562513 C3.17811838,19.562513 3.11171205,19.5898568 3.0570248,19.644544 C3.00233755,19.6992313 2.97499374,19.7656376 2.97499374,19.8437623 L2.97499374,21.1562592 C2.97499374,21.2343839 3.00233755,21.3007902 3.0570248,21.3554774 C3.11171205,21.4101647 3.17811838,21.4375085 3.25624307,21.4375085 L5.41248793,21.4375085 L5.41248793,23.5937534 C5.41248793,23.671878 5.43983173,23.7382844 5.49451898,23.7929716 C5.54920623,23.8476589 5.61561256,23.8750027 5.69373726,23.8750027 L7.00623413,23.8750027 C7.08435882,23.8750027 7.15076515,23.8476589 7.20545241,23.7929716 C7.26013966,23.7382844 7.28748346,23.671878 7.28748346,23.5937534 L7.28748346,21.4375085 L9.44372832,21.4375085 C9.52185301,21.4375085 9.58825934,21.4101647 9.64294659,21.3554774 C9.69763384,21.3007902 9.72497765,21.2343839 9.72497765,21.1562592 Z" id="plus-circle" fill="#C0B394"></path></g></g></g></g></g></svg>
        </a>
      
    
  
  
</form>

  
      
          <div class="product-card__variant--popup">
              <div class="product-card__variant--popup--content">
    
    <form action="/cart/add" method="post" class="variants" id="swatch-list-product-form-7798196830430-product-recommendations-list" data-id="product-actions-7798196830430" enctype="multipart/form-data">
        
            
            
            
                
                    <div class="selector-wrapper selector-wrapper-1 swatch js product-form__item" data-option-index="0">
                        <label class="form-label ">
                            Size: <span class="label-value-1">XS</span>
                        </label>
                        
                        
                        

                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size xs available" data-value="XS">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7798196830430-product-recommendationslist" data-index="option1" value="XS" id="SingleOptionSelector0-XS-7798196830430-product-recommendationslist" tabindex="0">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-XS-7798196830430-product-recommendationslist" data-original-title="XS">XS</label>
                                </div>
                            
                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size s available" data-value="S">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7798196830430-product-recommendationslist" data-index="option1" value="S" id="SingleOptionSelector0-S-7798196830430-product-recommendationslist" tabindex="0">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-S-7798196830430-product-recommendationslist" data-original-title="S">S</label>
                                </div>
                            
                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size m available" data-value="M">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7798196830430-product-recommendationslist" data-index="option1" value="M" id="SingleOptionSelector0-M-7798196830430-product-recommendationslist" tabindex="0">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-M-7798196830430-product-recommendationslist" data-original-title="M">M</label>
                                </div>
                            
                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size l available" data-value="L">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7798196830430-product-recommendationslist" data-index="option1" value="L" id="SingleOptionSelector0-L-7798196830430-product-recommendationslist" tabindex="0">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-L-7798196830430-product-recommendationslist" data-original-title="L">L</label>
                                </div>
                            
                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size xl available" data-value="XL">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7798196830430-product-recommendationslist" data-index="option1" value="XL" id="SingleOptionSelector0-XL-7798196830430-product-recommendationslist" tabindex="0">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-XL-7798196830430-product-recommendationslist" data-original-title="XL">XL</label>
                                </div>
                            
                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size xxl available" data-value="XXL">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7798196830430-product-recommendationslist" data-index="option1" value="XXL" id="SingleOptionSelector0-XXL-7798196830430-product-recommendationslist" tabindex="0">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-XXL-7798196830430-product-recommendationslist" data-original-title="XXL">XXL</label>
                                </div>
                            
                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size xxxl available" data-value="XXXL">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7798196830430-product-recommendationslist" data-index="option1" value="XXXL" id="SingleOptionSelector0-XXXL-7798196830430-product-recommendationslist" tabindex="0">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-XXXL-7798196830430-product-recommendationslist" data-original-title="XXXL">XXXL</label>
                                </div>
                            
                        
                    </div>
                
            
        

        <select name="id" id="ProductSelect-7798196830430-product-recommendationslist" class="product-form__variants no-js" tabindex="0">
            
                
                    <option value="43400766980318">
                        XS
                    </option>
                
            
                
                    <option value="43400767013086">
                        S
                    </option>
                
            
                
                    <option value="43400767045854">
                        M
                    </option>
                
            
                
                    <option value="43400767078622">
                        L
                    </option>
                
            
                
                    <option value="43400767111390">
                        XL
                    </option>
                
            
                
                    <option value="43400767144158">
                        XXL
                    </option>
                
            
                
                    <option value="43400767176926">
                        XXXL
                    </option>
                
            
        </select>
        <div class="product-card__button_cancel_mobile">
            <a class="btn btn-cancel" data-cancel-swatch-qs="" tabindex="0"><svg aria-hidden="true" data-prefix="fal" data-icon="times" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" class="svg-inline--fa fa-times fa-w-10 fa-2x"><path fill="currentColor" d="M193.94 256L296.5 153.44l21.15-21.15c3.12-3.12 3.12-8.19 0-11.31l-22.63-22.63c-3.12-3.12-8.19-3.12-11.31 0L160 222.06 36.29 98.34c-3.12-3.12-8.19-3.12-11.31 0L2.34 120.97c-3.12 3.12-3.12 8.19 0 11.31L126.06 256 2.34 379.71c-3.12 3.12-3.12 8.19 0 11.31l22.63 22.63c3.12 3.12 8.19 3.12 11.31 0L160 289.94 262.56 392.5l21.15 21.15c3.12 3.12 8.19 3.12 11.31 0l22.63-22.63c3.12-3.12 3.12-8.19 0-11.31L193.94 256z" class=""></path></svg></a>
        </div>
        <div class="product-card__button2">
            <input type="hidden" name="quantity" value="1" tabindex="0">
            <button data-btn-addtocart="" class="btn add-to-cart-btn" type="submit" data-form-id="#swatch-list-product-form-7798196830430-product-recommendations-list" tabindex="0">
                Submit
            </button>
            <a class="btn btn-cancel" data-cancel-swatch-qs="" tabindex="0">Cancel</a>
        </div>
        
    </form>
</div>
          </div>
      
  

        </div>
        
      </div>
      
    </div>
    
  </div>
</div>

          
        </div><div class="grid-item small--one-half medium-up--one-quarter col-12 slick-slide" style="width: 173px;" tabindex="-1" role="option" aria-describedby="slick-slide22" data-slick-index="2" aria-hidden="true">
          
            
<div class="inner product-item on-sale" data-product-id="product-7798003106014" data-json-product="{&quot;id&quot;: 7798003106014,&quot;handle&quot;: &quot;set-of-2-off-white-and-golden-kurta-with-palazzo&quot;,&quot;media&quot;: [{&quot;alt&quot;:null,&quot;id&quot;:30633425469662,&quot;position&quot;:1,&quot;preview_image&quot;:{&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:2360,&quot;width&quot;:1600,&quot;src&quot;:&quot;https:\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/TJ-MK-246-31_1.jpg?v=1663264095&quot;},&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:2360,&quot;media_type&quot;:&quot;image&quot;,&quot;src&quot;:&quot;https:\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/TJ-MK-246-31_1.jpg?v=1663264095&quot;,&quot;width&quot;:1600},{&quot;alt&quot;:null,&quot;id&quot;:30633425535198,&quot;position&quot;:2,&quot;preview_image&quot;:{&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:2360,&quot;width&quot;:1600,&quot;src&quot;:&quot;https:\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/TJ-MK-246-31_2.jpg?v=1663264095&quot;},&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:2360,&quot;media_type&quot;:&quot;image&quot;,&quot;src&quot;:&quot;https:\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/TJ-MK-246-31_2.jpg?v=1663264095&quot;,&quot;width&quot;:1600},{&quot;alt&quot;:null,&quot;id&quot;:30633425567966,&quot;position&quot;:3,&quot;preview_image&quot;:{&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:2360,&quot;width&quot;:1600,&quot;src&quot;:&quot;https:\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/TJ-MK-246-31_3.jpg?v=1663264095&quot;},&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:2360,&quot;media_type&quot;:&quot;image&quot;,&quot;src&quot;:&quot;https:\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/TJ-MK-246-31_3.jpg?v=1663264095&quot;,&quot;width&quot;:1600},{&quot;alt&quot;:null,&quot;id&quot;:30633425600734,&quot;position&quot;:4,&quot;preview_image&quot;:{&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:2360,&quot;width&quot;:1600,&quot;src&quot;:&quot;https:\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/TJ-MK-246-31_4.jpg?v=1663264095&quot;},&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:2360,&quot;media_type&quot;:&quot;image&quot;,&quot;src&quot;:&quot;https:\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/TJ-MK-246-31_4.jpg?v=1663264095&quot;,&quot;width&quot;:1600},{&quot;alt&quot;:null,&quot;id&quot;:30633425633502,&quot;position&quot;:5,&quot;preview_image&quot;:{&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:2360,&quot;width&quot;:1600,&quot;src&quot;:&quot;https:\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/TJ-MK-246-31_5.jpg?v=1663264095&quot;},&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:2360,&quot;media_type&quot;:&quot;image&quot;,&quot;src&quot;:&quot;https:\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/TJ-MK-246-31_5.jpg?v=1663264095&quot;,&quot;width&quot;:1600}],&quot;variants&quot;: [{&quot;id&quot;:43400071971038,&quot;title&quot;:&quot;XS&quot;,&quot;option1&quot;:&quot;XS&quot;,&quot;option2&quot;:null,&quot;option3&quot;:null,&quot;sku&quot;:&quot;TJ-MK-246-31-XS&quot;,&quot;requires_shipping&quot;:true,&quot;taxable&quot;:true,&quot;featured_image&quot;:null,&quot;available&quot;:true,&quot;name&quot;:&quot;Set of 2- Off White and Golden Kurta with Palazzo - XS&quot;,&quot;public_title&quot;:&quot;XS&quot;,&quot;options&quot;:[&quot;XS&quot;],&quot;price&quot;:556600,&quot;weight&quot;:0,&quot;compare_at_price&quot;:795200,&quot;inventory_management&quot;:&quot;shopify&quot;,&quot;barcode&quot;:null,&quot;quantity_rule&quot;:{&quot;min&quot;:1,&quot;max&quot;:null,&quot;increment&quot;:1}},{&quot;id&quot;:43400072003806,&quot;title&quot;:&quot;S&quot;,&quot;option1&quot;:&quot;S&quot;,&quot;option2&quot;:null,&quot;option3&quot;:null,&quot;sku&quot;:&quot;TJ-MK-246-31-S&quot;,&quot;requires_shipping&quot;:true,&quot;taxable&quot;:true,&quot;featured_image&quot;:null,&quot;available&quot;:true,&quot;name&quot;:&quot;Set of 2- Off White and Golden Kurta with Palazzo - S&quot;,&quot;public_title&quot;:&quot;S&quot;,&quot;options&quot;:[&quot;S&quot;],&quot;price&quot;:556600,&quot;weight&quot;:0,&quot;compare_at_price&quot;:795200,&quot;inventory_management&quot;:&quot;shopify&quot;,&quot;barcode&quot;:null,&quot;quantity_rule&quot;:{&quot;min&quot;:1,&quot;max&quot;:null,&quot;increment&quot;:1}},{&quot;id&quot;:43400072036574,&quot;title&quot;:&quot;M&quot;,&quot;option1&quot;:&quot;M&quot;,&quot;option2&quot;:null,&quot;option3&quot;:null,&quot;sku&quot;:&quot;TJ-MK-246-31-M&quot;,&quot;requires_shipping&quot;:true,&quot;taxable&quot;:true,&quot;featured_image&quot;:null,&quot;available&quot;:true,&quot;name&quot;:&quot;Set of 2- Off White and Golden Kurta with Palazzo - M&quot;,&quot;public_title&quot;:&quot;M&quot;,&quot;options&quot;:[&quot;M&quot;],&quot;price&quot;:556600,&quot;weight&quot;:0,&quot;compare_at_price&quot;:795200,&quot;inventory_management&quot;:&quot;shopify&quot;,&quot;barcode&quot;:null,&quot;quantity_rule&quot;:{&quot;min&quot;:1,&quot;max&quot;:null,&quot;increment&quot;:1}},{&quot;id&quot;:43400072069342,&quot;title&quot;:&quot;L&quot;,&quot;option1&quot;:&quot;L&quot;,&quot;option2&quot;:null,&quot;option3&quot;:null,&quot;sku&quot;:&quot;TJ-MK-246-31-L&quot;,&quot;requires_shipping&quot;:true,&quot;taxable&quot;:true,&quot;featured_image&quot;:null,&quot;available&quot;:true,&quot;name&quot;:&quot;Set of 2- Off White and Golden Kurta with Palazzo - L&quot;,&quot;public_title&quot;:&quot;L&quot;,&quot;options&quot;:[&quot;L&quot;],&quot;price&quot;:556600,&quot;weight&quot;:0,&quot;compare_at_price&quot;:795200,&quot;inventory_management&quot;:&quot;shopify&quot;,&quot;barcode&quot;:null,&quot;quantity_rule&quot;:{&quot;min&quot;:1,&quot;max&quot;:null,&quot;increment&quot;:1}},{&quot;id&quot;:43400072102110,&quot;title&quot;:&quot;XL&quot;,&quot;option1&quot;:&quot;XL&quot;,&quot;option2&quot;:null,&quot;option3&quot;:null,&quot;sku&quot;:&quot;TJ-MK-246-31-XL&quot;,&quot;requires_shipping&quot;:true,&quot;taxable&quot;:true,&quot;featured_image&quot;:null,&quot;available&quot;:true,&quot;name&quot;:&quot;Set of 2- Off White and Golden Kurta with Palazzo - XL&quot;,&quot;public_title&quot;:&quot;XL&quot;,&quot;options&quot;:[&quot;XL&quot;],&quot;price&quot;:556600,&quot;weight&quot;:0,&quot;compare_at_price&quot;:795200,&quot;inventory_management&quot;:&quot;shopify&quot;,&quot;barcode&quot;:null,&quot;quantity_rule&quot;:{&quot;min&quot;:1,&quot;max&quot;:null,&quot;increment&quot;:1}},{&quot;id&quot;:43400072134878,&quot;title&quot;:&quot;XXL&quot;,&quot;option1&quot;:&quot;XXL&quot;,&quot;option2&quot;:null,&quot;option3&quot;:null,&quot;sku&quot;:&quot;TJ-MK-246-31-XXL&quot;,&quot;requires_shipping&quot;:true,&quot;taxable&quot;:true,&quot;featured_image&quot;:null,&quot;available&quot;:true,&quot;name&quot;:&quot;Set of 2- Off White and Golden Kurta with Palazzo - XXL&quot;,&quot;public_title&quot;:&quot;XXL&quot;,&quot;options&quot;:[&quot;XXL&quot;],&quot;price&quot;:556600,&quot;weight&quot;:0,&quot;compare_at_price&quot;:795200,&quot;inventory_management&quot;:&quot;shopify&quot;,&quot;barcode&quot;:null,&quot;quantity_rule&quot;:{&quot;min&quot;:1,&quot;max&quot;:null,&quot;increment&quot;:1}},{&quot;id&quot;:43400072167646,&quot;title&quot;:&quot;XXXL&quot;,&quot;option1&quot;:&quot;XXXL&quot;,&quot;option2&quot;:null,&quot;option3&quot;:null,&quot;sku&quot;:&quot;TJ-MK-246-31-XXXL&quot;,&quot;requires_shipping&quot;:true,&quot;taxable&quot;:true,&quot;featured_image&quot;:null,&quot;available&quot;:true,&quot;name&quot;:&quot;Set of 2- Off White and Golden Kurta with Palazzo - XXXL&quot;,&quot;public_title&quot;:&quot;XXXL&quot;,&quot;options&quot;:[&quot;XXXL&quot;],&quot;price&quot;:556600,&quot;weight&quot;:0,&quot;compare_at_price&quot;:795200,&quot;inventory_management&quot;:&quot;shopify&quot;,&quot;barcode&quot;:null,&quot;quantity_rule&quot;:{&quot;min&quot;:1,&quot;max&quot;:null,&quot;increment&quot;:1}}]}">
  <div class="inner-top">
    <div class="product-top">
      <div class="product-image image-swap">
        <a href="/products/set-of-2-off-white-and-golden-kurta-with-palazzo?pr_prod_strat=copurchase&amp;pr_rec_id=e3a9801a1&amp;pr_rec_pid=7798003106014&amp;pr_ref_pid=7797834973406&amp;pr_seq=uniform" class="product-grid-image adaptive_height" data-collections-related="/collections/?view=related" style="padding-top: 147.5%" tabindex="-1">
          



  <picture data-index="0">
    <source class="image-source-1" data-srcset="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-246-31_1_300x.jpg?v=1663264095" media="(max-width: 767px)" sizes="153px" srcset="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-246-31_1_300x.jpg?v=1663264095">
    <source class="image-source-2" data-srcset=" //cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-246-31_1_360x.jpg?v=1663264095 360w,
                          //cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-246-31_1_540x.jpg?v=1663264095 540w,
                          //cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-246-31_1_720x.jpg?v=1663264095 720w,
                          //cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-246-31_1_900x.jpg?v=1663264095 900w" media="(min-width: 768px)" sizes="153px" srcset=" //cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-246-31_1_360x.jpg?v=1663264095 360w,
                          //cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-246-31_1_540x.jpg?v=1663264095 540w,
                          //cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-246-31_1_720x.jpg?v=1663264095 720w,
                          //cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-246-31_1_900x.jpg?v=1663264095 900w">

    <img alt="Set of 2- Off White and Golden Kurta with Palazzo" class="images-one lazyautosizes lazyloaded" data-widths="[180, 360, 540, 720, 900, 1080, 1296, 1512, 1728, 2048]" data-aspectratio="0.6779661016949152" data-sizes="auto" data-srcset="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-246-31_1_360x.jpg?v=1663264095 360w,
                    //cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-246-31_1_540x.jpg?v=1663264095 540w,
                    //cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-246-31_1_720x.jpg?v=1663264095 720w,
                    //cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-246-31_1_900x.jpg?v=1663264095 900w" data-image="" sizes="153px" srcset="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-246-31_1_360x.jpg?v=1663264095 360w,
                    //cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-246-31_1_540x.jpg?v=1663264095 540w,
                    //cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-246-31_1_720x.jpg?v=1663264095 720w,
                    //cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-246-31_1_900x.jpg?v=1663264095 900w">
  </picture>
  <span class="images-two">
    
    <picture data-index="1">
      <source data-srcset="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-246-31_2_300x.jpg?v=1663264095" media="(max-width: 767px)" sizes="153px" srcset="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-246-31_2_300x.jpg?v=1663264095">
      <source data-srcset="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-246-31_2_360x.jpg?v=1663264095 360w,
                            //cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-246-31_2_540x.jpg?v=1663264095 540w,
                            //cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-246-31_2_720x.jpg?v=1663264095 720w,
                            //cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-246-31_2_900x.jpg?v=1663264095 900w" media="(min-width: 768px)" sizes="153px" srcset="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-246-31_2_360x.jpg?v=1663264095 360w,
                            //cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-246-31_2_540x.jpg?v=1663264095 540w,
                            //cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-246-31_2_720x.jpg?v=1663264095 720w,
                            //cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-246-31_2_900x.jpg?v=1663264095 900w">

      <img alt="Set of 2- Off White and Golden Kurta with Palazzo" class="lazyautosizes lazyloaded" data-widths="[180, 360, 540, 720, 900, 1080, 1296, 1512, 1728, 2048]" data-aspectratio="0.6779661016949152" data-sizes="auto" data-srcset="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-246-31_2_360x.jpg?v=1663264095 360w,
                        //cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-246-31_2_540x.jpg?v=1663264095 540w,
                        //cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-246-31_2_720x.jpg?v=1663264095 720w,
                        //cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-246-31_2_900x.jpg?v=1663264095 900w" data-image="" sizes="153px" srcset="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-246-31_2_360x.jpg?v=1663264095 360w,
                        //cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-246-31_2_540x.jpg?v=1663264095 540w,
                        //cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-246-31_2_720x.jpg?v=1663264095 720w,
                        //cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-246-31_2_900x.jpg?v=1663264095 900w">
    </picture>
    
  </span>

        </a>
      </div>

      
  
  <div class="product-label" data-label-new-number="30">
    

    
      
        <strong class="label sale-label">
          Sale
        </strong>
        
      
    

    
    
    

    
    
    
  </div>
  






      
<a class="wishlist " data-icon-wishlist="" aria-label="Wish Lists" href="#" data-product-handle="set-of-2-off-white-and-golden-kurta-with-palazzo" data-id="7798003106014" tabindex="-1">
	    	<svg viewBox="0 0 512 512">
<g>
<g>
  <path d="M474.644,74.27C449.391,45.616,414.358,29.836,376,29.836c-53.948,0-88.103,32.22-107.255,59.25
    c-4.969,7.014-9.196,14.047-12.745,20.665c-3.549-6.618-7.775-13.651-12.745-20.665c-19.152-27.03-53.307-59.25-107.255-59.25
    c-38.358,0-73.391,15.781-98.645,44.435C13.267,101.605,0,138.213,0,177.351c0,42.603,16.633,82.228,52.345,124.7
    c31.917,37.96,77.834,77.088,131.005,122.397c19.813,16.884,40.302,34.344,62.115,53.429l0.655,0.574
    c2.828,2.476,6.354,3.713,9.88,3.713s7.052-1.238,9.88-3.713l0.655-0.574c21.813-19.085,42.302-36.544,62.118-53.431
    c53.168-45.306,99.085-84.434,131.002-122.395C495.367,259.578,512,219.954,512,177.351
    C512,138.213,498.733,101.605,474.644,74.27z M309.193,401.614c-17.08,14.554-34.658,29.533-53.193,45.646
    c-18.534-16.111-36.113-31.091-53.196-45.648C98.745,312.939,30,254.358,30,177.351c0-31.83,10.605-61.394,29.862-83.245
    C79.34,72.007,106.379,59.836,136,59.836c41.129,0,67.716,25.338,82.776,46.594c13.509,19.064,20.558,38.282,22.962,45.659
    c2.011,6.175,7.768,10.354,14.262,10.354c6.494,0,12.251-4.179,14.262-10.354c2.404-7.377,9.453-26.595,22.962-45.66
    c15.06-21.255,41.647-46.593,82.776-46.593c29.621,0,56.66,12.171,76.137,34.27C471.395,115.957,482,145.521,482,177.351
    C482,254.358,413.255,312.939,309.193,401.614z"></path>
</g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
</svg>
		
</a>
 


      
        <a class="quickview-button" href="javascript:void(0)" id="set-of-2-off-white-and-golden-kurta-with-palazzo" title="Quick View" tabindex="-1">
          <span>Quick view</span>
          <svg viewBox="0 0 511.999 511.999" width="100%" height="100%">
    <path d="M508.745,246.041c-4.574-6.257-113.557-153.206-252.748-153.206S7.818,239.784,3.249,246.035         c-4.332,5.936-4.332,13.987,0,19.923c4.569,6.257,113.557,153.206,252.748,153.206s248.174-146.95,252.748-153.201         C513.083,260.028,513.083,251.971,508.745,246.041z M255.997,385.406c-102.529,0-191.33-97.533-217.617-129.418         c26.253-31.913,114.868-129.395,217.617-129.395c102.524,0,191.319,97.516,217.617,129.418         C447.361,287.923,358.746,385.406,255.997,385.406z"></path>
      <path d="M255.997,154.725c-55.842,0-101.275,45.433-101.275,101.275s45.433,101.275,101.275,101.275         s101.275-45.433,101.275-101.275S311.839,154.725,255.997,154.725z M255.997,323.516c-37.23,0-67.516-30.287-67.516-67.516         s30.287-67.516,67.516-67.516s67.516,30.287,67.516,67.516S293.227,323.516,255.997,323.516z"></path>
</svg>
        </a>
      

      

    
    <div class="product-bottom">
      
      
        <span class="spr-badge" id="spr_badge_7798003106014" data-rating="0.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star-empty" aria-hidden="true"></i><i class="spr-icon spr-icon-star-empty" aria-hidden="true"></i><i class="spr-icon spr-icon-star-empty" aria-hidden="true"></i><i class="spr-icon spr-icon-star-empty" aria-hidden="true"></i><i class="spr-icon spr-icon-star-empty" aria-hidden="true"></i></span><span class="spr-badge-caption">No reviews</span>
</span>

      
      <a class="product-title " href="/products/set-of-2-off-white-and-golden-kurta-with-palazzo?pr_prod_strat=copurchase&amp;pr_rec_id=e3a9801a1&amp;pr_rec_pid=7798003106014&amp;pr_ref_pid=7797834973406&amp;pr_seq=uniform" tabindex="-1">
        
<span>Set of 2- Off White and Golden Kurta with Palazzo</span>

        
          
          
      </a>
      <div class="half-grid-rais">
      <div class="price-box">
        
        <div class="price-sale">
          
            <span class="special-price" data-price-grid=""><span class="money">₹5566.00</span></span>
            <span class="old-price" data-compare-price-grid="">7952.00</span>
            <span class="percentage-price">(30%)</span>
         
        </div>
        
      </div>
    </div>

      <div class="add-to-cart-grid-rais">
        
        <div class="action">
          


<form action="/cart/add" method="post" class="variants" id="grid-product-form--7798003106014-product-recommendations" data-id="product-actions-7798003106014" enctype="multipart/form-data">
  
  

    
      
        <a class="rais-svgcart" data-href="/products/set-of-2-off-white-and-golden-kurta-with-palazzo?pr_prod_strat=copurchase&amp;pr_rec_id=e3a9801a1&amp;pr_rec_pid=7798003106014&amp;pr_ref_pid=7797834973406&amp;pr_seq=uniform" title="Set of 2- Off White and Golden Kurta with Palazzo" data-init-quickshop="" tabindex="-1">
        <svg width="27px" height="27px" viewBox="0 0 27 27" version="1.1" xmlns="http://www.w3.org/2000/svg"><g id="Tjori-" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g id="PLP" transform="translate(-361.000000, -962.000000)"><g id="Product-Widget-Copy" transform="translate(18.000000, 646.000000)"><g id="Product-Widget"><g id="Shopping-Bag-Icon" transform="translate(343.000000, 316.000000)"><path d="M21.6999607,6.00003576 L26.1999499,6.00003576 L26.1999499,20.2500018 C26.1999499,21.2812493 25.8327635,22.1640595 25.0983901,22.898433 C24.3640166,23.6328065 23.4812064,23.9999928 22.4499589,23.9999928 L8.94999106,23.9999928 C7.91874352,23.9999928 7.03593336,23.6328065 6.30155987,22.898433 C5.56718639,22.1640595 5.2,21.2812493 5.2,20.2500018 L5.2,6.00003576 L9.69998927,6.00003576 C9.69998927,4.90628861 9.96561388,3.89847851 10.4968624,2.97660547 C11.0281109,2.05473243 11.7546716,1.32817166 12.6765447,0.796923168 C13.5984177,0.265674673 14.6062278,5.00679016e-05 15.699975,5.00679016e-05 C16.7937221,5.00679016e-05 17.8015322,0.265674673 18.7234053,0.796923168 C19.6452783,1.32817166 20.3718391,2.05473243 20.9030876,2.97660547 C21.4343361,3.89847851 21.6999607,4.90628861 21.6999607,6.00003576 Z M15.699975,1.50004649 C14.4499777,1.50004649 13.3874807,1.93754521 12.5124826,2.81254336 C11.6374844,3.68754151 11.1999857,4.7500385 11.1999857,6.00003576 L20.1999642,6.00003576 C20.1999642,4.7500385 19.7624655,3.68754151 18.8874674,2.81254336 C18.0124692,1.93754521 16.9499722,1.50004649 15.699975,1.50004649 Z M24.6999535,20.2500018 L24.6999535,7.50003219 L21.6999607,7.50003219 L21.6999607,9.75002682 C21.6999607,9.96877654 21.6296483,10.1484634 21.4890237,10.289088 C21.348399,10.4297127 21.1687122,10.500025 20.9499624,10.500025 C20.7312127,10.500025 20.5515259,10.4297127 20.4109012,10.289088 C20.2702766,10.1484634 20.1999642,9.96877654 20.1999642,9.75002682 L20.1999642,7.50003219 L11.1999857,7.50003219 L11.1999857,9.75002682 C11.1999857,9.96877654 11.1296734,10.1484634 10.9890487,10.289088 C10.848424,10.4297127 10.6687372,10.500025 10.4499875,10.500025 C10.2312378,10.500025 10.0515509,10.4297127 9.91092627,10.289088 C9.7703016,10.1484634 9.69998927,9.96877654 9.69998927,9.75002682 L9.69998927,7.50003219 L6.69999642,7.50003219 L6.69999642,20.2500018 C6.69999642,20.8750001 6.91874614,21.4062493 7.35624486,21.843748 C7.79374358,22.2812467 8.32499279,22.4999964 8.94999106,22.4999964 L22.4499589,22.4999964 C23.0749571,22.4999964 23.6062064,22.2812467 24.0437051,21.843748 C24.4812038,21.4062493 24.6999535,20.8750001 24.6999535,20.2500018 Z" id="shopping-bag" fill="#34302B"></path><circle id="Oval" fill="#FFFFFF" cx="6.5" cy="20.5" r="5.5"></circle><path d="M6.34998569,14.6875246 C7.39685832,14.6875246 8.36560589,14.9492428 9.25622877,15.472679 C10.1468516,15.9961151 10.8538813,16.7031448 11.3773175,17.5937677 C11.9007536,18.4843905 12.1624718,19.4531381 12.1624718,20.5000107 C12.1624718,21.5468834 11.9007536,22.5156309 11.3773175,23.4062538 C10.8538813,24.2968767 10.1468516,25.0039064 9.25622877,25.5273425 C8.36560589,26.0507786 7.39685832,26.3124969 6.34998569,26.3124969 C5.30311307,26.3124969 4.3343655,26.0507786 3.44374262,25.5273425 C2.55311975,25.0039064 1.84609006,24.2968767 1.32265393,23.4062538 C0.799217798,22.5156309 0.537499553,21.5468834 0.537499553,20.5000107 C0.537499553,19.4531381 0.799217798,18.4843905 1.32265393,17.5937677 C1.84609006,16.7031448 2.55311975,15.9961151 3.44374262,15.472679 C4.3343655,14.9492428 5.30311307,14.6875246 6.34998569,14.6875246 Z M9.72497765,21.1562592 L9.72497765,19.8437623 C9.72497765,19.7656376 9.69763384,19.6992313 9.64294659,19.644544 C9.58825934,19.5898568 9.52185301,19.562513 9.44372832,19.562513 L7.28748346,19.562513 L7.28748346,17.4062681 C7.28748346,17.3281434 7.26013966,17.2617371 7.20545241,17.2070498 C7.15076515,17.1523626 7.08435882,17.1250188 7.00623413,17.1250188 L5.69373726,17.1250188 C5.61561256,17.1250188 5.54920623,17.1523626 5.49451898,17.2070498 C5.43983173,17.2617371 5.41248793,17.3281434 5.41248793,17.4062681 L5.41248793,19.562513 L3.25624307,19.562513 C3.17811838,19.562513 3.11171205,19.5898568 3.0570248,19.644544 C3.00233755,19.6992313 2.97499374,19.7656376 2.97499374,19.8437623 L2.97499374,21.1562592 C2.97499374,21.2343839 3.00233755,21.3007902 3.0570248,21.3554774 C3.11171205,21.4101647 3.17811838,21.4375085 3.25624307,21.4375085 L5.41248793,21.4375085 L5.41248793,23.5937534 C5.41248793,23.671878 5.43983173,23.7382844 5.49451898,23.7929716 C5.54920623,23.8476589 5.61561256,23.8750027 5.69373726,23.8750027 L7.00623413,23.8750027 C7.08435882,23.8750027 7.15076515,23.8476589 7.20545241,23.7929716 C7.26013966,23.7382844 7.28748346,23.671878 7.28748346,23.5937534 L7.28748346,21.4375085 L9.44372832,21.4375085 C9.52185301,21.4375085 9.58825934,21.4101647 9.64294659,21.3554774 C9.69763384,21.3007902 9.72497765,21.2343839 9.72497765,21.1562592 Z" id="plus-circle" fill="#C0B394"></path></g></g></g></g></g></svg>
        </a>
      
    
  
  
</form>

  
      
          <div class="product-card__variant--popup">
              <div class="product-card__variant--popup--content">
    
    <form action="/cart/add" method="post" class="variants" id="swatch-grid-product-form--7798003106014-product-recommendations-" data-id="product-actions-7798003106014" enctype="multipart/form-data">
        
            
            
            
                
                    <div class="selector-wrapper selector-wrapper-1 swatch js product-form__item" data-option-index="0">
                        <label class="form-label ">
                            Size: <span class="label-value-1">XS</span>
                        </label>
                        
                        
                        

                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size xs available" data-value="XS">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7798003106014-product-recommendations" data-index="option1" value="XS" id="SingleOptionSelector0-XS-7798003106014-product-recommendations" tabindex="-1">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-XS-7798003106014-product-recommendations" data-original-title="XS">XS</label>
                                </div>
                            
                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size s available" data-value="S">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7798003106014-product-recommendations" data-index="option1" value="S" id="SingleOptionSelector0-S-7798003106014-product-recommendations" tabindex="-1">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-S-7798003106014-product-recommendations" data-original-title="S">S</label>
                                </div>
                            
                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size m available" data-value="M">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7798003106014-product-recommendations" data-index="option1" value="M" id="SingleOptionSelector0-M-7798003106014-product-recommendations" tabindex="-1">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-M-7798003106014-product-recommendations" data-original-title="M">M</label>
                                </div>
                            
                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size l available" data-value="L">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7798003106014-product-recommendations" data-index="option1" value="L" id="SingleOptionSelector0-L-7798003106014-product-recommendations" tabindex="-1">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-L-7798003106014-product-recommendations" data-original-title="L">L</label>
                                </div>
                            
                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size xl available" data-value="XL">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7798003106014-product-recommendations" data-index="option1" value="XL" id="SingleOptionSelector0-XL-7798003106014-product-recommendations" tabindex="-1">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-XL-7798003106014-product-recommendations" data-original-title="XL">XL</label>
                                </div>
                            
                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size xxl available" data-value="XXL">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7798003106014-product-recommendations" data-index="option1" value="XXL" id="SingleOptionSelector0-XXL-7798003106014-product-recommendations" tabindex="-1">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-XXL-7798003106014-product-recommendations" data-original-title="XXL">XXL</label>
                                </div>
                            
                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size xxxl available" data-value="XXXL">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7798003106014-product-recommendations" data-index="option1" value="XXXL" id="SingleOptionSelector0-XXXL-7798003106014-product-recommendations" tabindex="-1">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-XXXL-7798003106014-product-recommendations" data-original-title="XXXL">XXXL</label>
                                </div>
                            
                        
                    </div>
                
            
        

        <select name="id" id="ProductSelect-7798003106014-product-recommendations" class="product-form__variants no-js" tabindex="-1">
            
                
                    <option value="43400071971038">
                        XS
                    </option>
                
            
                
                    <option value="43400072003806">
                        S
                    </option>
                
            
                
                    <option value="43400072036574">
                        M
                    </option>
                
            
                
                    <option value="43400072069342">
                        L
                    </option>
                
            
                
                    <option value="43400072102110">
                        XL
                    </option>
                
            
                
                    <option value="43400072134878">
                        XXL
                    </option>
                
            
                
                    <option value="43400072167646">
                        XXXL
                    </option>
                
            
        </select>
        <div class="product-card__button_cancel_mobile">
            <a class="btn btn-cancel" data-cancel-swatch-qs="" tabindex="-1"><svg aria-hidden="true" data-prefix="fal" data-icon="times" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" class="svg-inline--fa fa-times fa-w-10 fa-2x"><path fill="currentColor" d="M193.94 256L296.5 153.44l21.15-21.15c3.12-3.12 3.12-8.19 0-11.31l-22.63-22.63c-3.12-3.12-8.19-3.12-11.31 0L160 222.06 36.29 98.34c-3.12-3.12-8.19-3.12-11.31 0L2.34 120.97c-3.12 3.12-3.12 8.19 0 11.31L126.06 256 2.34 379.71c-3.12 3.12-3.12 8.19 0 11.31l22.63 22.63c3.12 3.12 8.19 3.12 11.31 0L160 289.94 262.56 392.5l21.15 21.15c3.12 3.12 8.19 3.12 11.31 0l22.63-22.63c3.12-3.12 3.12-8.19 0-11.31L193.94 256z" class=""></path></svg></a>
        </div>
        <div class="product-card__button2">
            <input type="hidden" name="quantity" value="1" tabindex="-1">
            <button data-btn-addtocart="" class="btn add-to-cart-btn" type="submit" data-form-id="#swatch-grid-product-form--7798003106014-product-recommendations-" tabindex="-1">
                Submit
            </button>
            <a class="btn btn-cancel" data-cancel-swatch-qs="" tabindex="-1">Cancel</a>
        </div>
        
    </form>
</div>
          </div>
      
  

        </div>
        
      </div>
    </div>

      <div class="wrapper-size">
        
      </div>
      
    </div>

    
     
    <div class="product-details">
      

      <a class="product-title " href="/products/set-of-2-off-white-and-golden-kurta-with-palazzo?pr_prod_strat=copurchase&amp;pr_rec_id=e3a9801a1&amp;pr_rec_pid=7798003106014&amp;pr_ref_pid=7797834973406&amp;pr_seq=uniform" tabindex="-1">
        
<span>Set of 2- Off White and Golden Kurta with Palazzo</span>

        
          
        
      </a>

      
      <span class="spr-badge" id="spr_badge_7798003106014" data-rating="0.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star-empty" aria-hidden="true"></i><i class="spr-icon spr-icon-star-empty" aria-hidden="true"></i><i class="spr-icon spr-icon-star-empty" aria-hidden="true"></i><i class="spr-icon spr-icon-star-empty" aria-hidden="true"></i><i class="spr-icon spr-icon-star-empty" aria-hidden="true"></i></span><span class="spr-badge-caption">No reviews</span>
</span>

      

      <div class="short-description">
        
        <div>
          This set is an ideal outfit if you are looking for something elegant yet subtle outfit. This set of two includes a beautiful cotton Kurta and handblock printed flared palazzo that have been handcrafted in soft, pure cotton. The color...
        </div>
      </div>

      <div class="price-box">
        
        <div class="price-sale">
          
            <span class="old-price" data-compare-price-grid=""><span class="money">₹7,952.00</span></span>
            <span class="special-price" data-price-grid=""><span class="money">₹5,566.00</span></span>
          
        </div>
        
      </div>

      

      <div class="wrapper-button-card">
        
        <div class="action">
          


<form action="/cart/add" method="post" class="variants" id="list-product-form-7798003106014-product-recommendations" data-id="product-actions-7798003106014" enctype="multipart/form-data">
  
  

    
      
        <a class="rais-svgcart" data-href="/products/set-of-2-off-white-and-golden-kurta-with-palazzo?pr_prod_strat=copurchase&amp;pr_rec_id=e3a9801a1&amp;pr_rec_pid=7798003106014&amp;pr_ref_pid=7797834973406&amp;pr_seq=uniform" title="Set of 2- Off White and Golden Kurta with Palazzo" data-init-quickshop="" tabindex="-1">
        <svg width="27px" height="27px" viewBox="0 0 27 27" version="1.1" xmlns="http://www.w3.org/2000/svg"><g id="Tjori-" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g id="PLP" transform="translate(-361.000000, -962.000000)"><g id="Product-Widget-Copy" transform="translate(18.000000, 646.000000)"><g id="Product-Widget"><g id="Shopping-Bag-Icon" transform="translate(343.000000, 316.000000)"><path d="M21.6999607,6.00003576 L26.1999499,6.00003576 L26.1999499,20.2500018 C26.1999499,21.2812493 25.8327635,22.1640595 25.0983901,22.898433 C24.3640166,23.6328065 23.4812064,23.9999928 22.4499589,23.9999928 L8.94999106,23.9999928 C7.91874352,23.9999928 7.03593336,23.6328065 6.30155987,22.898433 C5.56718639,22.1640595 5.2,21.2812493 5.2,20.2500018 L5.2,6.00003576 L9.69998927,6.00003576 C9.69998927,4.90628861 9.96561388,3.89847851 10.4968624,2.97660547 C11.0281109,2.05473243 11.7546716,1.32817166 12.6765447,0.796923168 C13.5984177,0.265674673 14.6062278,5.00679016e-05 15.699975,5.00679016e-05 C16.7937221,5.00679016e-05 17.8015322,0.265674673 18.7234053,0.796923168 C19.6452783,1.32817166 20.3718391,2.05473243 20.9030876,2.97660547 C21.4343361,3.89847851 21.6999607,4.90628861 21.6999607,6.00003576 Z M15.699975,1.50004649 C14.4499777,1.50004649 13.3874807,1.93754521 12.5124826,2.81254336 C11.6374844,3.68754151 11.1999857,4.7500385 11.1999857,6.00003576 L20.1999642,6.00003576 C20.1999642,4.7500385 19.7624655,3.68754151 18.8874674,2.81254336 C18.0124692,1.93754521 16.9499722,1.50004649 15.699975,1.50004649 Z M24.6999535,20.2500018 L24.6999535,7.50003219 L21.6999607,7.50003219 L21.6999607,9.75002682 C21.6999607,9.96877654 21.6296483,10.1484634 21.4890237,10.289088 C21.348399,10.4297127 21.1687122,10.500025 20.9499624,10.500025 C20.7312127,10.500025 20.5515259,10.4297127 20.4109012,10.289088 C20.2702766,10.1484634 20.1999642,9.96877654 20.1999642,9.75002682 L20.1999642,7.50003219 L11.1999857,7.50003219 L11.1999857,9.75002682 C11.1999857,9.96877654 11.1296734,10.1484634 10.9890487,10.289088 C10.848424,10.4297127 10.6687372,10.500025 10.4499875,10.500025 C10.2312378,10.500025 10.0515509,10.4297127 9.91092627,10.289088 C9.7703016,10.1484634 9.69998927,9.96877654 9.69998927,9.75002682 L9.69998927,7.50003219 L6.69999642,7.50003219 L6.69999642,20.2500018 C6.69999642,20.8750001 6.91874614,21.4062493 7.35624486,21.843748 C7.79374358,22.2812467 8.32499279,22.4999964 8.94999106,22.4999964 L22.4499589,22.4999964 C23.0749571,22.4999964 23.6062064,22.2812467 24.0437051,21.843748 C24.4812038,21.4062493 24.6999535,20.8750001 24.6999535,20.2500018 Z" id="shopping-bag" fill="#34302B"></path><circle id="Oval" fill="#FFFFFF" cx="6.5" cy="20.5" r="5.5"></circle><path d="M6.34998569,14.6875246 C7.39685832,14.6875246 8.36560589,14.9492428 9.25622877,15.472679 C10.1468516,15.9961151 10.8538813,16.7031448 11.3773175,17.5937677 C11.9007536,18.4843905 12.1624718,19.4531381 12.1624718,20.5000107 C12.1624718,21.5468834 11.9007536,22.5156309 11.3773175,23.4062538 C10.8538813,24.2968767 10.1468516,25.0039064 9.25622877,25.5273425 C8.36560589,26.0507786 7.39685832,26.3124969 6.34998569,26.3124969 C5.30311307,26.3124969 4.3343655,26.0507786 3.44374262,25.5273425 C2.55311975,25.0039064 1.84609006,24.2968767 1.32265393,23.4062538 C0.799217798,22.5156309 0.537499553,21.5468834 0.537499553,20.5000107 C0.537499553,19.4531381 0.799217798,18.4843905 1.32265393,17.5937677 C1.84609006,16.7031448 2.55311975,15.9961151 3.44374262,15.472679 C4.3343655,14.9492428 5.30311307,14.6875246 6.34998569,14.6875246 Z M9.72497765,21.1562592 L9.72497765,19.8437623 C9.72497765,19.7656376 9.69763384,19.6992313 9.64294659,19.644544 C9.58825934,19.5898568 9.52185301,19.562513 9.44372832,19.562513 L7.28748346,19.562513 L7.28748346,17.4062681 C7.28748346,17.3281434 7.26013966,17.2617371 7.20545241,17.2070498 C7.15076515,17.1523626 7.08435882,17.1250188 7.00623413,17.1250188 L5.69373726,17.1250188 C5.61561256,17.1250188 5.54920623,17.1523626 5.49451898,17.2070498 C5.43983173,17.2617371 5.41248793,17.3281434 5.41248793,17.4062681 L5.41248793,19.562513 L3.25624307,19.562513 C3.17811838,19.562513 3.11171205,19.5898568 3.0570248,19.644544 C3.00233755,19.6992313 2.97499374,19.7656376 2.97499374,19.8437623 L2.97499374,21.1562592 C2.97499374,21.2343839 3.00233755,21.3007902 3.0570248,21.3554774 C3.11171205,21.4101647 3.17811838,21.4375085 3.25624307,21.4375085 L5.41248793,21.4375085 L5.41248793,23.5937534 C5.41248793,23.671878 5.43983173,23.7382844 5.49451898,23.7929716 C5.54920623,23.8476589 5.61561256,23.8750027 5.69373726,23.8750027 L7.00623413,23.8750027 C7.08435882,23.8750027 7.15076515,23.8476589 7.20545241,23.7929716 C7.26013966,23.7382844 7.28748346,23.671878 7.28748346,23.5937534 L7.28748346,21.4375085 L9.44372832,21.4375085 C9.52185301,21.4375085 9.58825934,21.4101647 9.64294659,21.3554774 C9.69763384,21.3007902 9.72497765,21.2343839 9.72497765,21.1562592 Z" id="plus-circle" fill="#C0B394"></path></g></g></g></g></g></svg>
        </a>
      
    
  
  
</form>

  
      
          <div class="product-card__variant--popup">
              <div class="product-card__variant--popup--content">
    
    <form action="/cart/add" method="post" class="variants" id="swatch-list-product-form-7798003106014-product-recommendations-list" data-id="product-actions-7798003106014" enctype="multipart/form-data">
        
            
            
            
                
                    <div class="selector-wrapper selector-wrapper-1 swatch js product-form__item" data-option-index="0">
                        <label class="form-label ">
                            Size: <span class="label-value-1">XS</span>
                        </label>
                        
                        
                        

                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size xs available" data-value="XS">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7798003106014-product-recommendationslist" data-index="option1" value="XS" id="SingleOptionSelector0-XS-7798003106014-product-recommendationslist" tabindex="-1">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-XS-7798003106014-product-recommendationslist" data-original-title="XS">XS</label>
                                </div>
                            
                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size s available" data-value="S">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7798003106014-product-recommendationslist" data-index="option1" value="S" id="SingleOptionSelector0-S-7798003106014-product-recommendationslist" tabindex="-1">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-S-7798003106014-product-recommendationslist" data-original-title="S">S</label>
                                </div>
                            
                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size m available" data-value="M">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7798003106014-product-recommendationslist" data-index="option1" value="M" id="SingleOptionSelector0-M-7798003106014-product-recommendationslist" tabindex="-1">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-M-7798003106014-product-recommendationslist" data-original-title="M">M</label>
                                </div>
                            
                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size l available" data-value="L">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7798003106014-product-recommendationslist" data-index="option1" value="L" id="SingleOptionSelector0-L-7798003106014-product-recommendationslist" tabindex="-1">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-L-7798003106014-product-recommendationslist" data-original-title="L">L</label>
                                </div>
                            
                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size xl available" data-value="XL">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7798003106014-product-recommendationslist" data-index="option1" value="XL" id="SingleOptionSelector0-XL-7798003106014-product-recommendationslist" tabindex="-1">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-XL-7798003106014-product-recommendationslist" data-original-title="XL">XL</label>
                                </div>
                            
                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size xxl available" data-value="XXL">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7798003106014-product-recommendationslist" data-index="option1" value="XXL" id="SingleOptionSelector0-XXL-7798003106014-product-recommendationslist" tabindex="-1">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-XXL-7798003106014-product-recommendationslist" data-original-title="XXL">XXL</label>
                                </div>
                            
                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size xxxl available" data-value="XXXL">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7798003106014-product-recommendationslist" data-index="option1" value="XXXL" id="SingleOptionSelector0-XXXL-7798003106014-product-recommendationslist" tabindex="-1">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-XXXL-7798003106014-product-recommendationslist" data-original-title="XXXL">XXXL</label>
                                </div>
                            
                        
                    </div>
                
            
        

        <select name="id" id="ProductSelect-7798003106014-product-recommendationslist" class="product-form__variants no-js" tabindex="-1">
            
                
                    <option value="43400071971038">
                        XS
                    </option>
                
            
                
                    <option value="43400072003806">
                        S
                    </option>
                
            
                
                    <option value="43400072036574">
                        M
                    </option>
                
            
                
                    <option value="43400072069342">
                        L
                    </option>
                
            
                
                    <option value="43400072102110">
                        XL
                    </option>
                
            
                
                    <option value="43400072134878">
                        XXL
                    </option>
                
            
                
                    <option value="43400072167646">
                        XXXL
                    </option>
                
            
        </select>
        <div class="product-card__button_cancel_mobile">
            <a class="btn btn-cancel" data-cancel-swatch-qs="" tabindex="-1"><svg aria-hidden="true" data-prefix="fal" data-icon="times" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" class="svg-inline--fa fa-times fa-w-10 fa-2x"><path fill="currentColor" d="M193.94 256L296.5 153.44l21.15-21.15c3.12-3.12 3.12-8.19 0-11.31l-22.63-22.63c-3.12-3.12-8.19-3.12-11.31 0L160 222.06 36.29 98.34c-3.12-3.12-8.19-3.12-11.31 0L2.34 120.97c-3.12 3.12-3.12 8.19 0 11.31L126.06 256 2.34 379.71c-3.12 3.12-3.12 8.19 0 11.31l22.63 22.63c3.12 3.12 8.19 3.12 11.31 0L160 289.94 262.56 392.5l21.15 21.15c3.12 3.12 8.19 3.12 11.31 0l22.63-22.63c3.12-3.12 3.12-8.19 0-11.31L193.94 256z" class=""></path></svg></a>
        </div>
        <div class="product-card__button2">
            <input type="hidden" name="quantity" value="1" tabindex="-1">
            <button data-btn-addtocart="" class="btn add-to-cart-btn" type="submit" data-form-id="#swatch-list-product-form-7798003106014-product-recommendations-list" tabindex="-1">
                Submit
            </button>
            <a class="btn btn-cancel" data-cancel-swatch-qs="" tabindex="-1">Cancel</a>
        </div>
        
    </form>
</div>
          </div>
      
  

        </div>
        
      </div>
      
    </div>
    
  </div>
</div>

          
        </div><div class="grid-item small--one-half medium-up--one-quarter col-12 slick-slide" style="width: 173px;" tabindex="-1" role="option" aria-describedby="slick-slide23" data-slick-index="3" aria-hidden="true">
          
            
<div class="inner product-item on-sale" data-product-id="product-7798219210974" data-json-product="{&quot;id&quot;: 7798219210974,&quot;handle&quot;: &quot;207-32&quot;,&quot;media&quot;: [{&quot;alt&quot;:null,&quot;id&quot;:30632458518750,&quot;position&quot;:1,&quot;preview_image&quot;:{&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:2360,&quot;width&quot;:1600,&quot;src&quot;:&quot;https:\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/276072-2019-04-30-15_28_49.581942-big-image.jpg?v=1663261255&quot;},&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:2360,&quot;media_type&quot;:&quot;image&quot;,&quot;src&quot;:&quot;https:\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/276072-2019-04-30-15_28_49.581942-big-image.jpg?v=1663261255&quot;,&quot;width&quot;:1600},{&quot;alt&quot;:null,&quot;id&quot;:30632458551518,&quot;position&quot;:2,&quot;preview_image&quot;:{&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:2360,&quot;width&quot;:1600,&quot;src&quot;:&quot;https:\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/276073-2019-04-30-15_29_24.466319-big-image.jpg?v=1663261255&quot;},&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:2360,&quot;media_type&quot;:&quot;image&quot;,&quot;src&quot;:&quot;https:\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/276073-2019-04-30-15_29_24.466319-big-image.jpg?v=1663261255&quot;,&quot;width&quot;:1600},{&quot;alt&quot;:null,&quot;id&quot;:30632458584286,&quot;position&quot;:3,&quot;preview_image&quot;:{&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:2360,&quot;width&quot;:1600,&quot;src&quot;:&quot;https:\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/276074-2019-04-30-15_29_25.987288-big-image.jpg?v=1663261255&quot;},&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:2360,&quot;media_type&quot;:&quot;image&quot;,&quot;src&quot;:&quot;https:\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/276074-2019-04-30-15_29_25.987288-big-image.jpg?v=1663261255&quot;,&quot;width&quot;:1600},{&quot;alt&quot;:null,&quot;id&quot;:30632458617054,&quot;position&quot;:4,&quot;preview_image&quot;:{&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:2360,&quot;width&quot;:1600,&quot;src&quot;:&quot;https:\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/276075-2019-04-30-15_29_27.179787-big-image.jpg?v=1663261255&quot;},&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:2360,&quot;media_type&quot;:&quot;image&quot;,&quot;src&quot;:&quot;https:\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/276075-2019-04-30-15_29_27.179787-big-image.jpg?v=1663261255&quot;,&quot;width&quot;:1600},{&quot;alt&quot;:null,&quot;id&quot;:30632458485982,&quot;position&quot;:5,&quot;preview_image&quot;:{&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:1180,&quot;width&quot;:800,&quot;src&quot;:&quot;https:\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/279899-2019-08-09-15_31_23.792813-big-image_7f1d433f-0269-4093-bb6d-42ed41a19818.jpg?v=1663261255&quot;},&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:1180,&quot;media_type&quot;:&quot;image&quot;,&quot;src&quot;:&quot;https:\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/279899-2019-08-09-15_31_23.792813-big-image_7f1d433f-0269-4093-bb6d-42ed41a19818.jpg?v=1663261255&quot;,&quot;width&quot;:800}],&quot;variants&quot;: [{&quot;id&quot;:43400834449630,&quot;title&quot;:&quot;XS&quot;,&quot;option1&quot;:&quot;XS&quot;,&quot;option2&quot;:null,&quot;option3&quot;:null,&quot;sku&quot;:&quot;TJ-MK-207-32-XS&quot;,&quot;requires_shipping&quot;:true,&quot;taxable&quot;:true,&quot;featured_image&quot;:null,&quot;available&quot;:true,&quot;name&quot;:&quot;The Easy Going Light Green Cotton Kurta - XS&quot;,&quot;public_title&quot;:&quot;XS&quot;,&quot;options&quot;:[&quot;XS&quot;],&quot;price&quot;:217300,&quot;weight&quot;:0,&quot;compare_at_price&quot;:310400,&quot;inventory_management&quot;:&quot;shopify&quot;,&quot;barcode&quot;:null,&quot;quantity_rule&quot;:{&quot;min&quot;:1,&quot;max&quot;:null,&quot;increment&quot;:1}},{&quot;id&quot;:43400834547934,&quot;title&quot;:&quot;S&quot;,&quot;option1&quot;:&quot;S&quot;,&quot;option2&quot;:null,&quot;option3&quot;:null,&quot;sku&quot;:&quot;TJ-MK-207-32-S&quot;,&quot;requires_shipping&quot;:true,&quot;taxable&quot;:true,&quot;featured_image&quot;:null,&quot;available&quot;:true,&quot;name&quot;:&quot;The Easy Going Light Green Cotton Kurta - S&quot;,&quot;public_title&quot;:&quot;S&quot;,&quot;options&quot;:[&quot;S&quot;],&quot;price&quot;:217300,&quot;weight&quot;:0,&quot;compare_at_price&quot;:310400,&quot;inventory_management&quot;:&quot;shopify&quot;,&quot;barcode&quot;:null,&quot;quantity_rule&quot;:{&quot;min&quot;:1,&quot;max&quot;:null,&quot;increment&quot;:1}},{&quot;id&quot;:43400834613470,&quot;title&quot;:&quot;M&quot;,&quot;option1&quot;:&quot;M&quot;,&quot;option2&quot;:null,&quot;option3&quot;:null,&quot;sku&quot;:&quot;TJ-MK-207-32-M&quot;,&quot;requires_shipping&quot;:true,&quot;taxable&quot;:true,&quot;featured_image&quot;:null,&quot;available&quot;:true,&quot;name&quot;:&quot;The Easy Going Light Green Cotton Kurta - M&quot;,&quot;public_title&quot;:&quot;M&quot;,&quot;options&quot;:[&quot;M&quot;],&quot;price&quot;:217300,&quot;weight&quot;:0,&quot;compare_at_price&quot;:310400,&quot;inventory_management&quot;:&quot;shopify&quot;,&quot;barcode&quot;:null,&quot;quantity_rule&quot;:{&quot;min&quot;:1,&quot;max&quot;:null,&quot;increment&quot;:1}},{&quot;id&quot;:43400834744542,&quot;title&quot;:&quot;L&quot;,&quot;option1&quot;:&quot;L&quot;,&quot;option2&quot;:null,&quot;option3&quot;:null,&quot;sku&quot;:&quot;TJ-MK-207-32-L&quot;,&quot;requires_shipping&quot;:true,&quot;taxable&quot;:true,&quot;featured_image&quot;:null,&quot;available&quot;:true,&quot;name&quot;:&quot;The Easy Going Light Green Cotton Kurta - L&quot;,&quot;public_title&quot;:&quot;L&quot;,&quot;options&quot;:[&quot;L&quot;],&quot;price&quot;:217300,&quot;weight&quot;:0,&quot;compare_at_price&quot;:310400,&quot;inventory_management&quot;:&quot;shopify&quot;,&quot;barcode&quot;:null,&quot;quantity_rule&quot;:{&quot;min&quot;:1,&quot;max&quot;:null,&quot;increment&quot;:1}},{&quot;id&quot;:43400834810078,&quot;title&quot;:&quot;XL&quot;,&quot;option1&quot;:&quot;XL&quot;,&quot;option2&quot;:null,&quot;option3&quot;:null,&quot;sku&quot;:&quot;TJ-MK-207-32-XL&quot;,&quot;requires_shipping&quot;:true,&quot;taxable&quot;:true,&quot;featured_image&quot;:null,&quot;available&quot;:true,&quot;name&quot;:&quot;The Easy Going Light Green Cotton Kurta - XL&quot;,&quot;public_title&quot;:&quot;XL&quot;,&quot;options&quot;:[&quot;XL&quot;],&quot;price&quot;:217300,&quot;weight&quot;:0,&quot;compare_at_price&quot;:310400,&quot;inventory_management&quot;:&quot;shopify&quot;,&quot;barcode&quot;:null,&quot;quantity_rule&quot;:{&quot;min&quot;:1,&quot;max&quot;:null,&quot;increment&quot;:1}},{&quot;id&quot;:43400834842846,&quot;title&quot;:&quot;XXL&quot;,&quot;option1&quot;:&quot;XXL&quot;,&quot;option2&quot;:null,&quot;option3&quot;:null,&quot;sku&quot;:&quot;TJ-MK-207-32-XXL&quot;,&quot;requires_shipping&quot;:true,&quot;taxable&quot;:true,&quot;featured_image&quot;:null,&quot;available&quot;:true,&quot;name&quot;:&quot;The Easy Going Light Green Cotton Kurta - XXL&quot;,&quot;public_title&quot;:&quot;XXL&quot;,&quot;options&quot;:[&quot;XXL&quot;],&quot;price&quot;:217300,&quot;weight&quot;:0,&quot;compare_at_price&quot;:310400,&quot;inventory_management&quot;:&quot;shopify&quot;,&quot;barcode&quot;:null,&quot;quantity_rule&quot;:{&quot;min&quot;:1,&quot;max&quot;:null,&quot;increment&quot;:1}},{&quot;id&quot;:43400834875614,&quot;title&quot;:&quot;XXXL&quot;,&quot;option1&quot;:&quot;XXXL&quot;,&quot;option2&quot;:null,&quot;option3&quot;:null,&quot;sku&quot;:&quot;TJ-MK-207-32-XXXL&quot;,&quot;requires_shipping&quot;:true,&quot;taxable&quot;:true,&quot;featured_image&quot;:null,&quot;available&quot;:true,&quot;name&quot;:&quot;The Easy Going Light Green Cotton Kurta - XXXL&quot;,&quot;public_title&quot;:&quot;XXXL&quot;,&quot;options&quot;:[&quot;XXXL&quot;],&quot;price&quot;:217300,&quot;weight&quot;:0,&quot;compare_at_price&quot;:310400,&quot;inventory_management&quot;:&quot;shopify&quot;,&quot;barcode&quot;:null,&quot;quantity_rule&quot;:{&quot;min&quot;:1,&quot;max&quot;:null,&quot;increment&quot;:1}}]}">
  <div class="inner-top">
    <div class="product-top">
      <div class="product-image image-swap">
        <a href="/products/207-32?pr_prod_strat=copurchase&amp;pr_rec_id=e3a9801a1&amp;pr_rec_pid=7798219210974&amp;pr_ref_pid=7797834973406&amp;pr_seq=uniform" class="product-grid-image adaptive_height" data-collections-related="/collections/?view=related" style="padding-top: 147.5%" tabindex="-1">
          



  <picture data-index="0">
    <source class="image-source-1" data-srcset="//cdn.shopify.com/s/files/1/0613/2039/7022/products/276072-2019-04-30-15_28_49.581942-big-image_300x.jpg?v=1663261255" media="(max-width: 767px)" sizes="153px" srcset="//cdn.shopify.com/s/files/1/0613/2039/7022/products/276072-2019-04-30-15_28_49.581942-big-image_300x.jpg?v=1663261255">
    <source class="image-source-2" data-srcset=" //cdn.shopify.com/s/files/1/0613/2039/7022/products/276072-2019-04-30-15_28_49.581942-big-image_360x.jpg?v=1663261255 360w,
                          //cdn.shopify.com/s/files/1/0613/2039/7022/products/276072-2019-04-30-15_28_49.581942-big-image_540x.jpg?v=1663261255 540w,
                          //cdn.shopify.com/s/files/1/0613/2039/7022/products/276072-2019-04-30-15_28_49.581942-big-image_720x.jpg?v=1663261255 720w,
                          //cdn.shopify.com/s/files/1/0613/2039/7022/products/276072-2019-04-30-15_28_49.581942-big-image_900x.jpg?v=1663261255 900w" media="(min-width: 768px)" sizes="153px" srcset=" //cdn.shopify.com/s/files/1/0613/2039/7022/products/276072-2019-04-30-15_28_49.581942-big-image_360x.jpg?v=1663261255 360w,
                          //cdn.shopify.com/s/files/1/0613/2039/7022/products/276072-2019-04-30-15_28_49.581942-big-image_540x.jpg?v=1663261255 540w,
                          //cdn.shopify.com/s/files/1/0613/2039/7022/products/276072-2019-04-30-15_28_49.581942-big-image_720x.jpg?v=1663261255 720w,
                          //cdn.shopify.com/s/files/1/0613/2039/7022/products/276072-2019-04-30-15_28_49.581942-big-image_900x.jpg?v=1663261255 900w">

    <img alt="The Easy Going Light Green Cotton Kurta" class="images-one lazyautosizes lazyloaded" data-widths="[180, 360, 540, 720, 900, 1080, 1296, 1512, 1728, 2048]" data-aspectratio="0.6779661016949152" data-sizes="auto" data-srcset="//cdn.shopify.com/s/files/1/0613/2039/7022/products/276072-2019-04-30-15_28_49.581942-big-image_360x.jpg?v=1663261255 360w,
                    //cdn.shopify.com/s/files/1/0613/2039/7022/products/276072-2019-04-30-15_28_49.581942-big-image_540x.jpg?v=1663261255 540w,
                    //cdn.shopify.com/s/files/1/0613/2039/7022/products/276072-2019-04-30-15_28_49.581942-big-image_720x.jpg?v=1663261255 720w,
                    //cdn.shopify.com/s/files/1/0613/2039/7022/products/276072-2019-04-30-15_28_49.581942-big-image_900x.jpg?v=1663261255 900w" data-image="" sizes="153px" srcset="//cdn.shopify.com/s/files/1/0613/2039/7022/products/276072-2019-04-30-15_28_49.581942-big-image_360x.jpg?v=1663261255 360w,
                    //cdn.shopify.com/s/files/1/0613/2039/7022/products/276072-2019-04-30-15_28_49.581942-big-image_540x.jpg?v=1663261255 540w,
                    //cdn.shopify.com/s/files/1/0613/2039/7022/products/276072-2019-04-30-15_28_49.581942-big-image_720x.jpg?v=1663261255 720w,
                    //cdn.shopify.com/s/files/1/0613/2039/7022/products/276072-2019-04-30-15_28_49.581942-big-image_900x.jpg?v=1663261255 900w">
  </picture>
  <span class="images-two">
    
    <picture data-index="1">
      <source data-srcset="//cdn.shopify.com/s/files/1/0613/2039/7022/products/276073-2019-04-30-15_29_24.466319-big-image_300x.jpg?v=1663261255" media="(max-width: 767px)" sizes="153px" srcset="//cdn.shopify.com/s/files/1/0613/2039/7022/products/276073-2019-04-30-15_29_24.466319-big-image_300x.jpg?v=1663261255">
      <source data-srcset="//cdn.shopify.com/s/files/1/0613/2039/7022/products/276073-2019-04-30-15_29_24.466319-big-image_360x.jpg?v=1663261255 360w,
                            //cdn.shopify.com/s/files/1/0613/2039/7022/products/276073-2019-04-30-15_29_24.466319-big-image_540x.jpg?v=1663261255 540w,
                            //cdn.shopify.com/s/files/1/0613/2039/7022/products/276073-2019-04-30-15_29_24.466319-big-image_720x.jpg?v=1663261255 720w,
                            //cdn.shopify.com/s/files/1/0613/2039/7022/products/276073-2019-04-30-15_29_24.466319-big-image_900x.jpg?v=1663261255 900w" media="(min-width: 768px)" sizes="153px" srcset="//cdn.shopify.com/s/files/1/0613/2039/7022/products/276073-2019-04-30-15_29_24.466319-big-image_360x.jpg?v=1663261255 360w,
                            //cdn.shopify.com/s/files/1/0613/2039/7022/products/276073-2019-04-30-15_29_24.466319-big-image_540x.jpg?v=1663261255 540w,
                            //cdn.shopify.com/s/files/1/0613/2039/7022/products/276073-2019-04-30-15_29_24.466319-big-image_720x.jpg?v=1663261255 720w,
                            //cdn.shopify.com/s/files/1/0613/2039/7022/products/276073-2019-04-30-15_29_24.466319-big-image_900x.jpg?v=1663261255 900w">

      <img alt="The Easy Going Light Green Cotton Kurta" class="lazyautosizes lazyloaded" data-widths="[180, 360, 540, 720, 900, 1080, 1296, 1512, 1728, 2048]" data-aspectratio="0.6779661016949152" data-sizes="auto" data-srcset="//cdn.shopify.com/s/files/1/0613/2039/7022/products/276073-2019-04-30-15_29_24.466319-big-image_360x.jpg?v=1663261255 360w,
                        //cdn.shopify.com/s/files/1/0613/2039/7022/products/276073-2019-04-30-15_29_24.466319-big-image_540x.jpg?v=1663261255 540w,
                        //cdn.shopify.com/s/files/1/0613/2039/7022/products/276073-2019-04-30-15_29_24.466319-big-image_720x.jpg?v=1663261255 720w,
                        //cdn.shopify.com/s/files/1/0613/2039/7022/products/276073-2019-04-30-15_29_24.466319-big-image_900x.jpg?v=1663261255 900w" data-image="" sizes="153px" srcset="//cdn.shopify.com/s/files/1/0613/2039/7022/products/276073-2019-04-30-15_29_24.466319-big-image_360x.jpg?v=1663261255 360w,
                        //cdn.shopify.com/s/files/1/0613/2039/7022/products/276073-2019-04-30-15_29_24.466319-big-image_540x.jpg?v=1663261255 540w,
                        //cdn.shopify.com/s/files/1/0613/2039/7022/products/276073-2019-04-30-15_29_24.466319-big-image_720x.jpg?v=1663261255 720w,
                        //cdn.shopify.com/s/files/1/0613/2039/7022/products/276073-2019-04-30-15_29_24.466319-big-image_900x.jpg?v=1663261255 900w">
    </picture>
    
  </span>

        </a>
      </div>

      
  
  <div class="product-label" data-label-new-number="30">
    

    
      
        <strong class="label sale-label">
          Sale
        </strong>
        
      
    

    
    
    

    
    
    
  </div>
  






      
<a class="wishlist " data-icon-wishlist="" aria-label="Wish Lists" href="#" data-product-handle="207-32" data-id="7798219210974" tabindex="-1">
	    	<svg viewBox="0 0 512 512">
<g>
<g>
  <path d="M474.644,74.27C449.391,45.616,414.358,29.836,376,29.836c-53.948,0-88.103,32.22-107.255,59.25
    c-4.969,7.014-9.196,14.047-12.745,20.665c-3.549-6.618-7.775-13.651-12.745-20.665c-19.152-27.03-53.307-59.25-107.255-59.25
    c-38.358,0-73.391,15.781-98.645,44.435C13.267,101.605,0,138.213,0,177.351c0,42.603,16.633,82.228,52.345,124.7
    c31.917,37.96,77.834,77.088,131.005,122.397c19.813,16.884,40.302,34.344,62.115,53.429l0.655,0.574
    c2.828,2.476,6.354,3.713,9.88,3.713s7.052-1.238,9.88-3.713l0.655-0.574c21.813-19.085,42.302-36.544,62.118-53.431
    c53.168-45.306,99.085-84.434,131.002-122.395C495.367,259.578,512,219.954,512,177.351
    C512,138.213,498.733,101.605,474.644,74.27z M309.193,401.614c-17.08,14.554-34.658,29.533-53.193,45.646
    c-18.534-16.111-36.113-31.091-53.196-45.648C98.745,312.939,30,254.358,30,177.351c0-31.83,10.605-61.394,29.862-83.245
    C79.34,72.007,106.379,59.836,136,59.836c41.129,0,67.716,25.338,82.776,46.594c13.509,19.064,20.558,38.282,22.962,45.659
    c2.011,6.175,7.768,10.354,14.262,10.354c6.494,0,12.251-4.179,14.262-10.354c2.404-7.377,9.453-26.595,22.962-45.66
    c15.06-21.255,41.647-46.593,82.776-46.593c29.621,0,56.66,12.171,76.137,34.27C471.395,115.957,482,145.521,482,177.351
    C482,254.358,413.255,312.939,309.193,401.614z"></path>
</g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
</svg>
		
</a>
 


      
        <a class="quickview-button" href="javascript:void(0)" id="207-32" title="Quick View" tabindex="-1">
          <span>Quick view</span>
          <svg viewBox="0 0 511.999 511.999" width="100%" height="100%">
    <path d="M508.745,246.041c-4.574-6.257-113.557-153.206-252.748-153.206S7.818,239.784,3.249,246.035         c-4.332,5.936-4.332,13.987,0,19.923c4.569,6.257,113.557,153.206,252.748,153.206s248.174-146.95,252.748-153.201         C513.083,260.028,513.083,251.971,508.745,246.041z M255.997,385.406c-102.529,0-191.33-97.533-217.617-129.418         c26.253-31.913,114.868-129.395,217.617-129.395c102.524,0,191.319,97.516,217.617,129.418         C447.361,287.923,358.746,385.406,255.997,385.406z"></path>
      <path d="M255.997,154.725c-55.842,0-101.275,45.433-101.275,101.275s45.433,101.275,101.275,101.275         s101.275-45.433,101.275-101.275S311.839,154.725,255.997,154.725z M255.997,323.516c-37.23,0-67.516-30.287-67.516-67.516         s30.287-67.516,67.516-67.516s67.516,30.287,67.516,67.516S293.227,323.516,255.997,323.516z"></path>
</svg>
        </a>
      

      

    
    <div class="product-bottom">
      
      
        <span class="spr-badge" id="spr_badge_7798219210974" data-rating="4.666666666666667"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star" aria-hidden="true"></i><i class="spr-icon spr-icon-star" aria-hidden="true"></i><i class="spr-icon spr-icon-star" aria-hidden="true"></i><i class="spr-icon spr-icon-star" aria-hidden="true"></i><i class="spr-icon spr-icon-star" aria-hidden="true"></i></span><span class="spr-badge-caption">3 reviews</span>
</span>

      
      <a class="product-title " href="/products/207-32?pr_prod_strat=copurchase&amp;pr_rec_id=e3a9801a1&amp;pr_rec_pid=7798219210974&amp;pr_ref_pid=7797834973406&amp;pr_seq=uniform" tabindex="-1">
        
<span>The Easy Going Light Green Cotton Kurta</span>

        
          
          
      </a>
      <div class="half-grid-rais">
      <div class="price-box">
        
        <div class="price-sale">
          
            <span class="special-price" data-price-grid=""><span class="money">₹2173.00</span></span>
            <span class="old-price" data-compare-price-grid="">3104.00</span>
            <span class="percentage-price">(30%)</span>
         
        </div>
        
      </div>
    </div>

      <div class="add-to-cart-grid-rais">
        
        <div class="action">
          


<form action="/cart/add" method="post" class="variants" id="grid-product-form--7798219210974-product-recommendations" data-id="product-actions-7798219210974" enctype="multipart/form-data">
  
  

    
      
        <a class="rais-svgcart" data-href="/products/207-32?pr_prod_strat=copurchase&amp;pr_rec_id=e3a9801a1&amp;pr_rec_pid=7798219210974&amp;pr_ref_pid=7797834973406&amp;pr_seq=uniform" title="The Easy Going Light Green Cotton Kurta" data-init-quickshop="" tabindex="-1">
        <svg width="27px" height="27px" viewBox="0 0 27 27" version="1.1" xmlns="http://www.w3.org/2000/svg"><g id="Tjori-" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g id="PLP" transform="translate(-361.000000, -962.000000)"><g id="Product-Widget-Copy" transform="translate(18.000000, 646.000000)"><g id="Product-Widget"><g id="Shopping-Bag-Icon" transform="translate(343.000000, 316.000000)"><path d="M21.6999607,6.00003576 L26.1999499,6.00003576 L26.1999499,20.2500018 C26.1999499,21.2812493 25.8327635,22.1640595 25.0983901,22.898433 C24.3640166,23.6328065 23.4812064,23.9999928 22.4499589,23.9999928 L8.94999106,23.9999928 C7.91874352,23.9999928 7.03593336,23.6328065 6.30155987,22.898433 C5.56718639,22.1640595 5.2,21.2812493 5.2,20.2500018 L5.2,6.00003576 L9.69998927,6.00003576 C9.69998927,4.90628861 9.96561388,3.89847851 10.4968624,2.97660547 C11.0281109,2.05473243 11.7546716,1.32817166 12.6765447,0.796923168 C13.5984177,0.265674673 14.6062278,5.00679016e-05 15.699975,5.00679016e-05 C16.7937221,5.00679016e-05 17.8015322,0.265674673 18.7234053,0.796923168 C19.6452783,1.32817166 20.3718391,2.05473243 20.9030876,2.97660547 C21.4343361,3.89847851 21.6999607,4.90628861 21.6999607,6.00003576 Z M15.699975,1.50004649 C14.4499777,1.50004649 13.3874807,1.93754521 12.5124826,2.81254336 C11.6374844,3.68754151 11.1999857,4.7500385 11.1999857,6.00003576 L20.1999642,6.00003576 C20.1999642,4.7500385 19.7624655,3.68754151 18.8874674,2.81254336 C18.0124692,1.93754521 16.9499722,1.50004649 15.699975,1.50004649 Z M24.6999535,20.2500018 L24.6999535,7.50003219 L21.6999607,7.50003219 L21.6999607,9.75002682 C21.6999607,9.96877654 21.6296483,10.1484634 21.4890237,10.289088 C21.348399,10.4297127 21.1687122,10.500025 20.9499624,10.500025 C20.7312127,10.500025 20.5515259,10.4297127 20.4109012,10.289088 C20.2702766,10.1484634 20.1999642,9.96877654 20.1999642,9.75002682 L20.1999642,7.50003219 L11.1999857,7.50003219 L11.1999857,9.75002682 C11.1999857,9.96877654 11.1296734,10.1484634 10.9890487,10.289088 C10.848424,10.4297127 10.6687372,10.500025 10.4499875,10.500025 C10.2312378,10.500025 10.0515509,10.4297127 9.91092627,10.289088 C9.7703016,10.1484634 9.69998927,9.96877654 9.69998927,9.75002682 L9.69998927,7.50003219 L6.69999642,7.50003219 L6.69999642,20.2500018 C6.69999642,20.8750001 6.91874614,21.4062493 7.35624486,21.843748 C7.79374358,22.2812467 8.32499279,22.4999964 8.94999106,22.4999964 L22.4499589,22.4999964 C23.0749571,22.4999964 23.6062064,22.2812467 24.0437051,21.843748 C24.4812038,21.4062493 24.6999535,20.8750001 24.6999535,20.2500018 Z" id="shopping-bag" fill="#34302B"></path><circle id="Oval" fill="#FFFFFF" cx="6.5" cy="20.5" r="5.5"></circle><path d="M6.34998569,14.6875246 C7.39685832,14.6875246 8.36560589,14.9492428 9.25622877,15.472679 C10.1468516,15.9961151 10.8538813,16.7031448 11.3773175,17.5937677 C11.9007536,18.4843905 12.1624718,19.4531381 12.1624718,20.5000107 C12.1624718,21.5468834 11.9007536,22.5156309 11.3773175,23.4062538 C10.8538813,24.2968767 10.1468516,25.0039064 9.25622877,25.5273425 C8.36560589,26.0507786 7.39685832,26.3124969 6.34998569,26.3124969 C5.30311307,26.3124969 4.3343655,26.0507786 3.44374262,25.5273425 C2.55311975,25.0039064 1.84609006,24.2968767 1.32265393,23.4062538 C0.799217798,22.5156309 0.537499553,21.5468834 0.537499553,20.5000107 C0.537499553,19.4531381 0.799217798,18.4843905 1.32265393,17.5937677 C1.84609006,16.7031448 2.55311975,15.9961151 3.44374262,15.472679 C4.3343655,14.9492428 5.30311307,14.6875246 6.34998569,14.6875246 Z M9.72497765,21.1562592 L9.72497765,19.8437623 C9.72497765,19.7656376 9.69763384,19.6992313 9.64294659,19.644544 C9.58825934,19.5898568 9.52185301,19.562513 9.44372832,19.562513 L7.28748346,19.562513 L7.28748346,17.4062681 C7.28748346,17.3281434 7.26013966,17.2617371 7.20545241,17.2070498 C7.15076515,17.1523626 7.08435882,17.1250188 7.00623413,17.1250188 L5.69373726,17.1250188 C5.61561256,17.1250188 5.54920623,17.1523626 5.49451898,17.2070498 C5.43983173,17.2617371 5.41248793,17.3281434 5.41248793,17.4062681 L5.41248793,19.562513 L3.25624307,19.562513 C3.17811838,19.562513 3.11171205,19.5898568 3.0570248,19.644544 C3.00233755,19.6992313 2.97499374,19.7656376 2.97499374,19.8437623 L2.97499374,21.1562592 C2.97499374,21.2343839 3.00233755,21.3007902 3.0570248,21.3554774 C3.11171205,21.4101647 3.17811838,21.4375085 3.25624307,21.4375085 L5.41248793,21.4375085 L5.41248793,23.5937534 C5.41248793,23.671878 5.43983173,23.7382844 5.49451898,23.7929716 C5.54920623,23.8476589 5.61561256,23.8750027 5.69373726,23.8750027 L7.00623413,23.8750027 C7.08435882,23.8750027 7.15076515,23.8476589 7.20545241,23.7929716 C7.26013966,23.7382844 7.28748346,23.671878 7.28748346,23.5937534 L7.28748346,21.4375085 L9.44372832,21.4375085 C9.52185301,21.4375085 9.58825934,21.4101647 9.64294659,21.3554774 C9.69763384,21.3007902 9.72497765,21.2343839 9.72497765,21.1562592 Z" id="plus-circle" fill="#C0B394"></path></g></g></g></g></g></svg>
        </a>
      
    
  
  
</form>

  
      
          <div class="product-card__variant--popup">
              <div class="product-card__variant--popup--content">
    
    <form action="/cart/add" method="post" class="variants" id="swatch-grid-product-form--7798219210974-product-recommendations-" data-id="product-actions-7798219210974" enctype="multipart/form-data">
        
            
            
            
                
                    <div class="selector-wrapper selector-wrapper-1 swatch js product-form__item" data-option-index="0">
                        <label class="form-label ">
                            Size: <span class="label-value-1">XS</span>
                        </label>
                        
                        
                        

                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size xs available" data-value="XS">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7798219210974-product-recommendations" data-index="option1" value="XS" id="SingleOptionSelector0-XS-7798219210974-product-recommendations" tabindex="-1">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-XS-7798219210974-product-recommendations" data-original-title="XS">XS</label>
                                </div>
                            
                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size s available" data-value="S">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7798219210974-product-recommendations" data-index="option1" value="S" id="SingleOptionSelector0-S-7798219210974-product-recommendations" tabindex="-1">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-S-7798219210974-product-recommendations" data-original-title="S">S</label>
                                </div>
                            
                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size m available" data-value="M">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7798219210974-product-recommendations" data-index="option1" value="M" id="SingleOptionSelector0-M-7798219210974-product-recommendations" tabindex="-1">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-M-7798219210974-product-recommendations" data-original-title="M">M</label>
                                </div>
                            
                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size l available" data-value="L">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7798219210974-product-recommendations" data-index="option1" value="L" id="SingleOptionSelector0-L-7798219210974-product-recommendations" tabindex="-1">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-L-7798219210974-product-recommendations" data-original-title="L">L</label>
                                </div>
                            
                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size xl available" data-value="XL">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7798219210974-product-recommendations" data-index="option1" value="XL" id="SingleOptionSelector0-XL-7798219210974-product-recommendations" tabindex="-1">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-XL-7798219210974-product-recommendations" data-original-title="XL">XL</label>
                                </div>
                            
                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size xxl available" data-value="XXL">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7798219210974-product-recommendations" data-index="option1" value="XXL" id="SingleOptionSelector0-XXL-7798219210974-product-recommendations" tabindex="-1">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-XXL-7798219210974-product-recommendations" data-original-title="XXL">XXL</label>
                                </div>
                            
                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size xxxl available" data-value="XXXL">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7798219210974-product-recommendations" data-index="option1" value="XXXL" id="SingleOptionSelector0-XXXL-7798219210974-product-recommendations" tabindex="-1">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-XXXL-7798219210974-product-recommendations" data-original-title="XXXL">XXXL</label>
                                </div>
                            
                        
                    </div>
                
            
        

        <select name="id" id="ProductSelect-7798219210974-product-recommendations" class="product-form__variants no-js" tabindex="-1">
            
                
                    <option value="43400834449630">
                        XS
                    </option>
                
            
                
                    <option value="43400834547934">
                        S
                    </option>
                
            
                
                    <option value="43400834613470">
                        M
                    </option>
                
            
                
                    <option value="43400834744542">
                        L
                    </option>
                
            
                
                    <option value="43400834810078">
                        XL
                    </option>
                
            
                
                    <option value="43400834842846">
                        XXL
                    </option>
                
            
                
                    <option value="43400834875614">
                        XXXL
                    </option>
                
            
        </select>
        <div class="product-card__button_cancel_mobile">
            <a class="btn btn-cancel" data-cancel-swatch-qs="" tabindex="-1"><svg aria-hidden="true" data-prefix="fal" data-icon="times" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" class="svg-inline--fa fa-times fa-w-10 fa-2x"><path fill="currentColor" d="M193.94 256L296.5 153.44l21.15-21.15c3.12-3.12 3.12-8.19 0-11.31l-22.63-22.63c-3.12-3.12-8.19-3.12-11.31 0L160 222.06 36.29 98.34c-3.12-3.12-8.19-3.12-11.31 0L2.34 120.97c-3.12 3.12-3.12 8.19 0 11.31L126.06 256 2.34 379.71c-3.12 3.12-3.12 8.19 0 11.31l22.63 22.63c3.12 3.12 8.19 3.12 11.31 0L160 289.94 262.56 392.5l21.15 21.15c3.12 3.12 8.19 3.12 11.31 0l22.63-22.63c3.12-3.12 3.12-8.19 0-11.31L193.94 256z" class=""></path></svg></a>
        </div>
        <div class="product-card__button2">
            <input type="hidden" name="quantity" value="1" tabindex="-1">
            <button data-btn-addtocart="" class="btn add-to-cart-btn" type="submit" data-form-id="#swatch-grid-product-form--7798219210974-product-recommendations-" tabindex="-1">
                Submit
            </button>
            <a class="btn btn-cancel" data-cancel-swatch-qs="" tabindex="-1">Cancel</a>
        </div>
        
    </form>
</div>
          </div>
      
  

        </div>
        
      </div>
    </div>

      <div class="wrapper-size">
        
      </div>
      
    </div>

    
     
    <div class="product-details">
      

      <a class="product-title " href="/products/207-32?pr_prod_strat=copurchase&amp;pr_rec_id=e3a9801a1&amp;pr_rec_pid=7798219210974&amp;pr_ref_pid=7797834973406&amp;pr_seq=uniform" tabindex="-1">
        
<span>The Easy Going Light Green Cotton Kurta</span>

        
          
        
      </a>

      
      <span class="spr-badge" id="spr_badge_7798219210974" data-rating="4.666666666666667"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star" aria-hidden="true"></i><i class="spr-icon spr-icon-star" aria-hidden="true"></i><i class="spr-icon spr-icon-star" aria-hidden="true"></i><i class="spr-icon spr-icon-star" aria-hidden="true"></i><i class="spr-icon spr-icon-star" aria-hidden="true"></i></span><span class="spr-badge-caption">3 reviews</span>
</span>

      

      <div class="short-description">
        
        <div>
          This light green cotton kurta with pintucks and a yoke on the front gathers on the sides. It is full sleeved, piped and has an A line fit, making it a light and easy piece of clothing for the summer....
        </div>
      </div>

      <div class="price-box">
        
        <div class="price-sale">
          
            <span class="old-price" data-compare-price-grid=""><span class="money">₹3,104.00</span></span>
            <span class="special-price" data-price-grid=""><span class="money">₹2,173.00</span></span>
          
        </div>
        
      </div>

      

      <div class="wrapper-button-card">
        
        <div class="action">
          


<form action="/cart/add" method="post" class="variants" id="list-product-form-7798219210974-product-recommendations" data-id="product-actions-7798219210974" enctype="multipart/form-data">
  
  

    
      
        <a class="rais-svgcart" data-href="/products/207-32?pr_prod_strat=copurchase&amp;pr_rec_id=e3a9801a1&amp;pr_rec_pid=7798219210974&amp;pr_ref_pid=7797834973406&amp;pr_seq=uniform" title="The Easy Going Light Green Cotton Kurta" data-init-quickshop="" tabindex="-1">
        <svg width="27px" height="27px" viewBox="0 0 27 27" version="1.1" xmlns="http://www.w3.org/2000/svg"><g id="Tjori-" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g id="PLP" transform="translate(-361.000000, -962.000000)"><g id="Product-Widget-Copy" transform="translate(18.000000, 646.000000)"><g id="Product-Widget"><g id="Shopping-Bag-Icon" transform="translate(343.000000, 316.000000)"><path d="M21.6999607,6.00003576 L26.1999499,6.00003576 L26.1999499,20.2500018 C26.1999499,21.2812493 25.8327635,22.1640595 25.0983901,22.898433 C24.3640166,23.6328065 23.4812064,23.9999928 22.4499589,23.9999928 L8.94999106,23.9999928 C7.91874352,23.9999928 7.03593336,23.6328065 6.30155987,22.898433 C5.56718639,22.1640595 5.2,21.2812493 5.2,20.2500018 L5.2,6.00003576 L9.69998927,6.00003576 C9.69998927,4.90628861 9.96561388,3.89847851 10.4968624,2.97660547 C11.0281109,2.05473243 11.7546716,1.32817166 12.6765447,0.796923168 C13.5984177,0.265674673 14.6062278,5.00679016e-05 15.699975,5.00679016e-05 C16.7937221,5.00679016e-05 17.8015322,0.265674673 18.7234053,0.796923168 C19.6452783,1.32817166 20.3718391,2.05473243 20.9030876,2.97660547 C21.4343361,3.89847851 21.6999607,4.90628861 21.6999607,6.00003576 Z M15.699975,1.50004649 C14.4499777,1.50004649 13.3874807,1.93754521 12.5124826,2.81254336 C11.6374844,3.68754151 11.1999857,4.7500385 11.1999857,6.00003576 L20.1999642,6.00003576 C20.1999642,4.7500385 19.7624655,3.68754151 18.8874674,2.81254336 C18.0124692,1.93754521 16.9499722,1.50004649 15.699975,1.50004649 Z M24.6999535,20.2500018 L24.6999535,7.50003219 L21.6999607,7.50003219 L21.6999607,9.75002682 C21.6999607,9.96877654 21.6296483,10.1484634 21.4890237,10.289088 C21.348399,10.4297127 21.1687122,10.500025 20.9499624,10.500025 C20.7312127,10.500025 20.5515259,10.4297127 20.4109012,10.289088 C20.2702766,10.1484634 20.1999642,9.96877654 20.1999642,9.75002682 L20.1999642,7.50003219 L11.1999857,7.50003219 L11.1999857,9.75002682 C11.1999857,9.96877654 11.1296734,10.1484634 10.9890487,10.289088 C10.848424,10.4297127 10.6687372,10.500025 10.4499875,10.500025 C10.2312378,10.500025 10.0515509,10.4297127 9.91092627,10.289088 C9.7703016,10.1484634 9.69998927,9.96877654 9.69998927,9.75002682 L9.69998927,7.50003219 L6.69999642,7.50003219 L6.69999642,20.2500018 C6.69999642,20.8750001 6.91874614,21.4062493 7.35624486,21.843748 C7.79374358,22.2812467 8.32499279,22.4999964 8.94999106,22.4999964 L22.4499589,22.4999964 C23.0749571,22.4999964 23.6062064,22.2812467 24.0437051,21.843748 C24.4812038,21.4062493 24.6999535,20.8750001 24.6999535,20.2500018 Z" id="shopping-bag" fill="#34302B"></path><circle id="Oval" fill="#FFFFFF" cx="6.5" cy="20.5" r="5.5"></circle><path d="M6.34998569,14.6875246 C7.39685832,14.6875246 8.36560589,14.9492428 9.25622877,15.472679 C10.1468516,15.9961151 10.8538813,16.7031448 11.3773175,17.5937677 C11.9007536,18.4843905 12.1624718,19.4531381 12.1624718,20.5000107 C12.1624718,21.5468834 11.9007536,22.5156309 11.3773175,23.4062538 C10.8538813,24.2968767 10.1468516,25.0039064 9.25622877,25.5273425 C8.36560589,26.0507786 7.39685832,26.3124969 6.34998569,26.3124969 C5.30311307,26.3124969 4.3343655,26.0507786 3.44374262,25.5273425 C2.55311975,25.0039064 1.84609006,24.2968767 1.32265393,23.4062538 C0.799217798,22.5156309 0.537499553,21.5468834 0.537499553,20.5000107 C0.537499553,19.4531381 0.799217798,18.4843905 1.32265393,17.5937677 C1.84609006,16.7031448 2.55311975,15.9961151 3.44374262,15.472679 C4.3343655,14.9492428 5.30311307,14.6875246 6.34998569,14.6875246 Z M9.72497765,21.1562592 L9.72497765,19.8437623 C9.72497765,19.7656376 9.69763384,19.6992313 9.64294659,19.644544 C9.58825934,19.5898568 9.52185301,19.562513 9.44372832,19.562513 L7.28748346,19.562513 L7.28748346,17.4062681 C7.28748346,17.3281434 7.26013966,17.2617371 7.20545241,17.2070498 C7.15076515,17.1523626 7.08435882,17.1250188 7.00623413,17.1250188 L5.69373726,17.1250188 C5.61561256,17.1250188 5.54920623,17.1523626 5.49451898,17.2070498 C5.43983173,17.2617371 5.41248793,17.3281434 5.41248793,17.4062681 L5.41248793,19.562513 L3.25624307,19.562513 C3.17811838,19.562513 3.11171205,19.5898568 3.0570248,19.644544 C3.00233755,19.6992313 2.97499374,19.7656376 2.97499374,19.8437623 L2.97499374,21.1562592 C2.97499374,21.2343839 3.00233755,21.3007902 3.0570248,21.3554774 C3.11171205,21.4101647 3.17811838,21.4375085 3.25624307,21.4375085 L5.41248793,21.4375085 L5.41248793,23.5937534 C5.41248793,23.671878 5.43983173,23.7382844 5.49451898,23.7929716 C5.54920623,23.8476589 5.61561256,23.8750027 5.69373726,23.8750027 L7.00623413,23.8750027 C7.08435882,23.8750027 7.15076515,23.8476589 7.20545241,23.7929716 C7.26013966,23.7382844 7.28748346,23.671878 7.28748346,23.5937534 L7.28748346,21.4375085 L9.44372832,21.4375085 C9.52185301,21.4375085 9.58825934,21.4101647 9.64294659,21.3554774 C9.69763384,21.3007902 9.72497765,21.2343839 9.72497765,21.1562592 Z" id="plus-circle" fill="#C0B394"></path></g></g></g></g></g></svg>
        </a>
      
    
  
  
</form>

  
      
          <div class="product-card__variant--popup">
              <div class="product-card__variant--popup--content">
    
    <form action="/cart/add" method="post" class="variants" id="swatch-list-product-form-7798219210974-product-recommendations-list" data-id="product-actions-7798219210974" enctype="multipart/form-data">
        
            
            
            
                
                    <div class="selector-wrapper selector-wrapper-1 swatch js product-form__item" data-option-index="0">
                        <label class="form-label ">
                            Size: <span class="label-value-1">XS</span>
                        </label>
                        
                        
                        

                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size xs available" data-value="XS">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7798219210974-product-recommendationslist" data-index="option1" value="XS" id="SingleOptionSelector0-XS-7798219210974-product-recommendationslist" tabindex="-1">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-XS-7798219210974-product-recommendationslist" data-original-title="XS">XS</label>
                                </div>
                            
                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size s available" data-value="S">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7798219210974-product-recommendationslist" data-index="option1" value="S" id="SingleOptionSelector0-S-7798219210974-product-recommendationslist" tabindex="-1">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-S-7798219210974-product-recommendationslist" data-original-title="S">S</label>
                                </div>
                            
                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size m available" data-value="M">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7798219210974-product-recommendationslist" data-index="option1" value="M" id="SingleOptionSelector0-M-7798219210974-product-recommendationslist" tabindex="-1">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-M-7798219210974-product-recommendationslist" data-original-title="M">M</label>
                                </div>
                            
                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size l available" data-value="L">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7798219210974-product-recommendationslist" data-index="option1" value="L" id="SingleOptionSelector0-L-7798219210974-product-recommendationslist" tabindex="-1">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-L-7798219210974-product-recommendationslist" data-original-title="L">L</label>
                                </div>
                            
                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size xl available" data-value="XL">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7798219210974-product-recommendationslist" data-index="option1" value="XL" id="SingleOptionSelector0-XL-7798219210974-product-recommendationslist" tabindex="-1">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-XL-7798219210974-product-recommendationslist" data-original-title="XL">XL</label>
                                </div>
                            
                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size xxl available" data-value="XXL">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7798219210974-product-recommendationslist" data-index="option1" value="XXL" id="SingleOptionSelector0-XXL-7798219210974-product-recommendationslist" tabindex="-1">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-XXL-7798219210974-product-recommendationslist" data-original-title="XXL">XXL</label>
                                </div>
                            
                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size xxxl available" data-value="XXXL">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7798219210974-product-recommendationslist" data-index="option1" value="XXXL" id="SingleOptionSelector0-XXXL-7798219210974-product-recommendationslist" tabindex="-1">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-XXXL-7798219210974-product-recommendationslist" data-original-title="XXXL">XXXL</label>
                                </div>
                            
                        
                    </div>
                
            
        

        <select name="id" id="ProductSelect-7798219210974-product-recommendationslist" class="product-form__variants no-js" tabindex="-1">
            
                
                    <option value="43400834449630">
                        XS
                    </option>
                
            
                
                    <option value="43400834547934">
                        S
                    </option>
                
            
                
                    <option value="43400834613470">
                        M
                    </option>
                
            
                
                    <option value="43400834744542">
                        L
                    </option>
                
            
                
                    <option value="43400834810078">
                        XL
                    </option>
                
            
                
                    <option value="43400834842846">
                        XXL
                    </option>
                
            
                
                    <option value="43400834875614">
                        XXXL
                    </option>
                
            
        </select>
        <div class="product-card__button_cancel_mobile">
            <a class="btn btn-cancel" data-cancel-swatch-qs="" tabindex="-1"><svg aria-hidden="true" data-prefix="fal" data-icon="times" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" class="svg-inline--fa fa-times fa-w-10 fa-2x"><path fill="currentColor" d="M193.94 256L296.5 153.44l21.15-21.15c3.12-3.12 3.12-8.19 0-11.31l-22.63-22.63c-3.12-3.12-8.19-3.12-11.31 0L160 222.06 36.29 98.34c-3.12-3.12-8.19-3.12-11.31 0L2.34 120.97c-3.12 3.12-3.12 8.19 0 11.31L126.06 256 2.34 379.71c-3.12 3.12-3.12 8.19 0 11.31l22.63 22.63c3.12 3.12 8.19 3.12 11.31 0L160 289.94 262.56 392.5l21.15 21.15c3.12 3.12 8.19 3.12 11.31 0l22.63-22.63c3.12-3.12 3.12-8.19 0-11.31L193.94 256z" class=""></path></svg></a>
        </div>
        <div class="product-card__button2">
            <input type="hidden" name="quantity" value="1" tabindex="-1">
            <button data-btn-addtocart="" class="btn add-to-cart-btn" type="submit" data-form-id="#swatch-list-product-form-7798219210974-product-recommendations-list" tabindex="-1">
                Submit
            </button>
            <a class="btn btn-cancel" data-cancel-swatch-qs="" tabindex="-1">Cancel</a>
        </div>
        
    </form>
</div>
          </div>
      
  

        </div>
        
      </div>
      
    </div>
    
  </div>
</div>

          
        </div><div class="grid-item small--one-half medium-up--one-quarter col-12 slick-slide" style="width: 173px;" tabindex="-1" role="option" aria-describedby="slick-slide24" data-slick-index="4" aria-hidden="true">
          
            
<div class="inner product-item on-sale" data-product-id="product-7797721759966" data-json-product="{&quot;id&quot;: 7797721759966,&quot;handle&quot;: &quot;black-cotton-plain-striped-cotton-kurta&quot;,&quot;media&quot;: [{&quot;alt&quot;:null,&quot;id&quot;:30633827172574,&quot;position&quot;:1,&quot;preview_image&quot;:{&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:2360,&quot;width&quot;:1600,&quot;src&quot;:&quot;https:\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/TJ-MK-261-17_1.jpg?v=1663265215&quot;},&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:2360,&quot;media_type&quot;:&quot;image&quot;,&quot;src&quot;:&quot;https:\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/TJ-MK-261-17_1.jpg?v=1663265215&quot;,&quot;width&quot;:1600},{&quot;alt&quot;:null,&quot;id&quot;:30633827238110,&quot;position&quot;:2,&quot;preview_image&quot;:{&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:2360,&quot;width&quot;:1600,&quot;src&quot;:&quot;https:\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/TJ-MK-261-17_2.jpg?v=1663265215&quot;},&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:2360,&quot;media_type&quot;:&quot;image&quot;,&quot;src&quot;:&quot;https:\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/TJ-MK-261-17_2.jpg?v=1663265215&quot;,&quot;width&quot;:1600},{&quot;alt&quot;:null,&quot;id&quot;:30633827303646,&quot;position&quot;:3,&quot;preview_image&quot;:{&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:2360,&quot;width&quot;:1600,&quot;src&quot;:&quot;https:\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/TJ-MK-261-17_3.jpg?v=1663265215&quot;},&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:2360,&quot;media_type&quot;:&quot;image&quot;,&quot;src&quot;:&quot;https:\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/TJ-MK-261-17_3.jpg?v=1663265215&quot;,&quot;width&quot;:1600},{&quot;alt&quot;:null,&quot;id&quot;:30633827336414,&quot;position&quot;:4,&quot;preview_image&quot;:{&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:2360,&quot;width&quot;:1600,&quot;src&quot;:&quot;https:\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/TJ-MK-261-17_4.jpg?v=1663265215&quot;},&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:2360,&quot;media_type&quot;:&quot;image&quot;,&quot;src&quot;:&quot;https:\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/TJ-MK-261-17_4.jpg?v=1663265215&quot;,&quot;width&quot;:1600},{&quot;alt&quot;:null,&quot;id&quot;:30633827369182,&quot;position&quot;:5,&quot;preview_image&quot;:{&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:2360,&quot;width&quot;:1600,&quot;src&quot;:&quot;https:\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/TJ-MK-261-17_5.jpg?v=1663265215&quot;},&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:2360,&quot;media_type&quot;:&quot;image&quot;,&quot;src&quot;:&quot;https:\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/TJ-MK-261-17_5.jpg?v=1663265215&quot;,&quot;width&quot;:1600},{&quot;alt&quot;:null,&quot;id&quot;:30633827401950,&quot;position&quot;:6,&quot;preview_image&quot;:{&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:2360,&quot;width&quot;:1600,&quot;src&quot;:&quot;https:\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/TJ-MK-261-17_6.jpg?v=1663265215&quot;},&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:2360,&quot;media_type&quot;:&quot;image&quot;,&quot;src&quot;:&quot;https:\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/TJ-MK-261-17_6.jpg?v=1663265215&quot;,&quot;width&quot;:1600}],&quot;variants&quot;: [{&quot;id&quot;:43398847496414,&quot;title&quot;:&quot;XS&quot;,&quot;option1&quot;:&quot;XS&quot;,&quot;option2&quot;:null,&quot;option3&quot;:null,&quot;sku&quot;:&quot;TJ-MK-261-17-XS&quot;,&quot;requires_shipping&quot;:true,&quot;taxable&quot;:true,&quot;featured_image&quot;:null,&quot;available&quot;:true,&quot;name&quot;:&quot;Black Cotton Plain Striped Cotton Kurta - XS&quot;,&quot;public_title&quot;:&quot;XS&quot;,&quot;options&quot;:[&quot;XS&quot;],&quot;price&quot;:173800,&quot;weight&quot;:0,&quot;compare_at_price&quot;:248300,&quot;inventory_management&quot;:&quot;shopify&quot;,&quot;barcode&quot;:null,&quot;quantity_rule&quot;:{&quot;min&quot;:1,&quot;max&quot;:null,&quot;increment&quot;:1}},{&quot;id&quot;:43398847561950,&quot;title&quot;:&quot;S&quot;,&quot;option1&quot;:&quot;S&quot;,&quot;option2&quot;:null,&quot;option3&quot;:null,&quot;sku&quot;:&quot;TJ-MK-261-17-S&quot;,&quot;requires_shipping&quot;:true,&quot;taxable&quot;:true,&quot;featured_image&quot;:null,&quot;available&quot;:true,&quot;name&quot;:&quot;Black Cotton Plain Striped Cotton Kurta - S&quot;,&quot;public_title&quot;:&quot;S&quot;,&quot;options&quot;:[&quot;S&quot;],&quot;price&quot;:173800,&quot;weight&quot;:0,&quot;compare_at_price&quot;:248300,&quot;inventory_management&quot;:&quot;shopify&quot;,&quot;barcode&quot;:null,&quot;quantity_rule&quot;:{&quot;min&quot;:1,&quot;max&quot;:null,&quot;increment&quot;:1}},{&quot;id&quot;:43398847594718,&quot;title&quot;:&quot;M&quot;,&quot;option1&quot;:&quot;M&quot;,&quot;option2&quot;:null,&quot;option3&quot;:null,&quot;sku&quot;:&quot;TJ-MK-261-17-M&quot;,&quot;requires_shipping&quot;:true,&quot;taxable&quot;:true,&quot;featured_image&quot;:null,&quot;available&quot;:true,&quot;name&quot;:&quot;Black Cotton Plain Striped Cotton Kurta - M&quot;,&quot;public_title&quot;:&quot;M&quot;,&quot;options&quot;:[&quot;M&quot;],&quot;price&quot;:173800,&quot;weight&quot;:0,&quot;compare_at_price&quot;:248300,&quot;inventory_management&quot;:&quot;shopify&quot;,&quot;barcode&quot;:null,&quot;quantity_rule&quot;:{&quot;min&quot;:1,&quot;max&quot;:null,&quot;increment&quot;:1}},{&quot;id&quot;:43398847627486,&quot;title&quot;:&quot;L&quot;,&quot;option1&quot;:&quot;L&quot;,&quot;option2&quot;:null,&quot;option3&quot;:null,&quot;sku&quot;:&quot;TJ-MK-261-17-L&quot;,&quot;requires_shipping&quot;:true,&quot;taxable&quot;:true,&quot;featured_image&quot;:null,&quot;available&quot;:true,&quot;name&quot;:&quot;Black Cotton Plain Striped Cotton Kurta - L&quot;,&quot;public_title&quot;:&quot;L&quot;,&quot;options&quot;:[&quot;L&quot;],&quot;price&quot;:173800,&quot;weight&quot;:0,&quot;compare_at_price&quot;:248300,&quot;inventory_management&quot;:&quot;shopify&quot;,&quot;barcode&quot;:null,&quot;quantity_rule&quot;:{&quot;min&quot;:1,&quot;max&quot;:null,&quot;increment&quot;:1}},{&quot;id&quot;:43398847660254,&quot;title&quot;:&quot;XL&quot;,&quot;option1&quot;:&quot;XL&quot;,&quot;option2&quot;:null,&quot;option3&quot;:null,&quot;sku&quot;:&quot;TJ-MK-261-17-XL&quot;,&quot;requires_shipping&quot;:true,&quot;taxable&quot;:true,&quot;featured_image&quot;:null,&quot;available&quot;:true,&quot;name&quot;:&quot;Black Cotton Plain Striped Cotton Kurta - XL&quot;,&quot;public_title&quot;:&quot;XL&quot;,&quot;options&quot;:[&quot;XL&quot;],&quot;price&quot;:173800,&quot;weight&quot;:0,&quot;compare_at_price&quot;:248300,&quot;inventory_management&quot;:&quot;shopify&quot;,&quot;barcode&quot;:null,&quot;quantity_rule&quot;:{&quot;min&quot;:1,&quot;max&quot;:null,&quot;increment&quot;:1}},{&quot;id&quot;:43398847693022,&quot;title&quot;:&quot;XXL&quot;,&quot;option1&quot;:&quot;XXL&quot;,&quot;option2&quot;:null,&quot;option3&quot;:null,&quot;sku&quot;:&quot;TJ-MK-261-17-XXL&quot;,&quot;requires_shipping&quot;:true,&quot;taxable&quot;:true,&quot;featured_image&quot;:null,&quot;available&quot;:true,&quot;name&quot;:&quot;Black Cotton Plain Striped Cotton Kurta - XXL&quot;,&quot;public_title&quot;:&quot;XXL&quot;,&quot;options&quot;:[&quot;XXL&quot;],&quot;price&quot;:173800,&quot;weight&quot;:0,&quot;compare_at_price&quot;:248300,&quot;inventory_management&quot;:&quot;shopify&quot;,&quot;barcode&quot;:null,&quot;quantity_rule&quot;:{&quot;min&quot;:1,&quot;max&quot;:null,&quot;increment&quot;:1}},{&quot;id&quot;:43398847725790,&quot;title&quot;:&quot;XXXL&quot;,&quot;option1&quot;:&quot;XXXL&quot;,&quot;option2&quot;:null,&quot;option3&quot;:null,&quot;sku&quot;:&quot;TJ-MK-261-17-XXXL&quot;,&quot;requires_shipping&quot;:true,&quot;taxable&quot;:true,&quot;featured_image&quot;:null,&quot;available&quot;:true,&quot;name&quot;:&quot;Black Cotton Plain Striped Cotton Kurta - XXXL&quot;,&quot;public_title&quot;:&quot;XXXL&quot;,&quot;options&quot;:[&quot;XXXL&quot;],&quot;price&quot;:173800,&quot;weight&quot;:0,&quot;compare_at_price&quot;:248300,&quot;inventory_management&quot;:&quot;shopify&quot;,&quot;barcode&quot;:null,&quot;quantity_rule&quot;:{&quot;min&quot;:1,&quot;max&quot;:null,&quot;increment&quot;:1}}]}">
  <div class="inner-top">
    <div class="product-top">
      <div class="product-image image-swap">
        <a href="/products/black-cotton-plain-striped-cotton-kurta?pr_prod_strat=copurchase&amp;pr_rec_id=e3a9801a1&amp;pr_rec_pid=7797721759966&amp;pr_ref_pid=7797834973406&amp;pr_seq=uniform" class="product-grid-image adaptive_height" data-collections-related="/collections/?view=related" style="padding-top: 147.5%" tabindex="-1">
          



  <picture data-index="0">
    <source class="image-source-1" data-srcset="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-261-17_1_300x.jpg?v=1663265215" media="(max-width: 767px)" sizes="153px" srcset="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-261-17_1_300x.jpg?v=1663265215">
    <source class="image-source-2" data-srcset=" //cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-261-17_1_360x.jpg?v=1663265215 360w,
                          //cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-261-17_1_540x.jpg?v=1663265215 540w,
                          //cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-261-17_1_720x.jpg?v=1663265215 720w,
                          //cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-261-17_1_900x.jpg?v=1663265215 900w" media="(min-width: 768px)" sizes="153px" srcset=" //cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-261-17_1_360x.jpg?v=1663265215 360w,
                          //cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-261-17_1_540x.jpg?v=1663265215 540w,
                          //cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-261-17_1_720x.jpg?v=1663265215 720w,
                          //cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-261-17_1_900x.jpg?v=1663265215 900w">

    <img alt="Black Cotton Plain Striped Cotton Kurta" class="images-one lazyautosizes lazyloaded" data-widths="[180, 360, 540, 720, 900, 1080, 1296, 1512, 1728, 2048]" data-aspectratio="0.6779661016949152" data-sizes="auto" data-srcset="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-261-17_1_360x.jpg?v=1663265215 360w,
                    //cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-261-17_1_540x.jpg?v=1663265215 540w,
                    //cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-261-17_1_720x.jpg?v=1663265215 720w,
                    //cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-261-17_1_900x.jpg?v=1663265215 900w" data-image="" sizes="153px" srcset="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-261-17_1_360x.jpg?v=1663265215 360w,
                    //cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-261-17_1_540x.jpg?v=1663265215 540w,
                    //cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-261-17_1_720x.jpg?v=1663265215 720w,
                    //cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-261-17_1_900x.jpg?v=1663265215 900w">
  </picture>
  <span class="images-two">
    
    <picture data-index="1">
      <source data-srcset="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-261-17_2_300x.jpg?v=1663265215" media="(max-width: 767px)" sizes="153px" srcset="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-261-17_2_300x.jpg?v=1663265215">
      <source data-srcset="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-261-17_2_360x.jpg?v=1663265215 360w,
                            //cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-261-17_2_540x.jpg?v=1663265215 540w,
                            //cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-261-17_2_720x.jpg?v=1663265215 720w,
                            //cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-261-17_2_900x.jpg?v=1663265215 900w" media="(min-width: 768px)" sizes="153px" srcset="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-261-17_2_360x.jpg?v=1663265215 360w,
                            //cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-261-17_2_540x.jpg?v=1663265215 540w,
                            //cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-261-17_2_720x.jpg?v=1663265215 720w,
                            //cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-261-17_2_900x.jpg?v=1663265215 900w">

      <img alt="Black Cotton Plain Striped Cotton Kurta" class="lazyautosizes lazyloaded" data-widths="[180, 360, 540, 720, 900, 1080, 1296, 1512, 1728, 2048]" data-aspectratio="0.6779661016949152" data-sizes="auto" data-srcset="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-261-17_2_360x.jpg?v=1663265215 360w,
                        //cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-261-17_2_540x.jpg?v=1663265215 540w,
                        //cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-261-17_2_720x.jpg?v=1663265215 720w,
                        //cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-261-17_2_900x.jpg?v=1663265215 900w" data-image="" sizes="153px" srcset="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-261-17_2_360x.jpg?v=1663265215 360w,
                        //cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-261-17_2_540x.jpg?v=1663265215 540w,
                        //cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-261-17_2_720x.jpg?v=1663265215 720w,
                        //cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-261-17_2_900x.jpg?v=1663265215 900w">
    </picture>
    
  </span>

        </a>
      </div>

      
  
  <div class="product-label" data-label-new-number="30">
    

    
      
        <strong class="label sale-label">
          Sale
        </strong>
        
      
    

    
    
    

    
    
    
  </div>
  






      
<a class="wishlist " data-icon-wishlist="" aria-label="Wish Lists" href="#" data-product-handle="black-cotton-plain-striped-cotton-kurta" data-id="7797721759966" tabindex="-1">
	    	<svg viewBox="0 0 512 512">
<g>
<g>
  <path d="M474.644,74.27C449.391,45.616,414.358,29.836,376,29.836c-53.948,0-88.103,32.22-107.255,59.25
    c-4.969,7.014-9.196,14.047-12.745,20.665c-3.549-6.618-7.775-13.651-12.745-20.665c-19.152-27.03-53.307-59.25-107.255-59.25
    c-38.358,0-73.391,15.781-98.645,44.435C13.267,101.605,0,138.213,0,177.351c0,42.603,16.633,82.228,52.345,124.7
    c31.917,37.96,77.834,77.088,131.005,122.397c19.813,16.884,40.302,34.344,62.115,53.429l0.655,0.574
    c2.828,2.476,6.354,3.713,9.88,3.713s7.052-1.238,9.88-3.713l0.655-0.574c21.813-19.085,42.302-36.544,62.118-53.431
    c53.168-45.306,99.085-84.434,131.002-122.395C495.367,259.578,512,219.954,512,177.351
    C512,138.213,498.733,101.605,474.644,74.27z M309.193,401.614c-17.08,14.554-34.658,29.533-53.193,45.646
    c-18.534-16.111-36.113-31.091-53.196-45.648C98.745,312.939,30,254.358,30,177.351c0-31.83,10.605-61.394,29.862-83.245
    C79.34,72.007,106.379,59.836,136,59.836c41.129,0,67.716,25.338,82.776,46.594c13.509,19.064,20.558,38.282,22.962,45.659
    c2.011,6.175,7.768,10.354,14.262,10.354c6.494,0,12.251-4.179,14.262-10.354c2.404-7.377,9.453-26.595,22.962-45.66
    c15.06-21.255,41.647-46.593,82.776-46.593c29.621,0,56.66,12.171,76.137,34.27C471.395,115.957,482,145.521,482,177.351
    C482,254.358,413.255,312.939,309.193,401.614z"></path>
</g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
</svg>
		
</a>
 


      
        <a class="quickview-button" href="javascript:void(0)" id="black-cotton-plain-striped-cotton-kurta" title="Quick View" tabindex="-1">
          <span>Quick view</span>
          <svg viewBox="0 0 511.999 511.999" width="100%" height="100%">
    <path d="M508.745,246.041c-4.574-6.257-113.557-153.206-252.748-153.206S7.818,239.784,3.249,246.035         c-4.332,5.936-4.332,13.987,0,19.923c4.569,6.257,113.557,153.206,252.748,153.206s248.174-146.95,252.748-153.201         C513.083,260.028,513.083,251.971,508.745,246.041z M255.997,385.406c-102.529,0-191.33-97.533-217.617-129.418         c26.253-31.913,114.868-129.395,217.617-129.395c102.524,0,191.319,97.516,217.617,129.418         C447.361,287.923,358.746,385.406,255.997,385.406z"></path>
      <path d="M255.997,154.725c-55.842,0-101.275,45.433-101.275,101.275s45.433,101.275,101.275,101.275         s101.275-45.433,101.275-101.275S311.839,154.725,255.997,154.725z M255.997,323.516c-37.23,0-67.516-30.287-67.516-67.516         s30.287-67.516,67.516-67.516s67.516,30.287,67.516,67.516S293.227,323.516,255.997,323.516z"></path>
</svg>
        </a>
      

      

    
    <div class="product-bottom">
      
      
        <span class="spr-badge" id="spr_badge_7797721759966" data-rating="0.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star-empty" aria-hidden="true"></i><i class="spr-icon spr-icon-star-empty" aria-hidden="true"></i><i class="spr-icon spr-icon-star-empty" aria-hidden="true"></i><i class="spr-icon spr-icon-star-empty" aria-hidden="true"></i><i class="spr-icon spr-icon-star-empty" aria-hidden="true"></i></span><span class="spr-badge-caption">No reviews</span>
</span>

      
      <a class="product-title " href="/products/black-cotton-plain-striped-cotton-kurta?pr_prod_strat=copurchase&amp;pr_rec_id=e3a9801a1&amp;pr_rec_pid=7797721759966&amp;pr_ref_pid=7797834973406&amp;pr_seq=uniform" tabindex="-1">
        
<span>Black Cotton Plain Striped Cotton Kurta</span>

        
          
          
      </a>
      <div class="half-grid-rais">
      <div class="price-box">
        
        <div class="price-sale">
          
            <span class="special-price" data-price-grid=""><span class="money">₹1738.00</span></span>
            <span class="old-price" data-compare-price-grid="">2483.00</span>
            <span class="percentage-price">(30%)</span>
         
        </div>
        
      </div>
    </div>

      <div class="add-to-cart-grid-rais">
        
        <div class="action">
          


<form action="/cart/add" method="post" class="variants" id="grid-product-form--7797721759966-product-recommendations" data-id="product-actions-7797721759966" enctype="multipart/form-data">
  
  

    
      
        <a class="rais-svgcart" data-href="/products/black-cotton-plain-striped-cotton-kurta?pr_prod_strat=copurchase&amp;pr_rec_id=e3a9801a1&amp;pr_rec_pid=7797721759966&amp;pr_ref_pid=7797834973406&amp;pr_seq=uniform" title="Black Cotton Plain Striped Cotton Kurta" data-init-quickshop="" tabindex="-1">
        <svg width="27px" height="27px" viewBox="0 0 27 27" version="1.1" xmlns="http://www.w3.org/2000/svg"><g id="Tjori-" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g id="PLP" transform="translate(-361.000000, -962.000000)"><g id="Product-Widget-Copy" transform="translate(18.000000, 646.000000)"><g id="Product-Widget"><g id="Shopping-Bag-Icon" transform="translate(343.000000, 316.000000)"><path d="M21.6999607,6.00003576 L26.1999499,6.00003576 L26.1999499,20.2500018 C26.1999499,21.2812493 25.8327635,22.1640595 25.0983901,22.898433 C24.3640166,23.6328065 23.4812064,23.9999928 22.4499589,23.9999928 L8.94999106,23.9999928 C7.91874352,23.9999928 7.03593336,23.6328065 6.30155987,22.898433 C5.56718639,22.1640595 5.2,21.2812493 5.2,20.2500018 L5.2,6.00003576 L9.69998927,6.00003576 C9.69998927,4.90628861 9.96561388,3.89847851 10.4968624,2.97660547 C11.0281109,2.05473243 11.7546716,1.32817166 12.6765447,0.796923168 C13.5984177,0.265674673 14.6062278,5.00679016e-05 15.699975,5.00679016e-05 C16.7937221,5.00679016e-05 17.8015322,0.265674673 18.7234053,0.796923168 C19.6452783,1.32817166 20.3718391,2.05473243 20.9030876,2.97660547 C21.4343361,3.89847851 21.6999607,4.90628861 21.6999607,6.00003576 Z M15.699975,1.50004649 C14.4499777,1.50004649 13.3874807,1.93754521 12.5124826,2.81254336 C11.6374844,3.68754151 11.1999857,4.7500385 11.1999857,6.00003576 L20.1999642,6.00003576 C20.1999642,4.7500385 19.7624655,3.68754151 18.8874674,2.81254336 C18.0124692,1.93754521 16.9499722,1.50004649 15.699975,1.50004649 Z M24.6999535,20.2500018 L24.6999535,7.50003219 L21.6999607,7.50003219 L21.6999607,9.75002682 C21.6999607,9.96877654 21.6296483,10.1484634 21.4890237,10.289088 C21.348399,10.4297127 21.1687122,10.500025 20.9499624,10.500025 C20.7312127,10.500025 20.5515259,10.4297127 20.4109012,10.289088 C20.2702766,10.1484634 20.1999642,9.96877654 20.1999642,9.75002682 L20.1999642,7.50003219 L11.1999857,7.50003219 L11.1999857,9.75002682 C11.1999857,9.96877654 11.1296734,10.1484634 10.9890487,10.289088 C10.848424,10.4297127 10.6687372,10.500025 10.4499875,10.500025 C10.2312378,10.500025 10.0515509,10.4297127 9.91092627,10.289088 C9.7703016,10.1484634 9.69998927,9.96877654 9.69998927,9.75002682 L9.69998927,7.50003219 L6.69999642,7.50003219 L6.69999642,20.2500018 C6.69999642,20.8750001 6.91874614,21.4062493 7.35624486,21.843748 C7.79374358,22.2812467 8.32499279,22.4999964 8.94999106,22.4999964 L22.4499589,22.4999964 C23.0749571,22.4999964 23.6062064,22.2812467 24.0437051,21.843748 C24.4812038,21.4062493 24.6999535,20.8750001 24.6999535,20.2500018 Z" id="shopping-bag" fill="#34302B"></path><circle id="Oval" fill="#FFFFFF" cx="6.5" cy="20.5" r="5.5"></circle><path d="M6.34998569,14.6875246 C7.39685832,14.6875246 8.36560589,14.9492428 9.25622877,15.472679 C10.1468516,15.9961151 10.8538813,16.7031448 11.3773175,17.5937677 C11.9007536,18.4843905 12.1624718,19.4531381 12.1624718,20.5000107 C12.1624718,21.5468834 11.9007536,22.5156309 11.3773175,23.4062538 C10.8538813,24.2968767 10.1468516,25.0039064 9.25622877,25.5273425 C8.36560589,26.0507786 7.39685832,26.3124969 6.34998569,26.3124969 C5.30311307,26.3124969 4.3343655,26.0507786 3.44374262,25.5273425 C2.55311975,25.0039064 1.84609006,24.2968767 1.32265393,23.4062538 C0.799217798,22.5156309 0.537499553,21.5468834 0.537499553,20.5000107 C0.537499553,19.4531381 0.799217798,18.4843905 1.32265393,17.5937677 C1.84609006,16.7031448 2.55311975,15.9961151 3.44374262,15.472679 C4.3343655,14.9492428 5.30311307,14.6875246 6.34998569,14.6875246 Z M9.72497765,21.1562592 L9.72497765,19.8437623 C9.72497765,19.7656376 9.69763384,19.6992313 9.64294659,19.644544 C9.58825934,19.5898568 9.52185301,19.562513 9.44372832,19.562513 L7.28748346,19.562513 L7.28748346,17.4062681 C7.28748346,17.3281434 7.26013966,17.2617371 7.20545241,17.2070498 C7.15076515,17.1523626 7.08435882,17.1250188 7.00623413,17.1250188 L5.69373726,17.1250188 C5.61561256,17.1250188 5.54920623,17.1523626 5.49451898,17.2070498 C5.43983173,17.2617371 5.41248793,17.3281434 5.41248793,17.4062681 L5.41248793,19.562513 L3.25624307,19.562513 C3.17811838,19.562513 3.11171205,19.5898568 3.0570248,19.644544 C3.00233755,19.6992313 2.97499374,19.7656376 2.97499374,19.8437623 L2.97499374,21.1562592 C2.97499374,21.2343839 3.00233755,21.3007902 3.0570248,21.3554774 C3.11171205,21.4101647 3.17811838,21.4375085 3.25624307,21.4375085 L5.41248793,21.4375085 L5.41248793,23.5937534 C5.41248793,23.671878 5.43983173,23.7382844 5.49451898,23.7929716 C5.54920623,23.8476589 5.61561256,23.8750027 5.69373726,23.8750027 L7.00623413,23.8750027 C7.08435882,23.8750027 7.15076515,23.8476589 7.20545241,23.7929716 C7.26013966,23.7382844 7.28748346,23.671878 7.28748346,23.5937534 L7.28748346,21.4375085 L9.44372832,21.4375085 C9.52185301,21.4375085 9.58825934,21.4101647 9.64294659,21.3554774 C9.69763384,21.3007902 9.72497765,21.2343839 9.72497765,21.1562592 Z" id="plus-circle" fill="#C0B394"></path></g></g></g></g></g></svg>
        </a>
      
    
  
  
</form>

  
      
          <div class="product-card__variant--popup">
              <div class="product-card__variant--popup--content">
    
    <form action="/cart/add" method="post" class="variants" id="swatch-grid-product-form--7797721759966-product-recommendations-" data-id="product-actions-7797721759966" enctype="multipart/form-data">
        
            
            
            
                
                    <div class="selector-wrapper selector-wrapper-1 swatch js product-form__item" data-option-index="0">
                        <label class="form-label ">
                            Size: <span class="label-value-1">XS</span>
                        </label>
                        
                        
                        

                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size xs available" data-value="XS">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7797721759966-product-recommendations" data-index="option1" value="XS" id="SingleOptionSelector0-XS-7797721759966-product-recommendations" tabindex="-1">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-XS-7797721759966-product-recommendations" data-original-title="XS">XS</label>
                                </div>
                            
                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size s available" data-value="S">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7797721759966-product-recommendations" data-index="option1" value="S" id="SingleOptionSelector0-S-7797721759966-product-recommendations" tabindex="-1">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-S-7797721759966-product-recommendations" data-original-title="S">S</label>
                                </div>
                            
                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size m available" data-value="M">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7797721759966-product-recommendations" data-index="option1" value="M" id="SingleOptionSelector0-M-7797721759966-product-recommendations" tabindex="-1">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-M-7797721759966-product-recommendations" data-original-title="M">M</label>
                                </div>
                            
                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size l available" data-value="L">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7797721759966-product-recommendations" data-index="option1" value="L" id="SingleOptionSelector0-L-7797721759966-product-recommendations" tabindex="-1">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-L-7797721759966-product-recommendations" data-original-title="L">L</label>
                                </div>
                            
                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size xl available" data-value="XL">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7797721759966-product-recommendations" data-index="option1" value="XL" id="SingleOptionSelector0-XL-7797721759966-product-recommendations" tabindex="-1">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-XL-7797721759966-product-recommendations" data-original-title="XL">XL</label>
                                </div>
                            
                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size xxl available" data-value="XXL">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7797721759966-product-recommendations" data-index="option1" value="XXL" id="SingleOptionSelector0-XXL-7797721759966-product-recommendations" tabindex="-1">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-XXL-7797721759966-product-recommendations" data-original-title="XXL">XXL</label>
                                </div>
                            
                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size xxxl available" data-value="XXXL">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7797721759966-product-recommendations" data-index="option1" value="XXXL" id="SingleOptionSelector0-XXXL-7797721759966-product-recommendations" tabindex="-1">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-XXXL-7797721759966-product-recommendations" data-original-title="XXXL">XXXL</label>
                                </div>
                            
                        
                    </div>
                
            
        

        <select name="id" id="ProductSelect-7797721759966-product-recommendations" class="product-form__variants no-js" tabindex="-1">
            
                
                    <option value="43398847496414">
                        XS
                    </option>
                
            
                
                    <option value="43398847561950">
                        S
                    </option>
                
            
                
                    <option value="43398847594718">
                        M
                    </option>
                
            
                
                    <option value="43398847627486">
                        L
                    </option>
                
            
                
                    <option value="43398847660254">
                        XL
                    </option>
                
            
                
                    <option value="43398847693022">
                        XXL
                    </option>
                
            
                
                    <option value="43398847725790">
                        XXXL
                    </option>
                
            
        </select>
        <div class="product-card__button_cancel_mobile">
            <a class="btn btn-cancel" data-cancel-swatch-qs="" tabindex="-1"><svg aria-hidden="true" data-prefix="fal" data-icon="times" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" class="svg-inline--fa fa-times fa-w-10 fa-2x"><path fill="currentColor" d="M193.94 256L296.5 153.44l21.15-21.15c3.12-3.12 3.12-8.19 0-11.31l-22.63-22.63c-3.12-3.12-8.19-3.12-11.31 0L160 222.06 36.29 98.34c-3.12-3.12-8.19-3.12-11.31 0L2.34 120.97c-3.12 3.12-3.12 8.19 0 11.31L126.06 256 2.34 379.71c-3.12 3.12-3.12 8.19 0 11.31l22.63 22.63c3.12 3.12 8.19 3.12 11.31 0L160 289.94 262.56 392.5l21.15 21.15c3.12 3.12 8.19 3.12 11.31 0l22.63-22.63c3.12-3.12 3.12-8.19 0-11.31L193.94 256z" class=""></path></svg></a>
        </div>
        <div class="product-card__button2">
            <input type="hidden" name="quantity" value="1" tabindex="-1">
            <button data-btn-addtocart="" class="btn add-to-cart-btn" type="submit" data-form-id="#swatch-grid-product-form--7797721759966-product-recommendations-" tabindex="-1">
                Submit
            </button>
            <a class="btn btn-cancel" data-cancel-swatch-qs="" tabindex="-1">Cancel</a>
        </div>
        
    </form>
</div>
          </div>
      
  

        </div>
        
      </div>
    </div>

      <div class="wrapper-size">
        
      </div>
      
    </div>

    
     
    <div class="product-details">
      

      <a class="product-title " href="/products/black-cotton-plain-striped-cotton-kurta?pr_prod_strat=copurchase&amp;pr_rec_id=e3a9801a1&amp;pr_rec_pid=7797721759966&amp;pr_ref_pid=7797834973406&amp;pr_seq=uniform" tabindex="-1">
        
<span>Black Cotton Plain Striped Cotton Kurta</span>

        
          
        
      </a>

      
      <span class="spr-badge" id="spr_badge_7797721759966" data-rating="0.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star-empty" aria-hidden="true"></i><i class="spr-icon spr-icon-star-empty" aria-hidden="true"></i><i class="spr-icon spr-icon-star-empty" aria-hidden="true"></i><i class="spr-icon spr-icon-star-empty" aria-hidden="true"></i><i class="spr-icon spr-icon-star-empty" aria-hidden="true"></i></span><span class="spr-badge-caption">No reviews</span>
</span>

      

      <div class="short-description">
        
        <div>
          Minimal and classy, this high-slit kurta is ideal for work-from-home or go-to-office days. Made in cotton linen, the fabric is soft and breathable. Dimension In Inches: Length - 43 Inches, Sleeves- 10.5 Inches Dimension In Centimetres: Length - 109.2 cm,...
        </div>
      </div>

      <div class="price-box">
        
        <div class="price-sale">
          
            <span class="old-price" data-compare-price-grid=""><span class="money">₹2,483.00</span></span>
            <span class="special-price" data-price-grid=""><span class="money">₹1,738.00</span></span>
          
        </div>
        
      </div>

      

      <div class="wrapper-button-card">
        
        <div class="action">
          


<form action="/cart/add" method="post" class="variants" id="list-product-form-7797721759966-product-recommendations" data-id="product-actions-7797721759966" enctype="multipart/form-data">
  
  

    
      
        <a class="rais-svgcart" data-href="/products/black-cotton-plain-striped-cotton-kurta?pr_prod_strat=copurchase&amp;pr_rec_id=e3a9801a1&amp;pr_rec_pid=7797721759966&amp;pr_ref_pid=7797834973406&amp;pr_seq=uniform" title="Black Cotton Plain Striped Cotton Kurta" data-init-quickshop="" tabindex="-1">
        <svg width="27px" height="27px" viewBox="0 0 27 27" version="1.1" xmlns="http://www.w3.org/2000/svg"><g id="Tjori-" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g id="PLP" transform="translate(-361.000000, -962.000000)"><g id="Product-Widget-Copy" transform="translate(18.000000, 646.000000)"><g id="Product-Widget"><g id="Shopping-Bag-Icon" transform="translate(343.000000, 316.000000)"><path d="M21.6999607,6.00003576 L26.1999499,6.00003576 L26.1999499,20.2500018 C26.1999499,21.2812493 25.8327635,22.1640595 25.0983901,22.898433 C24.3640166,23.6328065 23.4812064,23.9999928 22.4499589,23.9999928 L8.94999106,23.9999928 C7.91874352,23.9999928 7.03593336,23.6328065 6.30155987,22.898433 C5.56718639,22.1640595 5.2,21.2812493 5.2,20.2500018 L5.2,6.00003576 L9.69998927,6.00003576 C9.69998927,4.90628861 9.96561388,3.89847851 10.4968624,2.97660547 C11.0281109,2.05473243 11.7546716,1.32817166 12.6765447,0.796923168 C13.5984177,0.265674673 14.6062278,5.00679016e-05 15.699975,5.00679016e-05 C16.7937221,5.00679016e-05 17.8015322,0.265674673 18.7234053,0.796923168 C19.6452783,1.32817166 20.3718391,2.05473243 20.9030876,2.97660547 C21.4343361,3.89847851 21.6999607,4.90628861 21.6999607,6.00003576 Z M15.699975,1.50004649 C14.4499777,1.50004649 13.3874807,1.93754521 12.5124826,2.81254336 C11.6374844,3.68754151 11.1999857,4.7500385 11.1999857,6.00003576 L20.1999642,6.00003576 C20.1999642,4.7500385 19.7624655,3.68754151 18.8874674,2.81254336 C18.0124692,1.93754521 16.9499722,1.50004649 15.699975,1.50004649 Z M24.6999535,20.2500018 L24.6999535,7.50003219 L21.6999607,7.50003219 L21.6999607,9.75002682 C21.6999607,9.96877654 21.6296483,10.1484634 21.4890237,10.289088 C21.348399,10.4297127 21.1687122,10.500025 20.9499624,10.500025 C20.7312127,10.500025 20.5515259,10.4297127 20.4109012,10.289088 C20.2702766,10.1484634 20.1999642,9.96877654 20.1999642,9.75002682 L20.1999642,7.50003219 L11.1999857,7.50003219 L11.1999857,9.75002682 C11.1999857,9.96877654 11.1296734,10.1484634 10.9890487,10.289088 C10.848424,10.4297127 10.6687372,10.500025 10.4499875,10.500025 C10.2312378,10.500025 10.0515509,10.4297127 9.91092627,10.289088 C9.7703016,10.1484634 9.69998927,9.96877654 9.69998927,9.75002682 L9.69998927,7.50003219 L6.69999642,7.50003219 L6.69999642,20.2500018 C6.69999642,20.8750001 6.91874614,21.4062493 7.35624486,21.843748 C7.79374358,22.2812467 8.32499279,22.4999964 8.94999106,22.4999964 L22.4499589,22.4999964 C23.0749571,22.4999964 23.6062064,22.2812467 24.0437051,21.843748 C24.4812038,21.4062493 24.6999535,20.8750001 24.6999535,20.2500018 Z" id="shopping-bag" fill="#34302B"></path><circle id="Oval" fill="#FFFFFF" cx="6.5" cy="20.5" r="5.5"></circle><path d="M6.34998569,14.6875246 C7.39685832,14.6875246 8.36560589,14.9492428 9.25622877,15.472679 C10.1468516,15.9961151 10.8538813,16.7031448 11.3773175,17.5937677 C11.9007536,18.4843905 12.1624718,19.4531381 12.1624718,20.5000107 C12.1624718,21.5468834 11.9007536,22.5156309 11.3773175,23.4062538 C10.8538813,24.2968767 10.1468516,25.0039064 9.25622877,25.5273425 C8.36560589,26.0507786 7.39685832,26.3124969 6.34998569,26.3124969 C5.30311307,26.3124969 4.3343655,26.0507786 3.44374262,25.5273425 C2.55311975,25.0039064 1.84609006,24.2968767 1.32265393,23.4062538 C0.799217798,22.5156309 0.537499553,21.5468834 0.537499553,20.5000107 C0.537499553,19.4531381 0.799217798,18.4843905 1.32265393,17.5937677 C1.84609006,16.7031448 2.55311975,15.9961151 3.44374262,15.472679 C4.3343655,14.9492428 5.30311307,14.6875246 6.34998569,14.6875246 Z M9.72497765,21.1562592 L9.72497765,19.8437623 C9.72497765,19.7656376 9.69763384,19.6992313 9.64294659,19.644544 C9.58825934,19.5898568 9.52185301,19.562513 9.44372832,19.562513 L7.28748346,19.562513 L7.28748346,17.4062681 C7.28748346,17.3281434 7.26013966,17.2617371 7.20545241,17.2070498 C7.15076515,17.1523626 7.08435882,17.1250188 7.00623413,17.1250188 L5.69373726,17.1250188 C5.61561256,17.1250188 5.54920623,17.1523626 5.49451898,17.2070498 C5.43983173,17.2617371 5.41248793,17.3281434 5.41248793,17.4062681 L5.41248793,19.562513 L3.25624307,19.562513 C3.17811838,19.562513 3.11171205,19.5898568 3.0570248,19.644544 C3.00233755,19.6992313 2.97499374,19.7656376 2.97499374,19.8437623 L2.97499374,21.1562592 C2.97499374,21.2343839 3.00233755,21.3007902 3.0570248,21.3554774 C3.11171205,21.4101647 3.17811838,21.4375085 3.25624307,21.4375085 L5.41248793,21.4375085 L5.41248793,23.5937534 C5.41248793,23.671878 5.43983173,23.7382844 5.49451898,23.7929716 C5.54920623,23.8476589 5.61561256,23.8750027 5.69373726,23.8750027 L7.00623413,23.8750027 C7.08435882,23.8750027 7.15076515,23.8476589 7.20545241,23.7929716 C7.26013966,23.7382844 7.28748346,23.671878 7.28748346,23.5937534 L7.28748346,21.4375085 L9.44372832,21.4375085 C9.52185301,21.4375085 9.58825934,21.4101647 9.64294659,21.3554774 C9.69763384,21.3007902 9.72497765,21.2343839 9.72497765,21.1562592 Z" id="plus-circle" fill="#C0B394"></path></g></g></g></g></g></svg>
        </a>
      
    
  
  
</form>

  
      
          <div class="product-card__variant--popup">
              <div class="product-card__variant--popup--content">
    
    <form action="/cart/add" method="post" class="variants" id="swatch-list-product-form-7797721759966-product-recommendations-list" data-id="product-actions-7797721759966" enctype="multipart/form-data">
        
            
            
            
                
                    <div class="selector-wrapper selector-wrapper-1 swatch js product-form__item" data-option-index="0">
                        <label class="form-label ">
                            Size: <span class="label-value-1">XS</span>
                        </label>
                        
                        
                        

                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size xs available" data-value="XS">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7797721759966-product-recommendationslist" data-index="option1" value="XS" id="SingleOptionSelector0-XS-7797721759966-product-recommendationslist" tabindex="-1">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-XS-7797721759966-product-recommendationslist" data-original-title="XS">XS</label>
                                </div>
                            
                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size s available" data-value="S">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7797721759966-product-recommendationslist" data-index="option1" value="S" id="SingleOptionSelector0-S-7797721759966-product-recommendationslist" tabindex="-1">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-S-7797721759966-product-recommendationslist" data-original-title="S">S</label>
                                </div>
                            
                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size m available" data-value="M">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7797721759966-product-recommendationslist" data-index="option1" value="M" id="SingleOptionSelector0-M-7797721759966-product-recommendationslist" tabindex="-1">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-M-7797721759966-product-recommendationslist" data-original-title="M">M</label>
                                </div>
                            
                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size l available" data-value="L">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7797721759966-product-recommendationslist" data-index="option1" value="L" id="SingleOptionSelector0-L-7797721759966-product-recommendationslist" tabindex="-1">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-L-7797721759966-product-recommendationslist" data-original-title="L">L</label>
                                </div>
                            
                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size xl available" data-value="XL">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7797721759966-product-recommendationslist" data-index="option1" value="XL" id="SingleOptionSelector0-XL-7797721759966-product-recommendationslist" tabindex="-1">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-XL-7797721759966-product-recommendationslist" data-original-title="XL">XL</label>
                                </div>
                            
                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size xxl available" data-value="XXL">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7797721759966-product-recommendationslist" data-index="option1" value="XXL" id="SingleOptionSelector0-XXL-7797721759966-product-recommendationslist" tabindex="-1">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-XXL-7797721759966-product-recommendationslist" data-original-title="XXL">XXL</label>
                                </div>
                            
                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size xxxl available" data-value="XXXL">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7797721759966-product-recommendationslist" data-index="option1" value="XXXL" id="SingleOptionSelector0-XXXL-7797721759966-product-recommendationslist" tabindex="-1">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-XXXL-7797721759966-product-recommendationslist" data-original-title="XXXL">XXXL</label>
                                </div>
                            
                        
                    </div>
                
            
        

        <select name="id" id="ProductSelect-7797721759966-product-recommendationslist" class="product-form__variants no-js" tabindex="-1">
            
                
                    <option value="43398847496414">
                        XS
                    </option>
                
            
                
                    <option value="43398847561950">
                        S
                    </option>
                
            
                
                    <option value="43398847594718">
                        M
                    </option>
                
            
                
                    <option value="43398847627486">
                        L
                    </option>
                
            
                
                    <option value="43398847660254">
                        XL
                    </option>
                
            
                
                    <option value="43398847693022">
                        XXL
                    </option>
                
            
                
                    <option value="43398847725790">
                        XXXL
                    </option>
                
            
        </select>
        <div class="product-card__button_cancel_mobile">
            <a class="btn btn-cancel" data-cancel-swatch-qs="" tabindex="-1"><svg aria-hidden="true" data-prefix="fal" data-icon="times" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" class="svg-inline--fa fa-times fa-w-10 fa-2x"><path fill="currentColor" d="M193.94 256L296.5 153.44l21.15-21.15c3.12-3.12 3.12-8.19 0-11.31l-22.63-22.63c-3.12-3.12-8.19-3.12-11.31 0L160 222.06 36.29 98.34c-3.12-3.12-8.19-3.12-11.31 0L2.34 120.97c-3.12 3.12-3.12 8.19 0 11.31L126.06 256 2.34 379.71c-3.12 3.12-3.12 8.19 0 11.31l22.63 22.63c3.12 3.12 8.19 3.12 11.31 0L160 289.94 262.56 392.5l21.15 21.15c3.12 3.12 8.19 3.12 11.31 0l22.63-22.63c3.12-3.12 3.12-8.19 0-11.31L193.94 256z" class=""></path></svg></a>
        </div>
        <div class="product-card__button2">
            <input type="hidden" name="quantity" value="1" tabindex="-1">
            <button data-btn-addtocart="" class="btn add-to-cart-btn" type="submit" data-form-id="#swatch-list-product-form-7797721759966-product-recommendations-list" tabindex="-1">
                Submit
            </button>
            <a class="btn btn-cancel" data-cancel-swatch-qs="" tabindex="-1">Cancel</a>
        </div>
        
    </form>
</div>
          </div>
      
  

        </div>
        
      </div>
      
    </div>
    
  </div>
</div>

          
        </div><div class="grid-item small--one-half medium-up--one-quarter col-12 slick-slide" style="width: 173px;" tabindex="-1" role="option" aria-describedby="slick-slide25" data-slick-index="5" aria-hidden="true">
          
            
<div class="inner product-item on-sale" data-product-id="product-7797807448286" data-json-product="{&quot;id&quot;: 7797807448286,&quot;handle&quot;: &quot;207-44&quot;,&quot;media&quot;: [{&quot;alt&quot;:null,&quot;id&quot;:30632461205726,&quot;position&quot;:1,&quot;preview_image&quot;:{&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:2360,&quot;width&quot;:1600,&quot;src&quot;:&quot;https:\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/276118-2019-04-30-16_26_10.721959-big-image.jpg?v=1663261263&quot;},&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:2360,&quot;media_type&quot;:&quot;image&quot;,&quot;src&quot;:&quot;https:\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/276118-2019-04-30-16_26_10.721959-big-image.jpg?v=1663261263&quot;,&quot;width&quot;:1600},{&quot;alt&quot;:null,&quot;id&quot;:30632461271262,&quot;position&quot;:2,&quot;preview_image&quot;:{&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:2360,&quot;width&quot;:1600,&quot;src&quot;:&quot;https:\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/276119-2019-04-30-16_26_53.491391-big-image.jpg?v=1663261263&quot;},&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:2360,&quot;media_type&quot;:&quot;image&quot;,&quot;src&quot;:&quot;https:\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/276119-2019-04-30-16_26_53.491391-big-image.jpg?v=1663261263&quot;,&quot;width&quot;:1600},{&quot;alt&quot;:null,&quot;id&quot;:30632461762782,&quot;position&quot;:3,&quot;preview_image&quot;:{&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:2360,&quot;width&quot;:1600,&quot;src&quot;:&quot;https:\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/276120-2019-04-30-16_26_53.918945-big-image.jpg?v=1663261265&quot;},&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:2360,&quot;media_type&quot;:&quot;image&quot;,&quot;src&quot;:&quot;https:\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/276120-2019-04-30-16_26_53.918945-big-image.jpg?v=1663261265&quot;,&quot;width&quot;:1600},{&quot;alt&quot;:null,&quot;id&quot;:30632461140190,&quot;position&quot;:4,&quot;preview_image&quot;:{&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:1180,&quot;width&quot;:800,&quot;src&quot;:&quot;https:\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/279899-2019-08-09-15_31_23.792813-big-image_7b9bc66d-9d39-495e-bd00-e6ff79902eb8.jpg?v=1663261263&quot;},&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:1180,&quot;media_type&quot;:&quot;image&quot;,&quot;src&quot;:&quot;https:\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/279899-2019-08-09-15_31_23.792813-big-image_7b9bc66d-9d39-495e-bd00-e6ff79902eb8.jpg?v=1663261263&quot;,&quot;width&quot;:800}],&quot;variants&quot;: [{&quot;id&quot;:43399171309790,&quot;title&quot;:&quot;XS&quot;,&quot;option1&quot;:&quot;XS&quot;,&quot;option2&quot;:null,&quot;option3&quot;:null,&quot;sku&quot;:&quot;TJ-MK-207-44-XS&quot;,&quot;requires_shipping&quot;:true,&quot;taxable&quot;:true,&quot;featured_image&quot;:null,&quot;available&quot;:true,&quot;name&quot;:&quot;The Chaste Violet Cotton Palazzo - XS&quot;,&quot;public_title&quot;:&quot;XS&quot;,&quot;options&quot;:[&quot;XS&quot;],&quot;price&quot;:182600,&quot;weight&quot;:0,&quot;compare_at_price&quot;:260800,&quot;inventory_management&quot;:&quot;shopify&quot;,&quot;barcode&quot;:null,&quot;quantity_rule&quot;:{&quot;min&quot;:1,&quot;max&quot;:null,&quot;increment&quot;:1}},{&quot;id&quot;:43399171342558,&quot;title&quot;:&quot;S&quot;,&quot;option1&quot;:&quot;S&quot;,&quot;option2&quot;:null,&quot;option3&quot;:null,&quot;sku&quot;:&quot;TJ-MK-207-44-S&quot;,&quot;requires_shipping&quot;:true,&quot;taxable&quot;:true,&quot;featured_image&quot;:null,&quot;available&quot;:true,&quot;name&quot;:&quot;The Chaste Violet Cotton Palazzo - S&quot;,&quot;public_title&quot;:&quot;S&quot;,&quot;options&quot;:[&quot;S&quot;],&quot;price&quot;:182600,&quot;weight&quot;:0,&quot;compare_at_price&quot;:260800,&quot;inventory_management&quot;:&quot;shopify&quot;,&quot;barcode&quot;:null,&quot;quantity_rule&quot;:{&quot;min&quot;:1,&quot;max&quot;:null,&quot;increment&quot;:1}},{&quot;id&quot;:43399171375326,&quot;title&quot;:&quot;M&quot;,&quot;option1&quot;:&quot;M&quot;,&quot;option2&quot;:null,&quot;option3&quot;:null,&quot;sku&quot;:&quot;TJ-MK-207-44-M&quot;,&quot;requires_shipping&quot;:true,&quot;taxable&quot;:true,&quot;featured_image&quot;:null,&quot;available&quot;:true,&quot;name&quot;:&quot;The Chaste Violet Cotton Palazzo - M&quot;,&quot;public_title&quot;:&quot;M&quot;,&quot;options&quot;:[&quot;M&quot;],&quot;price&quot;:182600,&quot;weight&quot;:0,&quot;compare_at_price&quot;:260800,&quot;inventory_management&quot;:&quot;shopify&quot;,&quot;barcode&quot;:null,&quot;quantity_rule&quot;:{&quot;min&quot;:1,&quot;max&quot;:null,&quot;increment&quot;:1}},{&quot;id&quot;:43399171408094,&quot;title&quot;:&quot;L&quot;,&quot;option1&quot;:&quot;L&quot;,&quot;option2&quot;:null,&quot;option3&quot;:null,&quot;sku&quot;:&quot;TJ-MK-207-44-L&quot;,&quot;requires_shipping&quot;:true,&quot;taxable&quot;:true,&quot;featured_image&quot;:null,&quot;available&quot;:true,&quot;name&quot;:&quot;The Chaste Violet Cotton Palazzo - L&quot;,&quot;public_title&quot;:&quot;L&quot;,&quot;options&quot;:[&quot;L&quot;],&quot;price&quot;:182600,&quot;weight&quot;:0,&quot;compare_at_price&quot;:260800,&quot;inventory_management&quot;:&quot;shopify&quot;,&quot;barcode&quot;:null,&quot;quantity_rule&quot;:{&quot;min&quot;:1,&quot;max&quot;:null,&quot;increment&quot;:1}},{&quot;id&quot;:43399171440862,&quot;title&quot;:&quot;XL&quot;,&quot;option1&quot;:&quot;XL&quot;,&quot;option2&quot;:null,&quot;option3&quot;:null,&quot;sku&quot;:&quot;TJ-MK-207-44-XL&quot;,&quot;requires_shipping&quot;:true,&quot;taxable&quot;:true,&quot;featured_image&quot;:null,&quot;available&quot;:true,&quot;name&quot;:&quot;The Chaste Violet Cotton Palazzo - XL&quot;,&quot;public_title&quot;:&quot;XL&quot;,&quot;options&quot;:[&quot;XL&quot;],&quot;price&quot;:182600,&quot;weight&quot;:0,&quot;compare_at_price&quot;:260800,&quot;inventory_management&quot;:&quot;shopify&quot;,&quot;barcode&quot;:null,&quot;quantity_rule&quot;:{&quot;min&quot;:1,&quot;max&quot;:null,&quot;increment&quot;:1}},{&quot;id&quot;:43399171506398,&quot;title&quot;:&quot;XXL&quot;,&quot;option1&quot;:&quot;XXL&quot;,&quot;option2&quot;:null,&quot;option3&quot;:null,&quot;sku&quot;:&quot;TJ-MK-207-44-XXL&quot;,&quot;requires_shipping&quot;:true,&quot;taxable&quot;:true,&quot;featured_image&quot;:null,&quot;available&quot;:true,&quot;name&quot;:&quot;The Chaste Violet Cotton Palazzo - XXL&quot;,&quot;public_title&quot;:&quot;XXL&quot;,&quot;options&quot;:[&quot;XXL&quot;],&quot;price&quot;:182600,&quot;weight&quot;:0,&quot;compare_at_price&quot;:260800,&quot;inventory_management&quot;:&quot;shopify&quot;,&quot;barcode&quot;:null,&quot;quantity_rule&quot;:{&quot;min&quot;:1,&quot;max&quot;:null,&quot;increment&quot;:1}},{&quot;id&quot;:43399171539166,&quot;title&quot;:&quot;XXXL&quot;,&quot;option1&quot;:&quot;XXXL&quot;,&quot;option2&quot;:null,&quot;option3&quot;:null,&quot;sku&quot;:&quot;TJ-MK-207-44-XXXL&quot;,&quot;requires_shipping&quot;:true,&quot;taxable&quot;:true,&quot;featured_image&quot;:null,&quot;available&quot;:true,&quot;name&quot;:&quot;The Chaste Violet Cotton Palazzo - XXXL&quot;,&quot;public_title&quot;:&quot;XXXL&quot;,&quot;options&quot;:[&quot;XXXL&quot;],&quot;price&quot;:182600,&quot;weight&quot;:0,&quot;compare_at_price&quot;:260800,&quot;inventory_management&quot;:&quot;shopify&quot;,&quot;barcode&quot;:null,&quot;quantity_rule&quot;:{&quot;min&quot;:1,&quot;max&quot;:null,&quot;increment&quot;:1}}]}">
  <div class="inner-top">
    <div class="product-top">
      <div class="product-image image-swap">
        <a href="/products/207-44?pr_prod_strat=copurchase&amp;pr_rec_id=e3a9801a1&amp;pr_rec_pid=7797807448286&amp;pr_ref_pid=7797834973406&amp;pr_seq=uniform" class="product-grid-image adaptive_height" data-collections-related="/collections/?view=related" style="padding-top: 147.5%" tabindex="-1">
          



  <picture data-index="0">
    <source class="image-source-1" data-srcset="//cdn.shopify.com/s/files/1/0613/2039/7022/products/276118-2019-04-30-16_26_10.721959-big-image_300x.jpg?v=1663261263" media="(max-width: 767px)" sizes="153px" srcset="//cdn.shopify.com/s/files/1/0613/2039/7022/products/276118-2019-04-30-16_26_10.721959-big-image_300x.jpg?v=1663261263">
    <source class="image-source-2" data-srcset=" //cdn.shopify.com/s/files/1/0613/2039/7022/products/276118-2019-04-30-16_26_10.721959-big-image_360x.jpg?v=1663261263 360w,
                          //cdn.shopify.com/s/files/1/0613/2039/7022/products/276118-2019-04-30-16_26_10.721959-big-image_540x.jpg?v=1663261263 540w,
                          //cdn.shopify.com/s/files/1/0613/2039/7022/products/276118-2019-04-30-16_26_10.721959-big-image_720x.jpg?v=1663261263 720w,
                          //cdn.shopify.com/s/files/1/0613/2039/7022/products/276118-2019-04-30-16_26_10.721959-big-image_900x.jpg?v=1663261263 900w" media="(min-width: 768px)" sizes="153px" srcset=" //cdn.shopify.com/s/files/1/0613/2039/7022/products/276118-2019-04-30-16_26_10.721959-big-image_360x.jpg?v=1663261263 360w,
                          //cdn.shopify.com/s/files/1/0613/2039/7022/products/276118-2019-04-30-16_26_10.721959-big-image_540x.jpg?v=1663261263 540w,
                          //cdn.shopify.com/s/files/1/0613/2039/7022/products/276118-2019-04-30-16_26_10.721959-big-image_720x.jpg?v=1663261263 720w,
                          //cdn.shopify.com/s/files/1/0613/2039/7022/products/276118-2019-04-30-16_26_10.721959-big-image_900x.jpg?v=1663261263 900w">

    <img alt="The Chaste Violet Cotton Palazzo" class="images-one lazyautosizes lazyloaded" data-widths="[180, 360, 540, 720, 900, 1080, 1296, 1512, 1728, 2048]" data-aspectratio="0.6779661016949152" data-sizes="auto" data-srcset="//cdn.shopify.com/s/files/1/0613/2039/7022/products/276118-2019-04-30-16_26_10.721959-big-image_360x.jpg?v=1663261263 360w,
                    //cdn.shopify.com/s/files/1/0613/2039/7022/products/276118-2019-04-30-16_26_10.721959-big-image_540x.jpg?v=1663261263 540w,
                    //cdn.shopify.com/s/files/1/0613/2039/7022/products/276118-2019-04-30-16_26_10.721959-big-image_720x.jpg?v=1663261263 720w,
                    //cdn.shopify.com/s/files/1/0613/2039/7022/products/276118-2019-04-30-16_26_10.721959-big-image_900x.jpg?v=1663261263 900w" data-image="" sizes="153px" srcset="//cdn.shopify.com/s/files/1/0613/2039/7022/products/276118-2019-04-30-16_26_10.721959-big-image_360x.jpg?v=1663261263 360w,
                    //cdn.shopify.com/s/files/1/0613/2039/7022/products/276118-2019-04-30-16_26_10.721959-big-image_540x.jpg?v=1663261263 540w,
                    //cdn.shopify.com/s/files/1/0613/2039/7022/products/276118-2019-04-30-16_26_10.721959-big-image_720x.jpg?v=1663261263 720w,
                    //cdn.shopify.com/s/files/1/0613/2039/7022/products/276118-2019-04-30-16_26_10.721959-big-image_900x.jpg?v=1663261263 900w">
  </picture>
  <span class="images-two">
    
    <picture data-index="1">
      <source data-srcset="//cdn.shopify.com/s/files/1/0613/2039/7022/products/276119-2019-04-30-16_26_53.491391-big-image_300x.jpg?v=1663261263" media="(max-width: 767px)" sizes="153px" srcset="//cdn.shopify.com/s/files/1/0613/2039/7022/products/276119-2019-04-30-16_26_53.491391-big-image_300x.jpg?v=1663261263">
      <source data-srcset="//cdn.shopify.com/s/files/1/0613/2039/7022/products/276119-2019-04-30-16_26_53.491391-big-image_360x.jpg?v=1663261263 360w,
                            //cdn.shopify.com/s/files/1/0613/2039/7022/products/276119-2019-04-30-16_26_53.491391-big-image_540x.jpg?v=1663261263 540w,
                            //cdn.shopify.com/s/files/1/0613/2039/7022/products/276119-2019-04-30-16_26_53.491391-big-image_720x.jpg?v=1663261263 720w,
                            //cdn.shopify.com/s/files/1/0613/2039/7022/products/276119-2019-04-30-16_26_53.491391-big-image_900x.jpg?v=1663261263 900w" media="(min-width: 768px)" sizes="153px" srcset="//cdn.shopify.com/s/files/1/0613/2039/7022/products/276119-2019-04-30-16_26_53.491391-big-image_360x.jpg?v=1663261263 360w,
                            //cdn.shopify.com/s/files/1/0613/2039/7022/products/276119-2019-04-30-16_26_53.491391-big-image_540x.jpg?v=1663261263 540w,
                            //cdn.shopify.com/s/files/1/0613/2039/7022/products/276119-2019-04-30-16_26_53.491391-big-image_720x.jpg?v=1663261263 720w,
                            //cdn.shopify.com/s/files/1/0613/2039/7022/products/276119-2019-04-30-16_26_53.491391-big-image_900x.jpg?v=1663261263 900w">

      <img alt="The Chaste Violet Cotton Palazzo" class="lazyautosizes lazyloaded" data-widths="[180, 360, 540, 720, 900, 1080, 1296, 1512, 1728, 2048]" data-aspectratio="0.6779661016949152" data-sizes="auto" data-srcset="//cdn.shopify.com/s/files/1/0613/2039/7022/products/276119-2019-04-30-16_26_53.491391-big-image_360x.jpg?v=1663261263 360w,
                        //cdn.shopify.com/s/files/1/0613/2039/7022/products/276119-2019-04-30-16_26_53.491391-big-image_540x.jpg?v=1663261263 540w,
                        //cdn.shopify.com/s/files/1/0613/2039/7022/products/276119-2019-04-30-16_26_53.491391-big-image_720x.jpg?v=1663261263 720w,
                        //cdn.shopify.com/s/files/1/0613/2039/7022/products/276119-2019-04-30-16_26_53.491391-big-image_900x.jpg?v=1663261263 900w" data-image="" sizes="153px" srcset="//cdn.shopify.com/s/files/1/0613/2039/7022/products/276119-2019-04-30-16_26_53.491391-big-image_360x.jpg?v=1663261263 360w,
                        //cdn.shopify.com/s/files/1/0613/2039/7022/products/276119-2019-04-30-16_26_53.491391-big-image_540x.jpg?v=1663261263 540w,
                        //cdn.shopify.com/s/files/1/0613/2039/7022/products/276119-2019-04-30-16_26_53.491391-big-image_720x.jpg?v=1663261263 720w,
                        //cdn.shopify.com/s/files/1/0613/2039/7022/products/276119-2019-04-30-16_26_53.491391-big-image_900x.jpg?v=1663261263 900w">
    </picture>
    
  </span>

        </a>
      </div>

      
  
  <div class="product-label" data-label-new-number="30">
    

    
      
        <strong class="label sale-label">
          Sale
        </strong>
        
      
    

    
    
    

    
    
    
  </div>
  






      
<a class="wishlist " data-icon-wishlist="" aria-label="Wish Lists" href="#" data-product-handle="207-44" data-id="7797807448286" tabindex="-1">
	    	<svg viewBox="0 0 512 512">
<g>
<g>
  <path d="M474.644,74.27C449.391,45.616,414.358,29.836,376,29.836c-53.948,0-88.103,32.22-107.255,59.25
    c-4.969,7.014-9.196,14.047-12.745,20.665c-3.549-6.618-7.775-13.651-12.745-20.665c-19.152-27.03-53.307-59.25-107.255-59.25
    c-38.358,0-73.391,15.781-98.645,44.435C13.267,101.605,0,138.213,0,177.351c0,42.603,16.633,82.228,52.345,124.7
    c31.917,37.96,77.834,77.088,131.005,122.397c19.813,16.884,40.302,34.344,62.115,53.429l0.655,0.574
    c2.828,2.476,6.354,3.713,9.88,3.713s7.052-1.238,9.88-3.713l0.655-0.574c21.813-19.085,42.302-36.544,62.118-53.431
    c53.168-45.306,99.085-84.434,131.002-122.395C495.367,259.578,512,219.954,512,177.351
    C512,138.213,498.733,101.605,474.644,74.27z M309.193,401.614c-17.08,14.554-34.658,29.533-53.193,45.646
    c-18.534-16.111-36.113-31.091-53.196-45.648C98.745,312.939,30,254.358,30,177.351c0-31.83,10.605-61.394,29.862-83.245
    C79.34,72.007,106.379,59.836,136,59.836c41.129,0,67.716,25.338,82.776,46.594c13.509,19.064,20.558,38.282,22.962,45.659
    c2.011,6.175,7.768,10.354,14.262,10.354c6.494,0,12.251-4.179,14.262-10.354c2.404-7.377,9.453-26.595,22.962-45.66
    c15.06-21.255,41.647-46.593,82.776-46.593c29.621,0,56.66,12.171,76.137,34.27C471.395,115.957,482,145.521,482,177.351
    C482,254.358,413.255,312.939,309.193,401.614z"></path>
</g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
</svg>
		
</a>
 


      
        <a class="quickview-button" href="javascript:void(0)" id="207-44" title="Quick View" tabindex="-1">
          <span>Quick view</span>
          <svg viewBox="0 0 511.999 511.999" width="100%" height="100%">
    <path d="M508.745,246.041c-4.574-6.257-113.557-153.206-252.748-153.206S7.818,239.784,3.249,246.035         c-4.332,5.936-4.332,13.987,0,19.923c4.569,6.257,113.557,153.206,252.748,153.206s248.174-146.95,252.748-153.201         C513.083,260.028,513.083,251.971,508.745,246.041z M255.997,385.406c-102.529,0-191.33-97.533-217.617-129.418         c26.253-31.913,114.868-129.395,217.617-129.395c102.524,0,191.319,97.516,217.617,129.418         C447.361,287.923,358.746,385.406,255.997,385.406z"></path>
      <path d="M255.997,154.725c-55.842,0-101.275,45.433-101.275,101.275s45.433,101.275,101.275,101.275         s101.275-45.433,101.275-101.275S311.839,154.725,255.997,154.725z M255.997,323.516c-37.23,0-67.516-30.287-67.516-67.516         s30.287-67.516,67.516-67.516s67.516,30.287,67.516,67.516S293.227,323.516,255.997,323.516z"></path>
</svg>
        </a>
      

      

    
    <div class="product-bottom">
      
      
        <span class="spr-badge" id="spr_badge_7797807448286" data-rating="4.333333333333333"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star" aria-hidden="true"></i><i class="spr-icon spr-icon-star" aria-hidden="true"></i><i class="spr-icon spr-icon-star" aria-hidden="true"></i><i class="spr-icon spr-icon-star" aria-hidden="true"></i><i class="spr-icon spr-icon-star-half-alt" aria-hidden="true"></i></span><span class="spr-badge-caption">3 reviews</span>
</span>

      
      <a class="product-title " href="/products/207-44?pr_prod_strat=copurchase&amp;pr_rec_id=e3a9801a1&amp;pr_rec_pid=7797807448286&amp;pr_ref_pid=7797834973406&amp;pr_seq=uniform" tabindex="-1">
        
<span>The Chaste Violet Cotton Palazzo</span>

        
          
          
      </a>
      <div class="half-grid-rais">
      <div class="price-box">
        
        <div class="price-sale">
          
            <span class="special-price" data-price-grid=""><span class="money">₹1826.00</span></span>
            <span class="old-price" data-compare-price-grid="">2608.00</span>
            <span class="percentage-price">(30%)</span>
         
        </div>
        
      </div>
    </div>

      <div class="add-to-cart-grid-rais">
        
        <div class="action">
          


<form action="/cart/add" method="post" class="variants" id="grid-product-form--7797807448286-product-recommendations" data-id="product-actions-7797807448286" enctype="multipart/form-data">
  
  

    
      
        <a class="rais-svgcart" data-href="/products/207-44?pr_prod_strat=copurchase&amp;pr_rec_id=e3a9801a1&amp;pr_rec_pid=7797807448286&amp;pr_ref_pid=7797834973406&amp;pr_seq=uniform" title="The Chaste Violet Cotton Palazzo" data-init-quickshop="" tabindex="-1">
        <svg width="27px" height="27px" viewBox="0 0 27 27" version="1.1" xmlns="http://www.w3.org/2000/svg"><g id="Tjori-" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g id="PLP" transform="translate(-361.000000, -962.000000)"><g id="Product-Widget-Copy" transform="translate(18.000000, 646.000000)"><g id="Product-Widget"><g id="Shopping-Bag-Icon" transform="translate(343.000000, 316.000000)"><path d="M21.6999607,6.00003576 L26.1999499,6.00003576 L26.1999499,20.2500018 C26.1999499,21.2812493 25.8327635,22.1640595 25.0983901,22.898433 C24.3640166,23.6328065 23.4812064,23.9999928 22.4499589,23.9999928 L8.94999106,23.9999928 C7.91874352,23.9999928 7.03593336,23.6328065 6.30155987,22.898433 C5.56718639,22.1640595 5.2,21.2812493 5.2,20.2500018 L5.2,6.00003576 L9.69998927,6.00003576 C9.69998927,4.90628861 9.96561388,3.89847851 10.4968624,2.97660547 C11.0281109,2.05473243 11.7546716,1.32817166 12.6765447,0.796923168 C13.5984177,0.265674673 14.6062278,5.00679016e-05 15.699975,5.00679016e-05 C16.7937221,5.00679016e-05 17.8015322,0.265674673 18.7234053,0.796923168 C19.6452783,1.32817166 20.3718391,2.05473243 20.9030876,2.97660547 C21.4343361,3.89847851 21.6999607,4.90628861 21.6999607,6.00003576 Z M15.699975,1.50004649 C14.4499777,1.50004649 13.3874807,1.93754521 12.5124826,2.81254336 C11.6374844,3.68754151 11.1999857,4.7500385 11.1999857,6.00003576 L20.1999642,6.00003576 C20.1999642,4.7500385 19.7624655,3.68754151 18.8874674,2.81254336 C18.0124692,1.93754521 16.9499722,1.50004649 15.699975,1.50004649 Z M24.6999535,20.2500018 L24.6999535,7.50003219 L21.6999607,7.50003219 L21.6999607,9.75002682 C21.6999607,9.96877654 21.6296483,10.1484634 21.4890237,10.289088 C21.348399,10.4297127 21.1687122,10.500025 20.9499624,10.500025 C20.7312127,10.500025 20.5515259,10.4297127 20.4109012,10.289088 C20.2702766,10.1484634 20.1999642,9.96877654 20.1999642,9.75002682 L20.1999642,7.50003219 L11.1999857,7.50003219 L11.1999857,9.75002682 C11.1999857,9.96877654 11.1296734,10.1484634 10.9890487,10.289088 C10.848424,10.4297127 10.6687372,10.500025 10.4499875,10.500025 C10.2312378,10.500025 10.0515509,10.4297127 9.91092627,10.289088 C9.7703016,10.1484634 9.69998927,9.96877654 9.69998927,9.75002682 L9.69998927,7.50003219 L6.69999642,7.50003219 L6.69999642,20.2500018 C6.69999642,20.8750001 6.91874614,21.4062493 7.35624486,21.843748 C7.79374358,22.2812467 8.32499279,22.4999964 8.94999106,22.4999964 L22.4499589,22.4999964 C23.0749571,22.4999964 23.6062064,22.2812467 24.0437051,21.843748 C24.4812038,21.4062493 24.6999535,20.8750001 24.6999535,20.2500018 Z" id="shopping-bag" fill="#34302B"></path><circle id="Oval" fill="#FFFFFF" cx="6.5" cy="20.5" r="5.5"></circle><path d="M6.34998569,14.6875246 C7.39685832,14.6875246 8.36560589,14.9492428 9.25622877,15.472679 C10.1468516,15.9961151 10.8538813,16.7031448 11.3773175,17.5937677 C11.9007536,18.4843905 12.1624718,19.4531381 12.1624718,20.5000107 C12.1624718,21.5468834 11.9007536,22.5156309 11.3773175,23.4062538 C10.8538813,24.2968767 10.1468516,25.0039064 9.25622877,25.5273425 C8.36560589,26.0507786 7.39685832,26.3124969 6.34998569,26.3124969 C5.30311307,26.3124969 4.3343655,26.0507786 3.44374262,25.5273425 C2.55311975,25.0039064 1.84609006,24.2968767 1.32265393,23.4062538 C0.799217798,22.5156309 0.537499553,21.5468834 0.537499553,20.5000107 C0.537499553,19.4531381 0.799217798,18.4843905 1.32265393,17.5937677 C1.84609006,16.7031448 2.55311975,15.9961151 3.44374262,15.472679 C4.3343655,14.9492428 5.30311307,14.6875246 6.34998569,14.6875246 Z M9.72497765,21.1562592 L9.72497765,19.8437623 C9.72497765,19.7656376 9.69763384,19.6992313 9.64294659,19.644544 C9.58825934,19.5898568 9.52185301,19.562513 9.44372832,19.562513 L7.28748346,19.562513 L7.28748346,17.4062681 C7.28748346,17.3281434 7.26013966,17.2617371 7.20545241,17.2070498 C7.15076515,17.1523626 7.08435882,17.1250188 7.00623413,17.1250188 L5.69373726,17.1250188 C5.61561256,17.1250188 5.54920623,17.1523626 5.49451898,17.2070498 C5.43983173,17.2617371 5.41248793,17.3281434 5.41248793,17.4062681 L5.41248793,19.562513 L3.25624307,19.562513 C3.17811838,19.562513 3.11171205,19.5898568 3.0570248,19.644544 C3.00233755,19.6992313 2.97499374,19.7656376 2.97499374,19.8437623 L2.97499374,21.1562592 C2.97499374,21.2343839 3.00233755,21.3007902 3.0570248,21.3554774 C3.11171205,21.4101647 3.17811838,21.4375085 3.25624307,21.4375085 L5.41248793,21.4375085 L5.41248793,23.5937534 C5.41248793,23.671878 5.43983173,23.7382844 5.49451898,23.7929716 C5.54920623,23.8476589 5.61561256,23.8750027 5.69373726,23.8750027 L7.00623413,23.8750027 C7.08435882,23.8750027 7.15076515,23.8476589 7.20545241,23.7929716 C7.26013966,23.7382844 7.28748346,23.671878 7.28748346,23.5937534 L7.28748346,21.4375085 L9.44372832,21.4375085 C9.52185301,21.4375085 9.58825934,21.4101647 9.64294659,21.3554774 C9.69763384,21.3007902 9.72497765,21.2343839 9.72497765,21.1562592 Z" id="plus-circle" fill="#C0B394"></path></g></g></g></g></g></svg>
        </a>
      
    
  
  
</form>

  
      
          <div class="product-card__variant--popup">
              <div class="product-card__variant--popup--content">
    
    <form action="/cart/add" method="post" class="variants" id="swatch-grid-product-form--7797807448286-product-recommendations-" data-id="product-actions-7797807448286" enctype="multipart/form-data">
        
            
            
            
                
                    <div class="selector-wrapper selector-wrapper-1 swatch js product-form__item" data-option-index="0">
                        <label class="form-label ">
                            Size: <span class="label-value-1">XS</span>
                        </label>
                        
                        
                        

                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size xs available" data-value="XS">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7797807448286-product-recommendations" data-index="option1" value="XS" id="SingleOptionSelector0-XS-7797807448286-product-recommendations" tabindex="-1">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-XS-7797807448286-product-recommendations" data-original-title="XS">XS</label>
                                </div>
                            
                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size s available" data-value="S">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7797807448286-product-recommendations" data-index="option1" value="S" id="SingleOptionSelector0-S-7797807448286-product-recommendations" tabindex="-1">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-S-7797807448286-product-recommendations" data-original-title="S">S</label>
                                </div>
                            
                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size m available" data-value="M">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7797807448286-product-recommendations" data-index="option1" value="M" id="SingleOptionSelector0-M-7797807448286-product-recommendations" tabindex="-1">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-M-7797807448286-product-recommendations" data-original-title="M">M</label>
                                </div>
                            
                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size l available" data-value="L">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7797807448286-product-recommendations" data-index="option1" value="L" id="SingleOptionSelector0-L-7797807448286-product-recommendations" tabindex="-1">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-L-7797807448286-product-recommendations" data-original-title="L">L</label>
                                </div>
                            
                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size xl available" data-value="XL">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7797807448286-product-recommendations" data-index="option1" value="XL" id="SingleOptionSelector0-XL-7797807448286-product-recommendations" tabindex="-1">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-XL-7797807448286-product-recommendations" data-original-title="XL">XL</label>
                                </div>
                            
                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size xxl available" data-value="XXL">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7797807448286-product-recommendations" data-index="option1" value="XXL" id="SingleOptionSelector0-XXL-7797807448286-product-recommendations" tabindex="-1">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-XXL-7797807448286-product-recommendations" data-original-title="XXL">XXL</label>
                                </div>
                            
                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size xxxl available" data-value="XXXL">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7797807448286-product-recommendations" data-index="option1" value="XXXL" id="SingleOptionSelector0-XXXL-7797807448286-product-recommendations" tabindex="-1">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-XXXL-7797807448286-product-recommendations" data-original-title="XXXL">XXXL</label>
                                </div>
                            
                        
                    </div>
                
            
        

        <select name="id" id="ProductSelect-7797807448286-product-recommendations" class="product-form__variants no-js" tabindex="-1">
            
                
                    <option value="43399171309790">
                        XS
                    </option>
                
            
                
                    <option value="43399171342558">
                        S
                    </option>
                
            
                
                    <option value="43399171375326">
                        M
                    </option>
                
            
                
                    <option value="43399171408094">
                        L
                    </option>
                
            
                
                    <option value="43399171440862">
                        XL
                    </option>
                
            
                
                    <option value="43399171506398">
                        XXL
                    </option>
                
            
                
                    <option value="43399171539166">
                        XXXL
                    </option>
                
            
        </select>
        <div class="product-card__button_cancel_mobile">
            <a class="btn btn-cancel" data-cancel-swatch-qs="" tabindex="-1"><svg aria-hidden="true" data-prefix="fal" data-icon="times" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" class="svg-inline--fa fa-times fa-w-10 fa-2x"><path fill="currentColor" d="M193.94 256L296.5 153.44l21.15-21.15c3.12-3.12 3.12-8.19 0-11.31l-22.63-22.63c-3.12-3.12-8.19-3.12-11.31 0L160 222.06 36.29 98.34c-3.12-3.12-8.19-3.12-11.31 0L2.34 120.97c-3.12 3.12-3.12 8.19 0 11.31L126.06 256 2.34 379.71c-3.12 3.12-3.12 8.19 0 11.31l22.63 22.63c3.12 3.12 8.19 3.12 11.31 0L160 289.94 262.56 392.5l21.15 21.15c3.12 3.12 8.19 3.12 11.31 0l22.63-22.63c3.12-3.12 3.12-8.19 0-11.31L193.94 256z" class=""></path></svg></a>
        </div>
        <div class="product-card__button2">
            <input type="hidden" name="quantity" value="1" tabindex="-1">
            <button data-btn-addtocart="" class="btn add-to-cart-btn" type="submit" data-form-id="#swatch-grid-product-form--7797807448286-product-recommendations-" tabindex="-1">
                Submit
            </button>
            <a class="btn btn-cancel" data-cancel-swatch-qs="" tabindex="-1">Cancel</a>
        </div>
        
    </form>
</div>
          </div>
      
  

        </div>
        
      </div>
    </div>

      <div class="wrapper-size">
        
      </div>
      
    </div>

    
     
    <div class="product-details">
      

      <a class="product-title " href="/products/207-44?pr_prod_strat=copurchase&amp;pr_rec_id=e3a9801a1&amp;pr_rec_pid=7797807448286&amp;pr_ref_pid=7797834973406&amp;pr_seq=uniform" tabindex="-1">
        
<span>The Chaste Violet Cotton Palazzo</span>

        
          
        
      </a>

      
      <span class="spr-badge" id="spr_badge_7797807448286" data-rating="4.333333333333333"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star" aria-hidden="true"></i><i class="spr-icon spr-icon-star" aria-hidden="true"></i><i class="spr-icon spr-icon-star" aria-hidden="true"></i><i class="spr-icon spr-icon-star" aria-hidden="true"></i><i class="spr-icon spr-icon-star-half-alt" aria-hidden="true"></i></span><span class="spr-badge-caption">3 reviews</span>
</span>

      

      <div class="short-description">
        
        <div>
          This summer violet cotton palazzo is a comfortable and playful look for the season! It is easy to pair and trendy. This product includes the palazoo only

Dimension: L: 35 Inch

Color:  Summer Violet

Material: Cotton

Finish: Handcrafted

Lining: No
        </div>
      </div>

      <div class="price-box">
        
        <div class="price-sale">
          
            <span class="old-price" data-compare-price-grid=""><span class="money">₹2,608.00</span></span>
            <span class="special-price" data-price-grid=""><span class="money">₹1,826.00</span></span>
          
        </div>
        
      </div>

      

      <div class="wrapper-button-card">
        
        <div class="action">
          


<form action="/cart/add" method="post" class="variants" id="list-product-form-7797807448286-product-recommendations" data-id="product-actions-7797807448286" enctype="multipart/form-data">
  
  

    
      
        <a class="rais-svgcart" data-href="/products/207-44?pr_prod_strat=copurchase&amp;pr_rec_id=e3a9801a1&amp;pr_rec_pid=7797807448286&amp;pr_ref_pid=7797834973406&amp;pr_seq=uniform" title="The Chaste Violet Cotton Palazzo" data-init-quickshop="" tabindex="-1">
        <svg width="27px" height="27px" viewBox="0 0 27 27" version="1.1" xmlns="http://www.w3.org/2000/svg"><g id="Tjori-" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g id="PLP" transform="translate(-361.000000, -962.000000)"><g id="Product-Widget-Copy" transform="translate(18.000000, 646.000000)"><g id="Product-Widget"><g id="Shopping-Bag-Icon" transform="translate(343.000000, 316.000000)"><path d="M21.6999607,6.00003576 L26.1999499,6.00003576 L26.1999499,20.2500018 C26.1999499,21.2812493 25.8327635,22.1640595 25.0983901,22.898433 C24.3640166,23.6328065 23.4812064,23.9999928 22.4499589,23.9999928 L8.94999106,23.9999928 C7.91874352,23.9999928 7.03593336,23.6328065 6.30155987,22.898433 C5.56718639,22.1640595 5.2,21.2812493 5.2,20.2500018 L5.2,6.00003576 L9.69998927,6.00003576 C9.69998927,4.90628861 9.96561388,3.89847851 10.4968624,2.97660547 C11.0281109,2.05473243 11.7546716,1.32817166 12.6765447,0.796923168 C13.5984177,0.265674673 14.6062278,5.00679016e-05 15.699975,5.00679016e-05 C16.7937221,5.00679016e-05 17.8015322,0.265674673 18.7234053,0.796923168 C19.6452783,1.32817166 20.3718391,2.05473243 20.9030876,2.97660547 C21.4343361,3.89847851 21.6999607,4.90628861 21.6999607,6.00003576 Z M15.699975,1.50004649 C14.4499777,1.50004649 13.3874807,1.93754521 12.5124826,2.81254336 C11.6374844,3.68754151 11.1999857,4.7500385 11.1999857,6.00003576 L20.1999642,6.00003576 C20.1999642,4.7500385 19.7624655,3.68754151 18.8874674,2.81254336 C18.0124692,1.93754521 16.9499722,1.50004649 15.699975,1.50004649 Z M24.6999535,20.2500018 L24.6999535,7.50003219 L21.6999607,7.50003219 L21.6999607,9.75002682 C21.6999607,9.96877654 21.6296483,10.1484634 21.4890237,10.289088 C21.348399,10.4297127 21.1687122,10.500025 20.9499624,10.500025 C20.7312127,10.500025 20.5515259,10.4297127 20.4109012,10.289088 C20.2702766,10.1484634 20.1999642,9.96877654 20.1999642,9.75002682 L20.1999642,7.50003219 L11.1999857,7.50003219 L11.1999857,9.75002682 C11.1999857,9.96877654 11.1296734,10.1484634 10.9890487,10.289088 C10.848424,10.4297127 10.6687372,10.500025 10.4499875,10.500025 C10.2312378,10.500025 10.0515509,10.4297127 9.91092627,10.289088 C9.7703016,10.1484634 9.69998927,9.96877654 9.69998927,9.75002682 L9.69998927,7.50003219 L6.69999642,7.50003219 L6.69999642,20.2500018 C6.69999642,20.8750001 6.91874614,21.4062493 7.35624486,21.843748 C7.79374358,22.2812467 8.32499279,22.4999964 8.94999106,22.4999964 L22.4499589,22.4999964 C23.0749571,22.4999964 23.6062064,22.2812467 24.0437051,21.843748 C24.4812038,21.4062493 24.6999535,20.8750001 24.6999535,20.2500018 Z" id="shopping-bag" fill="#34302B"></path><circle id="Oval" fill="#FFFFFF" cx="6.5" cy="20.5" r="5.5"></circle><path d="M6.34998569,14.6875246 C7.39685832,14.6875246 8.36560589,14.9492428 9.25622877,15.472679 C10.1468516,15.9961151 10.8538813,16.7031448 11.3773175,17.5937677 C11.9007536,18.4843905 12.1624718,19.4531381 12.1624718,20.5000107 C12.1624718,21.5468834 11.9007536,22.5156309 11.3773175,23.4062538 C10.8538813,24.2968767 10.1468516,25.0039064 9.25622877,25.5273425 C8.36560589,26.0507786 7.39685832,26.3124969 6.34998569,26.3124969 C5.30311307,26.3124969 4.3343655,26.0507786 3.44374262,25.5273425 C2.55311975,25.0039064 1.84609006,24.2968767 1.32265393,23.4062538 C0.799217798,22.5156309 0.537499553,21.5468834 0.537499553,20.5000107 C0.537499553,19.4531381 0.799217798,18.4843905 1.32265393,17.5937677 C1.84609006,16.7031448 2.55311975,15.9961151 3.44374262,15.472679 C4.3343655,14.9492428 5.30311307,14.6875246 6.34998569,14.6875246 Z M9.72497765,21.1562592 L9.72497765,19.8437623 C9.72497765,19.7656376 9.69763384,19.6992313 9.64294659,19.644544 C9.58825934,19.5898568 9.52185301,19.562513 9.44372832,19.562513 L7.28748346,19.562513 L7.28748346,17.4062681 C7.28748346,17.3281434 7.26013966,17.2617371 7.20545241,17.2070498 C7.15076515,17.1523626 7.08435882,17.1250188 7.00623413,17.1250188 L5.69373726,17.1250188 C5.61561256,17.1250188 5.54920623,17.1523626 5.49451898,17.2070498 C5.43983173,17.2617371 5.41248793,17.3281434 5.41248793,17.4062681 L5.41248793,19.562513 L3.25624307,19.562513 C3.17811838,19.562513 3.11171205,19.5898568 3.0570248,19.644544 C3.00233755,19.6992313 2.97499374,19.7656376 2.97499374,19.8437623 L2.97499374,21.1562592 C2.97499374,21.2343839 3.00233755,21.3007902 3.0570248,21.3554774 C3.11171205,21.4101647 3.17811838,21.4375085 3.25624307,21.4375085 L5.41248793,21.4375085 L5.41248793,23.5937534 C5.41248793,23.671878 5.43983173,23.7382844 5.49451898,23.7929716 C5.54920623,23.8476589 5.61561256,23.8750027 5.69373726,23.8750027 L7.00623413,23.8750027 C7.08435882,23.8750027 7.15076515,23.8476589 7.20545241,23.7929716 C7.26013966,23.7382844 7.28748346,23.671878 7.28748346,23.5937534 L7.28748346,21.4375085 L9.44372832,21.4375085 C9.52185301,21.4375085 9.58825934,21.4101647 9.64294659,21.3554774 C9.69763384,21.3007902 9.72497765,21.2343839 9.72497765,21.1562592 Z" id="plus-circle" fill="#C0B394"></path></g></g></g></g></g></svg>
        </a>
      
    
  
  
</form>

  
      
          <div class="product-card__variant--popup">
              <div class="product-card__variant--popup--content">
    
    <form action="/cart/add" method="post" class="variants" id="swatch-list-product-form-7797807448286-product-recommendations-list" data-id="product-actions-7797807448286" enctype="multipart/form-data">
        
            
            
            
                
                    <div class="selector-wrapper selector-wrapper-1 swatch js product-form__item" data-option-index="0">
                        <label class="form-label ">
                            Size: <span class="label-value-1">XS</span>
                        </label>
                        
                        
                        

                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size xs available" data-value="XS">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7797807448286-product-recommendationslist" data-index="option1" value="XS" id="SingleOptionSelector0-XS-7797807448286-product-recommendationslist" tabindex="-1">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-XS-7797807448286-product-recommendationslist" data-original-title="XS">XS</label>
                                </div>
                            
                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size s available" data-value="S">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7797807448286-product-recommendationslist" data-index="option1" value="S" id="SingleOptionSelector0-S-7797807448286-product-recommendationslist" tabindex="-1">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-S-7797807448286-product-recommendationslist" data-original-title="S">S</label>
                                </div>
                            
                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size m available" data-value="M">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7797807448286-product-recommendationslist" data-index="option1" value="M" id="SingleOptionSelector0-M-7797807448286-product-recommendationslist" tabindex="-1">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-M-7797807448286-product-recommendationslist" data-original-title="M">M</label>
                                </div>
                            
                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size l available" data-value="L">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7797807448286-product-recommendationslist" data-index="option1" value="L" id="SingleOptionSelector0-L-7797807448286-product-recommendationslist" tabindex="-1">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-L-7797807448286-product-recommendationslist" data-original-title="L">L</label>
                                </div>
                            
                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size xl available" data-value="XL">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7797807448286-product-recommendationslist" data-index="option1" value="XL" id="SingleOptionSelector0-XL-7797807448286-product-recommendationslist" tabindex="-1">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-XL-7797807448286-product-recommendationslist" data-original-title="XL">XL</label>
                                </div>
                            
                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size xxl available" data-value="XXL">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7797807448286-product-recommendationslist" data-index="option1" value="XXL" id="SingleOptionSelector0-XXL-7797807448286-product-recommendationslist" tabindex="-1">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-XXL-7797807448286-product-recommendationslist" data-original-title="XXL">XXL</label>
                                </div>
                            
                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size xxxl available" data-value="XXXL">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7797807448286-product-recommendationslist" data-index="option1" value="XXXL" id="SingleOptionSelector0-XXXL-7797807448286-product-recommendationslist" tabindex="-1">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-XXXL-7797807448286-product-recommendationslist" data-original-title="XXXL">XXXL</label>
                                </div>
                            
                        
                    </div>
                
            
        

        <select name="id" id="ProductSelect-7797807448286-product-recommendationslist" class="product-form__variants no-js" tabindex="-1">
            
                
                    <option value="43399171309790">
                        XS
                    </option>
                
            
                
                    <option value="43399171342558">
                        S
                    </option>
                
            
                
                    <option value="43399171375326">
                        M
                    </option>
                
            
                
                    <option value="43399171408094">
                        L
                    </option>
                
            
                
                    <option value="43399171440862">
                        XL
                    </option>
                
            
                
                    <option value="43399171506398">
                        XXL
                    </option>
                
            
                
                    <option value="43399171539166">
                        XXXL
                    </option>
                
            
        </select>
        <div class="product-card__button_cancel_mobile">
            <a class="btn btn-cancel" data-cancel-swatch-qs="" tabindex="-1"><svg aria-hidden="true" data-prefix="fal" data-icon="times" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" class="svg-inline--fa fa-times fa-w-10 fa-2x"><path fill="currentColor" d="M193.94 256L296.5 153.44l21.15-21.15c3.12-3.12 3.12-8.19 0-11.31l-22.63-22.63c-3.12-3.12-8.19-3.12-11.31 0L160 222.06 36.29 98.34c-3.12-3.12-8.19-3.12-11.31 0L2.34 120.97c-3.12 3.12-3.12 8.19 0 11.31L126.06 256 2.34 379.71c-3.12 3.12-3.12 8.19 0 11.31l22.63 22.63c3.12 3.12 8.19 3.12 11.31 0L160 289.94 262.56 392.5l21.15 21.15c3.12 3.12 8.19 3.12 11.31 0l22.63-22.63c3.12-3.12 3.12-8.19 0-11.31L193.94 256z" class=""></path></svg></a>
        </div>
        <div class="product-card__button2">
            <input type="hidden" name="quantity" value="1" tabindex="-1">
            <button data-btn-addtocart="" class="btn add-to-cart-btn" type="submit" data-form-id="#swatch-list-product-form-7797807448286-product-recommendations-list" tabindex="-1">
                Submit
            </button>
            <a class="btn btn-cancel" data-cancel-swatch-qs="" tabindex="-1">Cancel</a>
        </div>
        
    </form>
</div>
          </div>
      
  

        </div>
        
      </div>
      
    </div>
    
  </div>
</div>

          
        </div><div class="grid-item small--one-half medium-up--one-quarter col-12 slick-slide" style="width: 173px;" tabindex="-1" role="option" aria-describedby="slick-slide26" data-slick-index="6" aria-hidden="true">
          
            
<div class="inner product-item on-sale" data-product-id="product-7798101573854" data-json-product="{&quot;id&quot;: 7798101573854,&quot;handle&quot;: &quot;peepal-cotton-slub-culottes&quot;,&quot;media&quot;: [{&quot;alt&quot;:null,&quot;id&quot;:30632331739358,&quot;position&quot;:1,&quot;preview_image&quot;:{&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:2360,&quot;width&quot;:1600,&quot;src&quot;:&quot;https:\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/273687-2019-02-18-13_22_22.212330-big-image.jpg?v=1663260846&quot;},&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:2360,&quot;media_type&quot;:&quot;image&quot;,&quot;src&quot;:&quot;https:\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/273687-2019-02-18-13_22_22.212330-big-image.jpg?v=1663260846&quot;,&quot;width&quot;:1600},{&quot;alt&quot;:null,&quot;id&quot;:30632331772126,&quot;position&quot;:2,&quot;preview_image&quot;:{&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:2360,&quot;width&quot;:1600,&quot;src&quot;:&quot;https:\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/273688-2019-02-18-13_23_00.742794-big-image.jpg?v=1663260846&quot;},&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:2360,&quot;media_type&quot;:&quot;image&quot;,&quot;src&quot;:&quot;https:\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/273688-2019-02-18-13_23_00.742794-big-image.jpg?v=1663260846&quot;,&quot;width&quot;:1600},{&quot;alt&quot;:null,&quot;id&quot;:30632331804894,&quot;position&quot;:3,&quot;preview_image&quot;:{&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:2360,&quot;width&quot;:1600,&quot;src&quot;:&quot;https:\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/273689-2019-02-18-13_23_01.024499-big-image.jpg?v=1663260846&quot;},&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:2360,&quot;media_type&quot;:&quot;image&quot;,&quot;src&quot;:&quot;https:\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/273689-2019-02-18-13_23_01.024499-big-image.jpg?v=1663260846&quot;,&quot;width&quot;:1600},{&quot;alt&quot;:null,&quot;id&quot;:30632331837662,&quot;position&quot;:4,&quot;preview_image&quot;:{&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:2360,&quot;width&quot;:1600,&quot;src&quot;:&quot;https:\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/273690-2019-02-18-13_23_23.823885-big-image.jpg?v=1663260846&quot;},&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:2360,&quot;media_type&quot;:&quot;image&quot;,&quot;src&quot;:&quot;https:\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/273690-2019-02-18-13_23_23.823885-big-image.jpg?v=1663260846&quot;,&quot;width&quot;:1600}],&quot;variants&quot;: [{&quot;id&quot;:43400424980702,&quot;title&quot;:&quot;XS&quot;,&quot;option1&quot;:&quot;XS&quot;,&quot;option2&quot;:null,&quot;option3&quot;:null,&quot;sku&quot;:&quot;TJ-MK-202-21-XS&quot;,&quot;requires_shipping&quot;:true,&quot;taxable&quot;:true,&quot;featured_image&quot;:null,&quot;available&quot;:true,&quot;name&quot;:&quot;Peepal Cotton Slub Culottes - XS&quot;,&quot;public_title&quot;:&quot;XS&quot;,&quot;options&quot;:[&quot;XS&quot;],&quot;price&quot;:226000,&quot;weight&quot;:0,&quot;compare_at_price&quot;:322900,&quot;inventory_management&quot;:&quot;shopify&quot;,&quot;barcode&quot;:null,&quot;quantity_rule&quot;:{&quot;min&quot;:1,&quot;max&quot;:null,&quot;increment&quot;:1}},{&quot;id&quot;:43400425013470,&quot;title&quot;:&quot;S&quot;,&quot;option1&quot;:&quot;S&quot;,&quot;option2&quot;:null,&quot;option3&quot;:null,&quot;sku&quot;:&quot;TJ-MK-202-21-S&quot;,&quot;requires_shipping&quot;:true,&quot;taxable&quot;:true,&quot;featured_image&quot;:null,&quot;available&quot;:true,&quot;name&quot;:&quot;Peepal Cotton Slub Culottes - S&quot;,&quot;public_title&quot;:&quot;S&quot;,&quot;options&quot;:[&quot;S&quot;],&quot;price&quot;:226000,&quot;weight&quot;:0,&quot;compare_at_price&quot;:322900,&quot;inventory_management&quot;:&quot;shopify&quot;,&quot;barcode&quot;:null,&quot;quantity_rule&quot;:{&quot;min&quot;:1,&quot;max&quot;:null,&quot;increment&quot;:1}},{&quot;id&quot;:43400425046238,&quot;title&quot;:&quot;M&quot;,&quot;option1&quot;:&quot;M&quot;,&quot;option2&quot;:null,&quot;option3&quot;:null,&quot;sku&quot;:&quot;TJ-MK-202-21-M&quot;,&quot;requires_shipping&quot;:true,&quot;taxable&quot;:true,&quot;featured_image&quot;:null,&quot;available&quot;:true,&quot;name&quot;:&quot;Peepal Cotton Slub Culottes - M&quot;,&quot;public_title&quot;:&quot;M&quot;,&quot;options&quot;:[&quot;M&quot;],&quot;price&quot;:226000,&quot;weight&quot;:0,&quot;compare_at_price&quot;:322900,&quot;inventory_management&quot;:&quot;shopify&quot;,&quot;barcode&quot;:null,&quot;quantity_rule&quot;:{&quot;min&quot;:1,&quot;max&quot;:null,&quot;increment&quot;:1}},{&quot;id&quot;:43400425079006,&quot;title&quot;:&quot;L&quot;,&quot;option1&quot;:&quot;L&quot;,&quot;option2&quot;:null,&quot;option3&quot;:null,&quot;sku&quot;:&quot;TJ-MK-202-21-L&quot;,&quot;requires_shipping&quot;:true,&quot;taxable&quot;:true,&quot;featured_image&quot;:null,&quot;available&quot;:true,&quot;name&quot;:&quot;Peepal Cotton Slub Culottes - L&quot;,&quot;public_title&quot;:&quot;L&quot;,&quot;options&quot;:[&quot;L&quot;],&quot;price&quot;:226000,&quot;weight&quot;:0,&quot;compare_at_price&quot;:322900,&quot;inventory_management&quot;:&quot;shopify&quot;,&quot;barcode&quot;:null,&quot;quantity_rule&quot;:{&quot;min&quot;:1,&quot;max&quot;:null,&quot;increment&quot;:1}},{&quot;id&quot;:43400425111774,&quot;title&quot;:&quot;XL&quot;,&quot;option1&quot;:&quot;XL&quot;,&quot;option2&quot;:null,&quot;option3&quot;:null,&quot;sku&quot;:&quot;TJ-MK-202-21-XL&quot;,&quot;requires_shipping&quot;:true,&quot;taxable&quot;:true,&quot;featured_image&quot;:null,&quot;available&quot;:true,&quot;name&quot;:&quot;Peepal Cotton Slub Culottes - XL&quot;,&quot;public_title&quot;:&quot;XL&quot;,&quot;options&quot;:[&quot;XL&quot;],&quot;price&quot;:226000,&quot;weight&quot;:0,&quot;compare_at_price&quot;:322900,&quot;inventory_management&quot;:&quot;shopify&quot;,&quot;barcode&quot;:null,&quot;quantity_rule&quot;:{&quot;min&quot;:1,&quot;max&quot;:null,&quot;increment&quot;:1}},{&quot;id&quot;:43400425144542,&quot;title&quot;:&quot;XXL&quot;,&quot;option1&quot;:&quot;XXL&quot;,&quot;option2&quot;:null,&quot;option3&quot;:null,&quot;sku&quot;:&quot;TJ-MK-202-21-XXL&quot;,&quot;requires_shipping&quot;:true,&quot;taxable&quot;:true,&quot;featured_image&quot;:null,&quot;available&quot;:true,&quot;name&quot;:&quot;Peepal Cotton Slub Culottes - XXL&quot;,&quot;public_title&quot;:&quot;XXL&quot;,&quot;options&quot;:[&quot;XXL&quot;],&quot;price&quot;:226000,&quot;weight&quot;:0,&quot;compare_at_price&quot;:322900,&quot;inventory_management&quot;:&quot;shopify&quot;,&quot;barcode&quot;:null,&quot;quantity_rule&quot;:{&quot;min&quot;:1,&quot;max&quot;:null,&quot;increment&quot;:1}},{&quot;id&quot;:43400425177310,&quot;title&quot;:&quot;XXXL&quot;,&quot;option1&quot;:&quot;XXXL&quot;,&quot;option2&quot;:null,&quot;option3&quot;:null,&quot;sku&quot;:&quot;TJ-MK-202-21-XXXL&quot;,&quot;requires_shipping&quot;:true,&quot;taxable&quot;:true,&quot;featured_image&quot;:null,&quot;available&quot;:true,&quot;name&quot;:&quot;Peepal Cotton Slub Culottes - XXXL&quot;,&quot;public_title&quot;:&quot;XXXL&quot;,&quot;options&quot;:[&quot;XXXL&quot;],&quot;price&quot;:226000,&quot;weight&quot;:0,&quot;compare_at_price&quot;:322900,&quot;inventory_management&quot;:&quot;shopify&quot;,&quot;barcode&quot;:null,&quot;quantity_rule&quot;:{&quot;min&quot;:1,&quot;max&quot;:null,&quot;increment&quot;:1}}]}">
  <div class="inner-top">
    <div class="product-top">
      <div class="product-image image-swap">
        <a href="/products/peepal-cotton-slub-culottes?pr_prod_strat=copurchase&amp;pr_rec_id=e3a9801a1&amp;pr_rec_pid=7798101573854&amp;pr_ref_pid=7797834973406&amp;pr_seq=uniform" class="product-grid-image adaptive_height" data-collections-related="/collections/?view=related" style="padding-top: 147.5%" tabindex="-1">
          



  <picture data-index="0">
    <source class="image-source-1" data-srcset="//cdn.shopify.com/s/files/1/0613/2039/7022/products/273687-2019-02-18-13_22_22.212330-big-image_300x.jpg?v=1663260846" media="(max-width: 767px)" sizes="153px" srcset="//cdn.shopify.com/s/files/1/0613/2039/7022/products/273687-2019-02-18-13_22_22.212330-big-image_300x.jpg?v=1663260846">
    <source class="image-source-2" data-srcset=" //cdn.shopify.com/s/files/1/0613/2039/7022/products/273687-2019-02-18-13_22_22.212330-big-image_360x.jpg?v=1663260846 360w,
                          //cdn.shopify.com/s/files/1/0613/2039/7022/products/273687-2019-02-18-13_22_22.212330-big-image_540x.jpg?v=1663260846 540w,
                          //cdn.shopify.com/s/files/1/0613/2039/7022/products/273687-2019-02-18-13_22_22.212330-big-image_720x.jpg?v=1663260846 720w,
                          //cdn.shopify.com/s/files/1/0613/2039/7022/products/273687-2019-02-18-13_22_22.212330-big-image_900x.jpg?v=1663260846 900w" media="(min-width: 768px)" sizes="153px" srcset=" //cdn.shopify.com/s/files/1/0613/2039/7022/products/273687-2019-02-18-13_22_22.212330-big-image_360x.jpg?v=1663260846 360w,
                          //cdn.shopify.com/s/files/1/0613/2039/7022/products/273687-2019-02-18-13_22_22.212330-big-image_540x.jpg?v=1663260846 540w,
                          //cdn.shopify.com/s/files/1/0613/2039/7022/products/273687-2019-02-18-13_22_22.212330-big-image_720x.jpg?v=1663260846 720w,
                          //cdn.shopify.com/s/files/1/0613/2039/7022/products/273687-2019-02-18-13_22_22.212330-big-image_900x.jpg?v=1663260846 900w">

    <img alt="Peepal Cotton Slub Culottes" class="images-one lazyautosizes lazyloaded" data-widths="[180, 360, 540, 720, 900, 1080, 1296, 1512, 1728, 2048]" data-aspectratio="0.6779661016949152" data-sizes="auto" data-srcset="//cdn.shopify.com/s/files/1/0613/2039/7022/products/273687-2019-02-18-13_22_22.212330-big-image_360x.jpg?v=1663260846 360w,
                    //cdn.shopify.com/s/files/1/0613/2039/7022/products/273687-2019-02-18-13_22_22.212330-big-image_540x.jpg?v=1663260846 540w,
                    //cdn.shopify.com/s/files/1/0613/2039/7022/products/273687-2019-02-18-13_22_22.212330-big-image_720x.jpg?v=1663260846 720w,
                    //cdn.shopify.com/s/files/1/0613/2039/7022/products/273687-2019-02-18-13_22_22.212330-big-image_900x.jpg?v=1663260846 900w" data-image="" sizes="153px" srcset="//cdn.shopify.com/s/files/1/0613/2039/7022/products/273687-2019-02-18-13_22_22.212330-big-image_360x.jpg?v=1663260846 360w,
                    //cdn.shopify.com/s/files/1/0613/2039/7022/products/273687-2019-02-18-13_22_22.212330-big-image_540x.jpg?v=1663260846 540w,
                    //cdn.shopify.com/s/files/1/0613/2039/7022/products/273687-2019-02-18-13_22_22.212330-big-image_720x.jpg?v=1663260846 720w,
                    //cdn.shopify.com/s/files/1/0613/2039/7022/products/273687-2019-02-18-13_22_22.212330-big-image_900x.jpg?v=1663260846 900w">
  </picture>
  <span class="images-two">
    
    <picture data-index="1">
      <source data-srcset="//cdn.shopify.com/s/files/1/0613/2039/7022/products/273688-2019-02-18-13_23_00.742794-big-image_300x.jpg?v=1663260846" media="(max-width: 767px)" sizes="153px" srcset="//cdn.shopify.com/s/files/1/0613/2039/7022/products/273688-2019-02-18-13_23_00.742794-big-image_300x.jpg?v=1663260846">
      <source data-srcset="//cdn.shopify.com/s/files/1/0613/2039/7022/products/273688-2019-02-18-13_23_00.742794-big-image_360x.jpg?v=1663260846 360w,
                            //cdn.shopify.com/s/files/1/0613/2039/7022/products/273688-2019-02-18-13_23_00.742794-big-image_540x.jpg?v=1663260846 540w,
                            //cdn.shopify.com/s/files/1/0613/2039/7022/products/273688-2019-02-18-13_23_00.742794-big-image_720x.jpg?v=1663260846 720w,
                            //cdn.shopify.com/s/files/1/0613/2039/7022/products/273688-2019-02-18-13_23_00.742794-big-image_900x.jpg?v=1663260846 900w" media="(min-width: 768px)" sizes="153px" srcset="//cdn.shopify.com/s/files/1/0613/2039/7022/products/273688-2019-02-18-13_23_00.742794-big-image_360x.jpg?v=1663260846 360w,
                            //cdn.shopify.com/s/files/1/0613/2039/7022/products/273688-2019-02-18-13_23_00.742794-big-image_540x.jpg?v=1663260846 540w,
                            //cdn.shopify.com/s/files/1/0613/2039/7022/products/273688-2019-02-18-13_23_00.742794-big-image_720x.jpg?v=1663260846 720w,
                            //cdn.shopify.com/s/files/1/0613/2039/7022/products/273688-2019-02-18-13_23_00.742794-big-image_900x.jpg?v=1663260846 900w">

      <img alt="Peepal Cotton Slub Culottes" class="lazyautosizes lazyloaded" data-widths="[180, 360, 540, 720, 900, 1080, 1296, 1512, 1728, 2048]" data-aspectratio="0.6779661016949152" data-sizes="auto" data-srcset="//cdn.shopify.com/s/files/1/0613/2039/7022/products/273688-2019-02-18-13_23_00.742794-big-image_360x.jpg?v=1663260846 360w,
                        //cdn.shopify.com/s/files/1/0613/2039/7022/products/273688-2019-02-18-13_23_00.742794-big-image_540x.jpg?v=1663260846 540w,
                        //cdn.shopify.com/s/files/1/0613/2039/7022/products/273688-2019-02-18-13_23_00.742794-big-image_720x.jpg?v=1663260846 720w,
                        //cdn.shopify.com/s/files/1/0613/2039/7022/products/273688-2019-02-18-13_23_00.742794-big-image_900x.jpg?v=1663260846 900w" data-image="" sizes="153px" srcset="//cdn.shopify.com/s/files/1/0613/2039/7022/products/273688-2019-02-18-13_23_00.742794-big-image_360x.jpg?v=1663260846 360w,
                        //cdn.shopify.com/s/files/1/0613/2039/7022/products/273688-2019-02-18-13_23_00.742794-big-image_540x.jpg?v=1663260846 540w,
                        //cdn.shopify.com/s/files/1/0613/2039/7022/products/273688-2019-02-18-13_23_00.742794-big-image_720x.jpg?v=1663260846 720w,
                        //cdn.shopify.com/s/files/1/0613/2039/7022/products/273688-2019-02-18-13_23_00.742794-big-image_900x.jpg?v=1663260846 900w">
    </picture>
    
  </span>

        </a>
      </div>

      
  
  <div class="product-label" data-label-new-number="30">
    

    
      
        <strong class="label sale-label">
          Sale
        </strong>
        
      
    

    
    
    

    
    
    
  </div>
  






      
<a class="wishlist " data-icon-wishlist="" aria-label="Wish Lists" href="#" data-product-handle="peepal-cotton-slub-culottes" data-id="7798101573854" tabindex="-1">
	    	<svg viewBox="0 0 512 512">
<g>
<g>
  <path d="M474.644,74.27C449.391,45.616,414.358,29.836,376,29.836c-53.948,0-88.103,32.22-107.255,59.25
    c-4.969,7.014-9.196,14.047-12.745,20.665c-3.549-6.618-7.775-13.651-12.745-20.665c-19.152-27.03-53.307-59.25-107.255-59.25
    c-38.358,0-73.391,15.781-98.645,44.435C13.267,101.605,0,138.213,0,177.351c0,42.603,16.633,82.228,52.345,124.7
    c31.917,37.96,77.834,77.088,131.005,122.397c19.813,16.884,40.302,34.344,62.115,53.429l0.655,0.574
    c2.828,2.476,6.354,3.713,9.88,3.713s7.052-1.238,9.88-3.713l0.655-0.574c21.813-19.085,42.302-36.544,62.118-53.431
    c53.168-45.306,99.085-84.434,131.002-122.395C495.367,259.578,512,219.954,512,177.351
    C512,138.213,498.733,101.605,474.644,74.27z M309.193,401.614c-17.08,14.554-34.658,29.533-53.193,45.646
    c-18.534-16.111-36.113-31.091-53.196-45.648C98.745,312.939,30,254.358,30,177.351c0-31.83,10.605-61.394,29.862-83.245
    C79.34,72.007,106.379,59.836,136,59.836c41.129,0,67.716,25.338,82.776,46.594c13.509,19.064,20.558,38.282,22.962,45.659
    c2.011,6.175,7.768,10.354,14.262,10.354c6.494,0,12.251-4.179,14.262-10.354c2.404-7.377,9.453-26.595,22.962-45.66
    c15.06-21.255,41.647-46.593,82.776-46.593c29.621,0,56.66,12.171,76.137,34.27C471.395,115.957,482,145.521,482,177.351
    C482,254.358,413.255,312.939,309.193,401.614z"></path>
</g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
</svg>
		
</a>
 


      
        <a class="quickview-button" href="javascript:void(0)" id="peepal-cotton-slub-culottes" title="Quick View" tabindex="-1">
          <span>Quick view</span>
          <svg viewBox="0 0 511.999 511.999" width="100%" height="100%">
    <path d="M508.745,246.041c-4.574-6.257-113.557-153.206-252.748-153.206S7.818,239.784,3.249,246.035         c-4.332,5.936-4.332,13.987,0,19.923c4.569,6.257,113.557,153.206,252.748,153.206s248.174-146.95,252.748-153.201         C513.083,260.028,513.083,251.971,508.745,246.041z M255.997,385.406c-102.529,0-191.33-97.533-217.617-129.418         c26.253-31.913,114.868-129.395,217.617-129.395c102.524,0,191.319,97.516,217.617,129.418         C447.361,287.923,358.746,385.406,255.997,385.406z"></path>
      <path d="M255.997,154.725c-55.842,0-101.275,45.433-101.275,101.275s45.433,101.275,101.275,101.275         s101.275-45.433,101.275-101.275S311.839,154.725,255.997,154.725z M255.997,323.516c-37.23,0-67.516-30.287-67.516-67.516         s30.287-67.516,67.516-67.516s67.516,30.287,67.516,67.516S293.227,323.516,255.997,323.516z"></path>
</svg>
        </a>
      

      

    
    <div class="product-bottom">
      
      
        <span class="spr-badge" id="spr_badge_7798101573854" data-rating="4.714285714285714"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star" aria-hidden="true"></i><i class="spr-icon spr-icon-star" aria-hidden="true"></i><i class="spr-icon spr-icon-star" aria-hidden="true"></i><i class="spr-icon spr-icon-star" aria-hidden="true"></i><i class="spr-icon spr-icon-star" aria-hidden="true"></i></span><span class="spr-badge-caption">7 reviews</span>
</span>

      
      <a class="product-title " href="/products/peepal-cotton-slub-culottes?pr_prod_strat=copurchase&amp;pr_rec_id=e3a9801a1&amp;pr_rec_pid=7798101573854&amp;pr_ref_pid=7797834973406&amp;pr_seq=uniform" tabindex="-1">
        
<span>Peepal Cotton Slub Culottes</span>

        
          
          
      </a>
      <div class="half-grid-rais">
      <div class="price-box">
        
        <div class="price-sale">
          
            <span class="special-price" data-price-grid=""><span class="money">₹2260.00</span></span>
            <span class="old-price" data-compare-price-grid="">3229.00</span>
            <span class="percentage-price">(30%)</span>
         
        </div>
        
      </div>
    </div>

      <div class="add-to-cart-grid-rais">
        
        <div class="action">
          


<form action="/cart/add" method="post" class="variants" id="grid-product-form--7798101573854-product-recommendations" data-id="product-actions-7798101573854" enctype="multipart/form-data">
  
  

    
      
        <a class="rais-svgcart" data-href="/products/peepal-cotton-slub-culottes?pr_prod_strat=copurchase&amp;pr_rec_id=e3a9801a1&amp;pr_rec_pid=7798101573854&amp;pr_ref_pid=7797834973406&amp;pr_seq=uniform" title="Peepal Cotton Slub Culottes" data-init-quickshop="" tabindex="-1">
        <svg width="27px" height="27px" viewBox="0 0 27 27" version="1.1" xmlns="http://www.w3.org/2000/svg"><g id="Tjori-" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g id="PLP" transform="translate(-361.000000, -962.000000)"><g id="Product-Widget-Copy" transform="translate(18.000000, 646.000000)"><g id="Product-Widget"><g id="Shopping-Bag-Icon" transform="translate(343.000000, 316.000000)"><path d="M21.6999607,6.00003576 L26.1999499,6.00003576 L26.1999499,20.2500018 C26.1999499,21.2812493 25.8327635,22.1640595 25.0983901,22.898433 C24.3640166,23.6328065 23.4812064,23.9999928 22.4499589,23.9999928 L8.94999106,23.9999928 C7.91874352,23.9999928 7.03593336,23.6328065 6.30155987,22.898433 C5.56718639,22.1640595 5.2,21.2812493 5.2,20.2500018 L5.2,6.00003576 L9.69998927,6.00003576 C9.69998927,4.90628861 9.96561388,3.89847851 10.4968624,2.97660547 C11.0281109,2.05473243 11.7546716,1.32817166 12.6765447,0.796923168 C13.5984177,0.265674673 14.6062278,5.00679016e-05 15.699975,5.00679016e-05 C16.7937221,5.00679016e-05 17.8015322,0.265674673 18.7234053,0.796923168 C19.6452783,1.32817166 20.3718391,2.05473243 20.9030876,2.97660547 C21.4343361,3.89847851 21.6999607,4.90628861 21.6999607,6.00003576 Z M15.699975,1.50004649 C14.4499777,1.50004649 13.3874807,1.93754521 12.5124826,2.81254336 C11.6374844,3.68754151 11.1999857,4.7500385 11.1999857,6.00003576 L20.1999642,6.00003576 C20.1999642,4.7500385 19.7624655,3.68754151 18.8874674,2.81254336 C18.0124692,1.93754521 16.9499722,1.50004649 15.699975,1.50004649 Z M24.6999535,20.2500018 L24.6999535,7.50003219 L21.6999607,7.50003219 L21.6999607,9.75002682 C21.6999607,9.96877654 21.6296483,10.1484634 21.4890237,10.289088 C21.348399,10.4297127 21.1687122,10.500025 20.9499624,10.500025 C20.7312127,10.500025 20.5515259,10.4297127 20.4109012,10.289088 C20.2702766,10.1484634 20.1999642,9.96877654 20.1999642,9.75002682 L20.1999642,7.50003219 L11.1999857,7.50003219 L11.1999857,9.75002682 C11.1999857,9.96877654 11.1296734,10.1484634 10.9890487,10.289088 C10.848424,10.4297127 10.6687372,10.500025 10.4499875,10.500025 C10.2312378,10.500025 10.0515509,10.4297127 9.91092627,10.289088 C9.7703016,10.1484634 9.69998927,9.96877654 9.69998927,9.75002682 L9.69998927,7.50003219 L6.69999642,7.50003219 L6.69999642,20.2500018 C6.69999642,20.8750001 6.91874614,21.4062493 7.35624486,21.843748 C7.79374358,22.2812467 8.32499279,22.4999964 8.94999106,22.4999964 L22.4499589,22.4999964 C23.0749571,22.4999964 23.6062064,22.2812467 24.0437051,21.843748 C24.4812038,21.4062493 24.6999535,20.8750001 24.6999535,20.2500018 Z" id="shopping-bag" fill="#34302B"></path><circle id="Oval" fill="#FFFFFF" cx="6.5" cy="20.5" r="5.5"></circle><path d="M6.34998569,14.6875246 C7.39685832,14.6875246 8.36560589,14.9492428 9.25622877,15.472679 C10.1468516,15.9961151 10.8538813,16.7031448 11.3773175,17.5937677 C11.9007536,18.4843905 12.1624718,19.4531381 12.1624718,20.5000107 C12.1624718,21.5468834 11.9007536,22.5156309 11.3773175,23.4062538 C10.8538813,24.2968767 10.1468516,25.0039064 9.25622877,25.5273425 C8.36560589,26.0507786 7.39685832,26.3124969 6.34998569,26.3124969 C5.30311307,26.3124969 4.3343655,26.0507786 3.44374262,25.5273425 C2.55311975,25.0039064 1.84609006,24.2968767 1.32265393,23.4062538 C0.799217798,22.5156309 0.537499553,21.5468834 0.537499553,20.5000107 C0.537499553,19.4531381 0.799217798,18.4843905 1.32265393,17.5937677 C1.84609006,16.7031448 2.55311975,15.9961151 3.44374262,15.472679 C4.3343655,14.9492428 5.30311307,14.6875246 6.34998569,14.6875246 Z M9.72497765,21.1562592 L9.72497765,19.8437623 C9.72497765,19.7656376 9.69763384,19.6992313 9.64294659,19.644544 C9.58825934,19.5898568 9.52185301,19.562513 9.44372832,19.562513 L7.28748346,19.562513 L7.28748346,17.4062681 C7.28748346,17.3281434 7.26013966,17.2617371 7.20545241,17.2070498 C7.15076515,17.1523626 7.08435882,17.1250188 7.00623413,17.1250188 L5.69373726,17.1250188 C5.61561256,17.1250188 5.54920623,17.1523626 5.49451898,17.2070498 C5.43983173,17.2617371 5.41248793,17.3281434 5.41248793,17.4062681 L5.41248793,19.562513 L3.25624307,19.562513 C3.17811838,19.562513 3.11171205,19.5898568 3.0570248,19.644544 C3.00233755,19.6992313 2.97499374,19.7656376 2.97499374,19.8437623 L2.97499374,21.1562592 C2.97499374,21.2343839 3.00233755,21.3007902 3.0570248,21.3554774 C3.11171205,21.4101647 3.17811838,21.4375085 3.25624307,21.4375085 L5.41248793,21.4375085 L5.41248793,23.5937534 C5.41248793,23.671878 5.43983173,23.7382844 5.49451898,23.7929716 C5.54920623,23.8476589 5.61561256,23.8750027 5.69373726,23.8750027 L7.00623413,23.8750027 C7.08435882,23.8750027 7.15076515,23.8476589 7.20545241,23.7929716 C7.26013966,23.7382844 7.28748346,23.671878 7.28748346,23.5937534 L7.28748346,21.4375085 L9.44372832,21.4375085 C9.52185301,21.4375085 9.58825934,21.4101647 9.64294659,21.3554774 C9.69763384,21.3007902 9.72497765,21.2343839 9.72497765,21.1562592 Z" id="plus-circle" fill="#C0B394"></path></g></g></g></g></g></svg>
        </a>
      
    
  
  
</form>

  
      
          <div class="product-card__variant--popup">
              <div class="product-card__variant--popup--content">
    
    <form action="/cart/add" method="post" class="variants" id="swatch-grid-product-form--7798101573854-product-recommendations-" data-id="product-actions-7798101573854" enctype="multipart/form-data">
        
            
            
            
                
                    <div class="selector-wrapper selector-wrapper-1 swatch js product-form__item" data-option-index="0">
                        <label class="form-label ">
                            Size: <span class="label-value-1">XS</span>
                        </label>
                        
                        
                        

                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size xs available" data-value="XS">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7798101573854-product-recommendations" data-index="option1" value="XS" id="SingleOptionSelector0-XS-7798101573854-product-recommendations" tabindex="-1">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-XS-7798101573854-product-recommendations" data-original-title="XS">XS</label>
                                </div>
                            
                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size s available" data-value="S">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7798101573854-product-recommendations" data-index="option1" value="S" id="SingleOptionSelector0-S-7798101573854-product-recommendations" tabindex="-1">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-S-7798101573854-product-recommendations" data-original-title="S">S</label>
                                </div>
                            
                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size m available" data-value="M">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7798101573854-product-recommendations" data-index="option1" value="M" id="SingleOptionSelector0-M-7798101573854-product-recommendations" tabindex="-1">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-M-7798101573854-product-recommendations" data-original-title="M">M</label>
                                </div>
                            
                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size l available" data-value="L">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7798101573854-product-recommendations" data-index="option1" value="L" id="SingleOptionSelector0-L-7798101573854-product-recommendations" tabindex="-1">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-L-7798101573854-product-recommendations" data-original-title="L">L</label>
                                </div>
                            
                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size xl available" data-value="XL">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7798101573854-product-recommendations" data-index="option1" value="XL" id="SingleOptionSelector0-XL-7798101573854-product-recommendations" tabindex="-1">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-XL-7798101573854-product-recommendations" data-original-title="XL">XL</label>
                                </div>
                            
                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size xxl available" data-value="XXL">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7798101573854-product-recommendations" data-index="option1" value="XXL" id="SingleOptionSelector0-XXL-7798101573854-product-recommendations" tabindex="-1">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-XXL-7798101573854-product-recommendations" data-original-title="XXL">XXL</label>
                                </div>
                            
                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size xxxl available" data-value="XXXL">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7798101573854-product-recommendations" data-index="option1" value="XXXL" id="SingleOptionSelector0-XXXL-7798101573854-product-recommendations" tabindex="-1">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-XXXL-7798101573854-product-recommendations" data-original-title="XXXL">XXXL</label>
                                </div>
                            
                        
                    </div>
                
            
        

        <select name="id" id="ProductSelect-7798101573854-product-recommendations" class="product-form__variants no-js" tabindex="-1">
            
                
                    <option value="43400424980702">
                        XS
                    </option>
                
            
                
                    <option value="43400425013470">
                        S
                    </option>
                
            
                
                    <option value="43400425046238">
                        M
                    </option>
                
            
                
                    <option value="43400425079006">
                        L
                    </option>
                
            
                
                    <option value="43400425111774">
                        XL
                    </option>
                
            
                
                    <option value="43400425144542">
                        XXL
                    </option>
                
            
                
                    <option value="43400425177310">
                        XXXL
                    </option>
                
            
        </select>
        <div class="product-card__button_cancel_mobile">
            <a class="btn btn-cancel" data-cancel-swatch-qs="" tabindex="-1"><svg aria-hidden="true" data-prefix="fal" data-icon="times" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" class="svg-inline--fa fa-times fa-w-10 fa-2x"><path fill="currentColor" d="M193.94 256L296.5 153.44l21.15-21.15c3.12-3.12 3.12-8.19 0-11.31l-22.63-22.63c-3.12-3.12-8.19-3.12-11.31 0L160 222.06 36.29 98.34c-3.12-3.12-8.19-3.12-11.31 0L2.34 120.97c-3.12 3.12-3.12 8.19 0 11.31L126.06 256 2.34 379.71c-3.12 3.12-3.12 8.19 0 11.31l22.63 22.63c3.12 3.12 8.19 3.12 11.31 0L160 289.94 262.56 392.5l21.15 21.15c3.12 3.12 8.19 3.12 11.31 0l22.63-22.63c3.12-3.12 3.12-8.19 0-11.31L193.94 256z" class=""></path></svg></a>
        </div>
        <div class="product-card__button2">
            <input type="hidden" name="quantity" value="1" tabindex="-1">
            <button data-btn-addtocart="" class="btn add-to-cart-btn" type="submit" data-form-id="#swatch-grid-product-form--7798101573854-product-recommendations-" tabindex="-1">
                Submit
            </button>
            <a class="btn btn-cancel" data-cancel-swatch-qs="" tabindex="-1">Cancel</a>
        </div>
        
    </form>
</div>
          </div>
      
  

        </div>
        
      </div>
    </div>

      <div class="wrapper-size">
        
      </div>
      
    </div>

    
     
    <div class="product-details">
      

      <a class="product-title " href="/products/peepal-cotton-slub-culottes?pr_prod_strat=copurchase&amp;pr_rec_id=e3a9801a1&amp;pr_rec_pid=7798101573854&amp;pr_ref_pid=7797834973406&amp;pr_seq=uniform" tabindex="-1">
        
<span>Peepal Cotton Slub Culottes</span>

        
          
        
      </a>

      
      <span class="spr-badge" id="spr_badge_7798101573854" data-rating="4.714285714285714"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star" aria-hidden="true"></i><i class="spr-icon spr-icon-star" aria-hidden="true"></i><i class="spr-icon spr-icon-star" aria-hidden="true"></i><i class="spr-icon spr-icon-star" aria-hidden="true"></i><i class="spr-icon spr-icon-star" aria-hidden="true"></i></span><span class="spr-badge-caption">7 reviews</span>
</span>

      

      <div class="short-description">
        
        <div>
          Beautiful light green culottes that have been handcrafted using cotton slub. These culottes come with an elasticised waist for maximum comfort.

Dimension:L:37 Inch, Length of sleeves:20 inch

Color:  Peepal

Material:100% Cotton

Finish: Handcrafted
        </div>
      </div>

      <div class="price-box">
        
        <div class="price-sale">
          
            <span class="old-price" data-compare-price-grid=""><span class="money">₹3,229.00</span></span>
            <span class="special-price" data-price-grid=""><span class="money">₹2,260.00</span></span>
          
        </div>
        
      </div>

      

      <div class="wrapper-button-card">
        
        <div class="action">
          


<form action="/cart/add" method="post" class="variants" id="list-product-form-7798101573854-product-recommendations" data-id="product-actions-7798101573854" enctype="multipart/form-data">
  
  

    
      
        <a class="rais-svgcart" data-href="/products/peepal-cotton-slub-culottes?pr_prod_strat=copurchase&amp;pr_rec_id=e3a9801a1&amp;pr_rec_pid=7798101573854&amp;pr_ref_pid=7797834973406&amp;pr_seq=uniform" title="Peepal Cotton Slub Culottes" data-init-quickshop="" tabindex="-1">
        <svg width="27px" height="27px" viewBox="0 0 27 27" version="1.1" xmlns="http://www.w3.org/2000/svg"><g id="Tjori-" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g id="PLP" transform="translate(-361.000000, -962.000000)"><g id="Product-Widget-Copy" transform="translate(18.000000, 646.000000)"><g id="Product-Widget"><g id="Shopping-Bag-Icon" transform="translate(343.000000, 316.000000)"><path d="M21.6999607,6.00003576 L26.1999499,6.00003576 L26.1999499,20.2500018 C26.1999499,21.2812493 25.8327635,22.1640595 25.0983901,22.898433 C24.3640166,23.6328065 23.4812064,23.9999928 22.4499589,23.9999928 L8.94999106,23.9999928 C7.91874352,23.9999928 7.03593336,23.6328065 6.30155987,22.898433 C5.56718639,22.1640595 5.2,21.2812493 5.2,20.2500018 L5.2,6.00003576 L9.69998927,6.00003576 C9.69998927,4.90628861 9.96561388,3.89847851 10.4968624,2.97660547 C11.0281109,2.05473243 11.7546716,1.32817166 12.6765447,0.796923168 C13.5984177,0.265674673 14.6062278,5.00679016e-05 15.699975,5.00679016e-05 C16.7937221,5.00679016e-05 17.8015322,0.265674673 18.7234053,0.796923168 C19.6452783,1.32817166 20.3718391,2.05473243 20.9030876,2.97660547 C21.4343361,3.89847851 21.6999607,4.90628861 21.6999607,6.00003576 Z M15.699975,1.50004649 C14.4499777,1.50004649 13.3874807,1.93754521 12.5124826,2.81254336 C11.6374844,3.68754151 11.1999857,4.7500385 11.1999857,6.00003576 L20.1999642,6.00003576 C20.1999642,4.7500385 19.7624655,3.68754151 18.8874674,2.81254336 C18.0124692,1.93754521 16.9499722,1.50004649 15.699975,1.50004649 Z M24.6999535,20.2500018 L24.6999535,7.50003219 L21.6999607,7.50003219 L21.6999607,9.75002682 C21.6999607,9.96877654 21.6296483,10.1484634 21.4890237,10.289088 C21.348399,10.4297127 21.1687122,10.500025 20.9499624,10.500025 C20.7312127,10.500025 20.5515259,10.4297127 20.4109012,10.289088 C20.2702766,10.1484634 20.1999642,9.96877654 20.1999642,9.75002682 L20.1999642,7.50003219 L11.1999857,7.50003219 L11.1999857,9.75002682 C11.1999857,9.96877654 11.1296734,10.1484634 10.9890487,10.289088 C10.848424,10.4297127 10.6687372,10.500025 10.4499875,10.500025 C10.2312378,10.500025 10.0515509,10.4297127 9.91092627,10.289088 C9.7703016,10.1484634 9.69998927,9.96877654 9.69998927,9.75002682 L9.69998927,7.50003219 L6.69999642,7.50003219 L6.69999642,20.2500018 C6.69999642,20.8750001 6.91874614,21.4062493 7.35624486,21.843748 C7.79374358,22.2812467 8.32499279,22.4999964 8.94999106,22.4999964 L22.4499589,22.4999964 C23.0749571,22.4999964 23.6062064,22.2812467 24.0437051,21.843748 C24.4812038,21.4062493 24.6999535,20.8750001 24.6999535,20.2500018 Z" id="shopping-bag" fill="#34302B"></path><circle id="Oval" fill="#FFFFFF" cx="6.5" cy="20.5" r="5.5"></circle><path d="M6.34998569,14.6875246 C7.39685832,14.6875246 8.36560589,14.9492428 9.25622877,15.472679 C10.1468516,15.9961151 10.8538813,16.7031448 11.3773175,17.5937677 C11.9007536,18.4843905 12.1624718,19.4531381 12.1624718,20.5000107 C12.1624718,21.5468834 11.9007536,22.5156309 11.3773175,23.4062538 C10.8538813,24.2968767 10.1468516,25.0039064 9.25622877,25.5273425 C8.36560589,26.0507786 7.39685832,26.3124969 6.34998569,26.3124969 C5.30311307,26.3124969 4.3343655,26.0507786 3.44374262,25.5273425 C2.55311975,25.0039064 1.84609006,24.2968767 1.32265393,23.4062538 C0.799217798,22.5156309 0.537499553,21.5468834 0.537499553,20.5000107 C0.537499553,19.4531381 0.799217798,18.4843905 1.32265393,17.5937677 C1.84609006,16.7031448 2.55311975,15.9961151 3.44374262,15.472679 C4.3343655,14.9492428 5.30311307,14.6875246 6.34998569,14.6875246 Z M9.72497765,21.1562592 L9.72497765,19.8437623 C9.72497765,19.7656376 9.69763384,19.6992313 9.64294659,19.644544 C9.58825934,19.5898568 9.52185301,19.562513 9.44372832,19.562513 L7.28748346,19.562513 L7.28748346,17.4062681 C7.28748346,17.3281434 7.26013966,17.2617371 7.20545241,17.2070498 C7.15076515,17.1523626 7.08435882,17.1250188 7.00623413,17.1250188 L5.69373726,17.1250188 C5.61561256,17.1250188 5.54920623,17.1523626 5.49451898,17.2070498 C5.43983173,17.2617371 5.41248793,17.3281434 5.41248793,17.4062681 L5.41248793,19.562513 L3.25624307,19.562513 C3.17811838,19.562513 3.11171205,19.5898568 3.0570248,19.644544 C3.00233755,19.6992313 2.97499374,19.7656376 2.97499374,19.8437623 L2.97499374,21.1562592 C2.97499374,21.2343839 3.00233755,21.3007902 3.0570248,21.3554774 C3.11171205,21.4101647 3.17811838,21.4375085 3.25624307,21.4375085 L5.41248793,21.4375085 L5.41248793,23.5937534 C5.41248793,23.671878 5.43983173,23.7382844 5.49451898,23.7929716 C5.54920623,23.8476589 5.61561256,23.8750027 5.69373726,23.8750027 L7.00623413,23.8750027 C7.08435882,23.8750027 7.15076515,23.8476589 7.20545241,23.7929716 C7.26013966,23.7382844 7.28748346,23.671878 7.28748346,23.5937534 L7.28748346,21.4375085 L9.44372832,21.4375085 C9.52185301,21.4375085 9.58825934,21.4101647 9.64294659,21.3554774 C9.69763384,21.3007902 9.72497765,21.2343839 9.72497765,21.1562592 Z" id="plus-circle" fill="#C0B394"></path></g></g></g></g></g></svg>
        </a>
      
    
  
  
</form>

  
      
          <div class="product-card__variant--popup">
              <div class="product-card__variant--popup--content">
    
    <form action="/cart/add" method="post" class="variants" id="swatch-list-product-form-7798101573854-product-recommendations-list" data-id="product-actions-7798101573854" enctype="multipart/form-data">
        
            
            
            
                
                    <div class="selector-wrapper selector-wrapper-1 swatch js product-form__item" data-option-index="0">
                        <label class="form-label ">
                            Size: <span class="label-value-1">XS</span>
                        </label>
                        
                        
                        

                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size xs available" data-value="XS">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7798101573854-product-recommendationslist" data-index="option1" value="XS" id="SingleOptionSelector0-XS-7798101573854-product-recommendationslist" tabindex="-1">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-XS-7798101573854-product-recommendationslist" data-original-title="XS">XS</label>
                                </div>
                            
                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size s available" data-value="S">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7798101573854-product-recommendationslist" data-index="option1" value="S" id="SingleOptionSelector0-S-7798101573854-product-recommendationslist" tabindex="-1">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-S-7798101573854-product-recommendationslist" data-original-title="S">S</label>
                                </div>
                            
                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size m available" data-value="M">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7798101573854-product-recommendationslist" data-index="option1" value="M" id="SingleOptionSelector0-M-7798101573854-product-recommendationslist" tabindex="-1">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-M-7798101573854-product-recommendationslist" data-original-title="M">M</label>
                                </div>
                            
                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size l available" data-value="L">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7798101573854-product-recommendationslist" data-index="option1" value="L" id="SingleOptionSelector0-L-7798101573854-product-recommendationslist" tabindex="-1">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-L-7798101573854-product-recommendationslist" data-original-title="L">L</label>
                                </div>
                            
                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size xl available" data-value="XL">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7798101573854-product-recommendationslist" data-index="option1" value="XL" id="SingleOptionSelector0-XL-7798101573854-product-recommendationslist" tabindex="-1">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-XL-7798101573854-product-recommendationslist" data-original-title="XL">XL</label>
                                </div>
                            
                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size xxl available" data-value="XXL">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7798101573854-product-recommendationslist" data-index="option1" value="XXL" id="SingleOptionSelector0-XXL-7798101573854-product-recommendationslist" tabindex="-1">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-XXL-7798101573854-product-recommendationslist" data-original-title="XXL">XXL</label>
                                </div>
                            
                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size xxxl available" data-value="XXXL">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7798101573854-product-recommendationslist" data-index="option1" value="XXXL" id="SingleOptionSelector0-XXXL-7798101573854-product-recommendationslist" tabindex="-1">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-XXXL-7798101573854-product-recommendationslist" data-original-title="XXXL">XXXL</label>
                                </div>
                            
                        
                    </div>
                
            
        

        <select name="id" id="ProductSelect-7798101573854-product-recommendationslist" class="product-form__variants no-js" tabindex="-1">
            
                
                    <option value="43400424980702">
                        XS
                    </option>
                
            
                
                    <option value="43400425013470">
                        S
                    </option>
                
            
                
                    <option value="43400425046238">
                        M
                    </option>
                
            
                
                    <option value="43400425079006">
                        L
                    </option>
                
            
                
                    <option value="43400425111774">
                        XL
                    </option>
                
            
                
                    <option value="43400425144542">
                        XXL
                    </option>
                
            
                
                    <option value="43400425177310">
                        XXXL
                    </option>
                
            
        </select>
        <div class="product-card__button_cancel_mobile">
            <a class="btn btn-cancel" data-cancel-swatch-qs="" tabindex="-1"><svg aria-hidden="true" data-prefix="fal" data-icon="times" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" class="svg-inline--fa fa-times fa-w-10 fa-2x"><path fill="currentColor" d="M193.94 256L296.5 153.44l21.15-21.15c3.12-3.12 3.12-8.19 0-11.31l-22.63-22.63c-3.12-3.12-8.19-3.12-11.31 0L160 222.06 36.29 98.34c-3.12-3.12-8.19-3.12-11.31 0L2.34 120.97c-3.12 3.12-3.12 8.19 0 11.31L126.06 256 2.34 379.71c-3.12 3.12-3.12 8.19 0 11.31l22.63 22.63c3.12 3.12 8.19 3.12 11.31 0L160 289.94 262.56 392.5l21.15 21.15c3.12 3.12 8.19 3.12 11.31 0l22.63-22.63c3.12-3.12 3.12-8.19 0-11.31L193.94 256z" class=""></path></svg></a>
        </div>
        <div class="product-card__button2">
            <input type="hidden" name="quantity" value="1" tabindex="-1">
            <button data-btn-addtocart="" class="btn add-to-cart-btn" type="submit" data-form-id="#swatch-list-product-form-7798101573854-product-recommendations-list" tabindex="-1">
                Submit
            </button>
            <a class="btn btn-cancel" data-cancel-swatch-qs="" tabindex="-1">Cancel</a>
        </div>
        
    </form>
</div>
          </div>
      
  

        </div>
        
      </div>
      
    </div>
    
  </div>
</div>

          
        </div><div class="grid-item small--one-half medium-up--one-quarter col-12 slick-slide" style="width: 173px;" tabindex="-1" role="option" aria-describedby="slick-slide27" data-slick-index="7" aria-hidden="true">
          
            
<div class="inner product-item on-sale" data-product-id="product-7798054027486" data-json-product="{&quot;id&quot;: 7798054027486,&quot;handle&quot;: &quot;red-ajrakh-print-gathered-and-flared-kurta&quot;,&quot;media&quot;: [{&quot;alt&quot;:null,&quot;id&quot;:30634137813214,&quot;position&quot;:1,&quot;preview_image&quot;:{&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:2360,&quot;width&quot;:1600,&quot;src&quot;:&quot;https:\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/TJ-MK-270-18_1.jpg?v=1663265999&quot;},&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:2360,&quot;media_type&quot;:&quot;image&quot;,&quot;src&quot;:&quot;https:\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/TJ-MK-270-18_1.jpg?v=1663265999&quot;,&quot;width&quot;:1600},{&quot;alt&quot;:null,&quot;id&quot;:30634137845982,&quot;position&quot;:2,&quot;preview_image&quot;:{&quot;aspect_ratio&quot;:0.703,&quot;height&quot;:2360,&quot;width&quot;:1660,&quot;src&quot;:&quot;https:\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/TJ-MK-270-18_2.jpg?v=1663265999&quot;},&quot;aspect_ratio&quot;:0.703,&quot;height&quot;:2360,&quot;media_type&quot;:&quot;image&quot;,&quot;src&quot;:&quot;https:\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/TJ-MK-270-18_2.jpg?v=1663265999&quot;,&quot;width&quot;:1660},{&quot;alt&quot;:null,&quot;id&quot;:30634137878750,&quot;position&quot;:3,&quot;preview_image&quot;:{&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:2360,&quot;width&quot;:1600,&quot;src&quot;:&quot;https:\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/TJ-MK-270-18_3.jpg?v=1663265999&quot;},&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:2360,&quot;media_type&quot;:&quot;image&quot;,&quot;src&quot;:&quot;https:\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/TJ-MK-270-18_3.jpg?v=1663265999&quot;,&quot;width&quot;:1600},{&quot;alt&quot;:null,&quot;id&quot;:30634137911518,&quot;position&quot;:4,&quot;preview_image&quot;:{&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:2360,&quot;width&quot;:1600,&quot;src&quot;:&quot;https:\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/TJ-MK-270-18_4.jpg?v=1663265999&quot;},&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:2360,&quot;media_type&quot;:&quot;image&quot;,&quot;src&quot;:&quot;https:\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/TJ-MK-270-18_4.jpg?v=1663265999&quot;,&quot;width&quot;:1600},{&quot;alt&quot;:null,&quot;id&quot;:30634137944286,&quot;position&quot;:5,&quot;preview_image&quot;:{&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:2360,&quot;width&quot;:1600,&quot;src&quot;:&quot;https:\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/TJ-MK-270-18_5.jpg?v=1663265999&quot;},&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:2360,&quot;media_type&quot;:&quot;image&quot;,&quot;src&quot;:&quot;https:\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/TJ-MK-270-18_5.jpg?v=1663265999&quot;,&quot;width&quot;:1600}],&quot;variants&quot;: [{&quot;id&quot;:43400257863902,&quot;title&quot;:&quot;XS&quot;,&quot;option1&quot;:&quot;XS&quot;,&quot;option2&quot;:null,&quot;option3&quot;:null,&quot;sku&quot;:&quot;TJ-MK-270-18-XS&quot;,&quot;requires_shipping&quot;:true,&quot;taxable&quot;:true,&quot;featured_image&quot;:null,&quot;available&quot;:true,&quot;name&quot;:&quot;Red Ajrakh Print Gathered and Flared Kurta - XS&quot;,&quot;public_title&quot;:&quot;XS&quot;,&quot;options&quot;:[&quot;XS&quot;],&quot;price&quot;:426200,&quot;weight&quot;:0,&quot;compare_at_price&quot;:608800,&quot;inventory_management&quot;:&quot;shopify&quot;,&quot;barcode&quot;:null,&quot;quantity_rule&quot;:{&quot;min&quot;:1,&quot;max&quot;:null,&quot;increment&quot;:1}},{&quot;id&quot;:43400257896670,&quot;title&quot;:&quot;S&quot;,&quot;option1&quot;:&quot;S&quot;,&quot;option2&quot;:null,&quot;option3&quot;:null,&quot;sku&quot;:&quot;TJ-MK-270-18-S&quot;,&quot;requires_shipping&quot;:true,&quot;taxable&quot;:true,&quot;featured_image&quot;:null,&quot;available&quot;:true,&quot;name&quot;:&quot;Red Ajrakh Print Gathered and Flared Kurta - S&quot;,&quot;public_title&quot;:&quot;S&quot;,&quot;options&quot;:[&quot;S&quot;],&quot;price&quot;:426200,&quot;weight&quot;:0,&quot;compare_at_price&quot;:608800,&quot;inventory_management&quot;:&quot;shopify&quot;,&quot;barcode&quot;:null,&quot;quantity_rule&quot;:{&quot;min&quot;:1,&quot;max&quot;:null,&quot;increment&quot;:1}},{&quot;id&quot;:43400257929438,&quot;title&quot;:&quot;M&quot;,&quot;option1&quot;:&quot;M&quot;,&quot;option2&quot;:null,&quot;option3&quot;:null,&quot;sku&quot;:&quot;TJ-MK-270-18-M&quot;,&quot;requires_shipping&quot;:true,&quot;taxable&quot;:true,&quot;featured_image&quot;:null,&quot;available&quot;:true,&quot;name&quot;:&quot;Red Ajrakh Print Gathered and Flared Kurta - M&quot;,&quot;public_title&quot;:&quot;M&quot;,&quot;options&quot;:[&quot;M&quot;],&quot;price&quot;:426200,&quot;weight&quot;:0,&quot;compare_at_price&quot;:608800,&quot;inventory_management&quot;:&quot;shopify&quot;,&quot;barcode&quot;:null,&quot;quantity_rule&quot;:{&quot;min&quot;:1,&quot;max&quot;:null,&quot;increment&quot;:1}},{&quot;id&quot;:43400257962206,&quot;title&quot;:&quot;L&quot;,&quot;option1&quot;:&quot;L&quot;,&quot;option2&quot;:null,&quot;option3&quot;:null,&quot;sku&quot;:&quot;TJ-MK-270-18-L&quot;,&quot;requires_shipping&quot;:true,&quot;taxable&quot;:true,&quot;featured_image&quot;:null,&quot;available&quot;:true,&quot;name&quot;:&quot;Red Ajrakh Print Gathered and Flared Kurta - L&quot;,&quot;public_title&quot;:&quot;L&quot;,&quot;options&quot;:[&quot;L&quot;],&quot;price&quot;:426200,&quot;weight&quot;:0,&quot;compare_at_price&quot;:608800,&quot;inventory_management&quot;:&quot;shopify&quot;,&quot;barcode&quot;:null,&quot;quantity_rule&quot;:{&quot;min&quot;:1,&quot;max&quot;:null,&quot;increment&quot;:1}},{&quot;id&quot;:43400257994974,&quot;title&quot;:&quot;XL&quot;,&quot;option1&quot;:&quot;XL&quot;,&quot;option2&quot;:null,&quot;option3&quot;:null,&quot;sku&quot;:&quot;TJ-MK-270-18-XL&quot;,&quot;requires_shipping&quot;:true,&quot;taxable&quot;:true,&quot;featured_image&quot;:null,&quot;available&quot;:true,&quot;name&quot;:&quot;Red Ajrakh Print Gathered and Flared Kurta - XL&quot;,&quot;public_title&quot;:&quot;XL&quot;,&quot;options&quot;:[&quot;XL&quot;],&quot;price&quot;:426200,&quot;weight&quot;:0,&quot;compare_at_price&quot;:608800,&quot;inventory_management&quot;:&quot;shopify&quot;,&quot;barcode&quot;:null,&quot;quantity_rule&quot;:{&quot;min&quot;:1,&quot;max&quot;:null,&quot;increment&quot;:1}},{&quot;id&quot;:43400258027742,&quot;title&quot;:&quot;XXL&quot;,&quot;option1&quot;:&quot;XXL&quot;,&quot;option2&quot;:null,&quot;option3&quot;:null,&quot;sku&quot;:&quot;TJ-MK-270-18-XXL&quot;,&quot;requires_shipping&quot;:true,&quot;taxable&quot;:true,&quot;featured_image&quot;:null,&quot;available&quot;:true,&quot;name&quot;:&quot;Red Ajrakh Print Gathered and Flared Kurta - XXL&quot;,&quot;public_title&quot;:&quot;XXL&quot;,&quot;options&quot;:[&quot;XXL&quot;],&quot;price&quot;:426200,&quot;weight&quot;:0,&quot;compare_at_price&quot;:608800,&quot;inventory_management&quot;:&quot;shopify&quot;,&quot;barcode&quot;:null,&quot;quantity_rule&quot;:{&quot;min&quot;:1,&quot;max&quot;:null,&quot;increment&quot;:1}},{&quot;id&quot;:43400258060510,&quot;title&quot;:&quot;XXXL&quot;,&quot;option1&quot;:&quot;XXXL&quot;,&quot;option2&quot;:null,&quot;option3&quot;:null,&quot;sku&quot;:&quot;TJ-MK-270-18-XXXL&quot;,&quot;requires_shipping&quot;:true,&quot;taxable&quot;:true,&quot;featured_image&quot;:null,&quot;available&quot;:true,&quot;name&quot;:&quot;Red Ajrakh Print Gathered and Flared Kurta - XXXL&quot;,&quot;public_title&quot;:&quot;XXXL&quot;,&quot;options&quot;:[&quot;XXXL&quot;],&quot;price&quot;:426200,&quot;weight&quot;:0,&quot;compare_at_price&quot;:608800,&quot;inventory_management&quot;:&quot;shopify&quot;,&quot;barcode&quot;:null,&quot;quantity_rule&quot;:{&quot;min&quot;:1,&quot;max&quot;:null,&quot;increment&quot;:1}}]}">
  <div class="inner-top">
    <div class="product-top">
      <div class="product-image image-swap">
        <a href="/products/red-ajrakh-print-gathered-and-flared-kurta?pr_prod_strat=copurchase&amp;pr_rec_id=e3a9801a1&amp;pr_rec_pid=7798054027486&amp;pr_ref_pid=7797834973406&amp;pr_seq=uniform" class="product-grid-image adaptive_height" data-collections-related="/collections/?view=related" style="padding-top: 147.5%" tabindex="-1">
          



  <picture data-index="0">
    <source class="image-source-1" data-srcset="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-270-18_1_300x.jpg?v=1663265999" media="(max-width: 767px)">
    <source class="image-source-2" data-srcset=" //cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-270-18_1_360x.jpg?v=1663265999 360w,
                          //cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-270-18_1_540x.jpg?v=1663265999 540w,
                          //cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-270-18_1_720x.jpg?v=1663265999 720w,
                          //cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-270-18_1_900x.jpg?v=1663265999 900w" media="(min-width: 768px)">

    <img alt="Red Ajrakh Print Gathered and Flared Kurta" class="images-one lazyload" data-widths="[180, 360, 540, 720, 900, 1080, 1296, 1512, 1728, 2048]" data-aspectratio="0.6779661016949152" data-sizes="auto" data-srcset="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-270-18_1_360x.jpg?v=1663265999 360w,
                    //cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-270-18_1_540x.jpg?v=1663265999 540w,
                    //cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-270-18_1_720x.jpg?v=1663265999 720w,
                    //cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-270-18_1_900x.jpg?v=1663265999 900w" data-image="">
  </picture>
  <span class="images-two">
    
    <picture data-index="1">
      <source data-srcset="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-270-18_2_300x.jpg?v=1663265999" media="(max-width: 767px)">
      <source data-srcset="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-270-18_2_360x.jpg?v=1663265999 360w,
                            //cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-270-18_2_540x.jpg?v=1663265999 540w,
                            //cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-270-18_2_720x.jpg?v=1663265999 720w,
                            //cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-270-18_2_900x.jpg?v=1663265999 900w" media="(min-width: 768px)">

      <img alt="Red Ajrakh Print Gathered and Flared Kurta" class="lazyload" data-widths="[180, 360, 540, 720, 900, 1080, 1296, 1512, 1728, 2048]" data-aspectratio="0.6779661016949152" data-sizes="auto" data-srcset="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-270-18_2_360x.jpg?v=1663265999 360w,
                        //cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-270-18_2_540x.jpg?v=1663265999 540w,
                        //cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-270-18_2_720x.jpg?v=1663265999 720w,
                        //cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-270-18_2_900x.jpg?v=1663265999 900w" data-image="">
    </picture>
    
  </span>

        </a>
      </div>

      
  
  <div class="product-label" data-label-new-number="30">
    

    
      
        <strong class="label sale-label">
          Sale
        </strong>
        
      
    

    
    
    

    
    
    
  </div>
  






      
<a class="wishlist " data-icon-wishlist="" aria-label="Wish Lists" href="#" data-product-handle="red-ajrakh-print-gathered-and-flared-kurta" data-id="7798054027486" tabindex="-1">
	    	<svg viewBox="0 0 512 512">
<g>
<g>
  <path d="M474.644,74.27C449.391,45.616,414.358,29.836,376,29.836c-53.948,0-88.103,32.22-107.255,59.25
    c-4.969,7.014-9.196,14.047-12.745,20.665c-3.549-6.618-7.775-13.651-12.745-20.665c-19.152-27.03-53.307-59.25-107.255-59.25
    c-38.358,0-73.391,15.781-98.645,44.435C13.267,101.605,0,138.213,0,177.351c0,42.603,16.633,82.228,52.345,124.7
    c31.917,37.96,77.834,77.088,131.005,122.397c19.813,16.884,40.302,34.344,62.115,53.429l0.655,0.574
    c2.828,2.476,6.354,3.713,9.88,3.713s7.052-1.238,9.88-3.713l0.655-0.574c21.813-19.085,42.302-36.544,62.118-53.431
    c53.168-45.306,99.085-84.434,131.002-122.395C495.367,259.578,512,219.954,512,177.351
    C512,138.213,498.733,101.605,474.644,74.27z M309.193,401.614c-17.08,14.554-34.658,29.533-53.193,45.646
    c-18.534-16.111-36.113-31.091-53.196-45.648C98.745,312.939,30,254.358,30,177.351c0-31.83,10.605-61.394,29.862-83.245
    C79.34,72.007,106.379,59.836,136,59.836c41.129,0,67.716,25.338,82.776,46.594c13.509,19.064,20.558,38.282,22.962,45.659
    c2.011,6.175,7.768,10.354,14.262,10.354c6.494,0,12.251-4.179,14.262-10.354c2.404-7.377,9.453-26.595,22.962-45.66
    c15.06-21.255,41.647-46.593,82.776-46.593c29.621,0,56.66,12.171,76.137,34.27C471.395,115.957,482,145.521,482,177.351
    C482,254.358,413.255,312.939,309.193,401.614z"></path>
</g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
</svg>
		
</a>
 


      
        <a class="quickview-button" href="javascript:void(0)" id="red-ajrakh-print-gathered-and-flared-kurta" title="Quick View" tabindex="-1">
          <span>Quick view</span>
          <svg viewBox="0 0 511.999 511.999" width="100%" height="100%">
    <path d="M508.745,246.041c-4.574-6.257-113.557-153.206-252.748-153.206S7.818,239.784,3.249,246.035         c-4.332,5.936-4.332,13.987,0,19.923c4.569,6.257,113.557,153.206,252.748,153.206s248.174-146.95,252.748-153.201         C513.083,260.028,513.083,251.971,508.745,246.041z M255.997,385.406c-102.529,0-191.33-97.533-217.617-129.418         c26.253-31.913,114.868-129.395,217.617-129.395c102.524,0,191.319,97.516,217.617,129.418         C447.361,287.923,358.746,385.406,255.997,385.406z"></path>
      <path d="M255.997,154.725c-55.842,0-101.275,45.433-101.275,101.275s45.433,101.275,101.275,101.275         s101.275-45.433,101.275-101.275S311.839,154.725,255.997,154.725z M255.997,323.516c-37.23,0-67.516-30.287-67.516-67.516         s30.287-67.516,67.516-67.516s67.516,30.287,67.516,67.516S293.227,323.516,255.997,323.516z"></path>
</svg>
        </a>
      

      

    
    <div class="product-bottom">
      
      
        <span class="spr-badge" id="spr_badge_7798054027486" data-rating="0.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star-empty" aria-hidden="true"></i><i class="spr-icon spr-icon-star-empty" aria-hidden="true"></i><i class="spr-icon spr-icon-star-empty" aria-hidden="true"></i><i class="spr-icon spr-icon-star-empty" aria-hidden="true"></i><i class="spr-icon spr-icon-star-empty" aria-hidden="true"></i></span><span class="spr-badge-caption">No reviews</span>
</span>

      
      <a class="product-title " href="/products/red-ajrakh-print-gathered-and-flared-kurta?pr_prod_strat=copurchase&amp;pr_rec_id=e3a9801a1&amp;pr_rec_pid=7798054027486&amp;pr_ref_pid=7797834973406&amp;pr_seq=uniform" tabindex="-1">
        
<span>Red Ajrakh Print Gathered and Flared Kurta</span>

        
          
          
      </a>
      <div class="half-grid-rais">
      <div class="price-box">
        
        <div class="price-sale">
          
            <span class="special-price" data-price-grid=""><span class="money">₹4262.00</span></span>
            <span class="old-price" data-compare-price-grid="">6088.00</span>
            <span class="percentage-price">(30%)</span>
         
        </div>
        
      </div>
    </div>

      <div class="add-to-cart-grid-rais">
        
        <div class="action">
          


<form action="/cart/add" method="post" class="variants" id="grid-product-form--7798054027486-product-recommendations" data-id="product-actions-7798054027486" enctype="multipart/form-data">
  
  

    
      
        <a class="rais-svgcart" data-href="/products/red-ajrakh-print-gathered-and-flared-kurta?pr_prod_strat=copurchase&amp;pr_rec_id=e3a9801a1&amp;pr_rec_pid=7798054027486&amp;pr_ref_pid=7797834973406&amp;pr_seq=uniform" title="Red Ajrakh Print Gathered and Flared Kurta" data-init-quickshop="" tabindex="-1">
        <svg width="27px" height="27px" viewBox="0 0 27 27" version="1.1" xmlns="http://www.w3.org/2000/svg"><g id="Tjori-" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g id="PLP" transform="translate(-361.000000, -962.000000)"><g id="Product-Widget-Copy" transform="translate(18.000000, 646.000000)"><g id="Product-Widget"><g id="Shopping-Bag-Icon" transform="translate(343.000000, 316.000000)"><path d="M21.6999607,6.00003576 L26.1999499,6.00003576 L26.1999499,20.2500018 C26.1999499,21.2812493 25.8327635,22.1640595 25.0983901,22.898433 C24.3640166,23.6328065 23.4812064,23.9999928 22.4499589,23.9999928 L8.94999106,23.9999928 C7.91874352,23.9999928 7.03593336,23.6328065 6.30155987,22.898433 C5.56718639,22.1640595 5.2,21.2812493 5.2,20.2500018 L5.2,6.00003576 L9.69998927,6.00003576 C9.69998927,4.90628861 9.96561388,3.89847851 10.4968624,2.97660547 C11.0281109,2.05473243 11.7546716,1.32817166 12.6765447,0.796923168 C13.5984177,0.265674673 14.6062278,5.00679016e-05 15.699975,5.00679016e-05 C16.7937221,5.00679016e-05 17.8015322,0.265674673 18.7234053,0.796923168 C19.6452783,1.32817166 20.3718391,2.05473243 20.9030876,2.97660547 C21.4343361,3.89847851 21.6999607,4.90628861 21.6999607,6.00003576 Z M15.699975,1.50004649 C14.4499777,1.50004649 13.3874807,1.93754521 12.5124826,2.81254336 C11.6374844,3.68754151 11.1999857,4.7500385 11.1999857,6.00003576 L20.1999642,6.00003576 C20.1999642,4.7500385 19.7624655,3.68754151 18.8874674,2.81254336 C18.0124692,1.93754521 16.9499722,1.50004649 15.699975,1.50004649 Z M24.6999535,20.2500018 L24.6999535,7.50003219 L21.6999607,7.50003219 L21.6999607,9.75002682 C21.6999607,9.96877654 21.6296483,10.1484634 21.4890237,10.289088 C21.348399,10.4297127 21.1687122,10.500025 20.9499624,10.500025 C20.7312127,10.500025 20.5515259,10.4297127 20.4109012,10.289088 C20.2702766,10.1484634 20.1999642,9.96877654 20.1999642,9.75002682 L20.1999642,7.50003219 L11.1999857,7.50003219 L11.1999857,9.75002682 C11.1999857,9.96877654 11.1296734,10.1484634 10.9890487,10.289088 C10.848424,10.4297127 10.6687372,10.500025 10.4499875,10.500025 C10.2312378,10.500025 10.0515509,10.4297127 9.91092627,10.289088 C9.7703016,10.1484634 9.69998927,9.96877654 9.69998927,9.75002682 L9.69998927,7.50003219 L6.69999642,7.50003219 L6.69999642,20.2500018 C6.69999642,20.8750001 6.91874614,21.4062493 7.35624486,21.843748 C7.79374358,22.2812467 8.32499279,22.4999964 8.94999106,22.4999964 L22.4499589,22.4999964 C23.0749571,22.4999964 23.6062064,22.2812467 24.0437051,21.843748 C24.4812038,21.4062493 24.6999535,20.8750001 24.6999535,20.2500018 Z" id="shopping-bag" fill="#34302B"></path><circle id="Oval" fill="#FFFFFF" cx="6.5" cy="20.5" r="5.5"></circle><path d="M6.34998569,14.6875246 C7.39685832,14.6875246 8.36560589,14.9492428 9.25622877,15.472679 C10.1468516,15.9961151 10.8538813,16.7031448 11.3773175,17.5937677 C11.9007536,18.4843905 12.1624718,19.4531381 12.1624718,20.5000107 C12.1624718,21.5468834 11.9007536,22.5156309 11.3773175,23.4062538 C10.8538813,24.2968767 10.1468516,25.0039064 9.25622877,25.5273425 C8.36560589,26.0507786 7.39685832,26.3124969 6.34998569,26.3124969 C5.30311307,26.3124969 4.3343655,26.0507786 3.44374262,25.5273425 C2.55311975,25.0039064 1.84609006,24.2968767 1.32265393,23.4062538 C0.799217798,22.5156309 0.537499553,21.5468834 0.537499553,20.5000107 C0.537499553,19.4531381 0.799217798,18.4843905 1.32265393,17.5937677 C1.84609006,16.7031448 2.55311975,15.9961151 3.44374262,15.472679 C4.3343655,14.9492428 5.30311307,14.6875246 6.34998569,14.6875246 Z M9.72497765,21.1562592 L9.72497765,19.8437623 C9.72497765,19.7656376 9.69763384,19.6992313 9.64294659,19.644544 C9.58825934,19.5898568 9.52185301,19.562513 9.44372832,19.562513 L7.28748346,19.562513 L7.28748346,17.4062681 C7.28748346,17.3281434 7.26013966,17.2617371 7.20545241,17.2070498 C7.15076515,17.1523626 7.08435882,17.1250188 7.00623413,17.1250188 L5.69373726,17.1250188 C5.61561256,17.1250188 5.54920623,17.1523626 5.49451898,17.2070498 C5.43983173,17.2617371 5.41248793,17.3281434 5.41248793,17.4062681 L5.41248793,19.562513 L3.25624307,19.562513 C3.17811838,19.562513 3.11171205,19.5898568 3.0570248,19.644544 C3.00233755,19.6992313 2.97499374,19.7656376 2.97499374,19.8437623 L2.97499374,21.1562592 C2.97499374,21.2343839 3.00233755,21.3007902 3.0570248,21.3554774 C3.11171205,21.4101647 3.17811838,21.4375085 3.25624307,21.4375085 L5.41248793,21.4375085 L5.41248793,23.5937534 C5.41248793,23.671878 5.43983173,23.7382844 5.49451898,23.7929716 C5.54920623,23.8476589 5.61561256,23.8750027 5.69373726,23.8750027 L7.00623413,23.8750027 C7.08435882,23.8750027 7.15076515,23.8476589 7.20545241,23.7929716 C7.26013966,23.7382844 7.28748346,23.671878 7.28748346,23.5937534 L7.28748346,21.4375085 L9.44372832,21.4375085 C9.52185301,21.4375085 9.58825934,21.4101647 9.64294659,21.3554774 C9.69763384,21.3007902 9.72497765,21.2343839 9.72497765,21.1562592 Z" id="plus-circle" fill="#C0B394"></path></g></g></g></g></g></svg>
        </a>
      
    
  
  
</form>

  
      
          <div class="product-card__variant--popup">
              <div class="product-card__variant--popup--content">
    
    <form action="/cart/add" method="post" class="variants" id="swatch-grid-product-form--7798054027486-product-recommendations-" data-id="product-actions-7798054027486" enctype="multipart/form-data">
        
            
            
            
                
                    <div class="selector-wrapper selector-wrapper-1 swatch js product-form__item" data-option-index="0">
                        <label class="form-label ">
                            Size: <span class="label-value-1">XS</span>
                        </label>
                        
                        
                        

                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size xs available" data-value="XS">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7798054027486-product-recommendations" data-index="option1" value="XS" id="SingleOptionSelector0-XS-7798054027486-product-recommendations" tabindex="-1">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-XS-7798054027486-product-recommendations" data-original-title="XS">XS</label>
                                </div>
                            
                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size s available" data-value="S">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7798054027486-product-recommendations" data-index="option1" value="S" id="SingleOptionSelector0-S-7798054027486-product-recommendations" tabindex="-1">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-S-7798054027486-product-recommendations" data-original-title="S">S</label>
                                </div>
                            
                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size m available" data-value="M">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7798054027486-product-recommendations" data-index="option1" value="M" id="SingleOptionSelector0-M-7798054027486-product-recommendations" tabindex="-1">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-M-7798054027486-product-recommendations" data-original-title="M">M</label>
                                </div>
                            
                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size l available" data-value="L">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7798054027486-product-recommendations" data-index="option1" value="L" id="SingleOptionSelector0-L-7798054027486-product-recommendations" tabindex="-1">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-L-7798054027486-product-recommendations" data-original-title="L">L</label>
                                </div>
                            
                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size xl available" data-value="XL">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7798054027486-product-recommendations" data-index="option1" value="XL" id="SingleOptionSelector0-XL-7798054027486-product-recommendations" tabindex="-1">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-XL-7798054027486-product-recommendations" data-original-title="XL">XL</label>
                                </div>
                            
                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size xxl available" data-value="XXL">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7798054027486-product-recommendations" data-index="option1" value="XXL" id="SingleOptionSelector0-XXL-7798054027486-product-recommendations" tabindex="-1">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-XXL-7798054027486-product-recommendations" data-original-title="XXL">XXL</label>
                                </div>
                            
                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size xxxl available" data-value="XXXL">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7798054027486-product-recommendations" data-index="option1" value="XXXL" id="SingleOptionSelector0-XXXL-7798054027486-product-recommendations" tabindex="-1">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-XXXL-7798054027486-product-recommendations" data-original-title="XXXL">XXXL</label>
                                </div>
                            
                        
                    </div>
                
            
        

        <select name="id" id="ProductSelect-7798054027486-product-recommendations" class="product-form__variants no-js" tabindex="-1">
            
                
                    <option value="43400257863902">
                        XS
                    </option>
                
            
                
                    <option value="43400257896670">
                        S
                    </option>
                
            
                
                    <option value="43400257929438">
                        M
                    </option>
                
            
                
                    <option value="43400257962206">
                        L
                    </option>
                
            
                
                    <option value="43400257994974">
                        XL
                    </option>
                
            
                
                    <option value="43400258027742">
                        XXL
                    </option>
                
            
                
                    <option value="43400258060510">
                        XXXL
                    </option>
                
            
        </select>
        <div class="product-card__button_cancel_mobile">
            <a class="btn btn-cancel" data-cancel-swatch-qs="" tabindex="-1"><svg aria-hidden="true" data-prefix="fal" data-icon="times" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" class="svg-inline--fa fa-times fa-w-10 fa-2x"><path fill="currentColor" d="M193.94 256L296.5 153.44l21.15-21.15c3.12-3.12 3.12-8.19 0-11.31l-22.63-22.63c-3.12-3.12-8.19-3.12-11.31 0L160 222.06 36.29 98.34c-3.12-3.12-8.19-3.12-11.31 0L2.34 120.97c-3.12 3.12-3.12 8.19 0 11.31L126.06 256 2.34 379.71c-3.12 3.12-3.12 8.19 0 11.31l22.63 22.63c3.12 3.12 8.19 3.12 11.31 0L160 289.94 262.56 392.5l21.15 21.15c3.12 3.12 8.19 3.12 11.31 0l22.63-22.63c3.12-3.12 3.12-8.19 0-11.31L193.94 256z" class=""></path></svg></a>
        </div>
        <div class="product-card__button2">
            <input type="hidden" name="quantity" value="1" tabindex="-1">
            <button data-btn-addtocart="" class="btn add-to-cart-btn" type="submit" data-form-id="#swatch-grid-product-form--7798054027486-product-recommendations-" tabindex="-1">
                Submit
            </button>
            <a class="btn btn-cancel" data-cancel-swatch-qs="" tabindex="-1">Cancel</a>
        </div>
        
    </form>
</div>
          </div>
      
  

        </div>
        
      </div>
    </div>

      <div class="wrapper-size">
        
      </div>
      
    </div>

    
     
    <div class="product-details">
      

      <a class="product-title " href="/products/red-ajrakh-print-gathered-and-flared-kurta?pr_prod_strat=copurchase&amp;pr_rec_id=e3a9801a1&amp;pr_rec_pid=7798054027486&amp;pr_ref_pid=7797834973406&amp;pr_seq=uniform" tabindex="-1">
        
<span>Red Ajrakh Print Gathered and Flared Kurta</span>

        
          
        
      </a>

      
      <span class="spr-badge" id="spr_badge_7798054027486" data-rating="0.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star-empty" aria-hidden="true"></i><i class="spr-icon spr-icon-star-empty" aria-hidden="true"></i><i class="spr-icon spr-icon-star-empty" aria-hidden="true"></i><i class="spr-icon spr-icon-star-empty" aria-hidden="true"></i><i class="spr-icon spr-icon-star-empty" aria-hidden="true"></i></span><span class="spr-badge-caption">No reviews</span>
</span>

      

      <div class="short-description">
        
        <div>
          Move away from the monotonous regular designs with this Cotton Ajrakh Long Kurta. Made in Cotton , this neck long kurta with front pintucks and straight silhouette is easy to maintain and comfortable to wear for any occasion all around...
        </div>
      </div>

      <div class="price-box">
        
        <div class="price-sale">
          
            <span class="old-price" data-compare-price-grid=""><span class="money">₹6,088.00</span></span>
            <span class="special-price" data-price-grid=""><span class="money">₹4,262.00</span></span>
          
        </div>
        
      </div>

      

      <div class="wrapper-button-card">
        
        <div class="action">
          


<form action="/cart/add" method="post" class="variants" id="list-product-form-7798054027486-product-recommendations" data-id="product-actions-7798054027486" enctype="multipart/form-data">
  
  

    
      
        <a class="rais-svgcart" data-href="/products/red-ajrakh-print-gathered-and-flared-kurta?pr_prod_strat=copurchase&amp;pr_rec_id=e3a9801a1&amp;pr_rec_pid=7798054027486&amp;pr_ref_pid=7797834973406&amp;pr_seq=uniform" title="Red Ajrakh Print Gathered and Flared Kurta" data-init-quickshop="" tabindex="-1">
        <svg width="27px" height="27px" viewBox="0 0 27 27" version="1.1" xmlns="http://www.w3.org/2000/svg"><g id="Tjori-" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g id="PLP" transform="translate(-361.000000, -962.000000)"><g id="Product-Widget-Copy" transform="translate(18.000000, 646.000000)"><g id="Product-Widget"><g id="Shopping-Bag-Icon" transform="translate(343.000000, 316.000000)"><path d="M21.6999607,6.00003576 L26.1999499,6.00003576 L26.1999499,20.2500018 C26.1999499,21.2812493 25.8327635,22.1640595 25.0983901,22.898433 C24.3640166,23.6328065 23.4812064,23.9999928 22.4499589,23.9999928 L8.94999106,23.9999928 C7.91874352,23.9999928 7.03593336,23.6328065 6.30155987,22.898433 C5.56718639,22.1640595 5.2,21.2812493 5.2,20.2500018 L5.2,6.00003576 L9.69998927,6.00003576 C9.69998927,4.90628861 9.96561388,3.89847851 10.4968624,2.97660547 C11.0281109,2.05473243 11.7546716,1.32817166 12.6765447,0.796923168 C13.5984177,0.265674673 14.6062278,5.00679016e-05 15.699975,5.00679016e-05 C16.7937221,5.00679016e-05 17.8015322,0.265674673 18.7234053,0.796923168 C19.6452783,1.32817166 20.3718391,2.05473243 20.9030876,2.97660547 C21.4343361,3.89847851 21.6999607,4.90628861 21.6999607,6.00003576 Z M15.699975,1.50004649 C14.4499777,1.50004649 13.3874807,1.93754521 12.5124826,2.81254336 C11.6374844,3.68754151 11.1999857,4.7500385 11.1999857,6.00003576 L20.1999642,6.00003576 C20.1999642,4.7500385 19.7624655,3.68754151 18.8874674,2.81254336 C18.0124692,1.93754521 16.9499722,1.50004649 15.699975,1.50004649 Z M24.6999535,20.2500018 L24.6999535,7.50003219 L21.6999607,7.50003219 L21.6999607,9.75002682 C21.6999607,9.96877654 21.6296483,10.1484634 21.4890237,10.289088 C21.348399,10.4297127 21.1687122,10.500025 20.9499624,10.500025 C20.7312127,10.500025 20.5515259,10.4297127 20.4109012,10.289088 C20.2702766,10.1484634 20.1999642,9.96877654 20.1999642,9.75002682 L20.1999642,7.50003219 L11.1999857,7.50003219 L11.1999857,9.75002682 C11.1999857,9.96877654 11.1296734,10.1484634 10.9890487,10.289088 C10.848424,10.4297127 10.6687372,10.500025 10.4499875,10.500025 C10.2312378,10.500025 10.0515509,10.4297127 9.91092627,10.289088 C9.7703016,10.1484634 9.69998927,9.96877654 9.69998927,9.75002682 L9.69998927,7.50003219 L6.69999642,7.50003219 L6.69999642,20.2500018 C6.69999642,20.8750001 6.91874614,21.4062493 7.35624486,21.843748 C7.79374358,22.2812467 8.32499279,22.4999964 8.94999106,22.4999964 L22.4499589,22.4999964 C23.0749571,22.4999964 23.6062064,22.2812467 24.0437051,21.843748 C24.4812038,21.4062493 24.6999535,20.8750001 24.6999535,20.2500018 Z" id="shopping-bag" fill="#34302B"></path><circle id="Oval" fill="#FFFFFF" cx="6.5" cy="20.5" r="5.5"></circle><path d="M6.34998569,14.6875246 C7.39685832,14.6875246 8.36560589,14.9492428 9.25622877,15.472679 C10.1468516,15.9961151 10.8538813,16.7031448 11.3773175,17.5937677 C11.9007536,18.4843905 12.1624718,19.4531381 12.1624718,20.5000107 C12.1624718,21.5468834 11.9007536,22.5156309 11.3773175,23.4062538 C10.8538813,24.2968767 10.1468516,25.0039064 9.25622877,25.5273425 C8.36560589,26.0507786 7.39685832,26.3124969 6.34998569,26.3124969 C5.30311307,26.3124969 4.3343655,26.0507786 3.44374262,25.5273425 C2.55311975,25.0039064 1.84609006,24.2968767 1.32265393,23.4062538 C0.799217798,22.5156309 0.537499553,21.5468834 0.537499553,20.5000107 C0.537499553,19.4531381 0.799217798,18.4843905 1.32265393,17.5937677 C1.84609006,16.7031448 2.55311975,15.9961151 3.44374262,15.472679 C4.3343655,14.9492428 5.30311307,14.6875246 6.34998569,14.6875246 Z M9.72497765,21.1562592 L9.72497765,19.8437623 C9.72497765,19.7656376 9.69763384,19.6992313 9.64294659,19.644544 C9.58825934,19.5898568 9.52185301,19.562513 9.44372832,19.562513 L7.28748346,19.562513 L7.28748346,17.4062681 C7.28748346,17.3281434 7.26013966,17.2617371 7.20545241,17.2070498 C7.15076515,17.1523626 7.08435882,17.1250188 7.00623413,17.1250188 L5.69373726,17.1250188 C5.61561256,17.1250188 5.54920623,17.1523626 5.49451898,17.2070498 C5.43983173,17.2617371 5.41248793,17.3281434 5.41248793,17.4062681 L5.41248793,19.562513 L3.25624307,19.562513 C3.17811838,19.562513 3.11171205,19.5898568 3.0570248,19.644544 C3.00233755,19.6992313 2.97499374,19.7656376 2.97499374,19.8437623 L2.97499374,21.1562592 C2.97499374,21.2343839 3.00233755,21.3007902 3.0570248,21.3554774 C3.11171205,21.4101647 3.17811838,21.4375085 3.25624307,21.4375085 L5.41248793,21.4375085 L5.41248793,23.5937534 C5.41248793,23.671878 5.43983173,23.7382844 5.49451898,23.7929716 C5.54920623,23.8476589 5.61561256,23.8750027 5.69373726,23.8750027 L7.00623413,23.8750027 C7.08435882,23.8750027 7.15076515,23.8476589 7.20545241,23.7929716 C7.26013966,23.7382844 7.28748346,23.671878 7.28748346,23.5937534 L7.28748346,21.4375085 L9.44372832,21.4375085 C9.52185301,21.4375085 9.58825934,21.4101647 9.64294659,21.3554774 C9.69763384,21.3007902 9.72497765,21.2343839 9.72497765,21.1562592 Z" id="plus-circle" fill="#C0B394"></path></g></g></g></g></g></svg>
        </a>
      
    
  
  
</form>

  
      
          <div class="product-card__variant--popup">
              <div class="product-card__variant--popup--content">
    
    <form action="/cart/add" method="post" class="variants" id="swatch-list-product-form-7798054027486-product-recommendations-list" data-id="product-actions-7798054027486" enctype="multipart/form-data">
        
            
            
            
                
                    <div class="selector-wrapper selector-wrapper-1 swatch js product-form__item" data-option-index="0">
                        <label class="form-label ">
                            Size: <span class="label-value-1">XS</span>
                        </label>
                        
                        
                        

                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size xs available" data-value="XS">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7798054027486-product-recommendationslist" data-index="option1" value="XS" id="SingleOptionSelector0-XS-7798054027486-product-recommendationslist" tabindex="-1">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-XS-7798054027486-product-recommendationslist" data-original-title="XS">XS</label>
                                </div>
                            
                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size s available" data-value="S">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7798054027486-product-recommendationslist" data-index="option1" value="S" id="SingleOptionSelector0-S-7798054027486-product-recommendationslist" tabindex="-1">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-S-7798054027486-product-recommendationslist" data-original-title="S">S</label>
                                </div>
                            
                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size m available" data-value="M">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7798054027486-product-recommendationslist" data-index="option1" value="M" id="SingleOptionSelector0-M-7798054027486-product-recommendationslist" tabindex="-1">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-M-7798054027486-product-recommendationslist" data-original-title="M">M</label>
                                </div>
                            
                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size l available" data-value="L">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7798054027486-product-recommendationslist" data-index="option1" value="L" id="SingleOptionSelector0-L-7798054027486-product-recommendationslist" tabindex="-1">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-L-7798054027486-product-recommendationslist" data-original-title="L">L</label>
                                </div>
                            
                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size xl available" data-value="XL">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7798054027486-product-recommendationslist" data-index="option1" value="XL" id="SingleOptionSelector0-XL-7798054027486-product-recommendationslist" tabindex="-1">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-XL-7798054027486-product-recommendationslist" data-original-title="XL">XL</label>
                                </div>
                            
                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size xxl available" data-value="XXL">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7798054027486-product-recommendationslist" data-index="option1" value="XXL" id="SingleOptionSelector0-XXL-7798054027486-product-recommendationslist" tabindex="-1">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-XXL-7798054027486-product-recommendationslist" data-original-title="XXL">XXL</label>
                                </div>
                            
                        
                            
                            
                                
                                
                                
                                
                                <div class="swatch-element size xxxl available" data-value="XXXL">
                                    <input class="single-option-selector single-option-selector-quick single-option-selector-product-recommendations" type="radio" name="SingleOptionSelector-0-7798054027486-product-recommendationslist" data-index="option1" value="XXXL" id="SingleOptionSelector0-XXXL-7798054027486-product-recommendationslist" tabindex="-1">
                                    <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector0-XXXL-7798054027486-product-recommendationslist" data-original-title="XXXL">XXXL</label>
                                </div>
                            
                        
                    </div>
                
            
        

        <select name="id" id="ProductSelect-7798054027486-product-recommendationslist" class="product-form__variants no-js" tabindex="-1">
            
                
                    <option value="43400257863902">
                        XS
                    </option>
                
            
                
                    <option value="43400257896670">
                        S
                    </option>
                
            
                
                    <option value="43400257929438">
                        M
                    </option>
                
            
                
                    <option value="43400257962206">
                        L
                    </option>
                
            
                
                    <option value="43400257994974">
                        XL
                    </option>
                
            
                
                    <option value="43400258027742">
                        XXL
                    </option>
                
            
                
                    <option value="43400258060510">
                        XXXL
                    </option>
                
            
        </select>
        <div class="product-card__button_cancel_mobile">
            <a class="btn btn-cancel" data-cancel-swatch-qs="" tabindex="-1"><svg aria-hidden="true" data-prefix="fal" data-icon="times" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" class="svg-inline--fa fa-times fa-w-10 fa-2x"><path fill="currentColor" d="M193.94 256L296.5 153.44l21.15-21.15c3.12-3.12 3.12-8.19 0-11.31l-22.63-22.63c-3.12-3.12-8.19-3.12-11.31 0L160 222.06 36.29 98.34c-3.12-3.12-8.19-3.12-11.31 0L2.34 120.97c-3.12 3.12-3.12 8.19 0 11.31L126.06 256 2.34 379.71c-3.12 3.12-3.12 8.19 0 11.31l22.63 22.63c3.12 3.12 8.19 3.12 11.31 0L160 289.94 262.56 392.5l21.15 21.15c3.12 3.12 8.19 3.12 11.31 0l22.63-22.63c3.12-3.12 3.12-8.19 0-11.31L193.94 256z" class=""></path></svg></a>
        </div>
        <div class="product-card__button2">
            <input type="hidden" name="quantity" value="1" tabindex="-1">
            <button data-btn-addtocart="" class="btn add-to-cart-btn" type="submit" data-form-id="#swatch-list-product-form-7798054027486-product-recommendations-list" tabindex="-1">
                Submit
            </button>
            <a class="btn btn-cancel" data-cancel-swatch-qs="" tabindex="-1">Cancel</a>
        </div>
        
    </form>
</div>
          </div>
      
  

        </div>
        
      </div>
      
    </div>
    
  </div>
</div>

          
        </div></div></div><ul class="slick-dots" style="display: block;" role="tablist"><li class="slick-active" aria-hidden="false" role="presentation" aria-selected="true" aria-controls="navigation20" id="slick-slide20"><button type="button" data-role="none" role="button" tabindex="0">1</button></li><li aria-hidden="true" role="presentation" aria-selected="false" aria-controls="navigation21" id="slick-slide21"><button type="button" data-role="none" role="button" tabindex="0">2</button></li><li aria-hidden="true" role="presentation" aria-selected="false" aria-controls="navigation22" id="slick-slide22"><button type="button" data-role="none" role="button" tabindex="0">3</button></li><li aria-hidden="true" role="presentation" aria-selected="false" aria-controls="navigation23" id="slick-slide23"><button type="button" data-role="none" role="button" tabindex="0">4</button></li></ul></div>
    </div>
    
  </div></div></div>
</div>


<style data-shopify="">
    #product-recommendations, #shopify-section-product-recommendations {
        width: 100%;
    }
</style>
              
          </div>
          
      </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function() {
$('.smooth-scroll').on('click', function(evt) {
evt.preventDefault();
$('html, body').animate(
{ scrollTop: $($(this).attr('href')).offset().top - 200},
1000
);
});
});
</script>
      </div>
    </div>   
  </div>
</div>
  

<div class="modal fade viralb-global-custom-informatin-popup" id="viralb-global-custom-information-popup" data-global-custom-informatin-popup="">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
        	<div class="modal-header">
		        <h5 class="modal-title viralb-popup-title text-center"></h5>
		    </div>
            <a class="close close-modal" title="Close" href="javascript:void(0)" data-close-custom-information="">
                <svg aria-hidden="true" data-prefix="fal" data-icon="times" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" class="svg-inline--fa fa-times fa-w-10 fa-2x"><path fill="currentColor" d="M193.94 256L296.5 153.44l21.15-21.15c3.12-3.12 3.12-8.19 0-11.31l-22.63-22.63c-3.12-3.12-8.19-3.12-11.31 0L160 222.06 36.29 98.34c-3.12-3.12-8.19-3.12-11.31 0L2.34 120.97c-3.12 3.12-3.12 8.19 0 11.31L126.06 256 2.34 379.71c-3.12 3.12-3.12 8.19 0 11.31l22.63 22.63c3.12 3.12 8.19 3.12 11.31 0L160 289.94 262.56 392.5l21.15 21.15c3.12 3.12 8.19 3.12 11.31 0l22.63-22.63c3.12-3.12 3.12-8.19 0-11.31L193.94 256z" class=""></path></svg>
            </a>
            <div class="modal-body viralb-popup-content">
                
            </div>
        </div>     
    </div>
</div>


<script src="//cdn.shopify.com/shopifycloud/shopify/assets/themes_support/option_selection-9f517843f664ad329c689020fb1e45d03cac979f64b9eb1651ea32858b0ff452.js" type="text/javascript"></script>


<script>
  function updateContent(variantId) {
        var container = $('#store_availability')[0];
        var variantSectionUrl =
        container.dataset.baseUrl +
        'variants/' +
        variantId + 
        '/?section_id=store-availability';
        $.ajax({
          type: "get",
          url: variantSectionUrl,
          success: function(data) {
            if ($(data).find('.store-availability-container').html() != '') {
              $('#store_availability').html(data);
                var title = $('#store_availability').data('product-title');
              $('[data-store-availability-modal-product-title]').text(title);
            }
          }
        });
    };

  var buttonSlt = '[data-minus-qtt], [data-plus-qtt]',
      buttonElm = $(buttonSlt);

  $(document).on('click', buttonSlt, function(e) {
      e.preventDefault();
      e.stopPropagation();

      var self = $(this),
          input = $('.quantity input[name="quantity"]').not('.item-quantity').not('.custom-input-quantity'),
          oldVal = parseInt(input.val()),
          newVal = 1;

      switch (true) {
          case (self.hasClass('plus')): {
              newVal = oldVal + 1;
              break;
          }
          case (self.hasClass('minus') && oldVal > 1): {
              newVal = oldVal - 1;
              break;
          }
      }

      input.val(newVal);
      updatePricing();


  });
    
  
    var inven_array = {
      
      "43399271874782":"16",
      
      "43399271907550":"13",
      
      "43399271940318":"6",
      
      "43399271973086":"3",
      
      "43399272005854":"8",
      
      "43399272038622":"9",
      
      "43399272071390":"6",
      
    };
    var inven_num = '';

    var selectCallback = function(variant, selector) {
        var addToCartBtn = $('#product-add-to-cart'),
            productPrice = $('.product .price'),
            comparePrice = $('.product .compare-price'),
            productInventory = $('.product-inventory'),
            productSku = $('.product .sku-product'),
            labelSave = $('.product-photos .sale-label'),
            max_stock = $('[data-stock-hot]').data('value');

        if(variant) {
        // addToCartbtn
            if (variant.available) {
                if (variant.inventory_management!=null) {
                  for( variant_id in inven_array){
                    if(variant.id == variant_id ){
                      inven_num = inven_array[variant_id];
                      var inventoryQuantity = parseInt(inven_num);
                    }
                  }
                  // Hot stock
                  if(inventoryQuantity > 0 && inventoryQuantity <= max_stock){
                      var text_stock = window.inventory_text.hotStock.replace('[inventory]', inventoryQuantity);
                      $('[data-stock-hot]').text(text_stock).show();
                      productInventory.find('span').text(inventoryQuantity + ' ' + window.inventory_text.in_stock);
                  }else if (inventoryQuantity > 0) {
                      productInventory.find('span').text(inventoryQuantity + ' ' + window.inventory_text.in_stock);
                      $('[data-stock-hot]').hide();
                  } else{
                    productInventory.find('span').text(window.inventory_text.out_of_stock);
                    $('[data-stock-hot]').hide();
                  }
                  // Button Text
                  if(inventoryQuantity > 0){
                    // We have a valid product variant, so enable the submit button
                    addToCartBtn.removeClass('disabled').removeAttr('disabled').val(window.inventory_text.add_to_cart);
                    $('.groups-btn').removeClass('remove');
                  }else{
                    addToCartBtn.removeClass('disabled').removeAttr('disabled').val(window.inventory_text.pre_order);
                    $('.groups-btn').addClass('remove');
                  }
                } else {
                  
                  addToCartBtn.removeClass('disabled').removeAttr('disabled').val(window.inventory_text.add_to_cart);
                }
                $('[data-form-notify]').slideUp();
                $('[data-soldOut-product]').show();
            }
            else {
                $('[data-soldOut-product]').hide();
                addToCartBtn.val(window.inventory_text.sold_out).addClass('disabled').attr('disabled', 'disabled');
                $('[data-stock-hot]').hide();
                $('[data-form-notify]').slideDown();
                var url = window.location.href.split('?variant')[0];
                $('[data-value-email]').val(url + '?variant=' + variant.id);
                productInventory.find('span').text(window.inventory_text.out_of_stock);
                $('[data-stock-hot]').hide();
            };

        // Prices
            $('.product-template .swatch').each(function(){
              $(this).find('[data-option-select]').text($(this).find('input:checked').val());
            })
            $('.product #product_regular_price').val(variant.price);
            productPrice.html(Shopify.formatMoney(variant.price, "<span class=money>₹{{amount}}</span>"));

            if(variant.compare_at_price > variant.price) {
                productPrice.addClass("on-sale");
                comparePrice
                    .html(Shopify.formatMoney(variant.compare_at_price, "<span class=money>₹{{amount}}</span>"))
                    .show();
               
            }
            else {
                comparePrice.hide();
                productPrice.removeClass("on-sale");
                
            };

            

            
                Currency.convertAll(window.shop_currency, $('#currencies .active').attr('data-currency'), 'span.money', 'money_format');
            

            var form = $('#' + selector.domIdPrefix).closest('form');

            for (var i=0,length=variant.options.length; i<length; i++) {
                var radioButton = form.find('.swatch[data-option-index="' + i + '"] :radio[value="' + variant.options[i] +'"]');

                if (radioButton.size()) {
                    radioButton.get(0).checked = true;
                }
            };

            $('.product-template .swatch').each(function(){
              $(this).find('[data-option-select]').text($(this).find('input:checked').val());
            })
        }
        else {
            addToCartBtn.val(window.inventory_text.sold_out).addClass('disabled').attr('disabled', 'disabled');
        };

        

        updateContent(variant.id);

        
          /*begin variant image*/
          
          $(document).ready(function(){
              
              if (variant && variant.featured_image) {
                  var originalImage = $("img[id|='product-featured-image']");
                  var newImage = variant.featured_image;
                  var element = originalImage[0];

                  Shopify.Image.switchImage(newImage, element, function (newImageSizedSrc, newImage, element) {
                      jQuery('.slider-nav img').each(function() {
                          var grandSize = $(this).attr('src').split('?')[0].replace('_compact','');
                        

                        newImageSizedSrc = newImageSizedSrc.split('?')[0].replace('https:','').replace('http:','');
                          if(grandSize === newImageSizedSrc) {
                              var item = $(this).closest('.item');

                              item.trigger('click');
                              return false;
                          }
                      });
                  });
              };
          
          });
           
        

    };

    $(function($) {

        if($('#product-selectors').length > 0){
          new Shopify.OptionSelectors('product-selectors', {
              product: {"id":7797834973406,"title":"Mulmul Cotton Lilac V Neck Kurta","handle":"mulmul-cotton-lilac-v-neck-kurta","description":"\u003cp\u003eThis premium mul mul lilac kurta will be best pick this season for your yoga sessions. This kurta featuring three quarter balloon sleeves felicitates ease of movement and utmost comfort. Team it up with matching pants and you are good to go.\u003cbr\u003e\n\n\u003cstrong\u003eWash Care:\u003c\/strong\u003e Wash separately in cold water. Do Not Bleach and dry in shade.\u003c\/p\u003e","published_at":"2022-09-15T05:44:04+05:30","created_at":"2022-09-15T05:44:04+05:30","vendor":"Tjoritreasures","type":"","tags":["All Catalog Bestseller Sale","Apparel","Bestsellers | Apparel","Colour_Purple","EOSS - Buy 1 Get 1 - Women's Apparel","EOSS - Upto 90% Off","Gender_Women","Kurtas \u0026 Slips","Material_Pure Cotton Mulmul","Model Is Wearing_Size: S","Model's Height_5 Feet 8 Inches","New Arrivals | Apparel","Occasion_Daily","Occasion_Festive","Women Apparel - FLAT ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½1234","Women | All Products","Women | Bestsellers","Yogini"],"price":173800,"price_min":173800,"price_max":173800,"available":true,"price_varies":false,"compare_at_price":248300,"compare_at_price_min":248300,"compare_at_price_max":248300,"compare_at_price_varies":false,"variants":[{"id":43399271874782,"title":"XS","option1":"XS","option2":null,"option3":null,"sku":"TJ-MK-277-02-XS","requires_shipping":true,"taxable":true,"featured_image":null,"available":true,"name":"Mulmul Cotton Lilac V Neck Kurta - XS","public_title":"XS","options":["XS"],"price":173800,"weight":0,"compare_at_price":248300,"inventory_management":"shopify","barcode":null,"quantity_rule":{"min":1,"max":null,"increment":1}},{"id":43399271907550,"title":"S","option1":"S","option2":null,"option3":null,"sku":"TJ-MK-277-02-S","requires_shipping":true,"taxable":true,"featured_image":null,"available":true,"name":"Mulmul Cotton Lilac V Neck Kurta - S","public_title":"S","options":["S"],"price":173800,"weight":0,"compare_at_price":248300,"inventory_management":"shopify","barcode":null,"quantity_rule":{"min":1,"max":null,"increment":1}},{"id":43399271940318,"title":"M","option1":"M","option2":null,"option3":null,"sku":"TJ-MK-277-02-M","requires_shipping":true,"taxable":true,"featured_image":null,"available":true,"name":"Mulmul Cotton Lilac V Neck Kurta - M","public_title":"M","options":["M"],"price":173800,"weight":0,"compare_at_price":248300,"inventory_management":"shopify","barcode":null,"quantity_rule":{"min":1,"max":null,"increment":1}},{"id":43399271973086,"title":"L","option1":"L","option2":null,"option3":null,"sku":"TJ-MK-277-02-L","requires_shipping":true,"taxable":true,"featured_image":null,"available":true,"name":"Mulmul Cotton Lilac V Neck Kurta - L","public_title":"L","options":["L"],"price":173800,"weight":0,"compare_at_price":248300,"inventory_management":"shopify","barcode":null,"quantity_rule":{"min":1,"max":null,"increment":1}},{"id":43399272005854,"title":"XL","option1":"XL","option2":null,"option3":null,"sku":"TJ-MK-277-02-XL","requires_shipping":true,"taxable":true,"featured_image":null,"available":true,"name":"Mulmul Cotton Lilac V Neck Kurta - XL","public_title":"XL","options":["XL"],"price":173800,"weight":0,"compare_at_price":248300,"inventory_management":"shopify","barcode":null,"quantity_rule":{"min":1,"max":null,"increment":1}},{"id":43399272038622,"title":"XXL","option1":"XXL","option2":null,"option3":null,"sku":"TJ-MK-277-02-XXL","requires_shipping":true,"taxable":true,"featured_image":null,"available":true,"name":"Mulmul Cotton Lilac V Neck Kurta - XXL","public_title":"XXL","options":["XXL"],"price":173800,"weight":0,"compare_at_price":248300,"inventory_management":"shopify","barcode":null,"quantity_rule":{"min":1,"max":null,"increment":1}},{"id":43399272071390,"title":"XXXL","option1":"XXXL","option2":null,"option3":null,"sku":"TJ-MK-277-02-XXXL","requires_shipping":true,"taxable":true,"featured_image":null,"available":true,"name":"Mulmul Cotton Lilac V Neck Kurta - XXXL","public_title":"XXXL","options":["XXXL"],"price":173800,"weight":0,"compare_at_price":248300,"inventory_management":"shopify","barcode":null,"quantity_rule":{"min":1,"max":null,"increment":1}}],"images":["\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/TJ-MK-277-02_1.jpg?v=1663266908","\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/TJ-MK-277-02_2.jpg?v=1663266908","\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/TJ-MK-277-02_3.jpg?v=1663266908","\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/TJ-MK-277-02_4.jpg?v=1663266908","\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/TJ-MK-277-02_5.jpg?v=1663266908","\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/TJ-MK-277-02_6.jpg?v=1663266908"],"featured_image":"\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/TJ-MK-277-02_1.jpg?v=1663266908","options":["Size"],"media":[{"alt":null,"id":30634492461278,"position":1,"preview_image":{"aspect_ratio":0.678,"height":2360,"width":1600,"src":"https:\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/TJ-MK-277-02_1.jpg?v=1663266908"},"aspect_ratio":0.678,"height":2360,"media_type":"image","src":"https:\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/TJ-MK-277-02_1.jpg?v=1663266908","width":1600},{"alt":null,"id":30634492494046,"position":2,"preview_image":{"aspect_ratio":0.678,"height":2360,"width":1600,"src":"https:\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/TJ-MK-277-02_2.jpg?v=1663266908"},"aspect_ratio":0.678,"height":2360,"media_type":"image","src":"https:\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/TJ-MK-277-02_2.jpg?v=1663266908","width":1600},{"alt":null,"id":30634492526814,"position":3,"preview_image":{"aspect_ratio":0.678,"height":2360,"width":1600,"src":"https:\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/TJ-MK-277-02_3.jpg?v=1663266908"},"aspect_ratio":0.678,"height":2360,"media_type":"image","src":"https:\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/TJ-MK-277-02_3.jpg?v=1663266908","width":1600},{"alt":null,"id":30634492559582,"position":4,"preview_image":{"aspect_ratio":0.678,"height":2360,"width":1600,"src":"https:\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/TJ-MK-277-02_4.jpg?v=1663266908"},"aspect_ratio":0.678,"height":2360,"media_type":"image","src":"https:\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/TJ-MK-277-02_4.jpg?v=1663266908","width":1600},{"alt":null,"id":30634492592350,"position":5,"preview_image":{"aspect_ratio":0.678,"height":2360,"width":1600,"src":"https:\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/TJ-MK-277-02_5.jpg?v=1663266908"},"aspect_ratio":0.678,"height":2360,"media_type":"image","src":"https:\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/TJ-MK-277-02_5.jpg?v=1663266908","width":1600},{"alt":null,"id":30634492625118,"position":6,"preview_image":{"aspect_ratio":0.678,"height":2360,"width":1600,"src":"https:\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/TJ-MK-277-02_6.jpg?v=1663266908"},"aspect_ratio":0.678,"height":2360,"media_type":"image","src":"https:\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/TJ-MK-277-02_6.jpg?v=1663266908","width":1600}],"content":"\u003cp\u003eThis premium mul mul lilac kurta will be best pick this season for your yoga sessions. This kurta featuring three quarter balloon sleeves felicitates ease of movement and utmost comfort. Team it up with matching pants and you are good to go.\u003cbr\u003e\n\n\u003cstrong\u003eWash Care:\u003c\/strong\u003e Wash separately in cold water. Do Not Bleach and dry in shade.\u003c\/p\u003e"},
              onVariantSelected: selectCallback,
              enableHistoryState: true
          });

        }

        Shopify.linkOptionSelectors({"id":7797834973406,"title":"Mulmul Cotton Lilac V Neck Kurta","handle":"mulmul-cotton-lilac-v-neck-kurta","description":"\u003cp\u003eThis premium mul mul lilac kurta will be best pick this season for your yoga sessions. This kurta featuring three quarter balloon sleeves felicitates ease of movement and utmost comfort. Team it up with matching pants and you are good to go.\u003cbr\u003e\n\n\u003cstrong\u003eWash Care:\u003c\/strong\u003e Wash separately in cold water. Do Not Bleach and dry in shade.\u003c\/p\u003e","published_at":"2022-09-15T05:44:04+05:30","created_at":"2022-09-15T05:44:04+05:30","vendor":"Tjoritreasures","type":"","tags":["All Catalog Bestseller Sale","Apparel","Bestsellers | Apparel","Colour_Purple","EOSS - Buy 1 Get 1 - Women's Apparel","EOSS - Upto 90% Off","Gender_Women","Kurtas \u0026 Slips","Material_Pure Cotton Mulmul","Model Is Wearing_Size: S","Model's Height_5 Feet 8 Inches","New Arrivals | Apparel","Occasion_Daily","Occasion_Festive","Women Apparel - FLAT ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½1234","Women | All Products","Women | Bestsellers","Yogini"],"price":173800,"price_min":173800,"price_max":173800,"available":true,"price_varies":false,"compare_at_price":248300,"compare_at_price_min":248300,"compare_at_price_max":248300,"compare_at_price_varies":false,"variants":[{"id":43399271874782,"title":"XS","option1":"XS","option2":null,"option3":null,"sku":"TJ-MK-277-02-XS","requires_shipping":true,"taxable":true,"featured_image":null,"available":true,"name":"Mulmul Cotton Lilac V Neck Kurta - XS","public_title":"XS","options":["XS"],"price":173800,"weight":0,"compare_at_price":248300,"inventory_management":"shopify","barcode":null,"quantity_rule":{"min":1,"max":null,"increment":1}},{"id":43399271907550,"title":"S","option1":"S","option2":null,"option3":null,"sku":"TJ-MK-277-02-S","requires_shipping":true,"taxable":true,"featured_image":null,"available":true,"name":"Mulmul Cotton Lilac V Neck Kurta - S","public_title":"S","options":["S"],"price":173800,"weight":0,"compare_at_price":248300,"inventory_management":"shopify","barcode":null,"quantity_rule":{"min":1,"max":null,"increment":1}},{"id":43399271940318,"title":"M","option1":"M","option2":null,"option3":null,"sku":"TJ-MK-277-02-M","requires_shipping":true,"taxable":true,"featured_image":null,"available":true,"name":"Mulmul Cotton Lilac V Neck Kurta - M","public_title":"M","options":["M"],"price":173800,"weight":0,"compare_at_price":248300,"inventory_management":"shopify","barcode":null,"quantity_rule":{"min":1,"max":null,"increment":1}},{"id":43399271973086,"title":"L","option1":"L","option2":null,"option3":null,"sku":"TJ-MK-277-02-L","requires_shipping":true,"taxable":true,"featured_image":null,"available":true,"name":"Mulmul Cotton Lilac V Neck Kurta - L","public_title":"L","options":["L"],"price":173800,"weight":0,"compare_at_price":248300,"inventory_management":"shopify","barcode":null,"quantity_rule":{"min":1,"max":null,"increment":1}},{"id":43399272005854,"title":"XL","option1":"XL","option2":null,"option3":null,"sku":"TJ-MK-277-02-XL","requires_shipping":true,"taxable":true,"featured_image":null,"available":true,"name":"Mulmul Cotton Lilac V Neck Kurta - XL","public_title":"XL","options":["XL"],"price":173800,"weight":0,"compare_at_price":248300,"inventory_management":"shopify","barcode":null,"quantity_rule":{"min":1,"max":null,"increment":1}},{"id":43399272038622,"title":"XXL","option1":"XXL","option2":null,"option3":null,"sku":"TJ-MK-277-02-XXL","requires_shipping":true,"taxable":true,"featured_image":null,"available":true,"name":"Mulmul Cotton Lilac V Neck Kurta - XXL","public_title":"XXL","options":["XXL"],"price":173800,"weight":0,"compare_at_price":248300,"inventory_management":"shopify","barcode":null,"quantity_rule":{"min":1,"max":null,"increment":1}},{"id":43399272071390,"title":"XXXL","option1":"XXXL","option2":null,"option3":null,"sku":"TJ-MK-277-02-XXXL","requires_shipping":true,"taxable":true,"featured_image":null,"available":true,"name":"Mulmul Cotton Lilac V Neck Kurta - XXXL","public_title":"XXXL","options":["XXXL"],"price":173800,"weight":0,"compare_at_price":248300,"inventory_management":"shopify","barcode":null,"quantity_rule":{"min":1,"max":null,"increment":1}}],"images":["\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/TJ-MK-277-02_1.jpg?v=1663266908","\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/TJ-MK-277-02_2.jpg?v=1663266908","\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/TJ-MK-277-02_3.jpg?v=1663266908","\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/TJ-MK-277-02_4.jpg?v=1663266908","\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/TJ-MK-277-02_5.jpg?v=1663266908","\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/TJ-MK-277-02_6.jpg?v=1663266908"],"featured_image":"\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/TJ-MK-277-02_1.jpg?v=1663266908","options":["Size"],"media":[{"alt":null,"id":30634492461278,"position":1,"preview_image":{"aspect_ratio":0.678,"height":2360,"width":1600,"src":"https:\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/TJ-MK-277-02_1.jpg?v=1663266908"},"aspect_ratio":0.678,"height":2360,"media_type":"image","src":"https:\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/TJ-MK-277-02_1.jpg?v=1663266908","width":1600},{"alt":null,"id":30634492494046,"position":2,"preview_image":{"aspect_ratio":0.678,"height":2360,"width":1600,"src":"https:\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/TJ-MK-277-02_2.jpg?v=1663266908"},"aspect_ratio":0.678,"height":2360,"media_type":"image","src":"https:\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/TJ-MK-277-02_2.jpg?v=1663266908","width":1600},{"alt":null,"id":30634492526814,"position":3,"preview_image":{"aspect_ratio":0.678,"height":2360,"width":1600,"src":"https:\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/TJ-MK-277-02_3.jpg?v=1663266908"},"aspect_ratio":0.678,"height":2360,"media_type":"image","src":"https:\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/TJ-MK-277-02_3.jpg?v=1663266908","width":1600},{"alt":null,"id":30634492559582,"position":4,"preview_image":{"aspect_ratio":0.678,"height":2360,"width":1600,"src":"https:\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/TJ-MK-277-02_4.jpg?v=1663266908"},"aspect_ratio":0.678,"height":2360,"media_type":"image","src":"https:\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/TJ-MK-277-02_4.jpg?v=1663266908","width":1600},{"alt":null,"id":30634492592350,"position":5,"preview_image":{"aspect_ratio":0.678,"height":2360,"width":1600,"src":"https:\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/TJ-MK-277-02_5.jpg?v=1663266908"},"aspect_ratio":0.678,"height":2360,"media_type":"image","src":"https:\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/TJ-MK-277-02_5.jpg?v=1663266908","width":1600},{"alt":null,"id":30634492625118,"position":6,"preview_image":{"aspect_ratio":0.678,"height":2360,"width":1600,"src":"https:\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/TJ-MK-277-02_6.jpg?v=1663266908"},"aspect_ratio":0.678,"height":2360,"media_type":"image","src":"https:\/\/cdn.shopify.com\/s\/files\/1\/0613\/2039\/7022\/products\/TJ-MK-277-02_6.jpg?v=1663266908","width":1600}],"content":"\u003cp\u003eThis premium mul mul lilac kurta will be best pick this season for your yoga sessions. This kurta featuring three quarter balloon sleeves felicitates ease of movement and utmost comfort. Team it up with matching pants and you are good to go.\u003cbr\u003e\n\n\u003cstrong\u003eWash Care:\u003c\/strong\u003e Wash separately in cold water. Do Not Bleach and dry in shade.\u003c\/p\u003e"}, '.product');

        
          $('.selector-wrapper:eq(0)').prepend('<label>Size</label>');
        

        $('.product-shop .selector-wrapper label').append('<em>*</em>');

        

        

        // Add Data Countdown Sticky
        
          var countdown_different = $('.product-shop .countdown-item').data('countdown-value');
          $('[data-sticky-add-to-cart] .countdown-item').attr('data-countdown-value',countdown_different);
        

    });

    

    
    
</script>
<script>
  $(document).ready(function(){
    $('.model-viewer-click').click(function(){
      $(this).parent().find('.shopify-model-viewer-ui__button').hide();
    })
  })
</script>










</div><div id="shopify-section-template--16520034844894__recently-view" class="shopify-section"> 
<script src="//ajax.aspnetcdn.com/ajax/jquery.templates/beta1/jquery.tmpl.min.js" type="text/javascript"></script>
<script src="//cdn.shopify.com/s/files/1/0613/2039/7022/t/8/assets/jquery.products.min.js?v=70823434569899201341664368305" type="text/javascript"></script>
<script src="//cdn.shopify.com/s/files/1/0613/2039/7022/t/8/assets/slick.min.js?v=180406246729472946341664368305" type="text/javascript"></script>
<div class="container">
    <section class="recently-viewed-products" style="">
        
        <div class="widget-title not-before">   
          <h3 class="box-title">
            <span class="title"> 
              
<span>Recently Viewed</span>

            </span>
          </h3>
        </div>
        
<div class="widget-product">
          <div class="products-grid row slick-initialized slick-slider slick-dotted" data-rows="4" id="recently-viewed-products-grid" role="toolbar"><div aria-live="polite" class="slick-list draggable"><div class="slick-track" role="listbox" style="opacity: 1; width: 2076px; transform: translate3d(-346px, 0px, 0px);"><div class="grid-item col-6 col-md-4 col-lg-3 slick-slide slick-cloned" style="width: 173px;" tabindex="-1" role="option" aria-describedby="slick-slide36" data-slick-index="-2" aria-hidden="true">     <div class="inner product-item on-sale" data-product-id="product-7907884695774">       <div class="inner-top">       <div class="product-top">               <div class="product-image image-swap">                   <a href="/products/sand-gold-chanderi-brocade-dupatta" class="product-grid-image" data-collections-related="/collections/?view=related" tabindex="-1">                                                                                          <picture data-index="0">                         <img src="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-305-119_1.jpg?v=1671451508" class="images-one" alt="" data-index="0">                       </picture>                       <span class="images-two">                                                                                                        <picture data-index="1">                             <img src="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-305-119_2.jpg?v=1671451508" alt="" data-index="1">                           </picture>                                                                                                    </span>                                                                                      </a>                 </div>                                  <div class="product-label">                                                                                                  <strong class="label sale-label">                       <span>                           Sale                       </span>                     </strong>                                                               <br>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      </div>                                                                                               <a class="wishlist" data-icon-wishlist="" href="#" data-product-handle="sand-gold-chanderi-brocade-dupatta" data-id="7907884695774" tabindex="-1">                                                      <svg viewBox="0 0 512 512"> <g> <g>   <path d="M474.644,74.27C449.391,45.616,414.358,29.836,376,29.836c-53.948,0-88.103,32.22-107.255,59.25     c-4.969,7.014-9.196,14.047-12.745,20.665c-3.549-6.618-7.775-13.651-12.745-20.665c-19.152-27.03-53.307-59.25-107.255-59.25     c-38.358,0-73.391,15.781-98.645,44.435C13.267,101.605,0,138.213,0,177.351c0,42.603,16.633,82.228,52.345,124.7     c31.917,37.96,77.834,77.088,131.005,122.397c19.813,16.884,40.302,34.344,62.115,53.429l0.655,0.574     c2.828,2.476,6.354,3.713,9.88,3.713s7.052-1.238,9.88-3.713l0.655-0.574c21.813-19.085,42.302-36.544,62.118-53.431     c53.168-45.306,99.085-84.434,131.002-122.395C495.367,259.578,512,219.954,512,177.351     C512,138.213,498.733,101.605,474.644,74.27z M309.193,401.614c-17.08,14.554-34.658,29.533-53.193,45.646     c-18.534-16.111-36.113-31.091-53.196-45.648C98.745,312.939,30,254.358,30,177.351c0-31.83,10.605-61.394,29.862-83.245     C79.34,72.007,106.379,59.836,136,59.836c41.129,0,67.716,25.338,82.776,46.594c13.509,19.064,20.558,38.282,22.962,45.659     c2.011,6.175,7.768,10.354,14.262,10.354c6.494,0,12.251-4.179,14.262-10.354c2.404-7.377,9.453-26.595,22.962-45.66     c15.06-21.255,41.647-46.593,82.776-46.593c29.621,0,56.66,12.171,76.137,34.27C471.395,115.957,482,145.521,482,177.351     C482,254.358,413.255,312.939,309.193,401.614z"></path> </g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> </svg>                           <span class="wishlist-text">                               Add to wishlist                           </span>                                                    </a>                                                                                              <a class="quickview-button" href="javascript:void(0)" id="" tabindex="-1">                                                 <svg viewBox="0 0 511.999 511.999" width="100%" height="100%">     <path d="M508.745,246.041c-4.574-6.257-113.557-153.206-252.748-153.206S7.818,239.784,3.249,246.035         c-4.332,5.936-4.332,13.987,0,19.923c4.569,6.257,113.557,153.206,252.748,153.206s248.174-146.95,252.748-153.201         C513.083,260.028,513.083,251.971,508.745,246.041z M255.997,385.406c-102.529,0-191.33-97.533-217.617-129.418         c26.253-31.913,114.868-129.395,217.617-129.395c102.524,0,191.319,97.516,217.617,129.418         C447.361,287.923,358.746,385.406,255.997,385.406z"></path>       <path d="M255.997,154.725c-55.842,0-101.275,45.433-101.275,101.275s45.433,101.275,101.275,101.275         s101.275-45.433,101.275-101.275S311.839,154.725,255.997,154.725z M255.997,323.516c-37.23,0-67.516-30.287-67.516-67.516         s30.287-67.516,67.516-67.516s67.516,30.287,67.516,67.516S293.227,323.516,255.997,323.516z"></path> </svg>                                                   <span>                               Quick View                             </span>                                             </a>                                                                                 <div class="product-des abs-center">                                                                                    <div class="action">                         <form action="/cart/add" method="post" class="variants" id="" data-id="product-actions-7907884695774" enctype="multipart/form-data">                                                            <input type="hidden" name="id" value="43779002532062" tabindex="-1">                                                                                                                                                                       <button data-btn-addtocart="" class="btn add-to-cart-btn" type="submit" data-form-id="#recently-7907884695774" tabindex="-1">                                       Add to Cart                                     </button>                                                                                                                                                                                      </form>                                                                                                </div>                                                                                                    </div>         </div>                          <div class="product-bottom">               <div class="wrapper-vendor">                                                                                         <span class="spr-badge" id="" data-rating="0.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star-empty" aria-hidden="true"></i><i class="spr-icon spr-icon-star-empty" aria-hidden="true"></i><i class="spr-icon spr-icon-star-empty" aria-hidden="true"></i><i class="spr-icon spr-icon-star-empty" aria-hidden="true"></i><i class="spr-icon spr-icon-star-empty" aria-hidden="true"></i></span><span class="spr-badge-caption">No reviews</span>
</span>
                                                                                   </div>                                                              <a class="product-title change-text" href="/products/sand-gold-chanderi-brocade-dupatta" tabindex="-1">                 Sand Gold Chanderi Brocade Dupatta                                                                                       </a>                                                               <div class="price-box">                                         <div class="price-sale">                                                <span class="special-price" data-price-grid=""><span class="money">₹3,044.00</span></span>                         <span class="old-price" data-compare-price-grid=""><span class="money">₹4,348.00</span></span>                                            </div>                                  </div>               <div class="wrapper-size">                                                                                  </div>                        </div>                               </div>     </div>   </div><div class="grid-item col-6 col-md-4 col-lg-3 slick-slide slick-cloned" style="width: 173px;" tabindex="-1" role="option" aria-describedby="slick-slide37" data-slick-index="-1" aria-hidden="true">     <div class="inner product-item on-sale" data-product-id="product-7797640134878">       <div class="inner-top">       <div class="product-top">               <div class="product-image image-swap">                   <a href="/products/kalamkari-block-printed-cotton-slip" class="product-grid-image" data-collections-related="/collections/?view=related" tabindex="-1">                                                                                          <picture data-index="0">                         <img src="//cdn.shopify.com/s/files/1/0613/2039/7022/products/278376-2019-07-22-11_26_37.291355-big-image.jpg?v=1663261675" class="images-one" alt="" data-index="0">                       </picture>                       <span class="images-two">                                                                                                        <picture data-index="1">                             <img src="//cdn.shopify.com/s/files/1/0613/2039/7022/products/278377-2019-07-22-11_26_37.610582-big-image.jpg?v=1663261675" alt="" data-index="1">                           </picture>                                                                                                    </span>                                                                                      </a>                 </div>                                  <div class="product-label">                                                                                                  <strong class="label sale-label">                       <span>                           Sale                       </span>                     </strong>                                                               <br>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          </div>                                                                                               <a class="wishlist" data-icon-wishlist="" href="#" data-product-handle="kalamkari-block-printed-cotton-slip" data-id="7797640134878" tabindex="-1">                                                      <svg viewBox="0 0 512 512"> <g> <g>   <path d="M474.644,74.27C449.391,45.616,414.358,29.836,376,29.836c-53.948,0-88.103,32.22-107.255,59.25     c-4.969,7.014-9.196,14.047-12.745,20.665c-3.549-6.618-7.775-13.651-12.745-20.665c-19.152-27.03-53.307-59.25-107.255-59.25     c-38.358,0-73.391,15.781-98.645,44.435C13.267,101.605,0,138.213,0,177.351c0,42.603,16.633,82.228,52.345,124.7     c31.917,37.96,77.834,77.088,131.005,122.397c19.813,16.884,40.302,34.344,62.115,53.429l0.655,0.574     c2.828,2.476,6.354,3.713,9.88,3.713s7.052-1.238,9.88-3.713l0.655-0.574c21.813-19.085,42.302-36.544,62.118-53.431     c53.168-45.306,99.085-84.434,131.002-122.395C495.367,259.578,512,219.954,512,177.351     C512,138.213,498.733,101.605,474.644,74.27z M309.193,401.614c-17.08,14.554-34.658,29.533-53.193,45.646     c-18.534-16.111-36.113-31.091-53.196-45.648C98.745,312.939,30,254.358,30,177.351c0-31.83,10.605-61.394,29.862-83.245     C79.34,72.007,106.379,59.836,136,59.836c41.129,0,67.716,25.338,82.776,46.594c13.509,19.064,20.558,38.282,22.962,45.659     c2.011,6.175,7.768,10.354,14.262,10.354c6.494,0,12.251-4.179,14.262-10.354c2.404-7.377,9.453-26.595,22.962-45.66     c15.06-21.255,41.647-46.593,82.776-46.593c29.621,0,56.66,12.171,76.137,34.27C471.395,115.957,482,145.521,482,177.351     C482,254.358,413.255,312.939,309.193,401.614z"></path> </g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> </svg>                           <span class="wishlist-text">                               Add to wishlist                           </span>                                                    </a>                                                                                              <a class="quickview-button" href="javascript:void(0)" id="" tabindex="-1">                                                 <svg viewBox="0 0 511.999 511.999" width="100%" height="100%">     <path d="M508.745,246.041c-4.574-6.257-113.557-153.206-252.748-153.206S7.818,239.784,3.249,246.035         c-4.332,5.936-4.332,13.987,0,19.923c4.569,6.257,113.557,153.206,252.748,153.206s248.174-146.95,252.748-153.201         C513.083,260.028,513.083,251.971,508.745,246.041z M255.997,385.406c-102.529,0-191.33-97.533-217.617-129.418         c26.253-31.913,114.868-129.395,217.617-129.395c102.524,0,191.319,97.516,217.617,129.418         C447.361,287.923,358.746,385.406,255.997,385.406z"></path>       <path d="M255.997,154.725c-55.842,0-101.275,45.433-101.275,101.275s45.433,101.275,101.275,101.275         s101.275-45.433,101.275-101.275S311.839,154.725,255.997,154.725z M255.997,323.516c-37.23,0-67.516-30.287-67.516-67.516         s30.287-67.516,67.516-67.516s67.516,30.287,67.516,67.516S293.227,323.516,255.997,323.516z"></path> </svg>                                                   <span>                               Quick View                             </span>                                             </a>                                                                                 <div class="product-des abs-center">                                                                                    <div class="action">                         <form action="/cart/add" method="post" class="variants" id="" data-id="product-actions-7797640134878" enctype="multipart/form-data">                                                                                                                                                                                                                                             <a class="btn" data-href="javascript:void(0)" title="Kalamkari  Cotton Slip" data-init-quickshop="" tabindex="-1">                                       Add to Cart                                     </a>                                                                                                                                                                                                </form>                                                                             <div class="product-card__variant--popup">                               <div class="product-card__variant--popup--content">                                 <form action="/cart/add" method="post" class="variants" id="" data-id="product-actions-7797640134878" enctype="multipart/form-data">                                                                                                                                                                                                                                                                                                                                                                                                                                      <div class="selector-wrapper selector-wrapper-1 swatch js product-form__item" data-option-index="0">                                                   <label class="form-label">                                                       Size:                                                        <span class="label-value-1"></span>                                                   </label>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           <div class="swatch-element size xs available" data-value="XS">                                                               <input class="single-option-selector single-option-selector-quick single-option-selector-recently-view" type="radio" name="SingleOptionSelector-1-7797640134878-recently-view" data-index="option1" value="XS" id="" tabindex="-1">                                                               <label data-toggle="tooltip" data-placement="top" title="XS" for="SingleOptionSelector1-XS-7797640134878-recently-view">XS</label>                                                           </div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               <div class="swatch-element size s available" data-value="S">                                                               <input class="single-option-selector single-option-selector-quick single-option-selector-recently-view" type="radio" name="SingleOptionSelector-1-7797640134878-recently-view" data-index="option1" value="S" id="" tabindex="-1">                                                               <label data-toggle="tooltip" data-placement="top" title="S" for="SingleOptionSelector1-S-7797640134878-recently-view">S</label>                                                           </div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               <div class="swatch-element size m available" data-value="M">                                                               <input class="single-option-selector single-option-selector-quick single-option-selector-recently-view" type="radio" name="SingleOptionSelector-1-7797640134878-recently-view" data-index="option1" value="M" id="" tabindex="-1">                                                               <label data-toggle="tooltip" data-placement="top" title="M" for="SingleOptionSelector1-M-7797640134878-recently-view">M</label>                                                           </div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               <div class="swatch-element size l available" data-value="L">                                                               <input class="single-option-selector single-option-selector-quick single-option-selector-recently-view" type="radio" name="SingleOptionSelector-1-7797640134878-recently-view" data-index="option1" value="L" id="" tabindex="-1">                                                               <label data-toggle="tooltip" data-placement="top" title="L" for="SingleOptionSelector1-L-7797640134878-recently-view">L</label>                                                           </div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               <div class="swatch-element size xl available" data-value="XL">                                                               <input class="single-option-selector single-option-selector-quick single-option-selector-recently-view" type="radio" name="SingleOptionSelector-1-7797640134878-recently-view" data-index="option1" value="XL" id="" tabindex="-1">                                                               <label data-toggle="tooltip" data-placement="top" title="XL" for="SingleOptionSelector1-XL-7797640134878-recently-view">XL</label>                                                           </div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               <div class="swatch-element size xxl available" data-value="XXL">                                                               <input class="single-option-selector single-option-selector-quick single-option-selector-recently-view" type="radio" name="SingleOptionSelector-1-7797640134878-recently-view" data-index="option1" value="XXL" id="" tabindex="-1">                                                               <label data-toggle="tooltip" data-placement="top" title="XXL" for="SingleOptionSelector1-XXL-7797640134878-recently-view">XXL</label>                                                           </div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               <div class="swatch-element size xxxl available" data-value="XXXL">                                                               <input class="single-option-selector single-option-selector-quick single-option-selector-recently-view" type="radio" name="SingleOptionSelector-1-7797640134878-recently-view" data-index="option1" value="XXXL" id="" tabindex="-1">                                                               <label data-toggle="tooltip" data-placement="top" title="XXXL" for="SingleOptionSelector1-XXXL-7797640134878-recently-view">XXXL</label>                                                           </div>                                                                                                                                                         </div>                                                                                                                                                        <select name="id" id="" class="product-form__variants no-js" tabindex="-1">                                                                                                                                 <option value="43398576046302">                                                   XS                                               </option>                                                                                                                                                                            <option value="43398576079070">                                                   S                                               </option>                                                                                                                                                                            <option value="43398576111838">                                                   M                                               </option>                                                                                                                                                                            <option value="43398576144606">                                                   L                                               </option>                                                                                                                                                                            <option value="43398576177374">                                                   XL                                               </option>                                                                                                                                                                            <option value="43398576210142">                                                   XXL                                               </option>                                                                                                                                                                            <option value="43398576242910">                                                   XXXL                                               </option>                                                                                                                     </select>                                   <div class="product-card__button_cancel_mobile">                                       <a class="btn btn-cancel" data-cancel-swatch-qs="" tabindex="-1"><svg aria-hidden="true" data-prefix="fal" data-icon="times" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" class="svg-inline--fa fa-times fa-w-10 fa-2x"><path fill="currentColor" d="M193.94 256L296.5 153.44l21.15-21.15c3.12-3.12 3.12-8.19 0-11.31l-22.63-22.63c-3.12-3.12-8.19-3.12-11.31 0L160 222.06 36.29 98.34c-3.12-3.12-8.19-3.12-11.31 0L2.34 120.97c-3.12 3.12-3.12 8.19 0 11.31L126.06 256 2.34 379.71c-3.12 3.12-3.12 8.19 0 11.31l22.63 22.63c3.12 3.12 8.19 3.12 11.31 0L160 289.94 262.56 392.5l21.15 21.15c3.12 3.12 8.19 3.12 11.31 0l22.63-22.63c3.12-3.12 3.12-8.19 0-11.31L193.94 256z" class=""></path></svg></a>                                   </div>                                   <div class="product-card__button2">                                       <input type="hidden" name="quantity" value="1" tabindex="-1">                                       <button data-btn-addtocart="" class="btn add-to-cart-btn" type="submit" data-form-id="#recently-7797640134878-qs" tabindex="-1">                                           Submit                                       </button>                                   </div>                                 </form>                               </div>                           </div>                                                                       </div>                                                                                                    </div>         </div>                          <div class="product-bottom">               <div class="wrapper-vendor">                                                                                         <span class="spr-badge" id="" data-rating="4.833333333333333"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star" aria-hidden="true"></i><i class="spr-icon spr-icon-star" aria-hidden="true"></i><i class="spr-icon spr-icon-star" aria-hidden="true"></i><i class="spr-icon spr-icon-star" aria-hidden="true"></i><i class="spr-icon spr-icon-star" aria-hidden="true"></i></span><span class="spr-badge-caption">6 reviews</span>
</span>
                                                                                   </div>                                                              <a class="product-title change-text" href="/products/kalamkari-block-printed-cotton-slip" tabindex="-1">                 Kalamkari  Cotton Slip                                                                                       </a>                                                               <div class="price-box">                                         <div class="price-sale">                                                <span class="special-price" data-price-grid=""><span class="money">₹1,912.00</span></span>                         <span class="old-price" data-compare-price-grid=""><span class="money">₹2,732.00</span></span>                                            </div>                                  </div>               <div class="wrapper-size">                                                                                  </div>                        </div>                               </div>     </div>   </div><div class="grid-item col-6 col-md-4 col-lg-3 slick-slide slick-current slick-active" style="width: 173px;" tabindex="-1" role="option" aria-describedby="slick-slide30" data-slick-index="0" aria-hidden="false">     <div class="inner product-item on-sale" data-product-id="product-7798045671646" data-json-product="{&quot;id&quot;:7798045671646,&quot;handle&quot;:&quot;maroon-kota-tassel-embellished-saree&quot;,&quot;media&quot;:[{&quot;alt&quot;:null,&quot;id&quot;:30634935353566,&quot;position&quot;:1,&quot;preview_image&quot;:{&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:2360,&quot;width&quot;:1600,&quot;src&quot;:&quot;https://cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-298-55_1.jpg?v=1663268065&quot;},&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:2360,&quot;media_type&quot;:&quot;image&quot;,&quot;src&quot;:&quot;https://cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-298-55_1.jpg?v=1663268065&quot;,&quot;width&quot;:1600},{&quot;alt&quot;:null,&quot;id&quot;:30634935386334,&quot;position&quot;:2,&quot;preview_image&quot;:{&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:2360,&quot;width&quot;:1600,&quot;src&quot;:&quot;https://cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-298-55_2.jpg?v=1663268065&quot;},&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:2360,&quot;media_type&quot;:&quot;image&quot;,&quot;src&quot;:&quot;https://cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-298-55_2.jpg?v=1663268065&quot;,&quot;width&quot;:1600},{&quot;alt&quot;:null,&quot;id&quot;:30634935419102,&quot;position&quot;:3,&quot;preview_image&quot;:{&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:2360,&quot;width&quot;:1600,&quot;src&quot;:&quot;https://cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-298-55_3.jpg?v=1663268065&quot;},&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:2360,&quot;media_type&quot;:&quot;image&quot;,&quot;src&quot;:&quot;https://cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-298-55_3.jpg?v=1663268065&quot;,&quot;width&quot;:1600},{&quot;alt&quot;:null,&quot;id&quot;:30634935451870,&quot;position&quot;:4,&quot;preview_image&quot;:{&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:2360,&quot;width&quot;:1600,&quot;src&quot;:&quot;https://cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-298-55_4.jpg?v=1663268065&quot;},&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:2360,&quot;media_type&quot;:&quot;image&quot;,&quot;src&quot;:&quot;https://cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-298-55_4.jpg?v=1663268065&quot;,&quot;width&quot;:1600}],&quot;variants&quot;:[{&quot;id&quot;:43400227389662,&quot;title&quot;:&quot;XS&quot;,&quot;option1&quot;:&quot;XS&quot;,&quot;option2&quot;:null,&quot;option3&quot;:null,&quot;sku&quot;:&quot;TJ-MK-298-55-XS&quot;,&quot;requires_shipping&quot;:true,&quot;taxable&quot;:true,&quot;featured_image&quot;:null,&quot;available&quot;:true,&quot;name&quot;:&quot;Maroon kota tassel embellished saree - XS&quot;,&quot;public_title&quot;:&quot;XS&quot;,&quot;options&quot;:[&quot;XS&quot;],&quot;price&quot;:188400,&quot;weight&quot;:0,&quot;compare_at_price&quot;:269100,&quot;inventory_management&quot;:&quot;shopify&quot;,&quot;barcode&quot;:null,&quot;quantity_rule&quot;:{&quot;min&quot;:1,&quot;max&quot;:null,&quot;increment&quot;:1}},{&quot;id&quot;:43400227422430,&quot;title&quot;:&quot;S&quot;,&quot;option1&quot;:&quot;S&quot;,&quot;option2&quot;:null,&quot;option3&quot;:null,&quot;sku&quot;:&quot;TJ-MK-298-55-S&quot;,&quot;requires_shipping&quot;:true,&quot;taxable&quot;:true,&quot;featured_image&quot;:null,&quot;available&quot;:true,&quot;name&quot;:&quot;Maroon kota tassel embellished saree - S&quot;,&quot;public_title&quot;:&quot;S&quot;,&quot;options&quot;:[&quot;S&quot;],&quot;price&quot;:188400,&quot;weight&quot;:0,&quot;compare_at_price&quot;:269100,&quot;inventory_management&quot;:&quot;shopify&quot;,&quot;barcode&quot;:null,&quot;quantity_rule&quot;:{&quot;min&quot;:1,&quot;max&quot;:null,&quot;increment&quot;:1}},{&quot;id&quot;:43400227455198,&quot;title&quot;:&quot;M&quot;,&quot;option1&quot;:&quot;M&quot;,&quot;option2&quot;:null,&quot;option3&quot;:null,&quot;sku&quot;:&quot;TJ-MK-298-55-M&quot;,&quot;requires_shipping&quot;:true,&quot;taxable&quot;:true,&quot;featured_image&quot;:null,&quot;available&quot;:true,&quot;name&quot;:&quot;Maroon kota tassel embellished saree - M&quot;,&quot;public_title&quot;:&quot;M&quot;,&quot;options&quot;:[&quot;M&quot;],&quot;price&quot;:188400,&quot;weight&quot;:0,&quot;compare_at_price&quot;:269100,&quot;inventory_management&quot;:&quot;shopify&quot;,&quot;barcode&quot;:null,&quot;quantity_rule&quot;:{&quot;min&quot;:1,&quot;max&quot;:null,&quot;increment&quot;:1}},{&quot;id&quot;:43400227487966,&quot;title&quot;:&quot;L&quot;,&quot;option1&quot;:&quot;L&quot;,&quot;option2&quot;:null,&quot;option3&quot;:null,&quot;sku&quot;:&quot;TJ-MK-298-55-L&quot;,&quot;requires_shipping&quot;:true,&quot;taxable&quot;:true,&quot;featured_image&quot;:null,&quot;available&quot;:true,&quot;name&quot;:&quot;Maroon kota tassel embellished saree - L&quot;,&quot;public_title&quot;:&quot;L&quot;,&quot;options&quot;:[&quot;L&quot;],&quot;price&quot;:188400,&quot;weight&quot;:0,&quot;compare_at_price&quot;:269100,&quot;inventory_management&quot;:&quot;shopify&quot;,&quot;barcode&quot;:null,&quot;quantity_rule&quot;:{&quot;min&quot;:1,&quot;max&quot;:null,&quot;increment&quot;:1}},{&quot;id&quot;:43400227520734,&quot;title&quot;:&quot;XL&quot;,&quot;option1&quot;:&quot;XL&quot;,&quot;option2&quot;:null,&quot;option3&quot;:null,&quot;sku&quot;:&quot;TJ-MK-298-55-XL&quot;,&quot;requires_shipping&quot;:true,&quot;taxable&quot;:true,&quot;featured_image&quot;:null,&quot;available&quot;:true,&quot;name&quot;:&quot;Maroon kota tassel embellished saree - XL&quot;,&quot;public_title&quot;:&quot;XL&quot;,&quot;options&quot;:[&quot;XL&quot;],&quot;price&quot;:188400,&quot;weight&quot;:0,&quot;compare_at_price&quot;:269100,&quot;inventory_management&quot;:&quot;shopify&quot;,&quot;barcode&quot;:null,&quot;quantity_rule&quot;:{&quot;min&quot;:1,&quot;max&quot;:null,&quot;increment&quot;:1}},{&quot;id&quot;:43400227553502,&quot;title&quot;:&quot;XXL&quot;,&quot;option1&quot;:&quot;XXL&quot;,&quot;option2&quot;:null,&quot;option3&quot;:null,&quot;sku&quot;:&quot;TJ-MK-298-55-XXL&quot;,&quot;requires_shipping&quot;:true,&quot;taxable&quot;:true,&quot;featured_image&quot;:null,&quot;available&quot;:true,&quot;name&quot;:&quot;Maroon kota tassel embellished saree - XXL&quot;,&quot;public_title&quot;:&quot;XXL&quot;,&quot;options&quot;:[&quot;XXL&quot;],&quot;price&quot;:188400,&quot;weight&quot;:0,&quot;compare_at_price&quot;:269100,&quot;inventory_management&quot;:&quot;shopify&quot;,&quot;barcode&quot;:null,&quot;quantity_rule&quot;:{&quot;min&quot;:1,&quot;max&quot;:null,&quot;increment&quot;:1}},{&quot;id&quot;:43400227586270,&quot;title&quot;:&quot;XXXL&quot;,&quot;option1&quot;:&quot;XXXL&quot;,&quot;option2&quot;:null,&quot;option3&quot;:null,&quot;sku&quot;:&quot;TJ-MK-298-55-XXXL&quot;,&quot;requires_shipping&quot;:true,&quot;taxable&quot;:true,&quot;featured_image&quot;:null,&quot;available&quot;:true,&quot;name&quot;:&quot;Maroon kota tassel embellished saree - XXXL&quot;,&quot;public_title&quot;:&quot;XXXL&quot;,&quot;options&quot;:[&quot;XXXL&quot;],&quot;price&quot;:188400,&quot;weight&quot;:0,&quot;compare_at_price&quot;:269100,&quot;inventory_management&quot;:&quot;shopify&quot;,&quot;barcode&quot;:null,&quot;quantity_rule&quot;:{&quot;min&quot;:1,&quot;max&quot;:null,&quot;increment&quot;:1}}]}">       <div class="inner-top">       <div class="product-top">               <div class="product-image image-swap">                   <a href="/products/maroon-kota-tassel-embellished-saree" class="product-grid-image" data-collections-related="/collections/?view=related" tabindex="0">                                                                                          <picture data-index="0">                         <img src="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-298-55_1.jpg?v=1663268065" class="images-one" alt="" data-index="0">                       </picture>                       <span class="images-two">                                                                                                        <picture data-index="1">                             <img src="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-298-55_2.jpg?v=1663268065" alt="" data-index="1">                           </picture>                                                                                                    </span>                                                                                      </a>                 </div>                                  <div class="product-label">                                                                                                  <strong class="label sale-label">                       <span>                           Sale                       </span>                     </strong>                                                               <br>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          </div>                                                                                               <a class="wishlist" data-icon-wishlist="" href="#" data-product-handle="maroon-kota-tassel-embellished-saree" data-id="7798045671646" tabindex="0">                                                      <svg viewBox="0 0 512 512"> <g> <g>   <path d="M474.644,74.27C449.391,45.616,414.358,29.836,376,29.836c-53.948,0-88.103,32.22-107.255,59.25     c-4.969,7.014-9.196,14.047-12.745,20.665c-3.549-6.618-7.775-13.651-12.745-20.665c-19.152-27.03-53.307-59.25-107.255-59.25     c-38.358,0-73.391,15.781-98.645,44.435C13.267,101.605,0,138.213,0,177.351c0,42.603,16.633,82.228,52.345,124.7     c31.917,37.96,77.834,77.088,131.005,122.397c19.813,16.884,40.302,34.344,62.115,53.429l0.655,0.574     c2.828,2.476,6.354,3.713,9.88,3.713s7.052-1.238,9.88-3.713l0.655-0.574c21.813-19.085,42.302-36.544,62.118-53.431     c53.168-45.306,99.085-84.434,131.002-122.395C495.367,259.578,512,219.954,512,177.351     C512,138.213,498.733,101.605,474.644,74.27z M309.193,401.614c-17.08,14.554-34.658,29.533-53.193,45.646     c-18.534-16.111-36.113-31.091-53.196-45.648C98.745,312.939,30,254.358,30,177.351c0-31.83,10.605-61.394,29.862-83.245     C79.34,72.007,106.379,59.836,136,59.836c41.129,0,67.716,25.338,82.776,46.594c13.509,19.064,20.558,38.282,22.962,45.659     c2.011,6.175,7.768,10.354,14.262,10.354c6.494,0,12.251-4.179,14.262-10.354c2.404-7.377,9.453-26.595,22.962-45.66     c15.06-21.255,41.647-46.593,82.776-46.593c29.621,0,56.66,12.171,76.137,34.27C471.395,115.957,482,145.521,482,177.351     C482,254.358,413.255,312.939,309.193,401.614z"></path> </g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> </svg>                           <span class="wishlist-text">                               Add to wishlist                           </span>                                                    </a>                                                                                              <a class="quickview-button" href="javascript:void(0)" id="maroon-kota-tassel-embellished-saree" tabindex="0">                                                 <svg viewBox="0 0 511.999 511.999" width="100%" height="100%">     <path d="M508.745,246.041c-4.574-6.257-113.557-153.206-252.748-153.206S7.818,239.784,3.249,246.035         c-4.332,5.936-4.332,13.987,0,19.923c4.569,6.257,113.557,153.206,252.748,153.206s248.174-146.95,252.748-153.201         C513.083,260.028,513.083,251.971,508.745,246.041z M255.997,385.406c-102.529,0-191.33-97.533-217.617-129.418         c26.253-31.913,114.868-129.395,217.617-129.395c102.524,0,191.319,97.516,217.617,129.418         C447.361,287.923,358.746,385.406,255.997,385.406z"></path>       <path d="M255.997,154.725c-55.842,0-101.275,45.433-101.275,101.275s45.433,101.275,101.275,101.275         s101.275-45.433,101.275-101.275S311.839,154.725,255.997,154.725z M255.997,323.516c-37.23,0-67.516-30.287-67.516-67.516         s30.287-67.516,67.516-67.516s67.516,30.287,67.516,67.516S293.227,323.516,255.997,323.516z"></path> </svg>                                                   <span>                               Quick View                             </span>                                             </a>                                                                                 <div class="product-des abs-center">                                                                                    <div class="action">                         <form action="/cart/add" method="post" class="variants" id="recently-7798045671646" data-id="product-actions-7798045671646" enctype="multipart/form-data">                                                                                                                                                                                                                                             <a class="btn" data-href="javascript:void(0)" title="Maroon kota tassel embellished saree" data-init-quickshop="" tabindex="0">                                       Add to Cart                                     </a>                                                                                                                                                                                                </form>                                                                             <div class="product-card__variant--popup">                               <div class="product-card__variant--popup--content">                                 <form action="/cart/add" method="post" class="variants" id="recently-7798045671646-qs" data-id="product-actions-7798045671646" enctype="multipart/form-data">                                                                                                                                                                                                                                                                                                                                                                                                                                      <div class="selector-wrapper selector-wrapper-1 swatch js product-form__item" data-option-index="0">                                                   <label class="form-label">                                                       Size:                                                        <span class="label-value-1"></span>                                                   </label>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           <div class="swatch-element size xs available" data-value="XS">                                                               <input class="single-option-selector single-option-selector-quick single-option-selector-recently-view" type="radio" name="SingleOptionSelector-1-7798045671646-recently-view" data-index="option1" value="XS" id="SingleOptionSelector1-XS-7798045671646-recently-view" tabindex="0">                                                               <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector1-XS-7798045671646-recently-view" data-original-title="XS">XS</label>                                                           </div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               <div class="swatch-element size s available" data-value="S">                                                               <input class="single-option-selector single-option-selector-quick single-option-selector-recently-view" type="radio" name="SingleOptionSelector-1-7798045671646-recently-view" data-index="option1" value="S" id="SingleOptionSelector1-S-7798045671646-recently-view" tabindex="0">                                                               <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector1-S-7798045671646-recently-view" data-original-title="S">S</label>                                                           </div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               <div class="swatch-element size m available" data-value="M">                                                               <input class="single-option-selector single-option-selector-quick single-option-selector-recently-view" type="radio" name="SingleOptionSelector-1-7798045671646-recently-view" data-index="option1" value="M" id="SingleOptionSelector1-M-7798045671646-recently-view" tabindex="0">                                                               <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector1-M-7798045671646-recently-view" data-original-title="M">M</label>                                                           </div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               <div class="swatch-element size l available" data-value="L">                                                               <input class="single-option-selector single-option-selector-quick single-option-selector-recently-view" type="radio" name="SingleOptionSelector-1-7798045671646-recently-view" data-index="option1" value="L" id="SingleOptionSelector1-L-7798045671646-recently-view" tabindex="0">                                                               <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector1-L-7798045671646-recently-view" data-original-title="L">L</label>                                                           </div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               <div class="swatch-element size xl available" data-value="XL">                                                               <input class="single-option-selector single-option-selector-quick single-option-selector-recently-view" type="radio" name="SingleOptionSelector-1-7798045671646-recently-view" data-index="option1" value="XL" id="SingleOptionSelector1-XL-7798045671646-recently-view" tabindex="0">                                                               <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector1-XL-7798045671646-recently-view" data-original-title="XL">XL</label>                                                           </div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               <div class="swatch-element size xxl available" data-value="XXL">                                                               <input class="single-option-selector single-option-selector-quick single-option-selector-recently-view" type="radio" name="SingleOptionSelector-1-7798045671646-recently-view" data-index="option1" value="XXL" id="SingleOptionSelector1-XXL-7798045671646-recently-view" tabindex="0">                                                               <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector1-XXL-7798045671646-recently-view" data-original-title="XXL">XXL</label>                                                           </div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               <div class="swatch-element size xxxl available" data-value="XXXL">                                                               <input class="single-option-selector single-option-selector-quick single-option-selector-recently-view" type="radio" name="SingleOptionSelector-1-7798045671646-recently-view" data-index="option1" value="XXXL" id="SingleOptionSelector1-XXXL-7798045671646-recently-view" tabindex="0">                                                               <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector1-XXXL-7798045671646-recently-view" data-original-title="XXXL">XXXL</label>                                                           </div>                                                                                                                                                         </div>                                                                                                                                                        <select name="id" id="ProductSelect-7798045671646-recently-view" class="product-form__variants no-js" tabindex="0">                                                                                                                                 <option value="43400227389662">                                                   XS                                               </option>                                                                                                                                                                            <option value="43400227422430">                                                   S                                               </option>                                                                                                                                                                            <option value="43400227455198">                                                   M                                               </option>                                                                                                                                                                            <option value="43400227487966">                                                   L                                               </option>                                                                                                                                                                            <option value="43400227520734">                                                   XL                                               </option>                                                                                                                                                                            <option value="43400227553502">                                                   XXL                                               </option>                                                                                                                                                                            <option value="43400227586270">                                                   XXXL                                               </option>                                                                                                                     </select>                                   <div class="product-card__button_cancel_mobile">                                       <a class="btn btn-cancel" data-cancel-swatch-qs="" tabindex="0"><svg aria-hidden="true" data-prefix="fal" data-icon="times" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" class="svg-inline--fa fa-times fa-w-10 fa-2x"><path fill="currentColor" d="M193.94 256L296.5 153.44l21.15-21.15c3.12-3.12 3.12-8.19 0-11.31l-22.63-22.63c-3.12-3.12-8.19-3.12-11.31 0L160 222.06 36.29 98.34c-3.12-3.12-8.19-3.12-11.31 0L2.34 120.97c-3.12 3.12-3.12 8.19 0 11.31L126.06 256 2.34 379.71c-3.12 3.12-3.12 8.19 0 11.31l22.63 22.63c3.12 3.12 8.19 3.12 11.31 0L160 289.94 262.56 392.5l21.15 21.15c3.12 3.12 8.19 3.12 11.31 0l22.63-22.63c3.12-3.12 3.12-8.19 0-11.31L193.94 256z" class=""></path></svg></a>                                   </div>                                   <div class="product-card__button2">                                       <input type="hidden" name="quantity" value="1" tabindex="0">                                       <button data-btn-addtocart="" class="btn add-to-cart-btn" type="submit" data-form-id="#recently-7798045671646-qs" tabindex="0">                                           Submit                                       </button>                                   </div>                                 </form>                               </div>                           </div>                                                                       </div>                                                                                                    </div>         </div>                          <div class="product-bottom">               <div class="wrapper-vendor">                                                                                         <span class="spr-badge" id="spr_badge_7798045671646" data-rating="0.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star-empty" aria-hidden="true"></i><i class="spr-icon spr-icon-star-empty" aria-hidden="true"></i><i class="spr-icon spr-icon-star-empty" aria-hidden="true"></i><i class="spr-icon spr-icon-star-empty" aria-hidden="true"></i><i class="spr-icon spr-icon-star-empty" aria-hidden="true"></i></span><span class="spr-badge-caption">No reviews</span>
</span>
                                                                                   </div>                                                              <a class="product-title change-text" href="/products/maroon-kota-tassel-embellished-saree" tabindex="0">                 Maroon kota tassel embellished saree                                                                                       </a>                                                               <div class="price-box">                                         <div class="price-sale">                                                <span class="special-price" data-price-grid=""><span class="money">₹1,884.00</span></span>                         <span class="old-price" data-compare-price-grid=""><span class="money">₹2,691.00</span></span>                                            </div>                                  </div>               <div class="wrapper-size">                                                                                  </div>                        </div>                               </div>     </div>   </div><div class="grid-item col-6 col-md-4 col-lg-3 slick-slide slick-active" style="width: 173px;" tabindex="-1" role="option" aria-describedby="slick-slide31" data-slick-index="1" aria-hidden="false">     <div class="inner product-item on-sale" data-product-id="product-7797710979294" data-json-product="{&quot;id&quot;:7797710979294,&quot;handle&quot;:&quot;set-of-2-lilac-purple-handmade-crochia-backless-bralette-crop-top-blouse-with-cotton-ikkat-saree&quot;,&quot;media&quot;:[{&quot;alt&quot;:null,&quot;id&quot;:30634920411358,&quot;position&quot;:1,&quot;preview_image&quot;:{&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:2360,&quot;width&quot;:1600,&quot;src&quot;:&quot;https://cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-298-10_1.jpg?v=1663268035&quot;},&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:2360,&quot;media_type&quot;:&quot;image&quot;,&quot;src&quot;:&quot;https://cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-298-10_1.jpg?v=1663268035&quot;,&quot;width&quot;:1600},{&quot;alt&quot;:null,&quot;id&quot;:30634920444126,&quot;position&quot;:2,&quot;preview_image&quot;:{&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:2360,&quot;width&quot;:1600,&quot;src&quot;:&quot;https://cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-298-10_2.jpg?v=1663268035&quot;},&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:2360,&quot;media_type&quot;:&quot;image&quot;,&quot;src&quot;:&quot;https://cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-298-10_2.jpg?v=1663268035&quot;,&quot;width&quot;:1600},{&quot;alt&quot;:null,&quot;id&quot;:30634920476894,&quot;position&quot;:3,&quot;preview_image&quot;:{&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:2360,&quot;width&quot;:1600,&quot;src&quot;:&quot;https://cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-298-10_3.jpg?v=1663268035&quot;},&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:2360,&quot;media_type&quot;:&quot;image&quot;,&quot;src&quot;:&quot;https://cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-298-10_3.jpg?v=1663268035&quot;,&quot;width&quot;:1600},{&quot;alt&quot;:null,&quot;id&quot;:30634920509662,&quot;position&quot;:4,&quot;preview_image&quot;:{&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:2360,&quot;width&quot;:1600,&quot;src&quot;:&quot;https://cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-298-10_4.jpg?v=1663268035&quot;},&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:2360,&quot;media_type&quot;:&quot;image&quot;,&quot;src&quot;:&quot;https://cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-298-10_4.jpg?v=1663268035&quot;,&quot;width&quot;:1600},{&quot;alt&quot;:null,&quot;id&quot;:30634920542430,&quot;position&quot;:5,&quot;preview_image&quot;:{&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:2360,&quot;width&quot;:1600,&quot;src&quot;:&quot;https://cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-298-10_5.jpg?v=1663268035&quot;},&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:2360,&quot;media_type&quot;:&quot;image&quot;,&quot;src&quot;:&quot;https://cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-298-10_5.jpg?v=1663268035&quot;,&quot;width&quot;:1600},{&quot;alt&quot;:null,&quot;id&quot;:30634920575198,&quot;position&quot;:6,&quot;preview_image&quot;:{&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:2360,&quot;width&quot;:1600,&quot;src&quot;:&quot;https://cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-298-10_6.jpg?v=1663268035&quot;},&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:2360,&quot;media_type&quot;:&quot;image&quot;,&quot;src&quot;:&quot;https://cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-298-10_6.jpg?v=1663268035&quot;,&quot;width&quot;:1600}],&quot;variants&quot;:[{&quot;id&quot;:43398816760030,&quot;title&quot;:&quot;XS&quot;,&quot;option1&quot;:&quot;XS&quot;,&quot;option2&quot;:null,&quot;option3&quot;:null,&quot;sku&quot;:&quot;TJ-MK-298-10-XS&quot;,&quot;requires_shipping&quot;:true,&quot;taxable&quot;:true,&quot;featured_image&quot;:null,&quot;available&quot;:true,&quot;name&quot;:&quot;Set of 2- Lilac purple handmade crochia backless bralette crop top blouse with cotton ikkat saree - XS&quot;,&quot;public_title&quot;:&quot;XS&quot;,&quot;options&quot;:[&quot;XS&quot;],&quot;price&quot;:478400,&quot;weight&quot;:0,&quot;compare_at_price&quot;:683400,&quot;inventory_management&quot;:&quot;shopify&quot;,&quot;barcode&quot;:null,&quot;quantity_rule&quot;:{&quot;min&quot;:1,&quot;max&quot;:null,&quot;increment&quot;:1}},{&quot;id&quot;:43398816825566,&quot;title&quot;:&quot;S&quot;,&quot;option1&quot;:&quot;S&quot;,&quot;option2&quot;:null,&quot;option3&quot;:null,&quot;sku&quot;:&quot;TJ-MK-298-10-S&quot;,&quot;requires_shipping&quot;:true,&quot;taxable&quot;:true,&quot;featured_image&quot;:null,&quot;available&quot;:true,&quot;name&quot;:&quot;Set of 2- Lilac purple handmade crochia backless bralette crop top blouse with cotton ikkat saree - S&quot;,&quot;public_title&quot;:&quot;S&quot;,&quot;options&quot;:[&quot;S&quot;],&quot;price&quot;:478400,&quot;weight&quot;:0,&quot;compare_at_price&quot;:683400,&quot;inventory_management&quot;:&quot;shopify&quot;,&quot;barcode&quot;:null,&quot;quantity_rule&quot;:{&quot;min&quot;:1,&quot;max&quot;:null,&quot;increment&quot;:1}},{&quot;id&quot;:43398816858334,&quot;title&quot;:&quot;M&quot;,&quot;option1&quot;:&quot;M&quot;,&quot;option2&quot;:null,&quot;option3&quot;:null,&quot;sku&quot;:&quot;TJ-MK-298-10-M&quot;,&quot;requires_shipping&quot;:true,&quot;taxable&quot;:true,&quot;featured_image&quot;:null,&quot;available&quot;:true,&quot;name&quot;:&quot;Set of 2- Lilac purple handmade crochia backless bralette crop top blouse with cotton ikkat saree - M&quot;,&quot;public_title&quot;:&quot;M&quot;,&quot;options&quot;:[&quot;M&quot;],&quot;price&quot;:478400,&quot;weight&quot;:0,&quot;compare_at_price&quot;:683400,&quot;inventory_management&quot;:&quot;shopify&quot;,&quot;barcode&quot;:null,&quot;quantity_rule&quot;:{&quot;min&quot;:1,&quot;max&quot;:null,&quot;increment&quot;:1}},{&quot;id&quot;:43398816891102,&quot;title&quot;:&quot;L&quot;,&quot;option1&quot;:&quot;L&quot;,&quot;option2&quot;:null,&quot;option3&quot;:null,&quot;sku&quot;:&quot;TJ-MK-298-10-L&quot;,&quot;requires_shipping&quot;:true,&quot;taxable&quot;:true,&quot;featured_image&quot;:null,&quot;available&quot;:true,&quot;name&quot;:&quot;Set of 2- Lilac purple handmade crochia backless bralette crop top blouse with cotton ikkat saree - L&quot;,&quot;public_title&quot;:&quot;L&quot;,&quot;options&quot;:[&quot;L&quot;],&quot;price&quot;:478400,&quot;weight&quot;:0,&quot;compare_at_price&quot;:683400,&quot;inventory_management&quot;:&quot;shopify&quot;,&quot;barcode&quot;:null,&quot;quantity_rule&quot;:{&quot;min&quot;:1,&quot;max&quot;:null,&quot;increment&quot;:1}},{&quot;id&quot;:43398816923870,&quot;title&quot;:&quot;XL&quot;,&quot;option1&quot;:&quot;XL&quot;,&quot;option2&quot;:null,&quot;option3&quot;:null,&quot;sku&quot;:&quot;TJ-MK-298-10-XL&quot;,&quot;requires_shipping&quot;:true,&quot;taxable&quot;:true,&quot;featured_image&quot;:null,&quot;available&quot;:true,&quot;name&quot;:&quot;Set of 2- Lilac purple handmade crochia backless bralette crop top blouse with cotton ikkat saree - XL&quot;,&quot;public_title&quot;:&quot;XL&quot;,&quot;options&quot;:[&quot;XL&quot;],&quot;price&quot;:478400,&quot;weight&quot;:0,&quot;compare_at_price&quot;:683400,&quot;inventory_management&quot;:&quot;shopify&quot;,&quot;barcode&quot;:null,&quot;quantity_rule&quot;:{&quot;min&quot;:1,&quot;max&quot;:null,&quot;increment&quot;:1}}]}">       <div class="inner-top">       <div class="product-top">               <div class="product-image image-swap">                   <a href="/products/set-of-2-lilac-purple-handmade-crochia-backless-bralette-crop-top-blouse-with-cotton-ikkat-saree" class="product-grid-image" data-collections-related="/collections/?view=related" tabindex="0">                                                                                          <picture data-index="0">                         <img src="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-298-10_1.jpg?v=1663268035" class="images-one" alt="" data-index="0">                       </picture>                       <span class="images-two">                                                                                                        <picture data-index="1">                             <img src="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-298-10_2.jpg?v=1663268035" alt="" data-index="1">                           </picture>                                                                                                    </span>                                                                                      </a>                 </div>                                  <div class="product-label">                                                                                                  <strong class="label sale-label">                       <span>                           Sale                       </span>                     </strong>                                                               <br>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              </div>                                                                                               <a class="wishlist" data-icon-wishlist="" href="#" data-product-handle="set-of-2-lilac-purple-handmade-crochia-backless-bralette-crop-top-blouse-with-cotton-ikkat-saree" data-id="7797710979294" tabindex="0">                                                      <svg viewBox="0 0 512 512"> <g> <g>   <path d="M474.644,74.27C449.391,45.616,414.358,29.836,376,29.836c-53.948,0-88.103,32.22-107.255,59.25     c-4.969,7.014-9.196,14.047-12.745,20.665c-3.549-6.618-7.775-13.651-12.745-20.665c-19.152-27.03-53.307-59.25-107.255-59.25     c-38.358,0-73.391,15.781-98.645,44.435C13.267,101.605,0,138.213,0,177.351c0,42.603,16.633,82.228,52.345,124.7     c31.917,37.96,77.834,77.088,131.005,122.397c19.813,16.884,40.302,34.344,62.115,53.429l0.655,0.574     c2.828,2.476,6.354,3.713,9.88,3.713s7.052-1.238,9.88-3.713l0.655-0.574c21.813-19.085,42.302-36.544,62.118-53.431     c53.168-45.306,99.085-84.434,131.002-122.395C495.367,259.578,512,219.954,512,177.351     C512,138.213,498.733,101.605,474.644,74.27z M309.193,401.614c-17.08,14.554-34.658,29.533-53.193,45.646     c-18.534-16.111-36.113-31.091-53.196-45.648C98.745,312.939,30,254.358,30,177.351c0-31.83,10.605-61.394,29.862-83.245     C79.34,72.007,106.379,59.836,136,59.836c41.129,0,67.716,25.338,82.776,46.594c13.509,19.064,20.558,38.282,22.962,45.659     c2.011,6.175,7.768,10.354,14.262,10.354c6.494,0,12.251-4.179,14.262-10.354c2.404-7.377,9.453-26.595,22.962-45.66     c15.06-21.255,41.647-46.593,82.776-46.593c29.621,0,56.66,12.171,76.137,34.27C471.395,115.957,482,145.521,482,177.351     C482,254.358,413.255,312.939,309.193,401.614z"></path> </g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> </svg>                           <span class="wishlist-text">                               Add to wishlist                           </span>                                                    </a>                                                                                              <a class="quickview-button" href="javascript:void(0)" id="set-of-2-lilac-purple-handmade-crochia-backless-bralette-crop-top-blouse-with-cotton-ikkat-saree" tabindex="0">                                                 <svg viewBox="0 0 511.999 511.999" width="100%" height="100%">     <path d="M508.745,246.041c-4.574-6.257-113.557-153.206-252.748-153.206S7.818,239.784,3.249,246.035         c-4.332,5.936-4.332,13.987,0,19.923c4.569,6.257,113.557,153.206,252.748,153.206s248.174-146.95,252.748-153.201         C513.083,260.028,513.083,251.971,508.745,246.041z M255.997,385.406c-102.529,0-191.33-97.533-217.617-129.418         c26.253-31.913,114.868-129.395,217.617-129.395c102.524,0,191.319,97.516,217.617,129.418         C447.361,287.923,358.746,385.406,255.997,385.406z"></path>       <path d="M255.997,154.725c-55.842,0-101.275,45.433-101.275,101.275s45.433,101.275,101.275,101.275         s101.275-45.433,101.275-101.275S311.839,154.725,255.997,154.725z M255.997,323.516c-37.23,0-67.516-30.287-67.516-67.516         s30.287-67.516,67.516-67.516s67.516,30.287,67.516,67.516S293.227,323.516,255.997,323.516z"></path> </svg>                                                   <span>                               Quick View                             </span>                                             </a>                                                                                 <div class="product-des abs-center">                                                                                    <div class="action">                         <form action="/cart/add" method="post" class="variants" id="recently-7797710979294" data-id="product-actions-7797710979294" enctype="multipart/form-data">                                                                                                                                                                                                                                             <a class="btn" data-href="javascript:void(0)" title="Set of 2- Lilac purple handmade crochia backless bralette crop top blouse with cotton ikkat saree" data-init-quickshop="" tabindex="0">                                       Add to Cart                                     </a>                                                                                                                                                                                                </form>                                                                             <div class="product-card__variant--popup">                               <div class="product-card__variant--popup--content">                                 <form action="/cart/add" method="post" class="variants" id="recently-7797710979294-qs" data-id="product-actions-7797710979294" enctype="multipart/form-data">                                                                                                                                                                                                                                                                                                                                                                                                                                      <div class="selector-wrapper selector-wrapper-1 swatch js product-form__item" data-option-index="0">                                                   <label class="form-label">                                                       Size:                                                        <span class="label-value-1"></span>                                                   </label>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           <div class="swatch-element size xs available" data-value="XS">                                                               <input class="single-option-selector single-option-selector-quick single-option-selector-recently-view" type="radio" name="SingleOptionSelector-1-7797710979294-recently-view" data-index="option1" value="XS" id="SingleOptionSelector1-XS-7797710979294-recently-view" tabindex="0">                                                               <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector1-XS-7797710979294-recently-view" data-original-title="XS">XS</label>                                                           </div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               <div class="swatch-element size s available" data-value="S">                                                               <input class="single-option-selector single-option-selector-quick single-option-selector-recently-view" type="radio" name="SingleOptionSelector-1-7797710979294-recently-view" data-index="option1" value="S" id="SingleOptionSelector1-S-7797710979294-recently-view" tabindex="0">                                                               <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector1-S-7797710979294-recently-view" data-original-title="S">S</label>                                                           </div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               <div class="swatch-element size m available" data-value="M">                                                               <input class="single-option-selector single-option-selector-quick single-option-selector-recently-view" type="radio" name="SingleOptionSelector-1-7797710979294-recently-view" data-index="option1" value="M" id="SingleOptionSelector1-M-7797710979294-recently-view" tabindex="0">                                                               <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector1-M-7797710979294-recently-view" data-original-title="M">M</label>                                                           </div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               <div class="swatch-element size l available" data-value="L">                                                               <input class="single-option-selector single-option-selector-quick single-option-selector-recently-view" type="radio" name="SingleOptionSelector-1-7797710979294-recently-view" data-index="option1" value="L" id="SingleOptionSelector1-L-7797710979294-recently-view" tabindex="0">                                                               <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector1-L-7797710979294-recently-view" data-original-title="L">L</label>                                                           </div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               <div class="swatch-element size xl available" data-value="XL">                                                               <input class="single-option-selector single-option-selector-quick single-option-selector-recently-view" type="radio" name="SingleOptionSelector-1-7797710979294-recently-view" data-index="option1" value="XL" id="SingleOptionSelector1-XL-7797710979294-recently-view" tabindex="0">                                                               <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector1-XL-7797710979294-recently-view" data-original-title="XL">XL</label>                                                           </div>                                                                                                                                                         </div>                                                                                                                                                        <select name="id" id="ProductSelect-7797710979294-recently-view" class="product-form__variants no-js" tabindex="0">                                                                                                                                 <option value="43398816760030">                                                   XS                                               </option>                                                                                                                                                                            <option value="43398816825566">                                                   S                                               </option>                                                                                                                                                                            <option value="43398816858334">                                                   M                                               </option>                                                                                                                                                                            <option value="43398816891102">                                                   L                                               </option>                                                                                                                                                                            <option value="43398816923870">                                                   XL                                               </option>                                                                                                                     </select>                                   <div class="product-card__button_cancel_mobile">                                       <a class="btn btn-cancel" data-cancel-swatch-qs="" tabindex="0"><svg aria-hidden="true" data-prefix="fal" data-icon="times" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" class="svg-inline--fa fa-times fa-w-10 fa-2x"><path fill="currentColor" d="M193.94 256L296.5 153.44l21.15-21.15c3.12-3.12 3.12-8.19 0-11.31l-22.63-22.63c-3.12-3.12-8.19-3.12-11.31 0L160 222.06 36.29 98.34c-3.12-3.12-8.19-3.12-11.31 0L2.34 120.97c-3.12 3.12-3.12 8.19 0 11.31L126.06 256 2.34 379.71c-3.12 3.12-3.12 8.19 0 11.31l22.63 22.63c3.12 3.12 8.19 3.12 11.31 0L160 289.94 262.56 392.5l21.15 21.15c3.12 3.12 8.19 3.12 11.31 0l22.63-22.63c3.12-3.12 3.12-8.19 0-11.31L193.94 256z" class=""></path></svg></a>                                   </div>                                   <div class="product-card__button2">                                       <input type="hidden" name="quantity" value="1" tabindex="0">                                       <button data-btn-addtocart="" class="btn add-to-cart-btn" type="submit" data-form-id="#recently-7797710979294-qs" tabindex="0">                                           Submit                                       </button>                                   </div>                                 </form>                               </div>                           </div>                                                                       </div>                                                                                                    </div>         </div>                          <div class="product-bottom">               <div class="wrapper-vendor">                                                                                         <span class="spr-badge" id="spr_badge_7797710979294" data-rating="0.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star-empty" aria-hidden="true"></i><i class="spr-icon spr-icon-star-empty" aria-hidden="true"></i><i class="spr-icon spr-icon-star-empty" aria-hidden="true"></i><i class="spr-icon spr-icon-star-empty" aria-hidden="true"></i><i class="spr-icon spr-icon-star-empty" aria-hidden="true"></i></span><span class="spr-badge-caption">No reviews</span>
</span>
                                                                                   </div>                                                              <a class="product-title change-text" href="/products/set-of-2-lilac-purple-handmade-crochia-backless-bralette-crop-top-blouse-with-cotton-ikkat-saree" tabindex="0">                 Set of 2- Lilac purple handmade crochia backless bralette crop top blouse with cotton ikkat saree                                                                                       </a>                                                               <div class="price-box">                                         <div class="price-sale">                                                <span class="special-price" data-price-grid=""><span class="money">₹4,784.00</span></span>                         <span class="old-price" data-compare-price-grid=""><span class="money">₹6,834.00</span></span>                                            </div>                                  </div>               <div class="wrapper-size">                                                                                  </div>                        </div>                               </div>     </div>   </div><div class="grid-item col-6 col-md-4 col-lg-3 slick-slide" style="width: 173px;" tabindex="-1" role="option" aria-describedby="slick-slide32" data-slick-index="2" aria-hidden="true">     <div class="inner product-item on-sale" data-product-id="product-7798396420318" data-json-product="{&quot;id&quot;:7798396420318,&quot;handle&quot;:&quot;cotton-kota-mauve-color-blockprinted-border-saree&quot;,&quot;media&quot;:[{&quot;alt&quot;:null,&quot;id&quot;:30634913038558,&quot;position&quot;:1,&quot;preview_image&quot;:{&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:2360,&quot;width&quot;:1600,&quot;src&quot;:&quot;https://cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-298-02_1_55fb0453-e4ba-46af-90b5-ed18f23bb8fe.jpg?v=1663268014&quot;},&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:2360,&quot;media_type&quot;:&quot;image&quot;,&quot;src&quot;:&quot;https://cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-298-02_1_55fb0453-e4ba-46af-90b5-ed18f23bb8fe.jpg?v=1663268014&quot;,&quot;width&quot;:1600},{&quot;alt&quot;:null,&quot;id&quot;:30634913071326,&quot;position&quot;:2,&quot;preview_image&quot;:{&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:2360,&quot;width&quot;:1600,&quot;src&quot;:&quot;https://cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-298-02_2_a573dd05-2137-4bca-9462-d378a0674f7d.jpg?v=1663268014&quot;},&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:2360,&quot;media_type&quot;:&quot;image&quot;,&quot;src&quot;:&quot;https://cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-298-02_2_a573dd05-2137-4bca-9462-d378a0674f7d.jpg?v=1663268014&quot;,&quot;width&quot;:1600},{&quot;alt&quot;:null,&quot;id&quot;:30634913104094,&quot;position&quot;:3,&quot;preview_image&quot;:{&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:2360,&quot;width&quot;:1600,&quot;src&quot;:&quot;https://cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-298-02_3_0fcf3f05-6892-4677-9c19-16c91afa2368.jpg?v=1663268014&quot;},&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:2360,&quot;media_type&quot;:&quot;image&quot;,&quot;src&quot;:&quot;https://cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-298-02_3_0fcf3f05-6892-4677-9c19-16c91afa2368.jpg?v=1663268014&quot;,&quot;width&quot;:1600},{&quot;alt&quot;:null,&quot;id&quot;:30634913136862,&quot;position&quot;:4,&quot;preview_image&quot;:{&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:2360,&quot;width&quot;:1600,&quot;src&quot;:&quot;https://cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-298-02_4_480ccdf8-5581-499b-88c2-5bf1ff2bb0ac.jpg?v=1663268014&quot;},&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:2360,&quot;media_type&quot;:&quot;image&quot;,&quot;src&quot;:&quot;https://cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-298-02_4_480ccdf8-5581-499b-88c2-5bf1ff2bb0ac.jpg?v=1663268014&quot;,&quot;width&quot;:1600},{&quot;alt&quot;:null,&quot;id&quot;:30634913169630,&quot;position&quot;:5,&quot;preview_image&quot;:{&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:2360,&quot;width&quot;:1600,&quot;src&quot;:&quot;https://cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-298-02_5_fc95fc65-de85-445e-aaea-0d68e82fa3a1.jpg?v=1663268014&quot;},&quot;aspect_ratio&quot;:0.678,&quot;height&quot;:2360,&quot;media_type&quot;:&quot;image&quot;,&quot;src&quot;:&quot;https://cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-298-02_5_fc95fc65-de85-445e-aaea-0d68e82fa3a1.jpg?v=1663268014&quot;,&quot;width&quot;:1600}],&quot;variants&quot;:[{&quot;id&quot;:43401386033374,&quot;title&quot;:&quot;XS&quot;,&quot;option1&quot;:&quot;XS&quot;,&quot;option2&quot;:null,&quot;option3&quot;:null,&quot;sku&quot;:&quot;TJ-MK-298-02-XS&quot;,&quot;requires_shipping&quot;:true,&quot;taxable&quot;:true,&quot;featured_image&quot;:null,&quot;available&quot;:true,&quot;name&quot;:&quot;Cotton kota mauve color blockprinted border saree - XS&quot;,&quot;public_title&quot;:&quot;XS&quot;,&quot;options&quot;:[&quot;XS&quot;],&quot;price&quot;:376900,&quot;weight&quot;:0,&quot;compare_at_price&quot;:538400,&quot;inventory_management&quot;:&quot;shopify&quot;,&quot;barcode&quot;:null,&quot;quantity_rule&quot;:{&quot;min&quot;:1,&quot;max&quot;:null,&quot;increment&quot;:1}},{&quot;id&quot;:43401386066142,&quot;title&quot;:&quot;S&quot;,&quot;option1&quot;:&quot;S&quot;,&quot;option2&quot;:null,&quot;option3&quot;:null,&quot;sku&quot;:&quot;TJ-MK-298-02-S&quot;,&quot;requires_shipping&quot;:true,&quot;taxable&quot;:true,&quot;featured_image&quot;:null,&quot;available&quot;:true,&quot;name&quot;:&quot;Cotton kota mauve color blockprinted border saree - S&quot;,&quot;public_title&quot;:&quot;S&quot;,&quot;options&quot;:[&quot;S&quot;],&quot;price&quot;:376900,&quot;weight&quot;:0,&quot;compare_at_price&quot;:538400,&quot;inventory_management&quot;:&quot;shopify&quot;,&quot;barcode&quot;:null,&quot;quantity_rule&quot;:{&quot;min&quot;:1,&quot;max&quot;:null,&quot;increment&quot;:1}},{&quot;id&quot;:43401386098910,&quot;title&quot;:&quot;M&quot;,&quot;option1&quot;:&quot;M&quot;,&quot;option2&quot;:null,&quot;option3&quot;:null,&quot;sku&quot;:&quot;TJ-MK-298-02-M&quot;,&quot;requires_shipping&quot;:true,&quot;taxable&quot;:true,&quot;featured_image&quot;:null,&quot;available&quot;:true,&quot;name&quot;:&quot;Cotton kota mauve color blockprinted border saree - M&quot;,&quot;public_title&quot;:&quot;M&quot;,&quot;options&quot;:[&quot;M&quot;],&quot;price&quot;:376900,&quot;weight&quot;:0,&quot;compare_at_price&quot;:538400,&quot;inventory_management&quot;:&quot;shopify&quot;,&quot;barcode&quot;:null,&quot;quantity_rule&quot;:{&quot;min&quot;:1,&quot;max&quot;:null,&quot;increment&quot;:1}},{&quot;id&quot;:43401386131678,&quot;title&quot;:&quot;L&quot;,&quot;option1&quot;:&quot;L&quot;,&quot;option2&quot;:null,&quot;option3&quot;:null,&quot;sku&quot;:&quot;TJ-MK-298-02-L&quot;,&quot;requires_shipping&quot;:true,&quot;taxable&quot;:true,&quot;featured_image&quot;:null,&quot;available&quot;:true,&quot;name&quot;:&quot;Cotton kota mauve color blockprinted border saree - L&quot;,&quot;public_title&quot;:&quot;L&quot;,&quot;options&quot;:[&quot;L&quot;],&quot;price&quot;:376900,&quot;weight&quot;:0,&quot;compare_at_price&quot;:538400,&quot;inventory_management&quot;:&quot;shopify&quot;,&quot;barcode&quot;:null,&quot;quantity_rule&quot;:{&quot;min&quot;:1,&quot;max&quot;:null,&quot;increment&quot;:1}},{&quot;id&quot;:43401386164446,&quot;title&quot;:&quot;XL&quot;,&quot;option1&quot;:&quot;XL&quot;,&quot;option2&quot;:null,&quot;option3&quot;:null,&quot;sku&quot;:&quot;TJ-MK-298-02-XL&quot;,&quot;requires_shipping&quot;:true,&quot;taxable&quot;:true,&quot;featured_image&quot;:null,&quot;available&quot;:true,&quot;name&quot;:&quot;Cotton kota mauve color blockprinted border saree - XL&quot;,&quot;public_title&quot;:&quot;XL&quot;,&quot;options&quot;:[&quot;XL&quot;],&quot;price&quot;:376900,&quot;weight&quot;:0,&quot;compare_at_price&quot;:538400,&quot;inventory_management&quot;:&quot;shopify&quot;,&quot;barcode&quot;:null,&quot;quantity_rule&quot;:{&quot;min&quot;:1,&quot;max&quot;:null,&quot;increment&quot;:1}},{&quot;id&quot;:43401386197214,&quot;title&quot;:&quot;XXL&quot;,&quot;option1&quot;:&quot;XXL&quot;,&quot;option2&quot;:null,&quot;option3&quot;:null,&quot;sku&quot;:&quot;TJ-MK-298-02-XXL&quot;,&quot;requires_shipping&quot;:true,&quot;taxable&quot;:true,&quot;featured_image&quot;:null,&quot;available&quot;:true,&quot;name&quot;:&quot;Cotton kota mauve color blockprinted border saree - XXL&quot;,&quot;public_title&quot;:&quot;XXL&quot;,&quot;options&quot;:[&quot;XXL&quot;],&quot;price&quot;:376900,&quot;weight&quot;:0,&quot;compare_at_price&quot;:538400,&quot;inventory_management&quot;:&quot;shopify&quot;,&quot;barcode&quot;:null,&quot;quantity_rule&quot;:{&quot;min&quot;:1,&quot;max&quot;:null,&quot;increment&quot;:1}},{&quot;id&quot;:43401386229982,&quot;title&quot;:&quot;XXXL&quot;,&quot;option1&quot;:&quot;XXXL&quot;,&quot;option2&quot;:null,&quot;option3&quot;:null,&quot;sku&quot;:&quot;TJ-MK-298-02-XXXL&quot;,&quot;requires_shipping&quot;:true,&quot;taxable&quot;:true,&quot;featured_image&quot;:null,&quot;available&quot;:true,&quot;name&quot;:&quot;Cotton kota mauve color blockprinted border saree - XXXL&quot;,&quot;public_title&quot;:&quot;XXXL&quot;,&quot;options&quot;:[&quot;XXXL&quot;],&quot;price&quot;:376900,&quot;weight&quot;:0,&quot;compare_at_price&quot;:538400,&quot;inventory_management&quot;:&quot;shopify&quot;,&quot;barcode&quot;:null,&quot;quantity_rule&quot;:{&quot;min&quot;:1,&quot;max&quot;:null,&quot;increment&quot;:1}}]}">       <div class="inner-top">       <div class="product-top">               <div class="product-image image-swap">                   <a href="/products/cotton-kota-mauve-color-blockprinted-border-saree" class="product-grid-image" data-collections-related="/collections/?view=related" tabindex="-1">                                                                                          <picture data-index="0">                         <img src="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-298-02_1_55fb0453-e4ba-46af-90b5-ed18f23bb8fe.jpg?v=1663268014" class="images-one" alt="" data-index="0">                       </picture>                       <span class="images-two">                                                                                                        <picture data-index="1">                             <img src="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-298-02_2_a573dd05-2137-4bca-9462-d378a0674f7d.jpg?v=1663268014" alt="" data-index="1">                           </picture>                                                                                                    </span>                                                                                      </a>                 </div>                                  <div class="product-label">                                                                                                  <strong class="label sale-label">                       <span>                           Sale                       </span>                     </strong>                                                               <br>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          </div>                                                                                               <a class="wishlist" data-icon-wishlist="" href="#" data-product-handle="cotton-kota-mauve-color-blockprinted-border-saree" data-id="7798396420318" tabindex="-1">                                                      <svg viewBox="0 0 512 512"> <g> <g>   <path d="M474.644,74.27C449.391,45.616,414.358,29.836,376,29.836c-53.948,0-88.103,32.22-107.255,59.25     c-4.969,7.014-9.196,14.047-12.745,20.665c-3.549-6.618-7.775-13.651-12.745-20.665c-19.152-27.03-53.307-59.25-107.255-59.25     c-38.358,0-73.391,15.781-98.645,44.435C13.267,101.605,0,138.213,0,177.351c0,42.603,16.633,82.228,52.345,124.7     c31.917,37.96,77.834,77.088,131.005,122.397c19.813,16.884,40.302,34.344,62.115,53.429l0.655,0.574     c2.828,2.476,6.354,3.713,9.88,3.713s7.052-1.238,9.88-3.713l0.655-0.574c21.813-19.085,42.302-36.544,62.118-53.431     c53.168-45.306,99.085-84.434,131.002-122.395C495.367,259.578,512,219.954,512,177.351     C512,138.213,498.733,101.605,474.644,74.27z M309.193,401.614c-17.08,14.554-34.658,29.533-53.193,45.646     c-18.534-16.111-36.113-31.091-53.196-45.648C98.745,312.939,30,254.358,30,177.351c0-31.83,10.605-61.394,29.862-83.245     C79.34,72.007,106.379,59.836,136,59.836c41.129,0,67.716,25.338,82.776,46.594c13.509,19.064,20.558,38.282,22.962,45.659     c2.011,6.175,7.768,10.354,14.262,10.354c6.494,0,12.251-4.179,14.262-10.354c2.404-7.377,9.453-26.595,22.962-45.66     c15.06-21.255,41.647-46.593,82.776-46.593c29.621,0,56.66,12.171,76.137,34.27C471.395,115.957,482,145.521,482,177.351     C482,254.358,413.255,312.939,309.193,401.614z"></path> </g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> </svg>                           <span class="wishlist-text">                               Add to wishlist                           </span>                                                    </a>                                                                                              <a class="quickview-button" href="javascript:void(0)" id="cotton-kota-mauve-color-blockprinted-border-saree" tabindex="-1">                                                 <svg viewBox="0 0 511.999 511.999" width="100%" height="100%">     <path d="M508.745,246.041c-4.574-6.257-113.557-153.206-252.748-153.206S7.818,239.784,3.249,246.035         c-4.332,5.936-4.332,13.987,0,19.923c4.569,6.257,113.557,153.206,252.748,153.206s248.174-146.95,252.748-153.201         C513.083,260.028,513.083,251.971,508.745,246.041z M255.997,385.406c-102.529,0-191.33-97.533-217.617-129.418         c26.253-31.913,114.868-129.395,217.617-129.395c102.524,0,191.319,97.516,217.617,129.418         C447.361,287.923,358.746,385.406,255.997,385.406z"></path>       <path d="M255.997,154.725c-55.842,0-101.275,45.433-101.275,101.275s45.433,101.275,101.275,101.275         s101.275-45.433,101.275-101.275S311.839,154.725,255.997,154.725z M255.997,323.516c-37.23,0-67.516-30.287-67.516-67.516         s30.287-67.516,67.516-67.516s67.516,30.287,67.516,67.516S293.227,323.516,255.997,323.516z"></path> </svg>                                                   <span>                               Quick View                             </span>                                             </a>                                                                                 <div class="product-des abs-center">                                                                                    <div class="action">                         <form action="/cart/add" method="post" class="variants" id="recently-7798396420318" data-id="product-actions-7798396420318" enctype="multipart/form-data">                                                                                                                                                                                                                                             <a class="btn" data-href="javascript:void(0)" title="Cotton kota mauve color blockprinted border saree" data-init-quickshop="" tabindex="-1">                                       Add to Cart                                     </a>                                                                                                                                                                                                </form>                                                                             <div class="product-card__variant--popup">                               <div class="product-card__variant--popup--content">                                 <form action="/cart/add" method="post" class="variants" id="recently-7798396420318-qs" data-id="product-actions-7798396420318" enctype="multipart/form-data">                                                                                                                                                                                                                                                                                                                                                                                                                                      <div class="selector-wrapper selector-wrapper-1 swatch js product-form__item" data-option-index="0">                                                   <label class="form-label">                                                       Size:                                                        <span class="label-value-1"></span>                                                   </label>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           <div class="swatch-element size xs available" data-value="XS">                                                               <input class="single-option-selector single-option-selector-quick single-option-selector-recently-view" type="radio" name="SingleOptionSelector-1-7798396420318-recently-view" data-index="option1" value="XS" id="SingleOptionSelector1-XS-7798396420318-recently-view" tabindex="-1">                                                               <label data-toggle="tooltip" data-placement="top" title="XS" for="SingleOptionSelector1-XS-7798396420318-recently-view">XS</label>                                                           </div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               <div class="swatch-element size s available" data-value="S">                                                               <input class="single-option-selector single-option-selector-quick single-option-selector-recently-view" type="radio" name="SingleOptionSelector-1-7798396420318-recently-view" data-index="option1" value="S" id="SingleOptionSelector1-S-7798396420318-recently-view" tabindex="-1">                                                               <label data-toggle="tooltip" data-placement="top" title="S" for="SingleOptionSelector1-S-7798396420318-recently-view">S</label>                                                           </div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               <div class="swatch-element size m available" data-value="M">                                                               <input class="single-option-selector single-option-selector-quick single-option-selector-recently-view" type="radio" name="SingleOptionSelector-1-7798396420318-recently-view" data-index="option1" value="M" id="SingleOptionSelector1-M-7798396420318-recently-view" tabindex="-1">                                                               <label data-toggle="tooltip" data-placement="top" title="M" for="SingleOptionSelector1-M-7798396420318-recently-view">M</label>                                                           </div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               <div class="swatch-element size l available" data-value="L">                                                               <input class="single-option-selector single-option-selector-quick single-option-selector-recently-view" type="radio" name="SingleOptionSelector-1-7798396420318-recently-view" data-index="option1" value="L" id="SingleOptionSelector1-L-7798396420318-recently-view" tabindex="-1">                                                               <label data-toggle="tooltip" data-placement="top" title="L" for="SingleOptionSelector1-L-7798396420318-recently-view">L</label>                                                           </div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               <div class="swatch-element size xl available" data-value="XL">                                                               <input class="single-option-selector single-option-selector-quick single-option-selector-recently-view" type="radio" name="SingleOptionSelector-1-7798396420318-recently-view" data-index="option1" value="XL" id="SingleOptionSelector1-XL-7798396420318-recently-view" tabindex="-1">                                                               <label data-toggle="tooltip" data-placement="top" title="XL" for="SingleOptionSelector1-XL-7798396420318-recently-view">XL</label>                                                           </div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               <div class="swatch-element size xxl available" data-value="XXL">                                                               <input class="single-option-selector single-option-selector-quick single-option-selector-recently-view" type="radio" name="SingleOptionSelector-1-7798396420318-recently-view" data-index="option1" value="XXL" id="SingleOptionSelector1-XXL-7798396420318-recently-view" tabindex="-1">                                                               <label data-toggle="tooltip" data-placement="top" title="XXL" for="SingleOptionSelector1-XXL-7798396420318-recently-view">XXL</label>                                                           </div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               <div class="swatch-element size xxxl available" data-value="XXXL">                                                               <input class="single-option-selector single-option-selector-quick single-option-selector-recently-view" type="radio" name="SingleOptionSelector-1-7798396420318-recently-view" data-index="option1" value="XXXL" id="SingleOptionSelector1-XXXL-7798396420318-recently-view" tabindex="-1">                                                               <label data-toggle="tooltip" data-placement="top" title="XXXL" for="SingleOptionSelector1-XXXL-7798396420318-recently-view">XXXL</label>                                                           </div>                                                                                                                                                         </div>                                                                                                                                                        <select name="id" id="ProductSelect-7798396420318-recently-view" class="product-form__variants no-js" tabindex="-1">                                                                                                                                 <option value="43401386033374">                                                   XS                                               </option>                                                                                                                                                                            <option value="43401386066142">                                                   S                                               </option>                                                                                                                                                                            <option value="43401386098910">                                                   M                                               </option>                                                                                                                                                                            <option value="43401386131678">                                                   L                                               </option>                                                                                                                                                                            <option value="43401386164446">                                                   XL                                               </option>                                                                                                                                                                            <option value="43401386197214">                                                   XXL                                               </option>                                                                                                                                                                            <option value="43401386229982">                                                   XXXL                                               </option>                                                                                                                     </select>                                   <div class="product-card__button_cancel_mobile">                                       <a class="btn btn-cancel" data-cancel-swatch-qs="" tabindex="-1"><svg aria-hidden="true" data-prefix="fal" data-icon="times" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" class="svg-inline--fa fa-times fa-w-10 fa-2x"><path fill="currentColor" d="M193.94 256L296.5 153.44l21.15-21.15c3.12-3.12 3.12-8.19 0-11.31l-22.63-22.63c-3.12-3.12-8.19-3.12-11.31 0L160 222.06 36.29 98.34c-3.12-3.12-8.19-3.12-11.31 0L2.34 120.97c-3.12 3.12-3.12 8.19 0 11.31L126.06 256 2.34 379.71c-3.12 3.12-3.12 8.19 0 11.31l22.63 22.63c3.12 3.12 8.19 3.12 11.31 0L160 289.94 262.56 392.5l21.15 21.15c3.12 3.12 8.19 3.12 11.31 0l22.63-22.63c3.12-3.12 3.12-8.19 0-11.31L193.94 256z" class=""></path></svg></a>                                   </div>                                   <div class="product-card__button2">                                       <input type="hidden" name="quantity" value="1" tabindex="-1">                                       <button data-btn-addtocart="" class="btn add-to-cart-btn" type="submit" data-form-id="#recently-7798396420318-qs" tabindex="-1">                                           Submit                                       </button>                                   </div>                                 </form>                               </div>                           </div>                                                                       </div>                                                                                                    </div>         </div>                          <div class="product-bottom">               <div class="wrapper-vendor">                                                                                         <span class="spr-badge" id="spr_badge_7798396420318" data-rating="0.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star-empty" aria-hidden="true"></i><i class="spr-icon spr-icon-star-empty" aria-hidden="true"></i><i class="spr-icon spr-icon-star-empty" aria-hidden="true"></i><i class="spr-icon spr-icon-star-empty" aria-hidden="true"></i><i class="spr-icon spr-icon-star-empty" aria-hidden="true"></i></span><span class="spr-badge-caption">No reviews</span>
</span>
                                                                                   </div>                                                              <a class="product-title change-text" href="/products/cotton-kota-mauve-color-blockprinted-border-saree" tabindex="-1">                 Cotton kota mauve color blockprinted border saree                                                                                       </a>                                                               <div class="price-box">                                         <div class="price-sale">                                                <span class="special-price" data-price-grid=""><span class="money">₹3,769.00</span></span>                         <span class="old-price" data-compare-price-grid=""><span class="money">₹5,384.00</span></span>                                            </div>                                  </div>               <div class="wrapper-size">                                                                                  </div>                        </div>                               </div>     </div>   </div><div class="grid-item col-6 col-md-4 col-lg-3 slick-slide" style="width: 173px;" tabindex="-1" role="option" aria-describedby="slick-slide33" data-slick-index="3" aria-hidden="true">     <div class="inner product-item on-sale" data-product-id="product-7960720179422">       <div class="inner-top">       <div class="product-top">               <div class="product-image image-swap">                   <a href="/products/single-apple-green-fit-flare-assymetrical-long-dress-with-lace-work" class="product-grid-image" data-collections-related="/collections/?view=related" tabindex="-1">                                                                                          <picture data-index="0">                         <img src="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-309-07_1.jpg?v=1677145602" class="images-one" alt="" data-index="0">                       </picture>                       <span class="images-two">                                                                                                        <picture data-index="1">                             <img src="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-309-07_2.jpg?v=1677145602" alt="" data-index="1">                           </picture>                                                                                                    </span>                                                                                      </a>                 </div>                                  <div class="product-label">                                                                                                  <strong class="label sale-label">                       <span>                           Sale                       </span>                     </strong>                                                               <br>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      </div>                                                                                               <a class="wishlist" data-icon-wishlist="" href="#" data-product-handle="single-apple-green-fit-flare-assymetrical-long-dress-with-lace-work" data-id="7960720179422" tabindex="-1">                                                      <svg viewBox="0 0 512 512"> <g> <g>   <path d="M474.644,74.27C449.391,45.616,414.358,29.836,376,29.836c-53.948,0-88.103,32.22-107.255,59.25     c-4.969,7.014-9.196,14.047-12.745,20.665c-3.549-6.618-7.775-13.651-12.745-20.665c-19.152-27.03-53.307-59.25-107.255-59.25     c-38.358,0-73.391,15.781-98.645,44.435C13.267,101.605,0,138.213,0,177.351c0,42.603,16.633,82.228,52.345,124.7     c31.917,37.96,77.834,77.088,131.005,122.397c19.813,16.884,40.302,34.344,62.115,53.429l0.655,0.574     c2.828,2.476,6.354,3.713,9.88,3.713s7.052-1.238,9.88-3.713l0.655-0.574c21.813-19.085,42.302-36.544,62.118-53.431     c53.168-45.306,99.085-84.434,131.002-122.395C495.367,259.578,512,219.954,512,177.351     C512,138.213,498.733,101.605,474.644,74.27z M309.193,401.614c-17.08,14.554-34.658,29.533-53.193,45.646     c-18.534-16.111-36.113-31.091-53.196-45.648C98.745,312.939,30,254.358,30,177.351c0-31.83,10.605-61.394,29.862-83.245     C79.34,72.007,106.379,59.836,136,59.836c41.129,0,67.716,25.338,82.776,46.594c13.509,19.064,20.558,38.282,22.962,45.659     c2.011,6.175,7.768,10.354,14.262,10.354c6.494,0,12.251-4.179,14.262-10.354c2.404-7.377,9.453-26.595,22.962-45.66     c15.06-21.255,41.647-46.593,82.776-46.593c29.621,0,56.66,12.171,76.137,34.27C471.395,115.957,482,145.521,482,177.351     C482,254.358,413.255,312.939,309.193,401.614z"></path> </g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> </svg>                           <span class="wishlist-text">                               Add to wishlist                           </span>                                                    </a>                                                                                              <a class="quickview-button" href="javascript:void(0)" id="single-apple-green-fit-flare-assymetrical-long-dress-with-lace-work" tabindex="-1">                                                 <svg viewBox="0 0 511.999 511.999" width="100%" height="100%">     <path d="M508.745,246.041c-4.574-6.257-113.557-153.206-252.748-153.206S7.818,239.784,3.249,246.035         c-4.332,5.936-4.332,13.987,0,19.923c4.569,6.257,113.557,153.206,252.748,153.206s248.174-146.95,252.748-153.201         C513.083,260.028,513.083,251.971,508.745,246.041z M255.997,385.406c-102.529,0-191.33-97.533-217.617-129.418         c26.253-31.913,114.868-129.395,217.617-129.395c102.524,0,191.319,97.516,217.617,129.418         C447.361,287.923,358.746,385.406,255.997,385.406z"></path>       <path d="M255.997,154.725c-55.842,0-101.275,45.433-101.275,101.275s45.433,101.275,101.275,101.275         s101.275-45.433,101.275-101.275S311.839,154.725,255.997,154.725z M255.997,323.516c-37.23,0-67.516-30.287-67.516-67.516         s30.287-67.516,67.516-67.516s67.516,30.287,67.516,67.516S293.227,323.516,255.997,323.516z"></path> </svg>                                                   <span>                               Quick View                             </span>                                             </a>                                                                                 <div class="product-des abs-center">                                                                                    <div class="action">                         <form action="/cart/add" method="post" class="variants" id="recently-7960720179422" data-id="product-actions-7960720179422" enctype="multipart/form-data">                                                                                                                                                                                                                                             <a class="btn" data-href="javascript:void(0)" title="Apple Green Fit &amp; Flare Assymetrical Long Dress With Lace Work" data-init-quickshop="" tabindex="-1">                                       Add to Cart                                     </a>                                                                                                                                                                                                </form>                                                                             <div class="product-card__variant--popup">                               <div class="product-card__variant--popup--content">                                 <form action="/cart/add" method="post" class="variants" id="recently-7960720179422-qs" data-id="product-actions-7960720179422" enctype="multipart/form-data">                                                                                                                                                                                                                                                                                                                                                                                                                                      <div class="selector-wrapper selector-wrapper-1 swatch js product-form__item" data-option-index="0">                                                   <label class="form-label">                                                       Size:                                                        <span class="label-value-1"></span>                                                   </label>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           <div class="swatch-element size xs available" data-value="XS">                                                               <input class="single-option-selector single-option-selector-quick single-option-selector-recently-view" type="radio" name="SingleOptionSelector-1-7960720179422-recently-view" data-index="option1" value="XS" id="SingleOptionSelector1-XS-7960720179422-recently-view" tabindex="-1">                                                               <label data-toggle="tooltip" data-placement="top" title="XS" for="SingleOptionSelector1-XS-7960720179422-recently-view">XS</label>                                                           </div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               <div class="swatch-element size s available" data-value="S">                                                               <input class="single-option-selector single-option-selector-quick single-option-selector-recently-view" type="radio" name="SingleOptionSelector-1-7960720179422-recently-view" data-index="option1" value="S" id="SingleOptionSelector1-S-7960720179422-recently-view" tabindex="-1">                                                               <label data-toggle="tooltip" data-placement="top" title="S" for="SingleOptionSelector1-S-7960720179422-recently-view">S</label>                                                           </div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               <div class="swatch-element size m available" data-value="M">                                                               <input class="single-option-selector single-option-selector-quick single-option-selector-recently-view" type="radio" name="SingleOptionSelector-1-7960720179422-recently-view" data-index="option1" value="M" id="SingleOptionSelector1-M-7960720179422-recently-view" tabindex="-1">                                                               <label data-toggle="tooltip" data-placement="top" title="M" for="SingleOptionSelector1-M-7960720179422-recently-view">M</label>                                                           </div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               <div class="swatch-element size l available" data-value="L">                                                               <input class="single-option-selector single-option-selector-quick single-option-selector-recently-view" type="radio" name="SingleOptionSelector-1-7960720179422-recently-view" data-index="option1" value="L" id="SingleOptionSelector1-L-7960720179422-recently-view" tabindex="-1">                                                               <label data-toggle="tooltip" data-placement="top" title="L" for="SingleOptionSelector1-L-7960720179422-recently-view">L</label>                                                           </div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               <div class="swatch-element size xl available" data-value="XL">                                                               <input class="single-option-selector single-option-selector-quick single-option-selector-recently-view" type="radio" name="SingleOptionSelector-1-7960720179422-recently-view" data-index="option1" value="XL" id="SingleOptionSelector1-XL-7960720179422-recently-view" tabindex="-1">                                                               <label data-toggle="tooltip" data-placement="top" title="XL" for="SingleOptionSelector1-XL-7960720179422-recently-view">XL</label>                                                           </div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               <div class="swatch-element size xxl available" data-value="XXL">                                                               <input class="single-option-selector single-option-selector-quick single-option-selector-recently-view" type="radio" name="SingleOptionSelector-1-7960720179422-recently-view" data-index="option1" value="XXL" id="SingleOptionSelector1-XXL-7960720179422-recently-view" tabindex="-1">                                                               <label data-toggle="tooltip" data-placement="top" title="XXL" for="SingleOptionSelector1-XXL-7960720179422-recently-view">XXL</label>                                                           </div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               <div class="swatch-element size xxxl available" data-value="XXXL">                                                               <input class="single-option-selector single-option-selector-quick single-option-selector-recently-view" type="radio" name="SingleOptionSelector-1-7960720179422-recently-view" data-index="option1" value="XXXL" id="SingleOptionSelector1-XXXL-7960720179422-recently-view" tabindex="-1">                                                               <label data-toggle="tooltip" data-placement="top" title="XXXL" for="SingleOptionSelector1-XXXL-7960720179422-recently-view">XXXL</label>                                                           </div>                                                                                                                                                         </div>                                                                                                                                                        <select name="id" id="ProductSelect-7960720179422-recently-view" class="product-form__variants no-js" tabindex="-1">                                                                                                                                 <option value="44011611128030">                                                   XS                                               </option>                                                                                                                                                                            <option value="44011611160798">                                                   S                                               </option>                                                                                                                                                                            <option value="44011611193566">                                                   M                                               </option>                                                                                                                                                                            <option value="44011611226334">                                                   L                                               </option>                                                                                                                                                                            <option value="44011611259102">                                                   XL                                               </option>                                                                                                                                                                            <option value="44011611291870">                                                   XXL                                               </option>                                                                                                                                                                            <option value="44011611324638">                                                   XXXL                                               </option>                                                                                                                     </select>                                   <div class="product-card__button_cancel_mobile">                                       <a class="btn btn-cancel" data-cancel-swatch-qs="" tabindex="-1"><svg aria-hidden="true" data-prefix="fal" data-icon="times" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" class="svg-inline--fa fa-times fa-w-10 fa-2x"><path fill="currentColor" d="M193.94 256L296.5 153.44l21.15-21.15c3.12-3.12 3.12-8.19 0-11.31l-22.63-22.63c-3.12-3.12-8.19-3.12-11.31 0L160 222.06 36.29 98.34c-3.12-3.12-8.19-3.12-11.31 0L2.34 120.97c-3.12 3.12-3.12 8.19 0 11.31L126.06 256 2.34 379.71c-3.12 3.12-3.12 8.19 0 11.31l22.63 22.63c3.12 3.12 8.19 3.12 11.31 0L160 289.94 262.56 392.5l21.15 21.15c3.12 3.12 8.19 3.12 11.31 0l22.63-22.63c3.12-3.12 3.12-8.19 0-11.31L193.94 256z" class=""></path></svg></a>                                   </div>                                   <div class="product-card__button2">                                       <input type="hidden" name="quantity" value="1" tabindex="-1">                                       <button data-btn-addtocart="" class="btn add-to-cart-btn" type="submit" data-form-id="#recently-7960720179422-qs" tabindex="-1">                                           Submit                                       </button>                                   </div>                                 </form>                               </div>                           </div>                                                                       </div>                                                                                                    </div>         </div>                          <div class="product-bottom">               <div class="wrapper-vendor">                                                                                         <span class="spr-badge" id="spr_badge_7960720179422" data-rating="0.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star-empty" aria-hidden="true"></i><i class="spr-icon spr-icon-star-empty" aria-hidden="true"></i><i class="spr-icon spr-icon-star-empty" aria-hidden="true"></i><i class="spr-icon spr-icon-star-empty" aria-hidden="true"></i><i class="spr-icon spr-icon-star-empty" aria-hidden="true"></i></span><span class="spr-badge-caption">No reviews</span>
</span>
                                                                                   </div>                                                              <a class="product-title change-text" href="/products/single-apple-green-fit-flare-assymetrical-long-dress-with-lace-work" tabindex="-1">                 Apple Green Fit &amp; Flare Assymetrical Long Dress With Lace Work                                                                                       </a>                                                               <div class="price-box">                                         <div class="price-sale">                                                <span class="special-price" data-price-grid=""><span class="money">₹3,044.00</span></span>                         <span class="old-price" data-compare-price-grid=""><span class="money">₹4,348.00</span></span>                                            </div>                                  </div>               <div class="wrapper-size">                                                                                  </div>                        </div>                               </div>     </div>   </div><div class="grid-item col-6 col-md-4 col-lg-3 slick-slide" style="width: 173px;" tabindex="-1" role="option" aria-describedby="slick-slide34" data-slick-index="4" aria-hidden="true">     <div class="inner product-item on-sale" data-product-id="product-7960800264414">       <div class="inner-top">       <div class="product-top">               <div class="product-image image-swap">                   <a href="/products/set-of-3-yellow-sheer-organza-front-tie-up-cape-with-lace-work-kalidar-kurta-narrow-fit-pants" class="product-grid-image" data-collections-related="/collections/?view=related" tabindex="-1">                                                                                          <picture data-index="0">                         <img src="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-309-67_1_ff648e79-e482-4513-a7cd-2b9c4e57c8d3.jpg?v=1677146991" class="images-one" alt="" data-index="0">                       </picture>                       <span class="images-two">                                                                                                        <picture data-index="1">                             <img src="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-309-67_2_7bd07d67-915b-47e3-8594-5c80ce047ee4.jpg?v=1677146991" alt="" data-index="1">                           </picture>                                                                                                    </span>                                                                                      </a>                 </div>                                  <div class="product-label">                                                                                                  <strong class="label sale-label">                       <span>                           Sale                       </span>                     </strong>                                                               <br>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      </div>                                                                                               <a class="wishlist" data-icon-wishlist="" href="#" data-product-handle="set-of-3-yellow-sheer-organza-front-tie-up-cape-with-lace-work-kalidar-kurta-narrow-fit-pants" data-id="7960800264414" tabindex="-1">                                                      <svg viewBox="0 0 512 512"> <g> <g>   <path d="M474.644,74.27C449.391,45.616,414.358,29.836,376,29.836c-53.948,0-88.103,32.22-107.255,59.25     c-4.969,7.014-9.196,14.047-12.745,20.665c-3.549-6.618-7.775-13.651-12.745-20.665c-19.152-27.03-53.307-59.25-107.255-59.25     c-38.358,0-73.391,15.781-98.645,44.435C13.267,101.605,0,138.213,0,177.351c0,42.603,16.633,82.228,52.345,124.7     c31.917,37.96,77.834,77.088,131.005,122.397c19.813,16.884,40.302,34.344,62.115,53.429l0.655,0.574     c2.828,2.476,6.354,3.713,9.88,3.713s7.052-1.238,9.88-3.713l0.655-0.574c21.813-19.085,42.302-36.544,62.118-53.431     c53.168-45.306,99.085-84.434,131.002-122.395C495.367,259.578,512,219.954,512,177.351     C512,138.213,498.733,101.605,474.644,74.27z M309.193,401.614c-17.08,14.554-34.658,29.533-53.193,45.646     c-18.534-16.111-36.113-31.091-53.196-45.648C98.745,312.939,30,254.358,30,177.351c0-31.83,10.605-61.394,29.862-83.245     C79.34,72.007,106.379,59.836,136,59.836c41.129,0,67.716,25.338,82.776,46.594c13.509,19.064,20.558,38.282,22.962,45.659     c2.011,6.175,7.768,10.354,14.262,10.354c6.494,0,12.251-4.179,14.262-10.354c2.404-7.377,9.453-26.595,22.962-45.66     c15.06-21.255,41.647-46.593,82.776-46.593c29.621,0,56.66,12.171,76.137,34.27C471.395,115.957,482,145.521,482,177.351     C482,254.358,413.255,312.939,309.193,401.614z"></path> </g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> </svg>                           <span class="wishlist-text">                               Add to wishlist                           </span>                                                    </a>                                                                                              <a class="quickview-button" href="javascript:void(0)" id="set-of-3-yellow-sheer-organza-front-tie-up-cape-with-lace-work-kalidar-kurta-narrow-fit-pants" tabindex="-1">                                                 <svg viewBox="0 0 511.999 511.999" width="100%" height="100%">     <path d="M508.745,246.041c-4.574-6.257-113.557-153.206-252.748-153.206S7.818,239.784,3.249,246.035         c-4.332,5.936-4.332,13.987,0,19.923c4.569,6.257,113.557,153.206,252.748,153.206s248.174-146.95,252.748-153.201         C513.083,260.028,513.083,251.971,508.745,246.041z M255.997,385.406c-102.529,0-191.33-97.533-217.617-129.418         c26.253-31.913,114.868-129.395,217.617-129.395c102.524,0,191.319,97.516,217.617,129.418         C447.361,287.923,358.746,385.406,255.997,385.406z"></path>       <path d="M255.997,154.725c-55.842,0-101.275,45.433-101.275,101.275s45.433,101.275,101.275,101.275         s101.275-45.433,101.275-101.275S311.839,154.725,255.997,154.725z M255.997,323.516c-37.23,0-67.516-30.287-67.516-67.516         s30.287-67.516,67.516-67.516s67.516,30.287,67.516,67.516S293.227,323.516,255.997,323.516z"></path> </svg>                                                   <span>                               Quick View                             </span>                                             </a>                                                                                 <div class="product-des abs-center">                                                                                    <div class="action">                         <form action="/cart/add" method="post" class="variants" id="recently-7960800264414" data-id="product-actions-7960800264414" enctype="multipart/form-data">                                                                                                                                                                                                                                             <a class="btn" data-href="javascript:void(0)" title="Set Of 3: Yellow Sheer Organza Front Tie Up Cape With Lace Work Kalidar Kurta &amp; Narrow Fit Pants" data-init-quickshop="" tabindex="-1">                                       Add to Cart                                     </a>                                                                                                                                                                                                </form>                                                                             <div class="product-card__variant--popup">                               <div class="product-card__variant--popup--content">                                 <form action="/cart/add" method="post" class="variants" id="recently-7960800264414-qs" data-id="product-actions-7960800264414" enctype="multipart/form-data">                                                                                                                                                                                                                                                                                                                                                                                                                                      <div class="selector-wrapper selector-wrapper-1 swatch js product-form__item" data-option-index="0">                                                   <label class="form-label">                                                       Size:                                                        <span class="label-value-1"></span>                                                   </label>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           <div class="swatch-element size xs available" data-value="XS">                                                               <input class="single-option-selector single-option-selector-quick single-option-selector-recently-view" type="radio" name="SingleOptionSelector-1-7960800264414-recently-view" data-index="option1" value="XS" id="SingleOptionSelector1-XS-7960800264414-recently-view" tabindex="-1">                                                               <label data-toggle="tooltip" data-placement="top" title="XS" for="SingleOptionSelector1-XS-7960800264414-recently-view">XS</label>                                                           </div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               <div class="swatch-element size s available" data-value="S">                                                               <input class="single-option-selector single-option-selector-quick single-option-selector-recently-view" type="radio" name="SingleOptionSelector-1-7960800264414-recently-view" data-index="option1" value="S" id="SingleOptionSelector1-S-7960800264414-recently-view" tabindex="-1">                                                               <label data-toggle="tooltip" data-placement="top" title="S" for="SingleOptionSelector1-S-7960800264414-recently-view">S</label>                                                           </div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               <div class="swatch-element size m available" data-value="M">                                                               <input class="single-option-selector single-option-selector-quick single-option-selector-recently-view" type="radio" name="SingleOptionSelector-1-7960800264414-recently-view" data-index="option1" value="M" id="SingleOptionSelector1-M-7960800264414-recently-view" tabindex="-1">                                                               <label data-toggle="tooltip" data-placement="top" title="M" for="SingleOptionSelector1-M-7960800264414-recently-view">M</label>                                                           </div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               <div class="swatch-element size l available" data-value="L">                                                               <input class="single-option-selector single-option-selector-quick single-option-selector-recently-view" type="radio" name="SingleOptionSelector-1-7960800264414-recently-view" data-index="option1" value="L" id="SingleOptionSelector1-L-7960800264414-recently-view" tabindex="-1">                                                               <label data-toggle="tooltip" data-placement="top" title="L" for="SingleOptionSelector1-L-7960800264414-recently-view">L</label>                                                           </div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               <div class="swatch-element size xl available" data-value="XL">                                                               <input class="single-option-selector single-option-selector-quick single-option-selector-recently-view" type="radio" name="SingleOptionSelector-1-7960800264414-recently-view" data-index="option1" value="XL" id="SingleOptionSelector1-XL-7960800264414-recently-view" tabindex="-1">                                                               <label data-toggle="tooltip" data-placement="top" title="XL" for="SingleOptionSelector1-XL-7960800264414-recently-view">XL</label>                                                           </div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               <div class="swatch-element size xxl available" data-value="XXL">                                                               <input class="single-option-selector single-option-selector-quick single-option-selector-recently-view" type="radio" name="SingleOptionSelector-1-7960800264414-recently-view" data-index="option1" value="XXL" id="SingleOptionSelector1-XXL-7960800264414-recently-view" tabindex="-1">                                                               <label data-toggle="tooltip" data-placement="top" title="XXL" for="SingleOptionSelector1-XXL-7960800264414-recently-view">XXL</label>                                                           </div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               <div class="swatch-element size xxxl available" data-value="XXXL">                                                               <input class="single-option-selector single-option-selector-quick single-option-selector-recently-view" type="radio" name="SingleOptionSelector-1-7960800264414-recently-view" data-index="option1" value="XXXL" id="SingleOptionSelector1-XXXL-7960800264414-recently-view" tabindex="-1">                                                               <label data-toggle="tooltip" data-placement="top" title="XXXL" for="SingleOptionSelector1-XXXL-7960800264414-recently-view">XXXL</label>                                                           </div>                                                                                                                                                         </div>                                                                                                                                                        <select name="id" id="ProductSelect-7960800264414-recently-view" class="product-form__variants no-js" tabindex="-1">                                                                                                                                 <option value="44012027281630">                                                   XS                                               </option>                                                                                                                                                                            <option value="44012027314398">                                                   S                                               </option>                                                                                                                                                                            <option value="44012027347166">                                                   M                                               </option>                                                                                                                                                                            <option value="44012027379934">                                                   L                                               </option>                                                                                                                                                                            <option value="44012027412702">                                                   XL                                               </option>                                                                                                                                                                            <option value="44012027445470">                                                   XXL                                               </option>                                                                                                                                                                            <option value="44012027478238">                                                   XXXL                                               </option>                                                                                                                     </select>                                   <div class="product-card__button_cancel_mobile">                                       <a class="btn btn-cancel" data-cancel-swatch-qs="" tabindex="-1"><svg aria-hidden="true" data-prefix="fal" data-icon="times" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" class="svg-inline--fa fa-times fa-w-10 fa-2x"><path fill="currentColor" d="M193.94 256L296.5 153.44l21.15-21.15c3.12-3.12 3.12-8.19 0-11.31l-22.63-22.63c-3.12-3.12-8.19-3.12-11.31 0L160 222.06 36.29 98.34c-3.12-3.12-8.19-3.12-11.31 0L2.34 120.97c-3.12 3.12-3.12 8.19 0 11.31L126.06 256 2.34 379.71c-3.12 3.12-3.12 8.19 0 11.31l22.63 22.63c3.12 3.12 8.19 3.12 11.31 0L160 289.94 262.56 392.5l21.15 21.15c3.12 3.12 8.19 3.12 11.31 0l22.63-22.63c3.12-3.12 3.12-8.19 0-11.31L193.94 256z" class=""></path></svg></a>                                   </div>                                   <div class="product-card__button2">                                       <input type="hidden" name="quantity" value="1" tabindex="-1">                                       <button data-btn-addtocart="" class="btn add-to-cart-btn" type="submit" data-form-id="#recently-7960800264414-qs" tabindex="-1">                                           Submit                                       </button>                                   </div>                                 </form>                               </div>                           </div>                                                                       </div>                                                                                                    </div>         </div>                          <div class="product-bottom">               <div class="wrapper-vendor">                                                                                         <span class="spr-badge" id="spr_badge_7960800264414" data-rating="0.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star-empty" aria-hidden="true"></i><i class="spr-icon spr-icon-star-empty" aria-hidden="true"></i><i class="spr-icon spr-icon-star-empty" aria-hidden="true"></i><i class="spr-icon spr-icon-star-empty" aria-hidden="true"></i><i class="spr-icon spr-icon-star-empty" aria-hidden="true"></i></span><span class="spr-badge-caption">No reviews</span>
</span>
                                                                                   </div>                                                              <a class="product-title change-text" href="/products/set-of-3-yellow-sheer-organza-front-tie-up-cape-with-lace-work-kalidar-kurta-narrow-fit-pants" tabindex="-1">                 Set Of 3: Yellow Sheer Organza Front Tie Up Cape With Lace Work Kalidar Kurta &amp; Narrow Fit Pants                                                                                       </a>                                                               <div class="price-box">                                         <div class="price-sale">                                                <span class="special-price" data-price-grid=""><span class="money">₹6,959.00</span></span>                         <span class="old-price" data-compare-price-grid=""><span class="money">₹9,941.00</span></span>                                            </div>                                  </div>               <div class="wrapper-size">                                                                                  </div>                        </div>                               </div>     </div>   </div><div class="grid-item col-6 col-md-4 col-lg-3 slick-slide" style="width: 173px;" tabindex="-1" role="option" aria-describedby="slick-slide35" data-slick-index="5" aria-hidden="true">     <div class="inner product-item on-sale" data-product-id="product-7907883483358">       <div class="inner-top">       <div class="product-top">               <div class="product-image image-swap">                   <a href="/products/mahogany-chanderi-brocade-dupatta" class="product-grid-image" data-collections-related="/collections/?view=related" tabindex="-1">                                                                                          <picture data-index="0">                         <img src="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-305-85_1.jpg?v=1671451354" class="images-one" alt="" data-index="0">                       </picture>                       <span class="images-two">                                                                                                        <picture data-index="1">                             <img src="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-305-85_2.jpg?v=1671451354" alt="" data-index="1">                           </picture>                                                                                                    </span>                                                                                      </a>                 </div>                                  <div class="product-label">                                                                                                  <strong class="label sale-label">                       <span>                           Sale                       </span>                     </strong>                                                               <br>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      </div>                                                                                               <a class="wishlist" data-icon-wishlist="" href="#" data-product-handle="mahogany-chanderi-brocade-dupatta" data-id="7907883483358" tabindex="-1">                                                      <svg viewBox="0 0 512 512"> <g> <g>   <path d="M474.644,74.27C449.391,45.616,414.358,29.836,376,29.836c-53.948,0-88.103,32.22-107.255,59.25     c-4.969,7.014-9.196,14.047-12.745,20.665c-3.549-6.618-7.775-13.651-12.745-20.665c-19.152-27.03-53.307-59.25-107.255-59.25     c-38.358,0-73.391,15.781-98.645,44.435C13.267,101.605,0,138.213,0,177.351c0,42.603,16.633,82.228,52.345,124.7     c31.917,37.96,77.834,77.088,131.005,122.397c19.813,16.884,40.302,34.344,62.115,53.429l0.655,0.574     c2.828,2.476,6.354,3.713,9.88,3.713s7.052-1.238,9.88-3.713l0.655-0.574c21.813-19.085,42.302-36.544,62.118-53.431     c53.168-45.306,99.085-84.434,131.002-122.395C495.367,259.578,512,219.954,512,177.351     C512,138.213,498.733,101.605,474.644,74.27z M309.193,401.614c-17.08,14.554-34.658,29.533-53.193,45.646     c-18.534-16.111-36.113-31.091-53.196-45.648C98.745,312.939,30,254.358,30,177.351c0-31.83,10.605-61.394,29.862-83.245     C79.34,72.007,106.379,59.836,136,59.836c41.129,0,67.716,25.338,82.776,46.594c13.509,19.064,20.558,38.282,22.962,45.659     c2.011,6.175,7.768,10.354,14.262,10.354c6.494,0,12.251-4.179,14.262-10.354c2.404-7.377,9.453-26.595,22.962-45.66     c15.06-21.255,41.647-46.593,82.776-46.593c29.621,0,56.66,12.171,76.137,34.27C471.395,115.957,482,145.521,482,177.351     C482,254.358,413.255,312.939,309.193,401.614z"></path> </g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> </svg>                           <span class="wishlist-text">                               Add to wishlist                           </span>                                                    </a>                                                                                              <a class="quickview-button" href="javascript:void(0)" id="mahogany-chanderi-brocade-dupatta" tabindex="-1">                                                 <svg viewBox="0 0 511.999 511.999" width="100%" height="100%">     <path d="M508.745,246.041c-4.574-6.257-113.557-153.206-252.748-153.206S7.818,239.784,3.249,246.035         c-4.332,5.936-4.332,13.987,0,19.923c4.569,6.257,113.557,153.206,252.748,153.206s248.174-146.95,252.748-153.201         C513.083,260.028,513.083,251.971,508.745,246.041z M255.997,385.406c-102.529,0-191.33-97.533-217.617-129.418         c26.253-31.913,114.868-129.395,217.617-129.395c102.524,0,191.319,97.516,217.617,129.418         C447.361,287.923,358.746,385.406,255.997,385.406z"></path>       <path d="M255.997,154.725c-55.842,0-101.275,45.433-101.275,101.275s45.433,101.275,101.275,101.275         s101.275-45.433,101.275-101.275S311.839,154.725,255.997,154.725z M255.997,323.516c-37.23,0-67.516-30.287-67.516-67.516         s30.287-67.516,67.516-67.516s67.516,30.287,67.516,67.516S293.227,323.516,255.997,323.516z"></path> </svg>                                                   <span>                               Quick View                             </span>                                             </a>                                                                                 <div class="product-des abs-center">                                                                                    <div class="action">                         <form action="/cart/add" method="post" class="variants" id="recently-7907883483358" data-id="product-actions-7907883483358" enctype="multipart/form-data">                                                            <input type="hidden" name="id" value="43778989490398" tabindex="-1">                                                                                                                                                                       <button data-btn-addtocart="" class="btn add-to-cart-btn" type="submit" data-form-id="#recently-7907883483358" tabindex="-1">                                       Add to Cart                                     </button>                                                                                                                                                                                      </form>                                                                                                </div>                                                                                                    </div>         </div>                          <div class="product-bottom">               <div class="wrapper-vendor">                                                                                         <span class="spr-badge" id="spr_badge_7907883483358" data-rating="0.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star-empty" aria-hidden="true"></i><i class="spr-icon spr-icon-star-empty" aria-hidden="true"></i><i class="spr-icon spr-icon-star-empty" aria-hidden="true"></i><i class="spr-icon spr-icon-star-empty" aria-hidden="true"></i><i class="spr-icon spr-icon-star-empty" aria-hidden="true"></i></span><span class="spr-badge-caption">No reviews</span>
</span>
                                                                                   </div>                                                              <a class="product-title change-text" href="/products/mahogany-chanderi-brocade-dupatta" tabindex="-1">                 Mahogany Chanderi Brocade Dupatta                                                                                       </a>                                                               <div class="price-box">                                         <div class="price-sale">                                                <span class="special-price" data-price-grid=""><span class="money">₹3,044.00</span></span>                         <span class="old-price" data-compare-price-grid=""><span class="money">₹4,348.00</span></span>                                            </div>                                  </div>               <div class="wrapper-size">                                                                                  </div>                        </div>                               </div>     </div>   </div><div class="grid-item col-6 col-md-4 col-lg-3 slick-slide" style="width: 173px;" tabindex="-1" role="option" aria-describedby="slick-slide36" data-slick-index="6" aria-hidden="true">     <div class="inner product-item on-sale" data-product-id="product-7907884695774">       <div class="inner-top">       <div class="product-top">               <div class="product-image image-swap">                   <a href="/products/sand-gold-chanderi-brocade-dupatta" class="product-grid-image" data-collections-related="/collections/?view=related" tabindex="-1">                                                                                          <picture data-index="0">                         <img src="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-305-119_1.jpg?v=1671451508" class="images-one" alt="" data-index="0">                       </picture>                       <span class="images-two">                                                                                                        <picture data-index="1">                             <img src="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-305-119_2.jpg?v=1671451508" alt="" data-index="1">                           </picture>                                                                                                    </span>                                                                                      </a>                 </div>                                  <div class="product-label">                                                                                                  <strong class="label sale-label">                       <span>                           Sale                       </span>                     </strong>                                                               <br>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      </div>                                                                                               <a class="wishlist" data-icon-wishlist="" href="#" data-product-handle="sand-gold-chanderi-brocade-dupatta" data-id="7907884695774" tabindex="-1">                                                      <svg viewBox="0 0 512 512"> <g> <g>   <path d="M474.644,74.27C449.391,45.616,414.358,29.836,376,29.836c-53.948,0-88.103,32.22-107.255,59.25     c-4.969,7.014-9.196,14.047-12.745,20.665c-3.549-6.618-7.775-13.651-12.745-20.665c-19.152-27.03-53.307-59.25-107.255-59.25     c-38.358,0-73.391,15.781-98.645,44.435C13.267,101.605,0,138.213,0,177.351c0,42.603,16.633,82.228,52.345,124.7     c31.917,37.96,77.834,77.088,131.005,122.397c19.813,16.884,40.302,34.344,62.115,53.429l0.655,0.574     c2.828,2.476,6.354,3.713,9.88,3.713s7.052-1.238,9.88-3.713l0.655-0.574c21.813-19.085,42.302-36.544,62.118-53.431     c53.168-45.306,99.085-84.434,131.002-122.395C495.367,259.578,512,219.954,512,177.351     C512,138.213,498.733,101.605,474.644,74.27z M309.193,401.614c-17.08,14.554-34.658,29.533-53.193,45.646     c-18.534-16.111-36.113-31.091-53.196-45.648C98.745,312.939,30,254.358,30,177.351c0-31.83,10.605-61.394,29.862-83.245     C79.34,72.007,106.379,59.836,136,59.836c41.129,0,67.716,25.338,82.776,46.594c13.509,19.064,20.558,38.282,22.962,45.659     c2.011,6.175,7.768,10.354,14.262,10.354c6.494,0,12.251-4.179,14.262-10.354c2.404-7.377,9.453-26.595,22.962-45.66     c15.06-21.255,41.647-46.593,82.776-46.593c29.621,0,56.66,12.171,76.137,34.27C471.395,115.957,482,145.521,482,177.351     C482,254.358,413.255,312.939,309.193,401.614z"></path> </g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> </svg>                           <span class="wishlist-text">                               Add to wishlist                           </span>                                                    </a>                                                                                              <a class="quickview-button" href="javascript:void(0)" id="sand-gold-chanderi-brocade-dupatta" tabindex="-1">                                                 <svg viewBox="0 0 511.999 511.999" width="100%" height="100%">     <path d="M508.745,246.041c-4.574-6.257-113.557-153.206-252.748-153.206S7.818,239.784,3.249,246.035         c-4.332,5.936-4.332,13.987,0,19.923c4.569,6.257,113.557,153.206,252.748,153.206s248.174-146.95,252.748-153.201         C513.083,260.028,513.083,251.971,508.745,246.041z M255.997,385.406c-102.529,0-191.33-97.533-217.617-129.418         c26.253-31.913,114.868-129.395,217.617-129.395c102.524,0,191.319,97.516,217.617,129.418         C447.361,287.923,358.746,385.406,255.997,385.406z"></path>       <path d="M255.997,154.725c-55.842,0-101.275,45.433-101.275,101.275s45.433,101.275,101.275,101.275         s101.275-45.433,101.275-101.275S311.839,154.725,255.997,154.725z M255.997,323.516c-37.23,0-67.516-30.287-67.516-67.516         s30.287-67.516,67.516-67.516s67.516,30.287,67.516,67.516S293.227,323.516,255.997,323.516z"></path> </svg>                                                   <span>                               Quick View                             </span>                                             </a>                                                                                 <div class="product-des abs-center">                                                                                    <div class="action">                         <form action="/cart/add" method="post" class="variants" id="recently-7907884695774" data-id="product-actions-7907884695774" enctype="multipart/form-data">                                                            <input type="hidden" name="id" value="43779002532062" tabindex="-1">                                                                                                                                                                       <button data-btn-addtocart="" class="btn add-to-cart-btn" type="submit" data-form-id="#recently-7907884695774" tabindex="-1">                                       Add to Cart                                     </button>                                                                                                                                                                                      </form>                                                                                                </div>                                                                                                    </div>         </div>                          <div class="product-bottom">               <div class="wrapper-vendor">                                                                                         <span class="spr-badge" id="spr_badge_7907884695774" data-rating="0.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star-empty" aria-hidden="true"></i><i class="spr-icon spr-icon-star-empty" aria-hidden="true"></i><i class="spr-icon spr-icon-star-empty" aria-hidden="true"></i><i class="spr-icon spr-icon-star-empty" aria-hidden="true"></i><i class="spr-icon spr-icon-star-empty" aria-hidden="true"></i></span><span class="spr-badge-caption">No reviews</span>
</span>
                                                                                   </div>                                                              <a class="product-title change-text" href="/products/sand-gold-chanderi-brocade-dupatta" tabindex="-1">                 Sand Gold Chanderi Brocade Dupatta                                                                                       </a>                                                               <div class="price-box">                                         <div class="price-sale">                                                <span class="special-price" data-price-grid=""><span class="money">₹3,044.00</span></span>                         <span class="old-price" data-compare-price-grid=""><span class="money">₹4,348.00</span></span>                                            </div>                                  </div>               <div class="wrapper-size">                                                                                  </div>                        </div>                               </div>     </div>   </div><div class="grid-item col-6 col-md-4 col-lg-3 slick-slide" style="width: 173px;" tabindex="-1" role="option" aria-describedby="slick-slide37" data-slick-index="7" aria-hidden="true">     <div class="inner product-item on-sale" data-product-id="product-7797640134878">       <div class="inner-top">       <div class="product-top">               <div class="product-image image-swap">                   <a href="/products/kalamkari-block-printed-cotton-slip" class="product-grid-image" data-collections-related="/collections/?view=related" tabindex="-1">                                                                                          <picture data-index="0">                         <img src="//cdn.shopify.com/s/files/1/0613/2039/7022/products/278376-2019-07-22-11_26_37.291355-big-image.jpg?v=1663261675" class="images-one" alt="" data-index="0">                       </picture>                       <span class="images-two">                                                                                                        <picture data-index="1">                             <img src="//cdn.shopify.com/s/files/1/0613/2039/7022/products/278377-2019-07-22-11_26_37.610582-big-image.jpg?v=1663261675" alt="" data-index="1">                           </picture>                                                                                                    </span>                                                                                      </a>                 </div>                                  <div class="product-label">                                                                                                  <strong class="label sale-label">                       <span>                           Sale                       </span>                     </strong>                                                               <br>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          </div>                                                                                               <a class="wishlist" data-icon-wishlist="" href="#" data-product-handle="kalamkari-block-printed-cotton-slip" data-id="7797640134878" tabindex="-1">                                                      <svg viewBox="0 0 512 512"> <g> <g>   <path d="M474.644,74.27C449.391,45.616,414.358,29.836,376,29.836c-53.948,0-88.103,32.22-107.255,59.25     c-4.969,7.014-9.196,14.047-12.745,20.665c-3.549-6.618-7.775-13.651-12.745-20.665c-19.152-27.03-53.307-59.25-107.255-59.25     c-38.358,0-73.391,15.781-98.645,44.435C13.267,101.605,0,138.213,0,177.351c0,42.603,16.633,82.228,52.345,124.7     c31.917,37.96,77.834,77.088,131.005,122.397c19.813,16.884,40.302,34.344,62.115,53.429l0.655,0.574     c2.828,2.476,6.354,3.713,9.88,3.713s7.052-1.238,9.88-3.713l0.655-0.574c21.813-19.085,42.302-36.544,62.118-53.431     c53.168-45.306,99.085-84.434,131.002-122.395C495.367,259.578,512,219.954,512,177.351     C512,138.213,498.733,101.605,474.644,74.27z M309.193,401.614c-17.08,14.554-34.658,29.533-53.193,45.646     c-18.534-16.111-36.113-31.091-53.196-45.648C98.745,312.939,30,254.358,30,177.351c0-31.83,10.605-61.394,29.862-83.245     C79.34,72.007,106.379,59.836,136,59.836c41.129,0,67.716,25.338,82.776,46.594c13.509,19.064,20.558,38.282,22.962,45.659     c2.011,6.175,7.768,10.354,14.262,10.354c6.494,0,12.251-4.179,14.262-10.354c2.404-7.377,9.453-26.595,22.962-45.66     c15.06-21.255,41.647-46.593,82.776-46.593c29.621,0,56.66,12.171,76.137,34.27C471.395,115.957,482,145.521,482,177.351     C482,254.358,413.255,312.939,309.193,401.614z"></path> </g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> </svg>                           <span class="wishlist-text">                               Add to wishlist                           </span>                                                    </a>                                                                                              <a class="quickview-button" href="javascript:void(0)" id="kalamkari-block-printed-cotton-slip" tabindex="-1">                                                 <svg viewBox="0 0 511.999 511.999" width="100%" height="100%">     <path d="M508.745,246.041c-4.574-6.257-113.557-153.206-252.748-153.206S7.818,239.784,3.249,246.035         c-4.332,5.936-4.332,13.987,0,19.923c4.569,6.257,113.557,153.206,252.748,153.206s248.174-146.95,252.748-153.201         C513.083,260.028,513.083,251.971,508.745,246.041z M255.997,385.406c-102.529,0-191.33-97.533-217.617-129.418         c26.253-31.913,114.868-129.395,217.617-129.395c102.524,0,191.319,97.516,217.617,129.418         C447.361,287.923,358.746,385.406,255.997,385.406z"></path>       <path d="M255.997,154.725c-55.842,0-101.275,45.433-101.275,101.275s45.433,101.275,101.275,101.275         s101.275-45.433,101.275-101.275S311.839,154.725,255.997,154.725z M255.997,323.516c-37.23,0-67.516-30.287-67.516-67.516         s30.287-67.516,67.516-67.516s67.516,30.287,67.516,67.516S293.227,323.516,255.997,323.516z"></path> </svg>                                                   <span>                               Quick View                             </span>                                             </a>                                                                                 <div class="product-des abs-center">                                                                                    <div class="action">                         <form action="/cart/add" method="post" class="variants" id="recently-7797640134878" data-id="product-actions-7797640134878" enctype="multipart/form-data">                                                                                                                                                                                                                                             <a class="btn" data-href="javascript:void(0)" title="Kalamkari  Cotton Slip" data-init-quickshop="" tabindex="-1">                                       Add to Cart                                     </a>                                                                                                                                                                                                </form>                                                                             <div class="product-card__variant--popup">                               <div class="product-card__variant--popup--content">                                 <form action="/cart/add" method="post" class="variants" id="recently-7797640134878-qs" data-id="product-actions-7797640134878" enctype="multipart/form-data">                                                                                                                                                                                                                                                                                                                                                                                                                                      <div class="selector-wrapper selector-wrapper-1 swatch js product-form__item" data-option-index="0">                                                   <label class="form-label">                                                       Size:                                                        <span class="label-value-1"></span>                                                   </label>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           <div class="swatch-element size xs available" data-value="XS">                                                               <input class="single-option-selector single-option-selector-quick single-option-selector-recently-view" type="radio" name="SingleOptionSelector-1-7797640134878-recently-view" data-index="option1" value="XS" id="SingleOptionSelector1-XS-7797640134878-recently-view" tabindex="-1">                                                               <label data-toggle="tooltip" data-placement="top" title="XS" for="SingleOptionSelector1-XS-7797640134878-recently-view">XS</label>                                                           </div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               <div class="swatch-element size s available" data-value="S">                                                               <input class="single-option-selector single-option-selector-quick single-option-selector-recently-view" type="radio" name="SingleOptionSelector-1-7797640134878-recently-view" data-index="option1" value="S" id="SingleOptionSelector1-S-7797640134878-recently-view" tabindex="-1">                                                               <label data-toggle="tooltip" data-placement="top" title="S" for="SingleOptionSelector1-S-7797640134878-recently-view">S</label>                                                           </div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               <div class="swatch-element size m available" data-value="M">                                                               <input class="single-option-selector single-option-selector-quick single-option-selector-recently-view" type="radio" name="SingleOptionSelector-1-7797640134878-recently-view" data-index="option1" value="M" id="SingleOptionSelector1-M-7797640134878-recently-view" tabindex="-1">                                                               <label data-toggle="tooltip" data-placement="top" title="M" for="SingleOptionSelector1-M-7797640134878-recently-view">M</label>                                                           </div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               <div class="swatch-element size l available" data-value="L">                                                               <input class="single-option-selector single-option-selector-quick single-option-selector-recently-view" type="radio" name="SingleOptionSelector-1-7797640134878-recently-view" data-index="option1" value="L" id="SingleOptionSelector1-L-7797640134878-recently-view" tabindex="-1">                                                               <label data-toggle="tooltip" data-placement="top" title="L" for="SingleOptionSelector1-L-7797640134878-recently-view">L</label>                                                           </div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               <div class="swatch-element size xl available" data-value="XL">                                                               <input class="single-option-selector single-option-selector-quick single-option-selector-recently-view" type="radio" name="SingleOptionSelector-1-7797640134878-recently-view" data-index="option1" value="XL" id="SingleOptionSelector1-XL-7797640134878-recently-view" tabindex="-1">                                                               <label data-toggle="tooltip" data-placement="top" title="XL" for="SingleOptionSelector1-XL-7797640134878-recently-view">XL</label>                                                           </div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               <div class="swatch-element size xxl available" data-value="XXL">                                                               <input class="single-option-selector single-option-selector-quick single-option-selector-recently-view" type="radio" name="SingleOptionSelector-1-7797640134878-recently-view" data-index="option1" value="XXL" id="SingleOptionSelector1-XXL-7797640134878-recently-view" tabindex="-1">                                                               <label data-toggle="tooltip" data-placement="top" title="XXL" for="SingleOptionSelector1-XXL-7797640134878-recently-view">XXL</label>                                                           </div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               <div class="swatch-element size xxxl available" data-value="XXXL">                                                               <input class="single-option-selector single-option-selector-quick single-option-selector-recently-view" type="radio" name="SingleOptionSelector-1-7797640134878-recently-view" data-index="option1" value="XXXL" id="SingleOptionSelector1-XXXL-7797640134878-recently-view" tabindex="-1">                                                               <label data-toggle="tooltip" data-placement="top" title="XXXL" for="SingleOptionSelector1-XXXL-7797640134878-recently-view">XXXL</label>                                                           </div>                                                                                                                                                         </div>                                                                                                                                                        <select name="id" id="ProductSelect-7797640134878-recently-view" class="product-form__variants no-js" tabindex="-1">                                                                                                                                 <option value="43398576046302">                                                   XS                                               </option>                                                                                                                                                                            <option value="43398576079070">                                                   S                                               </option>                                                                                                                                                                            <option value="43398576111838">                                                   M                                               </option>                                                                                                                                                                            <option value="43398576144606">                                                   L                                               </option>                                                                                                                                                                            <option value="43398576177374">                                                   XL                                               </option>                                                                                                                                                                            <option value="43398576210142">                                                   XXL                                               </option>                                                                                                                                                                            <option value="43398576242910">                                                   XXXL                                               </option>                                                                                                                     </select>                                   <div class="product-card__button_cancel_mobile">                                       <a class="btn btn-cancel" data-cancel-swatch-qs="" tabindex="-1"><svg aria-hidden="true" data-prefix="fal" data-icon="times" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" class="svg-inline--fa fa-times fa-w-10 fa-2x"><path fill="currentColor" d="M193.94 256L296.5 153.44l21.15-21.15c3.12-3.12 3.12-8.19 0-11.31l-22.63-22.63c-3.12-3.12-8.19-3.12-11.31 0L160 222.06 36.29 98.34c-3.12-3.12-8.19-3.12-11.31 0L2.34 120.97c-3.12 3.12-3.12 8.19 0 11.31L126.06 256 2.34 379.71c-3.12 3.12-3.12 8.19 0 11.31l22.63 22.63c3.12 3.12 8.19 3.12 11.31 0L160 289.94 262.56 392.5l21.15 21.15c3.12 3.12 8.19 3.12 11.31 0l22.63-22.63c3.12-3.12 3.12-8.19 0-11.31L193.94 256z" class=""></path></svg></a>                                   </div>                                   <div class="product-card__button2">                                       <input type="hidden" name="quantity" value="1" tabindex="-1">                                       <button data-btn-addtocart="" class="btn add-to-cart-btn" type="submit" data-form-id="#recently-7797640134878-qs" tabindex="-1">                                           Submit                                       </button>                                   </div>                                 </form>                               </div>                           </div>                                                                       </div>                                                                                                    </div>         </div>                          <div class="product-bottom">               <div class="wrapper-vendor">                                                                                         <span class="spr-badge" id="spr_badge_7797640134878" data-rating="4.833333333333333"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star" aria-hidden="true"></i><i class="spr-icon spr-icon-star" aria-hidden="true"></i><i class="spr-icon spr-icon-star" aria-hidden="true"></i><i class="spr-icon spr-icon-star" aria-hidden="true"></i><i class="spr-icon spr-icon-star" aria-hidden="true"></i></span><span class="spr-badge-caption">6 reviews</span>
</span>
                                                                                   </div>                                                              <a class="product-title change-text" href="/products/kalamkari-block-printed-cotton-slip" tabindex="-1">                 Kalamkari  Cotton Slip                                                                                       </a>                                                               <div class="price-box">                                         <div class="price-sale">                                                <span class="special-price" data-price-grid=""><span class="money">₹1,912.00</span></span>                         <span class="old-price" data-compare-price-grid=""><span class="money">₹2,732.00</span></span>                                            </div>                                  </div>               <div class="wrapper-size">                                                                                  </div>                        </div>                               </div>     </div>   </div><div class="grid-item col-6 col-md-4 col-lg-3 slick-slide slick-cloned" style="width: 173px;" tabindex="-1" role="option" aria-describedby="slick-slide30" data-slick-index="8" aria-hidden="true">     <div class="inner product-item on-sale" data-product-id="product-7798045671646">       <div class="inner-top">       <div class="product-top">               <div class="product-image image-swap">                   <a href="/products/maroon-kota-tassel-embellished-saree" class="product-grid-image" data-collections-related="/collections/?view=related" tabindex="-1">                                                                                          <picture data-index="0">                         <img src="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-298-55_1.jpg?v=1663268065" class="images-one" alt="" data-index="0">                       </picture>                       <span class="images-two">                                                                                                        <picture data-index="1">                             <img src="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-298-55_2.jpg?v=1663268065" alt="" data-index="1">                           </picture>                                                                                                    </span>                                                                                      </a>                 </div>                                  <div class="product-label">                                                                                                  <strong class="label sale-label">                       <span>                           Sale                       </span>                     </strong>                                                               <br>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          </div>                                                                                               <a class="wishlist" data-icon-wishlist="" href="#" data-product-handle="maroon-kota-tassel-embellished-saree" data-id="7798045671646" tabindex="-1">                                                      <svg viewBox="0 0 512 512"> <g> <g>   <path d="M474.644,74.27C449.391,45.616,414.358,29.836,376,29.836c-53.948,0-88.103,32.22-107.255,59.25     c-4.969,7.014-9.196,14.047-12.745,20.665c-3.549-6.618-7.775-13.651-12.745-20.665c-19.152-27.03-53.307-59.25-107.255-59.25     c-38.358,0-73.391,15.781-98.645,44.435C13.267,101.605,0,138.213,0,177.351c0,42.603,16.633,82.228,52.345,124.7     c31.917,37.96,77.834,77.088,131.005,122.397c19.813,16.884,40.302,34.344,62.115,53.429l0.655,0.574     c2.828,2.476,6.354,3.713,9.88,3.713s7.052-1.238,9.88-3.713l0.655-0.574c21.813-19.085,42.302-36.544,62.118-53.431     c53.168-45.306,99.085-84.434,131.002-122.395C495.367,259.578,512,219.954,512,177.351     C512,138.213,498.733,101.605,474.644,74.27z M309.193,401.614c-17.08,14.554-34.658,29.533-53.193,45.646     c-18.534-16.111-36.113-31.091-53.196-45.648C98.745,312.939,30,254.358,30,177.351c0-31.83,10.605-61.394,29.862-83.245     C79.34,72.007,106.379,59.836,136,59.836c41.129,0,67.716,25.338,82.776,46.594c13.509,19.064,20.558,38.282,22.962,45.659     c2.011,6.175,7.768,10.354,14.262,10.354c6.494,0,12.251-4.179,14.262-10.354c2.404-7.377,9.453-26.595,22.962-45.66     c15.06-21.255,41.647-46.593,82.776-46.593c29.621,0,56.66,12.171,76.137,34.27C471.395,115.957,482,145.521,482,177.351     C482,254.358,413.255,312.939,309.193,401.614z"></path> </g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> </svg>                           <span class="wishlist-text">                               Add to wishlist                           </span>                                                    </a>                                                                                              <a class="quickview-button" href="javascript:void(0)" id="" tabindex="-1">                                                 <svg viewBox="0 0 511.999 511.999" width="100%" height="100%">     <path d="M508.745,246.041c-4.574-6.257-113.557-153.206-252.748-153.206S7.818,239.784,3.249,246.035         c-4.332,5.936-4.332,13.987,0,19.923c4.569,6.257,113.557,153.206,252.748,153.206s248.174-146.95,252.748-153.201         C513.083,260.028,513.083,251.971,508.745,246.041z M255.997,385.406c-102.529,0-191.33-97.533-217.617-129.418         c26.253-31.913,114.868-129.395,217.617-129.395c102.524,0,191.319,97.516,217.617,129.418         C447.361,287.923,358.746,385.406,255.997,385.406z"></path>       <path d="M255.997,154.725c-55.842,0-101.275,45.433-101.275,101.275s45.433,101.275,101.275,101.275         s101.275-45.433,101.275-101.275S311.839,154.725,255.997,154.725z M255.997,323.516c-37.23,0-67.516-30.287-67.516-67.516         s30.287-67.516,67.516-67.516s67.516,30.287,67.516,67.516S293.227,323.516,255.997,323.516z"></path> </svg>                                                   <span>                               Quick View                             </span>                                             </a>                                                                                 <div class="product-des abs-center">                                                                                    <div class="action">                         <form action="/cart/add" method="post" class="variants" id="" data-id="product-actions-7798045671646" enctype="multipart/form-data">                                                                                                                                                                                                                                             <a class="btn" data-href="javascript:void(0)" title="Maroon kota tassel embellished saree" data-init-quickshop="" tabindex="-1">                                       Add to Cart                                     </a>                                                                                                                                                                                                </form>                                                                             <div class="product-card__variant--popup">                               <div class="product-card__variant--popup--content">                                 <form action="/cart/add" method="post" class="variants" id="" data-id="product-actions-7798045671646" enctype="multipart/form-data">                                                                                                                                                                                                                                                                                                                                                                                                                                      <div class="selector-wrapper selector-wrapper-1 swatch js product-form__item" data-option-index="0">                                                   <label class="form-label">                                                       Size:                                                        <span class="label-value-1"></span>                                                   </label>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           <div class="swatch-element size xs available" data-value="XS">                                                               <input class="single-option-selector single-option-selector-quick single-option-selector-recently-view" type="radio" name="SingleOptionSelector-1-7798045671646-recently-view" data-index="option1" value="XS" id="" tabindex="-1">                                                               <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector1-XS-7798045671646-recently-view" data-original-title="XS">XS</label>                                                           </div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               <div class="swatch-element size s available" data-value="S">                                                               <input class="single-option-selector single-option-selector-quick single-option-selector-recently-view" type="radio" name="SingleOptionSelector-1-7798045671646-recently-view" data-index="option1" value="S" id="" tabindex="-1">                                                               <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector1-S-7798045671646-recently-view" data-original-title="S">S</label>                                                           </div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               <div class="swatch-element size m available" data-value="M">                                                               <input class="single-option-selector single-option-selector-quick single-option-selector-recently-view" type="radio" name="SingleOptionSelector-1-7798045671646-recently-view" data-index="option1" value="M" id="" tabindex="-1">                                                               <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector1-M-7798045671646-recently-view" data-original-title="M">M</label>                                                           </div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               <div class="swatch-element size l available" data-value="L">                                                               <input class="single-option-selector single-option-selector-quick single-option-selector-recently-view" type="radio" name="SingleOptionSelector-1-7798045671646-recently-view" data-index="option1" value="L" id="" tabindex="-1">                                                               <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector1-L-7798045671646-recently-view" data-original-title="L">L</label>                                                           </div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               <div class="swatch-element size xl available" data-value="XL">                                                               <input class="single-option-selector single-option-selector-quick single-option-selector-recently-view" type="radio" name="SingleOptionSelector-1-7798045671646-recently-view" data-index="option1" value="XL" id="" tabindex="-1">                                                               <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector1-XL-7798045671646-recently-view" data-original-title="XL">XL</label>                                                           </div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               <div class="swatch-element size xxl available" data-value="XXL">                                                               <input class="single-option-selector single-option-selector-quick single-option-selector-recently-view" type="radio" name="SingleOptionSelector-1-7798045671646-recently-view" data-index="option1" value="XXL" id="" tabindex="-1">                                                               <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector1-XXL-7798045671646-recently-view" data-original-title="XXL">XXL</label>                                                           </div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               <div class="swatch-element size xxxl available" data-value="XXXL">                                                               <input class="single-option-selector single-option-selector-quick single-option-selector-recently-view" type="radio" name="SingleOptionSelector-1-7798045671646-recently-view" data-index="option1" value="XXXL" id="" tabindex="-1">                                                               <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector1-XXXL-7798045671646-recently-view" data-original-title="XXXL">XXXL</label>                                                           </div>                                                                                                                                                         </div>                                                                                                                                                        <select name="id" id="" class="product-form__variants no-js" tabindex="-1">                                                                                                                                 <option value="43400227389662">                                                   XS                                               </option>                                                                                                                                                                            <option value="43400227422430">                                                   S                                               </option>                                                                                                                                                                            <option value="43400227455198">                                                   M                                               </option>                                                                                                                                                                            <option value="43400227487966">                                                   L                                               </option>                                                                                                                                                                            <option value="43400227520734">                                                   XL                                               </option>                                                                                                                                                                            <option value="43400227553502">                                                   XXL                                               </option>                                                                                                                                                                            <option value="43400227586270">                                                   XXXL                                               </option>                                                                                                                     </select>                                   <div class="product-card__button_cancel_mobile">                                       <a class="btn btn-cancel" data-cancel-swatch-qs="" tabindex="-1"><svg aria-hidden="true" data-prefix="fal" data-icon="times" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" class="svg-inline--fa fa-times fa-w-10 fa-2x"><path fill="currentColor" d="M193.94 256L296.5 153.44l21.15-21.15c3.12-3.12 3.12-8.19 0-11.31l-22.63-22.63c-3.12-3.12-8.19-3.12-11.31 0L160 222.06 36.29 98.34c-3.12-3.12-8.19-3.12-11.31 0L2.34 120.97c-3.12 3.12-3.12 8.19 0 11.31L126.06 256 2.34 379.71c-3.12 3.12-3.12 8.19 0 11.31l22.63 22.63c3.12 3.12 8.19 3.12 11.31 0L160 289.94 262.56 392.5l21.15 21.15c3.12 3.12 8.19 3.12 11.31 0l22.63-22.63c3.12-3.12 3.12-8.19 0-11.31L193.94 256z" class=""></path></svg></a>                                   </div>                                   <div class="product-card__button2">                                       <input type="hidden" name="quantity" value="1" tabindex="-1">                                       <button data-btn-addtocart="" class="btn add-to-cart-btn" type="submit" data-form-id="#recently-7798045671646-qs" tabindex="-1">                                           Submit                                       </button>                                   </div>                                 </form>                               </div>                           </div>                                                                       </div>                                                                                                    </div>         </div>                          <div class="product-bottom">               <div class="wrapper-vendor">                                                                                         <span class="spr-badge" id="" data-rating="0.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star-empty" aria-hidden="true"></i><i class="spr-icon spr-icon-star-empty" aria-hidden="true"></i><i class="spr-icon spr-icon-star-empty" aria-hidden="true"></i><i class="spr-icon spr-icon-star-empty" aria-hidden="true"></i><i class="spr-icon spr-icon-star-empty" aria-hidden="true"></i></span><span class="spr-badge-caption">No reviews</span>
</span>
                                                                                   </div>                                                              <a class="product-title change-text" href="/products/maroon-kota-tassel-embellished-saree" tabindex="-1">                 Maroon kota tassel embellished saree                                                                                       </a>                                                               <div class="price-box">                                         <div class="price-sale">                                                <span class="special-price" data-price-grid=""><span class="money">₹1,884.00</span></span>                         <span class="old-price" data-compare-price-grid=""><span class="money">₹2,691.00</span></span>                                            </div>                                  </div>               <div class="wrapper-size">                                                                                  </div>                        </div>                               </div>     </div>   </div><div class="grid-item col-6 col-md-4 col-lg-3 slick-slide slick-cloned" style="width: 173px;" tabindex="-1" role="option" aria-describedby="slick-slide31" data-slick-index="9" aria-hidden="true">     <div class="inner product-item on-sale" data-product-id="product-7797710979294">       <div class="inner-top">       <div class="product-top">               <div class="product-image image-swap">                   <a href="/products/set-of-2-lilac-purple-handmade-crochia-backless-bralette-crop-top-blouse-with-cotton-ikkat-saree" class="product-grid-image" data-collections-related="/collections/?view=related" tabindex="-1">                                                                                          <picture data-index="0">                         <img src="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-298-10_1.jpg?v=1663268035" class="images-one" alt="" data-index="0">                       </picture>                       <span class="images-two">                                                                                                        <picture data-index="1">                             <img src="//cdn.shopify.com/s/files/1/0613/2039/7022/products/TJ-MK-298-10_2.jpg?v=1663268035" alt="" data-index="1">                           </picture>                                                                                                    </span>                                                                                      </a>                 </div>                                  <div class="product-label">                                                                                                  <strong class="label sale-label">                       <span>                           Sale                       </span>                     </strong>                                                               <br>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              </div>                                                                                               <a class="wishlist" data-icon-wishlist="" href="#" data-product-handle="set-of-2-lilac-purple-handmade-crochia-backless-bralette-crop-top-blouse-with-cotton-ikkat-saree" data-id="7797710979294" tabindex="-1">                                                      <svg viewBox="0 0 512 512"> <g> <g>   <path d="M474.644,74.27C449.391,45.616,414.358,29.836,376,29.836c-53.948,0-88.103,32.22-107.255,59.25     c-4.969,7.014-9.196,14.047-12.745,20.665c-3.549-6.618-7.775-13.651-12.745-20.665c-19.152-27.03-53.307-59.25-107.255-59.25     c-38.358,0-73.391,15.781-98.645,44.435C13.267,101.605,0,138.213,0,177.351c0,42.603,16.633,82.228,52.345,124.7     c31.917,37.96,77.834,77.088,131.005,122.397c19.813,16.884,40.302,34.344,62.115,53.429l0.655,0.574     c2.828,2.476,6.354,3.713,9.88,3.713s7.052-1.238,9.88-3.713l0.655-0.574c21.813-19.085,42.302-36.544,62.118-53.431     c53.168-45.306,99.085-84.434,131.002-122.395C495.367,259.578,512,219.954,512,177.351     C512,138.213,498.733,101.605,474.644,74.27z M309.193,401.614c-17.08,14.554-34.658,29.533-53.193,45.646     c-18.534-16.111-36.113-31.091-53.196-45.648C98.745,312.939,30,254.358,30,177.351c0-31.83,10.605-61.394,29.862-83.245     C79.34,72.007,106.379,59.836,136,59.836c41.129,0,67.716,25.338,82.776,46.594c13.509,19.064,20.558,38.282,22.962,45.659     c2.011,6.175,7.768,10.354,14.262,10.354c6.494,0,12.251-4.179,14.262-10.354c2.404-7.377,9.453-26.595,22.962-45.66     c15.06-21.255,41.647-46.593,82.776-46.593c29.621,0,56.66,12.171,76.137,34.27C471.395,115.957,482,145.521,482,177.351     C482,254.358,413.255,312.939,309.193,401.614z"></path> </g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> </svg>                           <span class="wishlist-text">                               Add to wishlist                           </span>                                                    </a>                                                                                              <a class="quickview-button" href="javascript:void(0)" id="" tabindex="-1">                                                 <svg viewBox="0 0 511.999 511.999" width="100%" height="100%">     <path d="M508.745,246.041c-4.574-6.257-113.557-153.206-252.748-153.206S7.818,239.784,3.249,246.035         c-4.332,5.936-4.332,13.987,0,19.923c4.569,6.257,113.557,153.206,252.748,153.206s248.174-146.95,252.748-153.201         C513.083,260.028,513.083,251.971,508.745,246.041z M255.997,385.406c-102.529,0-191.33-97.533-217.617-129.418         c26.253-31.913,114.868-129.395,217.617-129.395c102.524,0,191.319,97.516,217.617,129.418         C447.361,287.923,358.746,385.406,255.997,385.406z"></path>       <path d="M255.997,154.725c-55.842,0-101.275,45.433-101.275,101.275s45.433,101.275,101.275,101.275         s101.275-45.433,101.275-101.275S311.839,154.725,255.997,154.725z M255.997,323.516c-37.23,0-67.516-30.287-67.516-67.516         s30.287-67.516,67.516-67.516s67.516,30.287,67.516,67.516S293.227,323.516,255.997,323.516z"></path> </svg>                                                   <span>                               Quick View                             </span>                                             </a>                                                                                 <div class="product-des abs-center">                                                                                    <div class="action">                         <form action="/cart/add" method="post" class="variants" id="" data-id="product-actions-7797710979294" enctype="multipart/form-data">                                                                                                                                                                                                                                             <a class="btn" data-href="javascript:void(0)" title="Set of 2- Lilac purple handmade crochia backless bralette crop top blouse with cotton ikkat saree" data-init-quickshop="" tabindex="-1">                                       Add to Cart                                     </a>                                                                                                                                                                                                </form>                                                                             <div class="product-card__variant--popup">                               <div class="product-card__variant--popup--content">                                 <form action="/cart/add" method="post" class="variants" id="" data-id="product-actions-7797710979294" enctype="multipart/form-data">                                                                                                                                                                                                                                                                                                                                                                                                                                      <div class="selector-wrapper selector-wrapper-1 swatch js product-form__item" data-option-index="0">                                                   <label class="form-label">                                                       Size:                                                        <span class="label-value-1"></span>                                                   </label>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           <div class="swatch-element size xs available" data-value="XS">                                                               <input class="single-option-selector single-option-selector-quick single-option-selector-recently-view" type="radio" name="SingleOptionSelector-1-7797710979294-recently-view" data-index="option1" value="XS" id="" tabindex="-1">                                                               <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector1-XS-7797710979294-recently-view" data-original-title="XS">XS</label>                                                           </div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               <div class="swatch-element size s available" data-value="S">                                                               <input class="single-option-selector single-option-selector-quick single-option-selector-recently-view" type="radio" name="SingleOptionSelector-1-7797710979294-recently-view" data-index="option1" value="S" id="" tabindex="-1">                                                               <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector1-S-7797710979294-recently-view" data-original-title="S">S</label>                                                           </div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               <div class="swatch-element size m available" data-value="M">                                                               <input class="single-option-selector single-option-selector-quick single-option-selector-recently-view" type="radio" name="SingleOptionSelector-1-7797710979294-recently-view" data-index="option1" value="M" id="" tabindex="-1">                                                               <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector1-M-7797710979294-recently-view" data-original-title="M">M</label>                                                           </div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               <div class="swatch-element size l available" data-value="L">                                                               <input class="single-option-selector single-option-selector-quick single-option-selector-recently-view" type="radio" name="SingleOptionSelector-1-7797710979294-recently-view" data-index="option1" value="L" id="" tabindex="-1">                                                               <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector1-L-7797710979294-recently-view" data-original-title="L">L</label>                                                           </div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               <div class="swatch-element size xl available" data-value="XL">                                                               <input class="single-option-selector single-option-selector-quick single-option-selector-recently-view" type="radio" name="SingleOptionSelector-1-7797710979294-recently-view" data-index="option1" value="XL" id="" tabindex="-1">                                                               <label data-toggle="tooltip" data-placement="top" title="" for="SingleOptionSelector1-XL-7797710979294-recently-view" data-original-title="XL">XL</label>                                                           </div>                                                                                                                                                         </div>                                                                                                                                                        <select name="id" id="" class="product-form__variants no-js" tabindex="-1">                                                                                                                                 <option value="43398816760030">                                                   XS                                               </option>                                                                                                                                                                            <option value="43398816825566">                                                   S                                               </option>                                                                                                                                                                            <option value="43398816858334">                                                   M                                               </option>                                                                                                                                                                            <option value="43398816891102">                                                   L                                               </option>                                                                                                                                                                            <option value="43398816923870">                                                   XL                                               </option>                                                                                                                     </select>                                   <div class="product-card__button_cancel_mobile">                                       <a class="btn btn-cancel" data-cancel-swatch-qs="" tabindex="-1"><svg aria-hidden="true" data-prefix="fal" data-icon="times" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" class="svg-inline--fa fa-times fa-w-10 fa-2x"><path fill="currentColor" d="M193.94 256L296.5 153.44l21.15-21.15c3.12-3.12 3.12-8.19 0-11.31l-22.63-22.63c-3.12-3.12-8.19-3.12-11.31 0L160 222.06 36.29 98.34c-3.12-3.12-8.19-3.12-11.31 0L2.34 120.97c-3.12 3.12-3.12 8.19 0 11.31L126.06 256 2.34 379.71c-3.12 3.12-3.12 8.19 0 11.31l22.63 22.63c3.12 3.12 8.19 3.12 11.31 0L160 289.94 262.56 392.5l21.15 21.15c3.12 3.12 8.19 3.12 11.31 0l22.63-22.63c3.12-3.12 3.12-8.19 0-11.31L193.94 256z" class=""></path></svg></a>                                   </div>                                   <div class="product-card__button2">                                       <input type="hidden" name="quantity" value="1" tabindex="-1">                                       <button data-btn-addtocart="" class="btn add-to-cart-btn" type="submit" data-form-id="#recently-7797710979294-qs" tabindex="-1">                                           Submit                                       </button>                                   </div>                                 </form>                               </div>                           </div>                                                                       </div>                                                                                                    </div>         </div>                          <div class="product-bottom">               <div class="wrapper-vendor">                                                                                         <span class="spr-badge" id="" data-rating="0.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star-empty" aria-hidden="true"></i><i class="spr-icon spr-icon-star-empty" aria-hidden="true"></i><i class="spr-icon spr-icon-star-empty" aria-hidden="true"></i><i class="spr-icon spr-icon-star-empty" aria-hidden="true"></i><i class="spr-icon spr-icon-star-empty" aria-hidden="true"></i></span><span class="spr-badge-caption">No reviews</span>
</span>
                                                                                   </div>                                                              <a class="product-title change-text" href="/products/set-of-2-lilac-purple-handmade-crochia-backless-bralette-crop-top-blouse-with-cotton-ikkat-saree" tabindex="-1">                 Set of 2- Lilac purple handmade crochia backless bralette crop top blouse with cotton ikkat saree                                                                                       </a>                                                               <div class="price-box">                                         <div class="price-sale">                                                <span class="special-price" data-price-grid=""><span class="money">₹4,784.00</span></span>                         <span class="old-price" data-compare-price-grid=""><span class="money">₹6,834.00</span></span>                                            </div>                                  </div>               <div class="wrapper-size">                                                                                  </div>                        </div>                               </div>     </div>   </div></div></div><ul class="slick-dots" style="display: block;" role="tablist"><li class="slick-active" aria-hidden="false" role="presentation" aria-selected="true" aria-controls="navigation30" id="slick-slide30"><button type="button" data-role="none" role="button" tabindex="0">1</button></li><li aria-hidden="true" role="presentation" aria-selected="false" aria-controls="navigation31" id="slick-slide31"><button type="button" data-role="none" role="button" tabindex="0">2</button></li><li aria-hidden="true" role="presentation" aria-selected="false" aria-controls="navigation32" id="slick-slide32"><button type="button" data-role="none" role="button" tabindex="0">3</button></li><li aria-hidden="true" role="presentation" aria-selected="false" aria-controls="navigation33" id="slick-slide33"><button type="button" data-role="none" role="button" tabindex="0">4</button></li></ul></div>
        </div>
    </section>
</div>


  
<script id="recently-viewed-product-grid-template" type="text/x-jquery-tmpl">
    
  ${( file = featured_image.split("/products", 1) ),''}
  ${( option_color_swatch = window.swatch_recently), ''}
  ${( product_id = id ),''}

  

  <div class="grid-item col-6 col-md-4 col-lg-3 ">
    <div class="inner product-item{{if !available}} sold-out {{/if}}{{if compare_at_price_min > price_min}} on-sale{{/if}}" data-product-id="product-${id}">
      <div class="inner-top">
      <div class="product-top">
              <div class="product-image {{if images[1] != null}}image-swap{{/if}}">
                  <a href="${url}" class="product-grid-image" data-collections-related="/collections/${collection}?view=related">
                    {{if featured_image != null}}
                      
                      {{if images[1] != null}}
                      <picture data-index="0">
                        <img src="${featured_image}" class="images-one" alt="${featured_image.alt}" data-index="0" />
                      </picture>
                      <span class="images-two">

                        
                        
                        

                          <picture data-index="1">
                            <img src="${images[1]}" alt="${images[1].alt}" data-index="1" />
                          </picture>

                        
                        
                        

                      </span>
                      {{else}}
                      <picture data-index="0">
                        <img src="${featured_image}" alt="${featured_image.alt}" data-index="0" />
                      </picture>
                      {{/if}}
                      
                    {{/if}}
                  </a>
                </div>
                
                <div class="product-label">
                  {{if compare_at_price_min > price_min }} 
                  
                  
                  
                    <strong class="label sale-label">
                      <span >
                          Sale
                      </span>
                    </strong>
                    
                    
                    <br>
                    
                    
                  {{/if}}
                  {{if !available}}
                    <strong class="label sold-out-label">
                      <span >
                        Sold Out
                      </span>
                    </strong>
                    <br>
                  {{/if}}
                  
                  ${( tagLabel = false ),''}
                   {{if tags}}
                      {{each tags}} 
                          {{if $value =="label" || $value =="Label"}}
                              ${( tagLabel = true ),''}
                          {{/if}}
                       {{/each}}
                   {{/if}}
                   
                   {{if tagLabel}}
                    <strong class="label custom-label">
                      <span >
                        Custom label
                      </span>
                    </strong>
                  {{/if}}
                </div>
                
              
                
                    
                        <a class="wishlist" data-icon-wishlist href="#" data-product-handle="${handle}" data-id="${id}">
                          
                          <svg viewBox="0 0 512 512">
<g>
<g>
  <path d="M474.644,74.27C449.391,45.616,414.358,29.836,376,29.836c-53.948,0-88.103,32.22-107.255,59.25
    c-4.969,7.014-9.196,14.047-12.745,20.665c-3.549-6.618-7.775-13.651-12.745-20.665c-19.152-27.03-53.307-59.25-107.255-59.25
    c-38.358,0-73.391,15.781-98.645,44.435C13.267,101.605,0,138.213,0,177.351c0,42.603,16.633,82.228,52.345,124.7
    c31.917,37.96,77.834,77.088,131.005,122.397c19.813,16.884,40.302,34.344,62.115,53.429l0.655,0.574
    c2.828,2.476,6.354,3.713,9.88,3.713s7.052-1.238,9.88-3.713l0.655-0.574c21.813-19.085,42.302-36.544,62.118-53.431
    c53.168-45.306,99.085-84.434,131.002-122.395C495.367,259.578,512,219.954,512,177.351
    C512,138.213,498.733,101.605,474.644,74.27z M309.193,401.614c-17.08,14.554-34.658,29.533-53.193,45.646
    c-18.534-16.111-36.113-31.091-53.196-45.648C98.745,312.939,30,254.358,30,177.351c0-31.83,10.605-61.394,29.862-83.245
    C79.34,72.007,106.379,59.836,136,59.836c41.129,0,67.716,25.338,82.776,46.594c13.509,19.064,20.558,38.282,22.962,45.659
    c2.011,6.175,7.768,10.354,14.262,10.354c6.494,0,12.251-4.179,14.262-10.354c2.404-7.377,9.453-26.595,22.962-45.66
    c15.06-21.255,41.647-46.593,82.776-46.593c29.621,0,56.66,12.171,76.137,34.27C471.395,115.957,482,145.521,482,177.351
    C482,254.358,413.255,312.939,309.193,401.614z"/>
</g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
</svg>
                          <span class="wishlist-text">
                              Add to wishlist
                          </span>
                          
                        </a>
                    
                

                
                
                    <a class="quickview-button" href="javascript:void(0)" id="${handle}">
                       
                        <svg viewBox="0 0 511.999 511.999" width="100%" height="100%">
    <path d="M508.745,246.041c-4.574-6.257-113.557-153.206-252.748-153.206S7.818,239.784,3.249,246.035         c-4.332,5.936-4.332,13.987,0,19.923c4.569,6.257,113.557,153.206,252.748,153.206s248.174-146.95,252.748-153.201         C513.083,260.028,513.083,251.971,508.745,246.041z M255.997,385.406c-102.529,0-191.33-97.533-217.617-129.418         c26.253-31.913,114.868-129.395,217.617-129.395c102.524,0,191.319,97.516,217.617,129.418         C447.361,287.923,358.746,385.406,255.997,385.406z"></path>
      <path d="M255.997,154.725c-55.842,0-101.275,45.433-101.275,101.275s45.433,101.275,101.275,101.275         s101.275-45.433,101.275-101.275S311.839,154.725,255.997,154.725z M255.997,323.516c-37.23,0-67.516-30.287-67.516-67.516         s30.287-67.516,67.516-67.516s67.516,30.287,67.516,67.516S293.227,323.516,255.997,323.516z"></path>
</svg>
                       
                          <span >
                              Quick View
                            </span>                        
                    </a>
                
                
                
            
                <div class="product-des abs-center">
                    
                    
                    
                    <div class="action">
                        <form action="/cart/add" method="post" class="variants" id="recently-${id}" data-id="product-actions-${id}" enctype="multipart/form-data">
                            {{if !available}}
                                <button class="btn add-to-cart-btn" type="submit" disabled="disabled" >
                                    Sold Out
                                </button>
                            {{else variants.length > 0 && variants[0].title != 'Default Title' }}
                                ${( downcased_option = options[0].name.toLowerCase() ),''}
                                {{if options.length == 1 && option_color_swatch.indexOf(downcased_option) > -1 && window.use_color_swatch}}
                                  <input type="hidden" name="id" value="${variants[0].id}" />
                                  <input type="hidden" name="quantity" value="1" />
                                  <button data-btn-addToCart class="btn add-to-cart-btn" type="submit" data-form-id="#recently-${id}" >
                                  Add to Cart
                                  </button>
                                {{else}}
                                  
                                  
                                  
                                    <a class="btn" data-href="javascript:void(0)" title="${title}" data-init-quickshop>
                                      Add to Cart
                                    </a>
                                  
                                  
                                  
                                {{/if}}
                            {{else}}
                              <input type="hidden" name="id" value="${variants[0].id}" />
                              
                                
                                  
                              
                                    <button data-btn-addToCart class="btn add-to-cart-btn" type="submit" data-form-id="#recently-${id}" >
                                      Add to Cart
                                    </button>
                              
                                
                                
                              
                            {{/if}}
                        </form>
                        {{if available}}
                        {{if variants.length > 0 && variants[0].title != 'Default Title'}}
                          <div class="product-card__variant--popup">
                              <div class="product-card__variant--popup--content">
                                <form action="/cart/add" method="post" class="variants" id="recently-${id}-qs" data-id="product-actions-${id}" enctype="multipart/form-data">
                                  ${( position_color = 0 ),''}
                                  {{each options}}
                                    ${( option_name = name.toLowerCase() ),''}
                                    {{if option_color_swatch.indexOf(option_name) > -1}}
                                      ${( position_color = position - 1 ),''}
                                    {{/if}}
                                  {{/each}}

                                  {{each options}}
                                      ${( option_name = name.toLowerCase() ),''}
                                      ${( colorlist = '' ),''}
                                      {{if window.use_color_swatch }}
                                          {{if option_color_swatch.indexOf(option_name) > -1}}
                                              <div class="selector-wrapper selector-wrapper-1 swatch js product-form__item option-color hide" data-option-index=0 data-option-position="${position - 1}">
                                                  <label class="form-label">
                                                      ${name}: 
                                                      <span class="label-value-${position}"></span>
                                                  </label>
                                                  ${( option_index = position ),''}
                                                  {{each variants}}
                                                    {{if position == 1}}
                                                    ${( value = variants[($index)].option1 ),''}
                                                    {{/if}}
                                                    {{if position == 2}}
                                                    ${( value = variants[($index)].option2 ),''}
                                                    {{/if}}
                                                    {{if position == 3}}
                                                    ${( value = variants[($index)].option3 ),''}
                                                    {{/if}}
                                                    ${( temp = ''),''}
                                                    ${( temp = temp.concat(";",value) ),''}
                                                    {{if colorlist.indexOf(value) < 0}}
                                                      ${( colorlist = colorlist.concat(";",value) ),''}
                                                      ${( value_2 = value.toLowerCase().replace(" ", "") ),''}
                                                      <div class="swatch-element color ${value_2}{{if available}} available{{else}} soldout{{/if}}" data-value="${value}">
                                                          <input {{if !available}}disabled{{/if}} class="single-option-selector single-option-selector-quick single-option-selector-recently-view" type="radio" name="SingleOptionSelector-${option_index}-${product_id}-recently-view" data-index="option${option_index}" value="${value}" {{if values[0] == value}}checked="checked"{{/if}} id="SingleOptionSelector${option_index}-${value}-${product_id}-recently-view">
                                                          <label data-toggle="tooltip" data-placement="top" title="${value}" for="SingleOptionSelector${option_index}-${value}-${product_id}-recently-view"></label>
                                                      </div>
                                                    {{/if}}
                                                  {{/each}}
                                              </div>
                                          {{else}}
                                              <div class="selector-wrapper selector-wrapper-{{if position_color == 0 }}${position}{{else}}{{if position == 1 }}2{{else}}3{{/if}}{{/if}} swatch js product-form__item" data-option-index={{if position_color == 0 }}${position - 1}{{else}}{{if position == 1}}1{{else}}2{{/if}}{{/if}}>
                                                  <label class="form-label">
                                                      ${name}: 
                                                      <span class="label-value-${position}"></span>
                                                  </label>
                                                  ${( option_index = position ),''}
                                                  {{each variants}}
                                                    {{if position == 1}}
                                                    ${( value = variants[($index)].option1 ),''}
                                                    {{/if}}
                                                    {{if position == 2}}
                                                    ${( value = variants[($index)].option2 ),''}
                                                    {{/if}}
                                                    {{if position == 3}}
                                                    ${( value = variants[($index)].option3 ),''}
                                                    {{/if}}
                                                    ${( temp = ''),''}
                                                    ${( temp = temp.concat(";",value) ),''}
                                                    {{if colorlist.indexOf(temp) < 0}}
                                                        ${( colorlist = colorlist.concat(";",value) ),''}
                                                        ${( value_2 = value.toLowerCase().replace(" ", "") ),''}
                                                          <div class="swatch-element size ${value_2}{{if available}} available{{else}} soldout{{/if}}" data-value="${value}">
                                                              <input {{if !available}}disabled{{/if}} class="single-option-selector single-option-selector-quick single-option-selector-recently-view" type="radio" name="SingleOptionSelector-${option_index}-${product_id}-recently-view" data-index="option${option_index}" value="${value}" id="SingleOptionSelector${option_index}-${value}-${product_id}-recently-view">
                                                              <label data-toggle="tooltip" data-placement="top" title="${value}" for="SingleOptionSelector${option_index}-${value}-${product_id}-recently-view">${value}</label>
                                                          </div>
                                                      {{/if}}
                                                  {{/each}}
                                              </div>
                                          {{/if}}
                                      {{else}}
                                        ${( position_color = position - 1 ),''}
                                        ${( check = false ),''}
                                        ${( select1 = ''),''}
                                        ${( select2 = ''),''}
                                        <div class="selector-wrapper selector-wrapper-${position} selector js product-form__item" data-option-index=${position}>
                                            <label class="form-label">
                                                ${name}: <span class="label-value-${position}"></span>
                                            </label>
                                            
                                            <select class="single-option-selector single-option-selector-quick single-option-selector-recently-view product-form__input form-control" id="SingleOptionSelector-${position}-${product_id}-recently-view" data-option="option${position}" data-option-index="${position_color}">
                                              {{each variants}}
                                                {{if check == false}}
                                                  {{if variants[($index)].available == true}}
                                                    ${( select1 = variants[($index)].option1 ),''}
                                                    ${( select2 = variants[($index)].option2 ),''}
                                                    ${( check = true ),''}
                                                  {{/if}}
                                                {{/if}}
                                                
                                                {{if position == 1}}
                                                ${( value = variants[($index)].option1 ),''}
                                                {{/if}}
                                                {{if position == 2}}
                                                ${( value = variants[($index)].option2 ),''}
                                                {{/if}}
                                                {{if position == 3}}
                                                ${( value = variants[($index)].option3 ),''}
                                                {{/if}}
                                                ${( temp = ''),''}
                                                ${( temp = temp.concat(";",value) ),''}
                                                {{if colorlist.indexOf(temp) < 0}}
                                                  ${( colorlist = colorlist.concat(";",value) ),''}
                                                  ${( value_2 = value.toLowerCase().replace(" ", "") ),''}
                                                  <option value="${value}" {{if select1 == value || select2 == value}}selected="selected"{{/if}}>${value}</option>
                                                {{/if}}
                                              {{/each}}
                                            </select>
                                        </div>
                                      {{/if}}
                                  {{/each}}
                                  <select name="id" id="ProductSelect-${id}-recently-view" class="product-form__variants no-js">
                                      {{each variants}}
                                          {{if available}}
                                              <option value="${id}">
                                                  ${title}
                                              </option>
                                          {{else}}
                                              <option disabled="disabled">${title} - Sold Out</option>
                                          {{/if}}
                                      {{/each}}
                                  </select>
                                  <div class="product-card__button_cancel_mobile">
                                      <a class="btn btn-cancel" data-cancel-swatch-qs><svg aria-hidden="true" data-prefix="fal" data-icon="times" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" class="svg-inline--fa fa-times fa-w-10 fa-2x"><path fill="currentColor" d="M193.94 256L296.5 153.44l21.15-21.15c3.12-3.12 3.12-8.19 0-11.31l-22.63-22.63c-3.12-3.12-8.19-3.12-11.31 0L160 222.06 36.29 98.34c-3.12-3.12-8.19-3.12-11.31 0L2.34 120.97c-3.12 3.12-3.12 8.19 0 11.31L126.06 256 2.34 379.71c-3.12 3.12-3.12 8.19 0 11.31l22.63 22.63c3.12 3.12 8.19 3.12 11.31 0L160 289.94 262.56 392.5l21.15 21.15c3.12 3.12 8.19 3.12 11.31 0l22.63-22.63c3.12-3.12 3.12-8.19 0-11.31L193.94 256z" class=""></path></svg></a>
                                  </div>
                                  <div class="product-card__button2">
                                      <input type="hidden" name="quantity" value="1" />
                                      <button data-btn-addToCart class="btn add-to-cart-btn" type="submit" data-form-id="#recently-${id}-qs">
                                          Submit
                                      </button>
                                  </div>
                                </form>
                              </div>
                          </div>
                        {{/if}}
                        {{/if}}
                    </div>
                    
                    
                                        
                </div>
        </div>
            
            <div class="product-bottom">
              <div class="wrapper-vendor">
                
                 

                
                
                  <span class="shopify-product-reviews-badge" data-id="${id}"></span>
                
                
                
                
              </div>
              
              
                
              <a class="product-title change-text" href="${url}">
                ${title}
                {{each options}}
                  ${( option_name = name.toLowerCase() ),''}
                  {{if option_color_swatch.indexOf(option_name) > -1}}
                    {{if position == 1}}
                    ${( option = variants[0].option1 ),''}
                    {{/if}}
                    {{if position == 2}}
                    ${( option = variants[0].option2 ),''}
                    {{/if}}
                    {{if position == 3}}
                    ${( option = variants[0].option3 ),''}
                    {{/if}}

                    
                    
                    
                      <span data-change-title> <span class='line'>-</span> ${option}</span>
                    
                    
                    

                  {{/if}}
                {{/each}}
              </a>

              
              
                
              <div class="price-box">
                  {{if compare_at_price_min > price_min }} 
                    <div class="price-sale">
                      
                        <span class="special-price" data-price-grid>{{html Shopify.formatMoney(variants[0].price, window.money_format)}}</span>
                        <span class="old-price" data-compare-price-grid>{{html Shopify.formatMoney(variants[0].compare_at_price, window.money_format)}}</span>
                      
                    </div>
                  {{else}}
                    
                    
                      <div class="price-regular">
                        <span data-price-grid>{{html Shopify.formatMoney(variants[0].price, window.money_format)}}</span>
                      </div>
                    
                  {{/if}}
              </div>
              <div class="wrapper-size">
                
                
                


            

              </div>
              
        </div>                        
      </div>
    </div>
  </div>

</script>



<script>
    $('.recently-viewed-products').hide();

    Shopify.Products.showRecentlyViewed({ 
        howManyToShow: 8,
        wrapperId: 'recently-viewed-products-grid', 
        templateId: 'recently-viewed-product-grid-template',
        onComplete: function() {
            var recentlyViewBlock = $('.recently-viewed-products');
            var recentlyGrid = recentlyViewBlock.find('#recently-viewed-products-grid');
            var productGrid = recentlyGrid.children();
            var rows = recentlyGrid.data('rows');
   
            if(productGrid.length) {
                recentlyViewBlock.show();

                if(recentlyViewBlock.is(':visible')) {          
                    
                    
                    if(!recentlyGrid.hasClass('slick-initialized')) {
                    recentlyGrid.slick({
                        infinite: true,
                        speed: 1000,
                        slidesToShow: rows,
                        get dots() {
                            if (window.layout_style == 'layout_style_surfup' || window.layout_style == 'layout_style_suppermarket') {
                                return this.dots = true
                
                            } else {
                                return this.dots = false
                            }
                        },
                        slidesToScroll: rows,
                        get nextArrow() {
                            if ((window.layout_style == 'layout_style_1170') || (window.layout_style == 'layout_style_flower')) {
                                return this.nextArrow = '<button type="button" aria-label="Next Product" class="slick-next"><svg viewBox="0 0 50 50"><path d="M 11.957031 13.988281 C 11.699219 14.003906 11.457031 14.117188 11.28125 14.308594 L 1.015625 25 L 11.28125 35.691406 C 11.527344 35.953125 11.894531 36.0625 12.242188 35.976563 C 12.589844 35.890625 12.867188 35.625 12.964844 35.28125 C 13.066406 34.933594 12.972656 34.5625 12.71875 34.308594 L 4.746094 26 L 48 26 C 48.359375 26.003906 48.695313 25.816406 48.878906 25.503906 C 49.058594 25.191406 49.058594 24.808594 48.878906 24.496094 C 48.695313 24.183594 48.359375 23.996094 48 24 L 4.746094 24 L 12.71875 15.691406 C 13.011719 15.398438 13.09375 14.957031 12.921875 14.582031 C 12.753906 14.203125 12.371094 13.96875 11.957031 13.988281 Z"></path></svg></button>';
                            } else if ((window.layout_style == 'layout_style_fullwidth') || (window.layout_style == 'layout_style_suppermarket') || (window.layout_style == 'layout_style_surfup') || (window.layout_home == 'layout_home_14')){
                                return this.nextArrow = '<button type="button" aria-label="Next Product" class="slick-next"><svg viewBox="0 0 478.448 478.448" class="icon icon-chevron-right" id="icon-chevron-right"><g><g><polygon points="131.659,0 100.494,32.035 313.804,239.232 100.494,446.373 131.65,478.448 377.954,239.232"></polygon></g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g></svg></button>';
                            }
                            else {
                                return this.nextArrow = '<button type="button" aria-label="Next Product" class="slick-next"><i class="fa fa-angle-right"></i></button>';
                            }
                        },
                        get prevArrow() {
                            if ((window.layout_style == 'layout_style_1170') || (window.layout_style == 'layout_style_flower')) {
                                return this.prevArrow = '<button type="button" aria-label="Previous Product" class="slick-prev"><svg viewBox="0 0 50 50"><path d="M 11.957031 13.988281 C 11.699219 14.003906 11.457031 14.117188 11.28125 14.308594 L 1.015625 25 L 11.28125 35.691406 C 11.527344 35.953125 11.894531 36.0625 12.242188 35.976563 C 12.589844 35.890625 12.867188 35.625 12.964844 35.28125 C 13.066406 34.933594 12.972656 34.5625 12.71875 34.308594 L 4.746094 26 L 48 26 C 48.359375 26.003906 48.695313 25.816406 48.878906 25.503906 C 49.058594 25.191406 49.058594 24.808594 48.878906 24.496094 C 48.695313 24.183594 48.359375 23.996094 48 24 L 4.746094 24 L 12.71875 15.691406 C 13.011719 15.398438 13.09375 14.957031 12.921875 14.582031 C 12.753906 14.203125 12.371094 13.96875 11.957031 13.988281 Z"></path></svg></button>';
                            } else if ((window.layout_style == 'layout_style_fullwidth') || (window.layout_style == 'layout_style_suppermarket') || (window.layout_style == 'layout_style_surfup') || (window.layout_home == 'layout_home_14')){
                                return this.prevArrow = '<button type="button" aria-label="Previous Product" class="slick-prev"><svg viewBox="0 0 370.814 370.814" class="icon icon-chevron-left" id="icon-chevron-left"><g><g><polygon points="292.92,24.848 268.781,0 77.895,185.401 268.781,370.814 292.92,345.961 127.638,185.401"></polygon></g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g></svg></button>';
                            } 
                            else {
                                return this.prevArrow = '<button type="button" aria-label="Previous Product" class="slick-prev"><i class="fa fa-angle-left"></i></button>';
                            }
                        },
                        responsive: [
                        {
                            breakpoint: 1450,
                            settings: {
                                get slidesToShow() {
                                    if (rows > 5) {
                                        if(window.layout_home == 'layout_home_7') {
                                            return this.slidesToShow = rows - 2;
                                        } else {
                                            return this.slidesToShow = rows - 1;
                                        }    
                                    } else {
                                        if(window.layout_home == 'layout_home_7') {
                                             return this.slidesToShow = rows - 1;
                                        } else {
                                            return this.slidesToShow = rows;
                                        }
                                        
                                    }
                                },
                                get slidesToScroll() {
                                    if (rows > 5) {
                                        if(window.layout_home == 'layout_home_7') {
                                            return this.slidesToScroll = rows - 2;
                                        } else {
                                            return this.slidesToScroll = rows - 1;
                                        }  
                                    } else {
                                        if(window.layout_home == 'layout_home_7') {
                                             return this.slidesToScroll = rows - 1;
                                        } else {
                                            return this.slidesToScroll = rows;
                                        }
                                    }
                                },
                            }
                        },
                        {
                            breakpoint: 1400,
                            settings: {
                                get slidesToShow() {
                                    if (rows > 5) {
                                        if(window.layout_home == 'layout_home_7') {
                                            return this.slidesToShow = rows - 2;
                                        } else {
                                            return this.slidesToShow = rows - 1;
                                        }  
                                    } else {
                                        if(window.layout_home == 'layout_home_7') {
                                             return this.slidesToShow = rows - 1;
                                        } else {
                                            return this.slidesToShow = rows;
                                        }
                                    }
                                },
                                get slidesToScroll() {
                                    if (rows > 5) {
                                        if(window.layout_home == 'layout_home_7') {
                                            return this.slidesToScroll = rows - 2;
                                        } else {
                                            return this.slidesToScroll = rows - 1;
                                        }
                                    } else {
                                        if(window.layout_home == 'layout_home_7') {
                                             return this.slidesToScroll = rows - 1;
                                        } else {
                                            return this.slidesToScroll = rows;
                                        }
                                    }
                                },
                            }
                        },
                            {
                            breakpoint: 1200,
                            settings: {
                                arrows:false,
                                dots: true,
                                get slidesToShow() {
                                    if (rows > 5) {
                                        return this.slidesToShow = rows - 2;
                                    } else {
                                        if (rows > 4) {
                                            return this.slidesToShow = rows - 1;
                                        } else {
                                            return this.slidesToShow = rows;
                                        }
                                    }
                                },
                                get slidesToScroll() {
                                    if (rows > 5) {
                                        return this.slidesToScroll = rows - 2;
                                    } else {
                                        if (rows > 4) {
                                            return this.slidesToScroll = rows - 1;
                                        } else {
                                            return this.slidesToScroll = rows;
                                        }
                                    }
                                },             
                            }
                            },
                            {
                            breakpoint: 992,
                            settings: {
                                arrows:false,
                                dots: true,
                                get slidesToShow() {
                                    if (rows > 5) {
                                        return this.slidesToShow = rows - 3;
                                    } else {
                                        if (rows > 4) {
                                            return this.slidesToShow = rows - 2;
                                        } else {
                                             if (rows > 3) {
                                                return this.slidesToShow = rows - 1;
                                             } else {
                                                return this.slidesToShow = rows;
                                             }
                                        }
                                    }
                                },
                                get slidesToScroll() {
                                    if (rows > 5) {
                                        return this.slidesToScroll = rows - 3;
                                    } else {
                                        if (rows > 4) {
                                            return this.slidesToScroll = rows - 2;
                                        } else {
                                             if (rows > 3) {
                                                return this.slidesToScroll = rows - 1;
                                             } else {
                                                return this.slidesToScroll = rows;
                                             }
                                        }
                                    }
                                },              
                            }
                            },
                            {
                            breakpoint: 768,
                            settings: {
                                arrows:false,
                                dots: true,
                                get slidesToShow() {
                                    if (rows > 5) {
                                        return this.slidesToShow = rows - 4;
                                    } else {
                                        if (rows > 4) {
                                            return this.slidesToShow = rows - 3;
                                        } else {
                                             if (rows > 3) {
                                                return this.slidesToShow = rows - 2;
                                             } else {
                                                if (rows > 2) {
                                                    return this.slidesToShow = rows - 1;
                                                } else {
                                                    return this.slidesToShow = rows;
                                                }
                                             }
                                        }
                                    }
                                },
                                get slidesToScroll() {
                                    if (rows > 5) {
                                        return this.slidesToScroll = rows - 4;
                                    } else {
                                        if (rows > 4) {
                                            return this.slidesToScroll = rows - 3;
                                        } else {
                                             if (rows > 3) {
                                                return this.slidesToScroll = rows - 2;
                                             } else {
                                                if (rows > 2) {
                                                    return this.slidesToScroll = rows - 1;
                                                } else {
                                                    return this.slidesToScroll = rows;
                                                }
                                             }
                                        }
                                    }
                                }
                            }
                            }
                        ]
                        });
                    };      

                    if($('.spr-badge').length > 0) {
                    return window.SPR.registerCallbacks(), window.SPR.initRatingHandler(), window.SPR.initDomEls(), window.SPR.loadProducts(), window.SPR.loadBadges();
                    }; 
        
                    if(window.show_multiple_currencies && Currency.currentCurrency != shopCurrency){
                      
                      Currency.convertAll(window.shop_currency, $('#currencies .active').attr('data-currency'), '.recently-viewed-products span.money', 'money_format');
                    }

                    $('[data-toggle="tooltip"]').tooltip();   
                }; 
            }
            
            else {
                recentlyViewBlock.hide();
            };

            if (window.innerWidth > 1200) {
                $('.inner-top').mouseenter(function(){
                    var chil = $(this).find('video');
                    var _chil = $(this).find('video').get(0);
                    if (chil.length > 0) {
                        _chil.play();
                    }
                });
                $('.inner-top').mouseleave(function(){
                    var chil = $(this).find('video');
                    var _chil = $(this).find('video').get(0);
                    if (chil.length > 0) {
                        _chil.pause();
                    }
                })
            }

            var wishListItems = localStorage.getItem('items') ? JSON.parse(localStorage.getItem('items')) : [];

            if (!wishListItems.length) {
                return;
            }

            for (var i = 0; i < wishListItems.length; i++) {
                var icon = $('[data-product-handle="'+ wishListItems[i] +'"]');
                icon.addClass('whislist-added');
                icon.find('.wishlist-text').text(window.inventory_text.remove_wishlist);
            };
        }
    });
</script>

<script>
    Shopify.Products.recordRecentlyViewed();
</script>

<style data-shopify="">
  #shopify-section-recently-viewed-products{
    width: 100%;
  }
</style>

</div>
        </main>
